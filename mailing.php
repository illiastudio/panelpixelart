<?php

die;

header('Content-Type: text/html; charset=utf-8');

require_once 'config.php';
require_once 'libs/phpmailer.php';

try {
  $pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (Exception $e) {
  header('Content-Type: text/html; charset=utf-8');
  die('Nie można nawiązać połączenia z bazą danych.');
}

$sth = $pdo->prepare('SELECT opiekun_id, opiekun_rodzina_id, opiekun_imie, opiekun_mail, opiekun_active_link FROM opiekunowie WHERE TRIM(opiekun_mail) NOT IN ("brak", "") AND opiekun_mail IS NOT NULL AND opiekun_mail_send = "0" ORDER BY opiekun_rodzina_id LIMIT 50');
$sth->execute();

$opiekunowie = $sth->fetchAll();


$title = 'UKS Żagle – PORTAL RODZICA UKS ŻAGLE – prosimy o zalogowanie się';

if (!empty($opiekunowie)) {

  $mail = new phpmailer();
  $mail->CharSet = "UTF-8";
  $mail->SetLanguage("pl", "libs/");
  $mail->IsHTML(true);

  // SMTP
  $mail->Mailer = 'smtp';
  $mail->Host = MAIL_HOST;
  $mail->Port = MAIL_PORT;
  $mail->SMTPKeepAlive = true;
  $mail->SMTPAuth = true;
  $mail->IsSMTP();
  $mail->Username = MAIL_USER;
  $mail->Password = MAIL_PASS;

  $mail->From = MAIL_FROM;
  $mail->FromName = MAIL_FROM_NAME;
  $mail->Subject = $title;

  foreach ($opiekunowie as $key => $val) {

    $link = URL.'login/'.$val['opiekun_active_link'];

    $bodyMsg = '
      <p>Drodzy Rodzice</p>
      <p>Z ogromną radością pragniemy Was poinformować, że portal <strong>rodzica UKS Żagle posiada już możliwość logowania się</strong>. Do tej pory korzystaliście z niego klikając na podany w treści mail’a aktywny link (Wasz aktywny link to: <a href="'.$link.'">'.$link.'</a>). Na etapie zbierania zgłoszeń było to bardzo wygodne i szybkie. Ze względów bezpieczeństwa jednak od początku zakładaliśmy, że każdy z Was będzie miał swój indywidualny login i hasło. Przyszedł już czas aby to założenie zmaterializować…
      </p>
      <p>Zakładamy, że <strong>dostęp do konta danej rodziny mają dwie osoby: tata i mama. Każdy loguje się podając jako login swój adres mail</strong>. Ze względów bezpieczeństwa system sprawdza czy mail podany przy pierwszym logowaniu zgadza się z mail’em, który podaliście nam przy wysyłce/aktualizacji zgłoszenia.</p>
      <p>Obecnie <strong>prosimy Was o dokonanie pierwszego logowania</strong> (oddzielnie loguje się tata i oddzielnie mama), w którym ustalicie <strong>indywidualne dla każdego (osobne dla taty i osobne dla mamy) hasło</strong> (każdy aktywny link daje możliwość pierwszego logowania dla tych osób, których mail’e znajdują się już w bazie danych UKS-u).</p>
      <p>Ci, którzy dokonali już pierwszego logowania proszeni są o sprawdzenie czy ich login i hasło nadal działają poprawnie.</p>
      <center>
        <h1>ABY ZALOGOWAĆ SIĘ PO RAZ PIERWSZY NALEŻY KLIKNĄĆ TU:<br>
          <a href="'.$link.'">'.$link.'</a>
        </h1>
      </center>
      <center>
        <h2 style="color: red;">LOGIN: '.$val['opiekun_mail'].'</h2>
        <h2 style="color: red;">HASŁO: zostanie ustalone przez użytkownika przy pierwszym logowaniu</h2>
      </center>
      <p>Gdyby pojawiły się trudności – prosimy o kontakt mail’owy na <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a></p>
      <center>
        <h1>
          ABY ZALOGOWAĆ SIĘ MAJĄC JUŻ LOGIN I HASŁO:<br>
            <a href="'.URL.'login/">'.URL.'login/</a>
        </h1>
      </center>
      <p>
        Ze sportowym pozdrowieniem,<br>
        Biuro UKS Żagle<br>
        <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a>
      </p>
    ';

    $mail->Body = $bodyMsg;
    $mail->AddAddress($val['opiekun_mail']);
    // $mail->AddAddress('testportaluuks@gmail.com');
    
    $mail->AddAddress('illia@studiograficzne.com');

    if ($mail->Send()) {
      $sth = $pdo->prepare('UPDATE opiekunowie SET opiekun_mail_send = "1" WHERE opiekun_id = '.$val['opiekun_id'].'');
      $sth->execute();
    }

    $mail->ClearAddresses();
    $mail->ClearAttachments();

    // die;
  }

  $mail->SmtpClose();

} else {
  echo 'Wszystkie maile wysłane';
}