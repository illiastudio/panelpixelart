  <div class="content-full">
    <div>
      <div class="sezon-buttons">
        <ul>
        <?php foreach ($this->sezony as $sezon): ?>
          <?php
            if ($this->thisSezon == $sezon['sezon_nazwa']) {
              $class = 'class="active"';
            } else {
              $class = '';
            }
          ?>

          <li>
            <a <?php echo $class; ?> href="<?php echo $sezon['sezon_nazwa']; ?>"><?php echo $sezon['sezon_nazwa']; ?></a>
          </li>
        <?php endforeach; ?>
        </ul>
      </div>

      <h3 class="platnosci-header">Saldo za sezon: <?php echo $this->thisSezon; ?></h3>

      <?php if (!empty($this->saldo) || !empty($this->poprzednieSezony)): ?>

      <?php $suma = 0; ?>

      <?php if (!empty($this->saldo)): ?>
      <?php foreach ($this->saldo as $key => $val): ?>
      <?php $suma += $val['ps_do_zaplaty']; ?>
      <?php endforeach; ?>
      <?php endif; ?>

<?php /*
      <?php if (!empty($this->poprzednieSezony)): ?>
      <?php foreach ($this->poprzednieSezony as $key2 => $val2): ?>
      <?php $suma -= $val2; ?>
      <?php endforeach; ?>
      <?php endif; ?>
*/ ?>

      <table class="platnosci" width="100%" cellpadding="0" style="margin-bottom: 0;">
        <body>
          <tr class="razem <?php echo $this->statusClass; ?>">
            <td colspan="2">Saldo:</td>
            <td>
            <?php
            $saldo = $this->zaplacono - $suma;

            if ($saldo > 0) {
              echo '+';
            }

            echo $saldo;
            ?>
            </td>
          </tr>
          <tr class="row razem">
            <td colspan="2">Rozliczone na dzień:</td>
            <?php if ($this->last != 0): ?>
            <td><?php echo $this->last; ?></td>
            <?php else: ?>
            <td>Brak płatności</td>
            <?php endif ?>
          </tr>
        </body>
      </table>

      <?php endif; ?>

      <table class="platnosci" width="100%" cellpadding="0">
        <thead>
          <tr>
            <th width="33%">Rok</th>
            <th width="33%">Miesiąc</th>
            <th width="33%">Do zapłaty</th>
          </tr>
        </thead>
        <tbody>
        <?php if (!empty($this->saldo) || !empty($this->poprzednieSezony)): ?>
        <?php $suma = 0; ?>

        <?php if (!empty($this->saldo)): ?>
          <?php foreach ($this->saldo as $key => $val): ?>
            <tr class="row">
              <td><?php echo $val['ps_rok']; ?></td>
              <td><?php echo $this->months[$val['ps_miesiac']]; ?></td>
              <td><?php echo $val['ps_do_zaplaty']; ?></td>
              <?php $suma += $val['ps_do_zaplaty']; ?>
            </tr>
          <?php endforeach ?>
        <?php endif; ?>

<?php /*
        <?php if (!empty($this->poprzednieSezony)): ?>
          <?php foreach ($this->poprzednieSezony as $key2 => $val2): ?>
            <tr class="row">
              <td colspan="2">Saldo z sezono <?php echo $key2; ?></td>
              <td>
              <?php
                if ($val2 < 0) {
                  echo $val2 * -1;
                } elseif ($val2 > 0) {
                  echo '-'.$val2;
                } else {
                  echo $val2;
                }
              ?>
              </td>
              <?php $suma -= $val2; ?>
            </tr>
          <?php endforeach; ?>
        <?php endif; ?>
*/ ?>

        <?php else: ?>
          <tr class="row">
            <td colspan="3">Brak należności</td>
          </tr>
        <?php endif ?>
          <tr class="row razem">
            <td colspan="2">Rozliczone na dzień:</td>
            <td>Brak płatności</td>
          </tr>
        </tbody>
      </table>
    </div>

    <div>
      <h3 class="platnosci-header">Wpłaty</h3>
      <table id="js-wplaty" class="platnosci" width="100%" cellpadding="0">
        <thead>
          <tr>
            <th>Nr dokumentu</th>
            <th>Nazwa</th>
            <th>Opis</th>
            <th>Kwota</th>
            <th>Typ</th>
            <th>Data</th>
            <th>Data sort</th>
          </tr>
        </thead>
        <tbody>
        <?php if (!empty($this->platnosci)): ?>
        <?php foreach ($this->platnosci as $key => $val): ?>
          <tr>
            <td><?php echo $val['platnosc_nr_dokumentu']; ?></td>
            <td><?php echo $val['platnosc_nazwa']; ?></td>
            <td><?php echo $val['platnosc_opis']; ?></td>
            <td>
            <?php
              if ($val['platnosc_zaplacono'] < 0) {
                echo $val['platnosc_zaplacono'] * -1;
              } else {
                echo $val['platnosc_zaplacono'];
              }
            ?>
            </td>
            <td>
            <?php
              if ($val['platnosc_zaplacono'] < 0) {
                echo 'Korekta';
              } else {
                echo 'Wpłata';
              }
            ?>
            </td>
            <td><?php echo $val['platnosc_data_operacji']; ?></td>
            <td><?php echo $val['data_operacji_sort']; ?></td>
          </tr>
        <?php endforeach ?>
        <?php endif ?>
        </tbody>
      </table>
    </div>
  </div>