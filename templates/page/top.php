  <div class="top-wrapper">
    <div class="header-wrapper">
    </div>
    <div class="menu-wrapper parentMenu">
      <h2>Panel Rodzica</h2>
      <ul>
        <li class="user">
          <div>
            <?php if (empty($_SESSION['opiekun_imie'])) : ?>
              <span class="important">Uaktualnij imię w profilu</span>
            <?php else : ?>
              <span><?php echo $_SESSION['opiekun_imie'] . ' ' . $_SESSION['opiekun_rodzina_nazwisko']; ?></span>
            <?php endif ?>
            <span><?php echo $_SESSION['opiekun_login']; ?></span>
          </div>
        </li>
        <li>
          <a class="<?php echo $this->active[1]; ?>" href="<?php echo URL . 'zgloszenia/'; ?>">Zgłoszenia</a>
        </li>
        <li>
          <a class="<?php echo $this->active[3]; ?>" href="<?php echo URL . 'historia/'; ?>">Historia zgłoszeń</a>
        </li>
        <li>
          <a class="<?php echo $this->active[2]; ?>" href="<?php echo URL . 'platnosci/'; ?>">Płatności</a>
        </li>
        <li>
          <a class="<?php echo $this->active[5]; ?>" href="<?php echo URL . 'obecnosci/'; ?>">Obecnosci</a>
        </li>
        <li>
          <a class="<?php echo $this->active[4]; ?>" href="<?php echo URL . 'dane/'; ?>">Dane do płatności</a>
        </li>
        <li>
          <a class="<?php echo $this->active[6]; ?>" href="<?php echo URL . 'regulamin/'; ?>">Regulamin</a>
        </li>
        <li>
          <!-- <a class="<?php echo $this->active[7] . ' ' . $this->active[8]; ?>" href="#">Harmonogram</a> -->
        </li>
        <?php
        $active = 7;
        foreach ($this->harmonograms as $key => $value) {
        ?>
          <li>
            <a class="<?php echo $this->active[$active]; ?>" href="<?php echo URL . 'harmonogram/' .  $value['id'] ?>">
              <?php echo $value['names'] ?>
            </a>
          </li>
        <?php
          $active++;
        }
        ?>
        <li><a class="profil " href="<?php echo URL . 'user/'; ?>" title="Zarządzaj profilem"><span></span>Profil</a></li>
        <li><a class="logout" href="<?php echo URL . 'login/wyloguj'; ?>" title="Wyloguj"><span></span>Wyloguj</a></li>
      </ul>
    </div>
  </div>
  <div class="content-wrapper">