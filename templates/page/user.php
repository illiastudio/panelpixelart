<div class="content-full">
  <h3 class="title-bottom-line">Zarządzanie danymi użytkownika</h3>
  <form action="" method="post">
    <div class="form-line">
      <span class="form-label"><label for="mail">Imię</label></span>
      <input type="text" name="imie" value="<?php echo $this->imie; ?>">
    </div>
    <div class="form-line">
      <span class="form-label"><label for="telefon">Telefon</label></span>
      <input type="text" name="telefon" value="<?php echo $this->telefon[0]['opiekun_telefon']; ?>">
    </div>
    <div class="form-line">
      <span class="form-label"><label for="telefon">E-mail</label></span>
      <input type="text" name="mail" value="<?php echo $this->mail[0]['opiekun_mail']; ?>">
    </div>
    <div class="zmien-haslo">
      <h3 class="title-bottom-line">Zmiana hasła</h3>
      <div class="form-line">
        <span class="form-label"><label for="new-password">Nowe hasło</label></span>
        <input type="password" name="new-password">
      </div>
      <div class="form-line">
        <span class="form-label"><label for="confirm-password">Potwierdź hasło</label></span>
        <input type="password" name="confirm-password">
      </div>
      <div class="form-line">
        <span class="form-label"></span>
        <input type="hidden" name="old-imie" value="<?php echo $this->imie; ?>">
        <input type="hidden" name="old-mail" value="<?php echo $this->mail[0]['opiekun_mail']; ?>">
        <input type="hidden" name="old-telefon" value="<?php echo $this->telefon[0]['opiekun_telefon']; ?>">
        <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
      </div>
    </div>
  </form>
</div>