<div class="content-full">
  <h2>ZGŁOSZENIA DZIECI NA ZAJĘCIA SPORTOWE</h2>

  <form action="" method="post" id="form">

  <div id="form-part-1" class="form">
    <h3>Zapisz/Zmień/Zrezygnuj</h3>

    <div class="info-txt">
      <h3>UWAGA!</h3>
      <p>Zapisanie dziecka na zajęcia równoznaczne jest z akceptacją regulaminu zajęć przedszkolnych lub szkolnych znajdującego się w zakładce „Regulamin”.</p>
      <p>Dokonanie zmiany w Portalu Rodzica (przepisanie do innej sekcji/rezygnacja/zapisanie na nową sekcję) i potwierdzenie jej przyciskiem „Zapisz” nie wywołuje skutku natychmiastowego. Każda decyzja musi być zatwierdzona przez Klub. Możliwość dokonania zmiany oraz jej skutek finansowy są potwierdzane maksymalnie w ciągu 7 dni.</p>
    </div>

    <table id="zgloszenia" cellpadding="0" width="100%" class="zgloszenia-table" >
      <thead>
        <tr>
          <th width="24%">imię i nazwisko syna</th>
          <th width="14%">klasa</th>
          <th width="24%">sekcja sportowa</th>
          <th width="15%">składka miesięczna (wg taryfikatora)</th>
          <th class="rezygnacja_head" width="5%">rezygnacja</th>
        </tr>
      </thead>
      <tbody>

      <?php
        $i = 1;
        foreach ($this->zgloszenia as $key => $dziecko): ?>

        <tr id="<?php echo $i; ?>" class="row">
          <td>
            <input type="hidden" name="imie_nazwisko[<?php echo $i; ?>]">
            <select class="imie_nazwisko input" name="imie_nazwisko[<?php echo $i; ?>]">
              <?php if (empty($this->dzieci)): ?>
              <option value="0">--- Wybierz dziecko ---</option>
              <?php endif; ?>
              <?php foreach ($this->dzieci as $k => $v): ?>
                <?php
                  if ($v['dziecko_id'] == $dziecko['dziecko_id']) {
                    $sel = ' selected';
                  } else {
                    $sel = '';
                  }
                ?>
                <option value="<?php echo $v['dziecko_id']; ?>" <?php echo $sel; ?>><?php echo $v['dziecko_imie'].' '.$v['rodzina_nazwisko']; ?></option>
              <?php endforeach ?>
            </select>
          </td>
          <td class="klasa">
            <span><?php echo $dziecko['klasa_nazwa']; ?></span>
            <input type="hidden" name="klasa[<?php echo $i; ?>]" value="<?php echo $dziecko['klasa_id']; ?>">
          </td>
          <td>
            <input type="hidden" name="sekcja_sportowa[<?php echo $i; ?>]">
            <select name="sekcja_sportowa[<?php echo $i; ?>]" class="sekcja_sportowa input">
              <?php if (empty($this->sekcje[$dziecko['klasa_id']])): ?>
              <option value="0">--- Wybierz sekcję ---</option>
              <?php endif ?>
              <?php foreach ($this->sekcje[$dziecko['klasa_id']] as $k => $v): ?>
                <?php
                  if ($k == $dziecko['zgloszenie_sekcja']) {
                    $sel = ' selected';
                  } else {
                    $sel = '';
                  }
                ?>
                <option value="<?php echo $k; ?>" <?php echo $sel; ?>><?php echo $v; ?></option>
              <?php endforeach ?>
            </select>
          </td>
          <td class="taryfa">
            <span><?php echo $dziecko['skladka']; ?></span>
            <input type="hidden" name="taryfa[<?php echo $i; ?>]" value="<?php echo $dziecko['skladka']; ?>">
          </td>

          <?php
            if ($dziecko['zgloszenie_propozycja'] > 0 && $dziecko['zgloszenie_propozycja'] != $dziecko['skladka']) {
              $sp = $dziecko['zgloszenie_propozycja'];
            } else {
              $sp = '';
            }
          ?>

          <td>
            <input type="hidden" name="rezygnacja[<?php echo $i; ?>]" value="0">
            <input class="rezygnuj" type="checkbox" name="rezygnacja[<?php echo $i; ?>]" value="1">
          </td>
          <input type="hidden" name="zgloszenie_id[<?php echo $i; ?>]" value="<?php echo $dziecko['zgloszenie_id']; ?>" >
        </tr>

      <?php
      $i++;
      endforeach ?>
      </tbody>
    </table>
    <table cellpadding="0" width="100%" class="zgloszenia-table">
      <tbody>
        <tr class="dodaj_wrapp" style="background: #f0f3f6 !important;">
          <td colspan="3"><button id="js-dodaj" class="add-button"><span></span>Dodaj dziecko</button></td>
        </tr>
        <tr>
          <td colspan="3"><textarea placeholder="Tutaj można wpisać uwagi lub komentarze dotyczące dokonywanych zmian" class="textarea" name="uwagi"></textarea></td>
        </tr>
        <tr class="suma">
          <td width="80%">SUMA SKŁADEK</td>
          <td width="20%" id="suma-taryfa">0 zł</td>
        </tr>
      </tbody>
    </table>

    <div class="regulamin">
      <div class="regulamin-checkbox">
        <input type="hidden" name="regulamin" value="0">
        <input class="js-regulamin-checkbox" type="checkbox" name="regulamin" value="1">
      </div>
      <div>Akceptuje <a href="<?php echo URL; ?>regulamin/">regulamin</a><sup>*</sup></div>
    </div>
    <div class="regulamin-info">
      <sup>*</sup> Akceptacja regulaminu jest obowiązkowa
    </div>
    <button class="save-button zapisz zgloszenie-zapisz" type="submit" id="js-zapisz" name="zapisz"><span></span>Zapisz</button>
    <div class="clear"></div>
  </div>

  </form>
</div>