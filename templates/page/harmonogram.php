<div class="content-full">
  <div class="cms-content harmonogram">
    <ul class="tabList">
      <?php
      foreach ($this->sections as  $key => $section) :  ?>
        <li class=" <?php echo $this->active === $section['sekcja_id'] ? 'active' : null ?>">
          <a href="/harmonogram/<?php echo $this->school . '/' . $section['sekcja_id']; ?>">
            <?php echo $section['sekcja_nazwa']; ?>
          </a>
        </li>
      <?php endforeach; ?>


    </ul>
    <div class="tab-content active">
      <table id="tablepress-pnz" class="tablepress tablepress-id-pnz">
        <thead>
          <tr class="row-1 odd">
            <th class="column-1">Grupy</th>
            <th class="column-2">Poniedziałek</th>
            <th class="column-3">Wtorek</th>
            <th class="column-4">Środa</th>
            <th class="column-5">Czwartek</th>
            <th class="column-6">Piątek</th>
          </tr>
        </thead>
        <tbody class="row-hover">
          <?php foreach ($this->table as $key => $value) : ?>
            <tr class="row-2 even">
              <td class="column-1"><?php echo $value['group']; ?></td>
              <td class="column-2">
                <strong><?php echo $value['mon_time']; ?></strong> <br>
                <?php echo $value['mon_place']; ?>
              </td>
              <td class="column-3">
                <strong><?php echo $value['tue_time']; ?></strong> <br>
                <?php echo $value['tue_place']; ?>
              <td class="column-4">
                <strong><?php echo $value['wed_time']; ?></strong> <br>
                <?php echo $value['wed_place']; ?>
              </td>
              <td class="column-5">
                <strong><?php echo $value['thu_time']; ?></strong> <br>
                <?php echo $value['thu_place']; ?>
              <td class="column-6">
                <strong><?php echo $value['fri_time']; ?></strong> <br>
                <?php echo $value['fri_place']; ?>
              </td>
            </tr>
          <?php endforeach; ?>

        </tbody>
      </table>
      <p>
        <!-- #tablepress-pnz from cache -->
      </p>
    </div>
  </div>
</div>