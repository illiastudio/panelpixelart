<!doctype html>
<html>
  <head>
    <title>Zresetuj hasło - Panel Administracyjny</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo URL; ?>public/css/login.css">
    <script type="text/javascript" src="<?php echo URL; ?>public/js/jquery.min.js"></script>
    <script>
      $(document).ready(function() {
        $('input[type="text"]').first().focus();

        $flashMessages = $(".messages > div:not(.sticky)");

        if ($flashMessages.length > 0) {
          $($flashMessages.get().reverse()).each(function(index) {
            var $this = $(this);
            var delayInMilliseconds = 1500;

            setTimeout(function() {
              $this.find('> *').css('visibility', 'hidden');
              $this.slideUp(500, function() {
                $(this).remove();
              });
            }, delayInMilliseconds * (index + 1));
          });
        }

        $(document).on("click", ".close-msg", function(e) {
          e.preventDefault();

          var $this = $(this).parent();

          $this.find('> *').css('visibility', 'hidden');
          $this.slideUp(500, function() {
            $(this).remove();
          });
        });
      });
    </script>
  </head>
<body class="password-reset">
  <div class="login-form">
    <p>Podaj login dla którego ma być zresetowane hasło</p>
    <form action="" method="post">
      <div>
        <input type="text" name="login" class="textbox" placeholder="Login" class="textbox">
      </div>
      <div>
        <span>Warto sprawdzić folder SPAM. W przypadku niepowodzenia zapraszamy do kontaktu z biurem – <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a></span>
      </div>
      <div>
        <input type="submit" name="wyslij" value="Wyślij" class="button">
      </div>
    </form>
  </div>
  <?php echo $this->msg; ?>
</body>
</html>