<div class="content-full">
  <div class="sezon-buttons">
    <ul>
    <?php foreach ($this->sezony as $sezon): ?>
      <?php
        if ($this->thisSezon == $sezon['sezon_nazwa']) {
          $class = 'class="active"';
        } else {
          $class = '';
        }
      ?>

      <li>
        <a <?php echo $class; ?> href="<?php echo $sezon['sezon_nazwa']; ?>"><?php echo $sezon['sezon_nazwa']; ?></a>
      </li>
    <?php endforeach; ?>
    </ul>
  </div>

  <h2>Historia zgłoszeń za sezon: <?php echo $this->thisSezon; ?></h2>

  <?php if (!empty($this->arr)): ?>
  <?php foreach ($this->arr as $okres => $zgloszenia): ?>

  <?php if (!empty($zgloszenia)): ?>

  <table class="zgloszenia-table" width="100%">
    <thead>
      <th colspan="5">
        <?php echo $this->months[date('m', strtotime($okres)) - 1].' '.date('Y', strtotime($okres)); ?>
      </th>
    </thead>
    <tbody>

      <?php
        $skladkaRazem = $propozycjaRazem = 0;
      ?>

      <?php foreach ($zgloszenia as $key => $val): ?>
        <tr>
          <td><?php echo $val['dziecko_imie']; ?></td>
          <td width="15%"><?php echo $val['klasa_nazwa']; ?></td>
          <td width="15%"><?php echo $val['sekcja_nazwa']; ?></td>
          <td width="15%">
            <?php echo $val['skladka']; ?>
            <?php $skladkaRazem += $val['skladka']; ?>
          </td>
          <td width="15%">
            <?php echo $val['zgloszenie_propozycja']; ?>
            <?php $propozycjaRazem += $val['zgloszenie_propozycja']; ?>
          </td>
        </tr>
      <?php endforeach; ?>

      <tr class="suma">
        <td colspan="3">SUMA SKŁADEK</td>
        <td><?php echo $skladkaRazem; ?> zł</td>
        <td><?php echo $propozycjaRazem; ?> zł</td>
      </tr>

    </tbody>
  </table>

  <?php endif; ?>

  <?php endforeach; ?>
  <?php else: ?>
  <p class="no-result-txt">Brak historii do wyświetlenia</p>
  <?php endif; ?>

</div>