<div class="content-full">
  <h2>Aktualne zgłoszenie</h2>

  <div>
    <?php if (!empty($this->activeArr)): ?>

    <h3><?php echo $this->activeArr[0]['zgloszenie_date_add']; ?></h3>

    <?php if (!empty($this->status)): ?>
    <h4 class="important"><?php echo $this->status; ?></h4>
    <?php endif ?>

    <table id="zgloszenia" cellpadding="0" width="100%" class="zgloszenia-table" >
      <thead>
        <tr>
          <th width="24%">imię syna</th>
          <th width="14%">klasa</th>
          <th width="24%">sekcja sportowa</th>
          <th width="15%">składka miesięczna (wg taryfikatora)</th>
          <th width="18%">propozycja indywidualnych ustaleń składki</th>
          <th>Zmiana wejdzie w życie</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $suma = 0;
        $propozycja = 0;
      ?>
      <?php foreach ($this->activeArr as $key => $val): ?>
        <tr class="row">
          <td><?php echo $val['dziecko_imie']; ?></td>
          <td><?php echo $val['klasa_nazwa']; ?></td>
          <td><?php echo $val['sekcja_nazwa']; ?></td>
          <td><?php echo $val['skladka']; ?></td>
          <?php $suma += $val['skladka']; ?>

          <?php if ($val['zgloszenie_propozycja'] > 0 && $val['zgloszenie_propozycja'] != $val['skladka']): ?>
          <td><?php echo $val['zgloszenie_propozycja']; ?></td>
          <?php else: ?>
          <td></td>
          <?php endif; ?>

          <?php $propozycja += $val['zgloszenie_propozycja']; ?>
          <th><?php echo $val['zgloszenie_date']; ?></th>
        </tr>

      <?php endforeach ?>
      </tbody>
    </table>
    <table cellpadding="0" width="100%" class="zgloszenia-table">
      <tbody>
        <tr class="uwagi">
          <td colspan="3"><?php echo $this->activeArr[0]['uwagi_tresc']; ?></td>
        </tr>
         <tr class="suma">
          <td width="60%">SUMA SKŁADEK</td>
          <td width="20%" id="suma-taryfa"><?php echo $suma; ?> zł</td>
          <?php if ($propozycja != $suma): ?>
          <td width="20%" id="suma-propozycja"><?php echo $propozycja; ?> zł</td>
          <?php else: ?>
          <td width="20%" id="suma-propozycja"></td>
          <?php endif ?>
        </tr>
      </tbody>
    </table>
    <?php else: ?>
    <h4 class="important">Brak aktualnego zgłoszenia</h4>
    <?php endif ?>

    <div class="clear"></div>
  </div>

  <h2>Historia zgłoszeń</h2>

  <?php if (!empty($this->historiaArr)): ?>
  <?php foreach ($this->historiaArr as $key => $value): ?>

  <div>
    <h3><?php echo $key; ?></h3>

    <table id="zgloszenia" cellpadding="0" width="100%" class="zgloszenia-table" >
      <thead>
        <tr>
          <th width="24%">imię syna</th>
          <th width="14%">klasa</th>
          <th width="24%">sekcja sportowa</th>
          <th width="15%">składka miesięczna (wg taryfikatora)</th>
          <th width="18%">propozycja indywidualnych ustaleń składki</th>
        </tr>
      </thead>
      <tbody>

      <?php
        $suma = 0;
        $propozycja = 0;
      ?>
      <?php foreach ($value as $key => $val): ?>
        <tr class="row">
          <td><?php echo $val['dziecko_imie']; ?></td>
          <td><?php echo $val['klasa_nazwa']; ?></td>
          <td><?php echo $val['sekcja_nazwa']; ?></td>
          <td><?php echo $val['skladka']; ?></td>
          <?php $suma += $val['skladka']; ?>

          <?php if ($val['zgloszenie_propozycja'] > 0 && $val['zgloszenie_propozycja'] != $val['skladka']): ?>
          <td><?php echo $val['zgloszenie_propozycja']; ?></td>
          <?php else: ?>
          <td></td>
          <?php endif; ?>

          <?php $propozycja += $val['zgloszenie_propozycja']; ?>
        </tr>
      <?php endforeach ?>
      </tbody>
    </table>
    <table cellpadding="0" width="100%" class="zgloszenia-table">
      <tbody>
        <tr class="uwagi">
          <td colspan="3"><?php echo $value[key($value)]['uwagi_tresc']; ?></td>
        </tr>
         <tr class="suma">
          <td width="60%">SUMA SKŁADEK</td>
          <td width="20%" id="suma-taryfa"><?php echo $suma; ?> zł</td>
          <?php if ($propozycja != $suma): ?>
          <td width="20%" id="suma-propozycja"><?php echo $propozycja; ?> zł</td>
          <?php else: ?>
          <td width="20%" id="suma-propozycja"></td>
          <?php endif ?>
        </tr>
      </tbody>
    </table>

    <div class="clear"></div>
  </div>

  <?php endforeach ?>
  <?php else: ?>
    <h4 class="important">Brak wcześniejszych zgłoszeń</h4>
  <?php endif; ?>
</div>