<!doctype html>
<html>

<head>
  <title>Panel Rodzica</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="<?php echo URL; ?>public/css/login.css">
  <script type="text/javascript" src="<?php echo URL; ?>public/js/jquery.min.js"></script>
  <script>
    $(document).ready(function() {
      $flashMessages = $(".messages > div:not(.sticky)");

      if ($flashMessages.length > 0) {
        $($flashMessages.get().reverse()).each(function(index) {
          var $this = $(this);
          var delayInMilliseconds = 1500;

          setTimeout(function() {
            $this.find('> *').css('visibility', 'hidden');
            $this.slideUp(500, function() {
              $(this).remove();
            });
          }, delayInMilliseconds * (index + 1));
        });
      }

      $(document).on("click", ".close-msg", function(e) {
        e.preventDefault();

        var $this = $(this).parent();

        $this.find('> *').css('visibility', 'hidden');
        $this.slideUp(500, function() {
          $(this).remove();
        });
      });
    });
  </script>
</head>

<body>
  <div class="top-header">
  </div>
  <div class="login-form">
    <br>
    <a href="<?php echo URL . 'admin/login'; ?>">Panel Administratora/Trenera</a>
    <form action="" method="post">
      <div><input type="text" name="login" class="textbox" placeholder="Login"></div>
      <div><input type="password" name="haslo" class="textbox" placeholder="Hasło"></div>
      <div><input  style="background-color: #BBB; box-shadow: 0 4px 0 0 #ccc;" type="submit" name="zaloguj" value="Zaloguj"></div>
      <br>
      <a href="<?php echo URL . 'password/reset'; ?>">Zapomniałem hasła</a>
    </form>
  </div>

  <div class="copyright-right">
    Projekt i wykonanie: <a target="_blank" href="http://www.studiograficzne.com">Studio Graficzne - Strony Internetowe</a>
  </div>
  <?php echo $this->msg; ?>
</body>

</html>