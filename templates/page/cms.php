<div class="content-full">

  <div class="cms-content">
    <h2><?php echo $this->cms[0]['cms_title']; ?></h2>

    <div>
      <?php echo htmlspecialchars_decode($this->cms[0]['cms_content']); ?>
    </div>
  </div>
</div>