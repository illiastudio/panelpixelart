<div class="messages">
  <?php foreach ($this->msgArray as $key => $value) { ?>
  <?php if (isset($value['sticky']) && $value['sticky'] == true): ?>
  <div class="<?php echo $value['class']; ?> sticky">
  <?php else: ?>
  <div class="<?php echo $value['class']; ?>">
  <?php endif ?>
  <span class="msg-icon"></span>
    <p><?php echo $value['msg']; ?></p>
    <?php if (isset($value['sticky']) && $value['sticky'] == true): ?>
    <span class="close-msg"></span>
    <?php endif ?>
  </div>
  <?php } ?>
</div>