<div class="content-full">

  <h2>Dane do płatności</h2>

  <style>
    table {
      font-size: 16px;
      border-collapse: collapse;
    }

    td {
      padding: 5px 10px;
      border: 1px solid;
    }
  </style>

  <center>
    <table>
      <tr>
        <td>Nazwa banku:</td>
        <td>BANK PEKAO S.A.</td>
      </tr>
      <tr>
        <td>Nr rachunku:</td>
        <td>27 1240 1095 1111 0010 4117 0545</td>
      </tr>
      <tr>
        <td>Właściciel rachunku:</td>
        <td>UCZNIOWSKI KLUB SPORTOWY ŻAGLE, UL. POŻARYSKIEGO 28, 04-703 WARSZAWA</td>
      </tr>
      <tr>
        <td>Tytuł przelewu:</td>
        <td>Imię i Nazwisko ucznia, klasa</td>
      </tr>
    </table>
  </center>
  <br>
  <p style="text-align: center;font-size: 14px">Informujemy, że ustalone kwoty mają charakter ryczałtu i nie zależą od ilości treningów zrealizowanych w danym miesiącu.<p>
</div>