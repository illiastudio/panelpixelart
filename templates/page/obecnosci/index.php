<div class="content-full">
  <h2>Obecności</h2>

  <?php if (empty($this->arr)): ?>
    <h4>Brak treningów do wyświetlenia</h4>
  <?php else: ?>

  <?php foreach ($this->arr as $key => $val): ?>

  <h3 style="text-align: center;"><?php echo $val['imie'].' '.$val['nazwisko'].' '.$val['klasa']; ?></h3>


  <?php foreach ($val['treningi'] as $key2 => $val2): ?>
    <h4><?php echo $val2['druzyna'].' - '.$val2['sekcja']; ?></h4>

    <?php foreach ($val2['arr'] as $key3 => $val3): ?>

  <table class="obecnosci-table" width="100%">
    <thead>
      <tr>
        <th colspan="0"><?php echo $this->miesiace[$key3]; ?></th>
      </tr>
    </thead>
    <tbody>
      <?php
        $daty = '';
        $obecnosci = '';
      ?>
      <?php foreach ($val3 as $key4 => $val4): ?>
        <?php
          $daty .= '<td>'.$key4.'</td>';

          if ($val4 == '1') {
            $ob = 'Obecny';
          } else {
            $ob = 'Brak danych';
          }

          $obecnosci .= '<td>'.$ob.'</td>';
        ?>
      <?php endforeach ?>

      <tr>
        <td width="10%"><strong>Dni trenigów</strong></td>
        <?php echo $daty; ?>
      </tr>
      <tr>
        <td><strong>Obecność</strong></td>
        <?php echo $obecnosci; ?>
      </tr>
    </tbody>
  </table>
    <?php endforeach ?>
  <?php endforeach ?>

  <?php endforeach ?>

  <?php endif; ?>
</div>