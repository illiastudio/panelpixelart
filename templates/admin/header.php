<!doctype html>
<html>
  <head>
    <title><?php echo stripslashes($this->title); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php foreach ($this->styles as $style) { ?>
      <?php echo $style; ?>
    <?php } ?>
    <?php foreach ($this->scripts as $script) { ?>
      <?php echo $script; ?>
    <?php } ?>
  </head>
<body>
  <div id="page-wrapper">