<div class="left">
  <h3>Dodaj uzytkownika</h3>
  <form action="" method="post">

    <div class="form-line">
      <span class="form-label"><label for="login">Login</label></span>
      <input id="js-login" type="text" name="login">
    </div>

    <div class="form-line">
      <span class="form-label"><label for="mail">Mail</label></span>
      <input id="js-mail" type="text" name="mail">
    </div>

    <div class="form-line">
      <span class="form-label"><label for="haslo">Hasło</label></span>
      <input id="js-haslo" type="text" name="haslo">
    </div>

    <div class="form-line">
      <span class="form-label"><label for="imie-nazwisko">Imię i nazwisko</label></span>
      <input id="js-imie-nazwisko" type="text" name="imie-nazwisko">
    </div>

    <div class="form-line">
      <span class="form-label"><label for="rola">Rola</label></span>
      <select name="rola">
        <option value="">--- Wybierz role ---</option>
        <option value="1">Trener</option>
        <option value="2">Admin</option>
      </select>
    </div>

    <div class="form-line">
      <span class="form-label"></span>
      <button class="add-button" type="submit" name="dodaj-uzytkownika"><span></span>Dodaj użytkownika</button>
    </div>
  </form>
</div>
<div class="content">
  <h3>Lista użytkowników</h3>
  <table id="js-users-table" width="100%" class="list-table">
    <thead>
      <tr>
        <th>Login</th>
        <th>Imię i nazwisko</th>
        <th>Mail</th>
        <th>Rola</th>
        <th>Opcje</th>
      </tr>
    </thead>

    <tbody>

    <?php foreach ($this->lista as $key => $val): ?>
    <?php
      if ($key % 2 == 0) {
        $class = 'even';
      } else {
        $class = 'odd';
      }
    ?>

      <tr class="<?php echo $class; ?>">
        <td><?php echo $val['user_login']; ?></td>
        <td><?php echo $val['user_name']; ?></td>
        <td><?php echo $val['user_mail']; ?></td>
        <td>
        <?php
          if ($val['user_level'] == "1") {
            echo "Trener";
          } elseif ($val['user_level'] == "2") {
            echo "Administrator";
          }
        ?>
        </td>
        <td>
          <div class="menu-buttons">
            <a class="edit js-edit-user" href="<?php echo URL.'admin/uzytkownicy/edytuj/'.$val['user_id']; ?>"><span class="none">Edytuj</span></a>
          </div>
        </td>
      </tr>
    <?php endforeach ?>

    </tbody>

  </table>
</div>