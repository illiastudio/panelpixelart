<div class="edit-box">
  <h3>Edycja: <?php echo $this->user[0]['user_login']; ?></h3>

  <form method="post" action="<?php echo URL; ?>admin/uzytkownicy/edytuj/<?php echo $this->user[0]['user_id']; ?>">
    <div class="form-line">
      <label class="form-label" for="imie-nazwisko">Imię i nazwisko</label>
      <input class="input" type="text" name="imie-nazwisko" value="<?php echo $this->user[0]['user_name']; ?>">
    </div>

    <div class="form-line">
      <span class="form-label"><label for="mail">Mail</label></span>
      <input id="js-mail" type="text" name="mail" value="<?php echo $this->user[0]['user_mail']; ?>">
      <input type="hidden" name="mail-old" value="<?php echo $this->user[0]['user_mail']; ?>">
    </div>

    <div class="form-line">
      <span class="form-label"><label for="rola">Rola</label></span>
      <select name="rola">
        <?php if ($this->user[0]['user_level'] == "1"): ?>
        <option value="1" selected>Trener</option>
        <option value="2">Admin</option>
        <?php elseif ($this->user[0]['user_level'] == "2"): ?>
        <option value="1">Trener</option>
        <option value="2" selected>Admin</option>
        <?php endif; ?>
      </select>
    </div>

    <div class="form-line">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
    </div>
  </form>
</div>