<div class="content-full">
  <h3>Lista zgłoszeń</h3>

  <div class="rekordy-info">
  <?php if ($this->strona > 1): ?>
    <a class="prev prev-button" href="<?php echo URL.'admin/zgloszenia/lista/'.($this->strona - 1); ?>"><span></span>Poprzednie</a>
  <?php else: ?>
    <span class="prev prev-button disabled"><span></span>Poprzednie</span>
  <?php endif ?>
  <span class="now">Zgłoszenie: <?php echo ($this->strona); ?> z <?php echo $this->ile; ?></span>
  <?php if ($this->strona < $this->ile): ?>
    <a class="next next-button" href="<?php echo URL.'admin/zgloszenia/lista/'.($this->strona + 1); ?>"><span></span>Następne</a>
  <?php else: ?>
    <span class="next next-button disabled"><span></span>Następne</span>
  <?php endif ?>
  </div>

  <form id="js-zgloszenia-form" action="" method="post">
  <table cellpadding="0" width="100%" class="zgloszenia">
    <thead>
      <tr>
        <th></th>
        <th colspan="5">Nazwisko</th>
        <th>Data zgłoszenia</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($this->zgloszenia as $key => $value): ?>
      <?php if (count($value['zgloszenia']) > 0): ?>
      <tr class="head-row">
        <td><button class="button" type="submit" name="wyslij">Wyślij</button></td>
        <td colspan="5"><?php echo $value['rodzina_nazwisko']; ?></td>
        <td><?php echo $value['zgloszenie_date_add']; ?></td>
      </tr>
      <tr class="head-second">
        <th width="120px">Zmień</th>
        <th>Imię i Nazwisko syna</th>
        <th>Klasa</th>
        <th>Sekcja</th>
        <th>Kwota</th>
        <th>Propozycja</th>
        <th>Data wejścia w życie</th>
      </tr>
      <?php
      $razem = 0;
      $propozycja = 0;

      foreach ($value['zgloszenia'] as $key2 => $value2): ?>
        <tr class="row" id="<?php echo $value2['zgloszenie_id']; ?>">
          <td>
            <input type="checkbox" name="zgloszenie[<?php echo $value['rodzina_id']; ?>][<?php echo $value2['zgloszenie_id']; ?>]" value="1">
            <input type="hidden" name="rodzina[<?php echo $value2['zgloszenie_id']; ?>]" value="<?php echo $value['rodzina_id']; ?>">
          </td>
          <td><?php echo $value2['dziecko_imie']; ?></td>
          <td><?php echo $value2['klasa']; ?></td>
          <td><?php echo $value2['sekcja_nazwa']; ?></td>
          <td>
            <?php echo $value2['zgloszenie_skladka']; ?>
            <input class="taryfa-zgl" type="hidden" name="taryfa[<?php echo $value2['zgloszenie_id']; ?>]" value="<?php echo $value2['zgloszenie_skladka']; ?>">
          </td>
          <td>
            <?php // echo $value2['zgloszenie_propozycja']; ?>
            <input class="propozycja-zgl input" type="text" name="propozycja[<?php echo $value2['zgloszenie_id']; ?>]" value="<?php echo $value2['zgloszenie_propozycja']; ?>">
          </td>
          <td>

            <input class="js-zgloszenie-date" class="input" style="max-width: 150px;" readonly="readonly" type="text" name="zgloszenie_date[<?php echo $value2['zgloszenie_id']; ?>]" value="<?php if (isset($value2['zgloszenie_date'])) { echo $value2['zgloszenie_date']; } ?>">

<?php /*
<input class="js-zgloszenie-date input" style="max-width: 150px;" readonly="readonly" type="text" name="zgloszenie_date[<?php echo $value2['zgloszenie_id']; ?>]" value="2015-09-01">
*/ ?>
          </td>
        </tr>
      <?php
        $razem += $value2['zgloszenie_skladka'];
        $propozycja += $value2['zgloszenie_propozycja'];
      ?>
      <?php endforeach ?>
      <?php if (!empty($value['uwagi_tresc'])): ?>
      <tr class="uwagi">
        <td colspan="3"><span>UWAGI:</span></td>
        <td colspan="4"><?php echo $value['uwagi_tresc']; ?></td>
      </tr>
      <?php endif ?>
      <tr class="odpowiedz">
        <td colspan="3"><span>ODPOWIEDŹ:</span></td>
        <td colspan="4">
          <textarea name="odpowiedz"></textarea>
          <input type="hidden" name="uwaga-id" value="<?php echo $value['uwagi_id']; ?>">
          <input type="hidden" name="uwaga-tresc" value="<?php echo $value['uwagi_tresc']; ?>">
        </td>
      </tr>
      <tr class="razem">
        <td colspan="3">RAZEM:</td>
        <td>Wg taryfikatora:</td>
        <td><?php echo $razem; ?></td>
        <td>Propozycja:</td>
        <td><?php echo $propozycja; ?></td>
      </tr>
    <?php endif ?>
    <?php endforeach ?>
    </tbody>
  </table>
  </form>
</div>