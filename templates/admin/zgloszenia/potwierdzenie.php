<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="UTF-8">

  <style>
    body {
      text-align: center;
      margin: 25px;
    }

    table.zgloszenia {
      text-align: center;
      border-collapse: collapse;
      margin: 50px 0;
      width: 100%;
    }

    .zgloszenia th {
      border-bottom: 2px solid black;
    }

    .zgloszenia td {
      border: 1px solid black;
    }

    td, th {
      padding: 5px;
    }

    .imp {
      color: red;
    }

    .imp-under {
      color: red;
      text-decoration: underline;
    }

    .zarzad {
      text-align: left;
      margin: 50px 0 0;
    }
  </style>

</head>
<body>

  <p>Drodzy Rodzice,</p>
  <p>Informujemy, że <strong>zgłoszona przez Was zmiana została przeprocesowana.</strong><br>
  Szczegóły są dostępne w Portalu Rodzica w zakładkach: "Historia zgłoszeń" i "Płatności".</p>
  <p><strong>Prosimy o upewnienie się</strong> czy wszystko zostało poprawnie odnotowane.</p>
  <p>Gdyby okazało się, że są jakieś błędy – prosimy o kontakt poprzez: <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a></p>
  <center>
    <h1>
      Wejście do Portalu Rodzica poprzez stronę klubową: <a href="http://ukszagle.pl/">http://ukszagle.pl/</a>
    </h1>
  </center>

  <?php if (isset($this->uwaga) && !empty($this->uwaga)): ?>

    <h2>Uwagi:</h2>
    <p><?php echo $this->uwaga; ?></p>

  <?php endif; ?>

  <?php if (isset($this->odpowiedz)): ?>

    <h2>Odpowiedź:</h2>
    <p><?php echo $this->odpowiedz; ?></p>
    <br>

  <?php endif; ?>

  <p>Ze sportowym pozdrowieniem,<br>
  Zarząd UKS Żagle</p>

</body>
</html>