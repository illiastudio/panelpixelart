<div class="edit-box">
  <?php $tmp = $this->tmp[0]; ?>
  <h3>Edycja:
      Sekcja: <?php echo $tmp['sekcja_nazwa']; ?>
      Klasa: <?php echo $tmp['klasa_nazwa']; ?>
      Sezon: <?php echo $tmp['taryfikator_sezon']; ?>
  </h3>

  <form method="post" action="<?php echo URL; ?>admin/taryfikator/edytuj/<?php echo $tmp['taryfikator_id']; ?>">
    <div class="form-line">
      <label class="form-label" for="taryfikator_oplata">Opłata</label>
      <input class="input" type="text" name="taryfikator_oplata" value="<?php echo $tmp['taryfikator_oplata']; ?>">
      <input type="hidden" name="taryfikator_oplata_old" value="<?php echo $tmp['taryfikator_oplata']; ?>">
    </div>

    <div class="form-line">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
    </div>
  </form>
</div>