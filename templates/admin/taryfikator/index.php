<div class="left">
  <h3>Dodaj taryfikator</h3>
  <form action="" method="post">
    <div class="form-line">
      <span class="form-label"><label for="klasa">Klasa</label></span>
      <select name="klasa">
        <option value="">--- Wybierz klase ---</option>
        <?php foreach ($this->klasy as $key => $val) : ?>
          <option value="<?php echo $val['klasa_id']; ?>"><?php echo $val['klasa_nazwa']; ?></option>
        <?php endforeach ?>
      </select>
    </div>
    <div class="form-line">
      <span class="form-label"><label for="sekcja">Sekcja</label></span>
      <select name="sekcja">
        <option value="">--- Wybierz sekcje ---</option>
        <?php foreach ($this->sekcje as $key => $val) : ?>
          <option value="<?php echo $val['sekcja_id']; ?>"><?php echo $val['sekcja_nazwa']; ?></option>
        <?php endforeach ?>
      </select>
    </div>
    <div class="form-line">
      <span class="form-label"><label for="sezon">Sezon</label></span>
      <select name="sezon">
        <option value="" selected>--- Wybierz sezon ---</option>
        <?php foreach ($this->sezon as $key => $val) : ?>
          <option value="<?php echo $val['sezon_nazwa']; ?>"><?php echo $val['sezon_nazwa']; ?></option>
        <?php endforeach ?>
      </select>
    </div>
    <div class="form-line">
      <span class="form-label"><label for="oplata">Opłata</label></span>
      <input type="text" name="oplata">
    </div>
    <div class="form-line">
      <span class="form-label"></span>
      <button class="add-button" type="submit" name="dodaj-taryfikator"><span></span>Dodaj taryfikator</button>
    </div>
  </form>
</div>
<div class="content">
  <h3>Lista opłat bieżącego sezonu</h3>
  <table id="js-taryfikator-table_last" width="100%" class="list-table">
    <thead>
      <tr>
        <th>Klasa</th>
        <th>Sekcja</th>
        <th>Sezon</th>
        <th>Opłata</th>
        <th width="5%">Opcje</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($this->taryfikator as $key => $val) : ?>
        <?php if ($val['taryfikator_sezon'] === '20-21') : ?>
          <tr>
            <td><?php echo $val['klasa_nazwa']; ?></td>
            <td><?php echo $val['sekcja_nazwa']; ?></td>
            <td><?php echo $val['taryfikator_sezon']; ?></td>
            <td><?php echo $val['taryfikator_oplata']; ?></td>
            <td>
              <div class="menu-buttons">
                <a class="edit js-edit-taryfikator" title="Edytuj" href="<?php echo URL . 'admin/taryfikator/edytuj/' . $val['taryfikator_id']; ?>"><span class="none">Edytuj</span></a>
                <a class="delete js-delete-taryfikator" title="Usuń" href="<?php echo URL . 'admin/taryfikator/usun/' . $val['taryfikator_id']; ?>"><span class="none">Usuń</span></a>
              </div>
            </td>
          </tr>
        <?php endif; ?>
      <?php endforeach ?>
    </tbody>
  </table>


  <h3>Lista opłat</h3>
  <table id="js-taryfikator-table" width="100%" class="list-table">
    <thead>
      <tr>
        <th>Klasa</th>
        <th>Sekcja</th>
        <th>Sezon</th>
        <th>Opłata</th>
        <th width="5%">Opcje</th>
      </tr>
    </thead>

    <tbody>
      <?php foreach ($this->taryfikator as $key => $val) : ?>
        <tr>
          <td><?php echo $val['klasa_nazwa']; ?></td>
          <td><?php echo $val['sekcja_nazwa']; ?></td>
          <td><?php echo $val['taryfikator_sezon']; ?></td>
          <td><?php echo $val['taryfikator_oplata']; ?></td>
          <td>
            <div class="menu-buttons">
              <a class="edit js-edit-taryfikator" title="Edytuj" href="<?php echo URL . 'admin/taryfikator/edytuj/' . $val['taryfikator_id']; ?>"><span class="none">Edytuj</span></a>
              <a class="delete js-delete-taryfikator" title="Usuń" href="<?php echo URL . 'admin/taryfikator/usun/' . $val['taryfikator_id']; ?>"><span class="none">Usuń</span></a>
            </div>
          </td>
        </tr>
      <?php endforeach ?>

    </tbody>

  </table>
</div>