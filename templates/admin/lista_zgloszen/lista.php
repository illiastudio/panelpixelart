<div class="content-full">
  <form id="js-ustaw-date-form" action="" method="post">
    <div class="content-center">
      <div>
        <label for="date">Data wejścia w życie:</label>
        <?php if (isset($_SESSION['selectedDate'])): ?>
          <input id="js-datepicker" class="input" type="text" name="date" readonly="readonly" value="<?php echo $_SESSION['selectedDate']; ?>"><br>
          <?php unset($_SESSION['selectedDate']); ?>
        <?php else: ?>
          <input id="js-datepicker" class="input" type="text" name="date" readonly="readonly"><br>
        <?php endif ?>
      </div>
      <button class="save-button" type="submit" name="zmien_date"><span></span>Zmień datę</button>
    </div>
    <div>
      <table id="js-zgloszenia-list" class="list-table" width="100%" cellpadding="0">
        <thead>
          <tr>
            <th>Zaznacz</th>
            <th>Imie</th>
            <th>Nazwisko</th>
            <th>Klasa</th>
            <th>Sekcja</th>
            <th>Skladka</th>
            <th>Propozycja</th>
            <th>Data zgłoszenia</th>
            <th>Data zgloszenia sort</th>
            <th>Data wejścia w życie</th>
            <th>Data wejścia w życie sort</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach ($this->lista as $key => $val): ?>
          <tr class="row">
            <td><input type="checkbox" name="check[]" value="<?php echo $val['zgloszenie_id']; ?>"></td>
            <td><?php echo $val['dziecko_imie']; ?></td>
            <td><?php echo $val['rodzina_nazwisko']; ?></td>
            <td><?php echo $val['klasa_nazwa']; ?></td>
            <td><?php echo $val['sekcja_nazwa']; ?></td>
            <td><?php echo $val['zgloszenie_skladka']; ?></td>
            <td><?php echo $val['zgloszenie_propozycja']; ?></td>
            <td><?php echo $val['zgloszenie_date_add']; ?></td>
            <td><?php echo $val['zgloszenie_date_add_sort']; ?></td>
            <td><?php echo $val['zgloszenie_date']; ?></td>
            <td><?php echo $val['zgloszenie_date_sort']; ?></td>
          </tr>
        <?php endforeach ?>
        </tbody>
      </table>
    </div>
  </form>
</div>
<div class="clear"></div>