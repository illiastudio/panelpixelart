<div class="content-full">
<h3>Import płatności z pliku: <span class="important"><?php echo $_SESSION['platnosci_filename']; ?></span></h3>

<?php if ($this->ile > 0): ?>
  <div class="rekordy-info">
  <?php if ($this->number > 1): ?>
    <a class="prev prev-button" href="<?php echo URL.'admin/platnosci/rekord/'.($this->number - 1); ?>"><span></span>Poprzednie</a>
  <?php else: ?>
    <span class="prev prev-button disabled"><span></span>Poprzednie</span>
  <?php endif ?>
  <span class="now">Rekord: <?php echo $this->number; ?> z <?php echo $this->ile; ?></span>
  <?php if ($this->number < $this->ile): ?>
    <a class="next next-button" href="<?php echo URL.'admin/platnosci/rekord/'.($this->number + 1); ?>"><span></span>Następne</a>
  <?php else: ?>
    <span class="next next-button disabled"><span></span>Następne</span>
  <?php endif ?>
  </div>

  <div>
    <table class="platnosci" width="100%" cellpadding="0">
      <thead>
        <tr>
          <th>Lp</th>
          <th>Nr dokumentu</th>
          <th>Podmiot Id</th>
          <th>Nazwa</th>
          <th>Opis</th>
          <th>Kwota</th>
          <th>Data</th>
        </tr>
      </thead>
      <tbody>
        <tr class="row">
          <td><?php echo $this->rekord['id']; ?></td>
          <td><?php echo $this->rekord['nr_dokumentu']; ?></td>
          <td><?php echo $this->rekord['platnik_id']; ?></td>
          <td><?php echo $this->rekord['nazwa']; ?></td>
          <td><?php echo $this->rekord['opis']; ?></td>
          <td><?php echo $this->rekord['kwota']; ?></td>
          <td><?php echo $this->rekord['data']; ?></td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="content-center">
    <form action="" method="post">
      <div>
        <span>
          <label class="form-label fixed-width" for="search-field">Wpisz nazwisko szukanej rodziny</label>
        </span>
        <input id="dt-search" type="text" class="input" name="search-field">
      </div>
      <div>
        <span>
          <label class="form-label fixed-width" for="platnik_id">Wpisz numer ID szukanej rodziny</label>
        </span>
        <input type="hidden" name="number" value="<?php echo $this->number; ?>">
        <input type="hidden" name="platnik_id" value="<?php echo $this->rekord['platnik_id']; ?>">
        <input type="text" class="input" name="rodzina_id" value="<?php echo $this->platnikRodzina; ?>">
      </div>
      <br>
      <div class="reverse-order-buttons">
        <button class="save-button" type="submit" name="zaimportuj_platnosc" style="display: inline-block;"><span></span>Zaimportuj płatność</button>
        <button class="no-button" type="submit" name="przerwij" style="display: inline-block;"><span></span>Przerwij procedurę importu</button>
      </div>
    </form>
  </div>

  <div>
    <table id="js-rodziny-list" class="list-table" width="100%" cellpadding="0">
      <thead>
        <tr>
          <th width="16%">Rodzina Id</th>
          <th width="21%">Ojciec</th>
          <th width="21%">Ojciec mail</th>
          <th width="21%">Matka</th>
          <th width="21%">Matka mail</th>
        </tr>
      </thead>
      <tbody>
      <?php foreach ($this->rodziny as $key => $val): ?>
        <tr class="row">
          <td><?php echo $val['rodzina_id']; ?></td>
          <td><?php echo $val['ojciec']; ?></td>
          <td><?php echo $val['ojciec_mail']; ?></td>
          <td><?php echo $val['matka']; ?></td>
          <td><?php echo $val['matka_mail']; ?></td>
        </tr>
      <?php endforeach ?>
      </tbody>
    </table>
  </div>
<?php else: ?>
  <div><p>Brak niezaimportowanych płatności</p></div>
<?php endif ?>
</div>