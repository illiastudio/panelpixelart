<div class="content-full">
  <h3>Dodaj zgłoszenie</h3>

  <form action="" method="post" id="form">

  <div class="form">
    <table id="zgloszenia" cellpadding="0" width="100%" class="zgloszenia-table" >
      <thead>
        <tr>
          <th width="20%">imię i nazwisko syna</th>
          <th width="10%">klasa</th>
          <th width="20%">sekcja sportowa</th>
          <th width="17%">składka miesięczna</th>
          <th width="17%">propozycja składki</th>
          <th width="17%">Od</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <input type="hidden" name="imie_nazwisko">
            <select class="imie_nazwisko input" name="imie_nazwisko" data-placeholder="--- Wybierz dziecko ---">
              <option></option>
              <?php
              foreach ($this->dzieciLista as $k => $v) {
                echo '<option value="'.$v['dziecko_id'].'">'.$v['dziecko_imie'].' '.$v['rodzina_nazwisko'].'</option>';
              }
              ?>
            </select>
          </td>
          <td class="klasa">
            <span></span>
            <input type="hidden" name="klasa" value="">
          </td>
          <td>
            <input type="hidden" name="sekcja_sportowa">
            <select name="sekcja_sportowa" class="sekcja_sportowa input" disabled>
            </select>
          </td>
          <td class="taryfa">
            <span></span>
            <input type="hidden" name="taryfa" value="0">
          </td>
          <td class="propozycja"><input class="input" type="text" name="propozycja" value="" disabled></td>
          <td>
            <input class="input js-date-start" readonly="readonly" type="text" name="zgloszenie_date">
          </td>
        </tr>
      </tbody>
    </table>
    <button class="save-button zapisz" style="float: none;" type="submit" id="js-zapisz" name="zapisz"><span></span>Zapisz</button>
    <div class="clear"></div>
  </div>

  </form>
</div>