<div class="left">
  <h3>Dodaj treningi</h3>
  <form action="" method="post">
    <div class="form-line">
      <span class="form-label"><label for="daty-treningow">Dni</label></span>
      <input id="js-daty-treningow" type="text" name="daty-treningow" readonly="readonly">
    </div>
    <div class="form-line">
      <span class="form-label"></span>
      <button class="add-button" type="submit" name="dodaj-treningi"><span></span>Dodaj treningi</button>
    </div>
  </form>
</div>
<div class="content">
  <h3>Lista treningów</h3>
  <table id="js-treningi-table" width="100%" class="list-table">
    <thead>
      <tr>
        <th>Data</th>
        <th>Data sort</th>
        <th width="5%">Opcje</th>
      </tr>
    </thead>

    <tbody>

    <?php foreach ($this->treningi as $key => $val): ?>
      <tr>
        <td><?php echo $val['trening_data']; ?></td>
        <td><?php echo $val['data_sort']; ?></td>
        <td>
          <div class="menu-buttons">
            <a class="edit" href="<?php echo URL.'admin/treningi/obecnosci/'.$val['trening_id']; ?>"><span class="none">Obecności</span></a>
            <a class="delete js-delete-trening" href="<?php echo URL.'admin/treningi/usun/'.$val['trening_id']; ?>"><span class="none">Usuń</span></a>
          </div>
        </td>
      </tr>
    <?php endforeach ?>

    </tbody>

  </table>
</div>