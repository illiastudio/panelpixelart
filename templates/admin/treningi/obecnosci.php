<div class="content-full">
  <h3>Trening: <?php echo $this->trening[0]['druzyna_nazwa'].' - '.$this->trening[0]['trening_data']; ?></h3>

  <a class="return-link" href="<?php echo URL.'admin/treningi/lista/'.$this->trening[0]['druzyna_id']; ?>">Powrót do treningów</a>

  <form class="obecnosci" action="" method="post">
    <table class="list-table">
      <thead>
        <tr>
          <th>Dziecko</th>
          <th>Klasa</th>
          <th>Obecny</th>
          <th>Nieobecny</th>
        </tr>
      </thead>

      <tbody>

      <?php foreach ($this->lista as $key => $val): ?>
      <?php
        if ($key % 2 == 0) {
          $class = 'even';
        } else {
          $class = 'odd';
        }
      ?>
        <tr class="<?php echo $class; ?>">
          <td><?php echo $val['dziecko_imie'].' '.$val['rodzina_nazwisko']; ?></td>
          <td><?php echo $val['klasa_nazwa']; ?></td>
          <?php
          $nieobecny = '';
          $obecny = '';

          if (!empty($this->obecnosci)) {
            foreach ($this->obecnosci as $k => $v) {
              if ($v['obecnosc_dziecko'] == $val['dziecko_id']) {
                if ($v['obecnosc_status'] == '1') {
                  $obecny = 'checked="checked"';
                } elseif ($v['obecnosc_status'] == '2') {
                  $nieobecny = 'checked="checked"';
                }
              }
            }
          }
          ?>
          <td>
            <!-- <input type="hidden" value="0" name="obecnosc[<?php echo $val['dziecko_id']; ?>]"> -->
            <!-- <input type="checkbox" value="1" name="obecnosc[<?php echo $val['dziecko_id']; ?>]" <?php echo $checked; ?>> -->

            <input type="hidden" value="0" name="obecnosc[<?php echo $val['dziecko_id']; ?>]">
            <input type="radio" name="obecnosc[<?php echo $val['dziecko_id']; ?>]" value="1" <?php echo $obecny; ?>>
          </td>
          <td>
            <input type="radio" name="obecnosc[<?php echo $val['dziecko_id']; ?>]" value="2" <?php echo $nieobecny; ?>>
          </td>
        </tr>

      <?php endforeach ?>

      <tr>
        <td colspan="2">
        <br>
          <button name="zapisz-obecnosci" class="save-button"><span></span>Zapisz</button>
        </td>
      </tr>

      </tbody>

    </table>
  </form>

</div>