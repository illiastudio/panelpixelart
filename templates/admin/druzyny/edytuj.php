<div class="left">
  <?php if ($_SESSION['role'] == 'admin'): ?>
    <div>
    <h3>Edytuj drużynę</h3>
    <form action="" method="post">
      <div class="form-line">
        <span class="form-label"><label for="nazwa">Nazwa</label></span>
        <input type="text" name="nazwa" value="<?php echo $this->druzyna['druzyna_nazwa']; ?>">
        <input type="hidden" name="nazwa-old" value="<?php echo $this->druzyna['druzyna_nazwa']; ?>">
      </div>

      <div class="form-line">
        <span class="form-label"><label for="trener">Trener</label></span>
        <select name="trener">
          <option value="">--- Wybierz trenera ---</option>
        <?php foreach ($this->trenerzy as $key => $val): ?>
          <?php
            if ($val['user_id'] == $this->druzyna['user_id']) {
              $select = "selected";
            } else {
              $select = "";
            }
          ?>
          <option value="<?php echo $val['user_id']; ?>" <?php echo $select; ?>><?php echo $val['user_name']; ?></option>
        <?php endforeach ?>
        </select>
      </div>

      <div class="form-line">
        <span class="form-label"><label for="sekcja">Sekcja</label></span>
        <select name="sekcja">
          <option value="">--- Wybierz sekcje ---</option>
        <?php foreach ($this->sekcje as $key => $val): ?>
          <?php
            if ($val['sekcja_id'] == $this->druzyna['sekcja_id']) {
              $select = "selected";
            } else {
              $select = "";
            }
          ?>
          <option value="<?php echo $val['sekcja_id']; ?>" <?php echo $select; ?>><?php echo $val['sekcja_nazwa']; ?></option>
        <?php endforeach ?>
        </select>
      </div>

      <div class="form-line">
        <span class="form-label"></span>
        <button class="save-button" type="submit" name="zapisz-druzyne"><span></span>Zapisz</button>
      </div>
    </form>
  </div>
<?php endif; ?>
  <div>
    <br>
    <h3>Dodaj dzieci</h3>
    <form action="" method="post">

      <div class="form-line">
        <span class="form-label">
          <label for="">Dzieci</label>
        </span>

        <div class="multiselect-container">
          <select id="js-dodaj-dziecko-list" name="dzieci-lista[]" multiple="multiple" style="display: none;">
            <?php foreach ($this->dzieciLista as $key => $val): ?>
              <option value="<?php echo $val['dziecko_id']; ?>">
                <?php echo $val['rodzina_nazwisko'].' '.$val['dziecko_imie'].' - '.$val['klasa_nazwa']; ?>
              </option>
            <?php endforeach ?>
          </select>
        </div>

      </div>

      <div class="form-line">
        <span class="form-label"></span>
        <button class="add-button" type="submit" name="dodaj-dziecko"><span></span>Dodaj dzieci</button>
      </div>
    </form>
  </div>
</div>
<div class="content">
  <h3>Lista dzieci</h3>
  <table id="js-druzyny-dzieci-table" width="100%" class="list-table">
    <thead>
      <tr>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Klasa</th>
        <th width="5%">Opcje</th>
      </tr>
    </thead>

    <tbody>

    <?php foreach ($this->dzieci as $key => $val): ?>
      <tr>
        <td><?php echo $val['dziecko_imie']; ?></td>
        <td><?php echo $val['rodzina_nazwisko']; ?></td>
        <td><?php echo $val['klasa_nazwa']; ?></td>
        <td>
          <div class="menu-buttons">
            <a class="delete js-delete-dziecko" title="Usuń" href="<?php echo URL.'admin/druzyny/usunDziecko/'.$this->druzyna['druzyna_id'].'/'.$val['dziecko_id']; ?>"><span class="none">Usuń</span></a>
          </div>
        </td>
      </tr>
    <?php endforeach ?>

    </tbody>

  </table>
</div>