<?php if ($_SESSION['role'] == 'admin'): ?>
<div class="left">
  <h3>Dodaj drużynę</h3>
  <form action="" method="post">
    <div class="form-line">
      <span class="form-label"><label for="nazwa">Nazwa</label></span>
      <input type="text" name="nazwa">
    </div>

    <div class="form-line">
      <span class="form-label"><label for="trener">Trener</label></span>
      <select name="trener">
        <option value="">--- Wybierz trenera ---</option>
      <?php foreach ($this->trenerzy as $key => $val): ?>
        <option value="<?php echo $val['user_id']; ?>"><?php echo $val['user_name']; ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-line">
      <span class="form-label"><label for="sekcja">Sekcja</label></span>
      <select name="sekcja">
        <option value="">--- Wybierz sekcje ---</option>
      <?php foreach ($this->sekcje as $key => $val): ?>
        <option value="<?php echo $val['sekcja_id']; ?>"><?php echo $val['sekcja_nazwa']; ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-line">
      <span class="form-label"></span>
      <button class="add-button" type="submit" name="dodaj-druzyne"><span></span>Dodaj drużynę</button>
    </div>
  </form>
</div>
<div class="content">
<?php else: ?>
<div class="content-full">
<?php endif; ?>
  <h3>Lista drużyn</h3>
  <table id="js-druzyny-table" width="100%" class="list-table">
    <thead>
      <tr>
        <th>Nazwa</th>
        <th>Trener</th>
        <th>Sekcja</th>
        <th width="5%">Opcje</th>
      </tr>
    </thead>

    <tbody>

    <?php foreach ($this->druzyny as $key => $val): ?>
      <tr>
        <td><?php echo $val['druzyna_nazwa']; ?></td>
        <td><?php echo $val['user_name']; ?></td>
        <td><?php echo $val['sekcja_nazwa']; ?></td>
        <td>
          <div class="menu-buttons">
            <a class="trening js-treningi-druzyne" title="Treningi" href="<?php echo URL.'admin/treningi/lista/'.$val['druzyna_id']; ?>"><span class="none">Treningi</span></a>
            <a class="edit js-edytuj-druzyne" title="Edytuj" href="<?php echo URL.'admin/druzyny/edytuj/'.$val['druzyna_id']; ?>"><span class="none">Edytuj</span></a>
            <a class="delete js-delete-druzyny" title="Usuń" href="<?php echo URL.'admin/druzyny/usun/'.$val['druzyna_id']; ?>"><span class="none">Usuń</span></a>
          </div>
        </td>
      </tr>
    <?php endforeach ?>

    </tbody>

  </table>
</div>