<div class="content-full">
  <h3 class="title-bottom-line">Zarządzanie danymi użytkownika</h3>
  <form action="" method="post">
    <div class="form-line">
      <span class="form-label"><label for="mail">E-mail</label></span>
      <input type="text" name="mail" value="<?php echo $this->mail; ?>">
    </div>
    <div class="zmien-haslo">
      <h3 class="title-bottom-line">Zmiana hasła</h3>
      <div class="form-line">
        <span class="form-label"><label for="new-password">Nowe hasło</label></span>
        <input type="password" name="new-password">
      </div>
      <div class="form-line">
        <span class="form-label"><label for="confirm-password">Potwierdź hasło</label></span>
        <input type="password" name="confirm-password">
      </div>
      <div class="form-line">
        <span class="form-label"></span>
        <input type="hidden" name="old-mail" value="<?php echo $this->mail; ?>">
        <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
      </div>
    </div>
  </form>
</div>