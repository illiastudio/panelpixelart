  <div class="top-wrapper">

    <div class="top">
      <div class="header-wrapper">
        <h1>Panel Administratora</h1>
      </div>
      <div class="menu-wrapper">
        <ul>
          <?php if ($_SESSION['role'] == 'admin') : ?>
            <li>
              <a class="<?php echo $this->active[1]; ?>" href="<?php echo URL . 'admin/salda/'; ?>">Salda</a>
            </li>
            <li>
              <a class="<?php echo $this->active[2]; ?>" href="<?php echo URL . 'admin/zgloszenia/'; ?>">Zgloszenia</a>
            </li>
            <li>
              <a class="<?php echo $this->active[6]; ?>" href="<?php echo URL . 'admin/raport/'; ?>">Generuj raport</a>
            </li>
            <li>
              <a class="<?php echo $this->active[3]; ?>" href="<?php echo URL . 'admin/import/'; ?>">Import zgłoszeń</a>
            </li>
            <li>
              <a class="<?php echo $this->active[4]; ?>" href="<?php echo URL . 'admin/platnosci/'; ?>">Import płatności</a>
            </li>
            <li>
              <a class="<?php echo $this->active[5]; ?>" href="<?php echo URL . 'admin/zarzadzanie/'; ?>">Zarządzanie</a>
            </li>
            <li>
              <a class="<?php echo $this->active[10]; ?>" href="<?php echo URL . 'admin/taryfikator/'; ?>">Taryfikator</a>
            </li>
            <li>
              <a class="<?php echo $this->active[14]; ?>" href="<?php echo URL . 'admin/sekcje/'; ?>">Sekcje</a>
            </li>
            <li>
              <a class="<?php echo $this->active[15]; ?>" href="<?php echo URL . 'admin/klasy/'; ?>">Klasy</a>
            </li>
            <?php /*
        <li>
          <a class="<?php echo $this->active[7]; ?>" href="<?php echo URL.'admin/rodziny/'; ?>">Trener - Rodziny</a>
        </li>
*/ ?>
            <li>
              <a class="<?php echo $this->active[9]; ?>" href="<?php echo URL . 'admin/druzyny/'; ?>">Druzyny</a>
            </li>
            <li>
              <a class="<?php echo $this->active[11]; ?>" href="<?php echo URL . 'admin/uzytkownicy/'; ?>">Użytkownicy</a>
            </li>
            <li>
              <a class="<?php echo $this->active[12]; ?>" href="<?php echo URL . 'admin/dodaj/'; ?>">Dodaj</a>
            </li>
            <li>
              <a class="<?php echo $this->active[16]; ?>" href="<?php echo URL . 'admin/cms/'; ?>">Strony</a>
            </li>
            <li>
              <a class="<?php echo $this->active[17]; ?>" href="<?php echo URL . 'admin/sezon/'; ?>">Sezony</a>
            </li>
            <li>
              <a class="<?php echo $this->active[18]; ?>" href="<?php echo URL . 'admin/harmonogram/'; ?>">Harmonogram</a>
            </li>
          <?php elseif ($_SESSION['role'] == 'trener') : ?>
            <li>
              <a class="<?php echo $this->active[1]; ?>" href="<?php echo URL . 'admin/rodziny/'; ?>">Rodziny</a>
            </li>
            <li>
              <a class="<?php echo $this->active[2]; ?>" href="<?php echo URL . 'admin/raport/'; ?>">Raporty</a>
            </li>
            <li>
              <a class="<?php echo $this->active[3]; ?>" href="<?php echo URL . 'admin/druzyny/'; ?>">Druzyny</a>
            </li>
          <?php endif; ?>
        </ul>
      </div>
      <ul class="user-menu">
        <li><a class="profil <?php echo $this->active[0]; ?>" href="<?php echo URL . 'admin/user/'; ?>" title="Zarządzaj profilem"><span></span><?php echo $_SESSION['user_login']; ?></a></li>
        <li><a class="logout" href="<?php echo URL . 'admin/login/wyloguj'; ?>" title="Wyloguj"><span></span>Wyloguj</a></li>
      </ul>
    </div>
  </div>
  <div class="content-wrapper">