<div class="left">
    <h3>Dodaj szkołę</h3>
    <form action="" method="post">
        <div class="form-line">
            <label for="name">Nazwa</label>
            <input type="text" name="name" style="width: 100%;">
        </div>
        <div class="form-line">
            <label for="title">Tytul</label>
            <textarea rows="2" cols="2" style="min-width: 100%; width: 100%;" name="title"></textarea>
        </div>
        <div class="form-line">
            <button style="margin: 0 auto;display: table; cursor:pointer" type="submit" name="dodaj-school">
                Dodaj
            </button>
        </div>
    </form>
    <table width="100%" style="margin-top: 20px;" id="js-sezon-table_last" class="list-table">
        <thead>
            <tr>
                <th>Szkoły</th>
            </tr>
        </thead>

        <tbody id="tbody-schools">
            <?php foreach ($this->schools as $key => $val) : ?>
                <tr>
                    <td style="display: flex;flex-direction: column;">
                        <?php echo $val['names']; ?>
                        <span><?php echo $val['title']; ?></span>
                        <div>
                            <button class="removeSchool" data-id="<?php echo $val['id']; ?>" style="cursor:pointer; padding: 5px 15px !important;">remove</button>
                            <button class="editSchool" style="cursor:pointer; padding: 5px 15px !important;">edit</button>
                        </div>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
<div class="content">
    <h3>Lista sezonów </h3>
    <table width="100%" id="js-sezon-table_last" class="list-table">
        <thead>
            <tr>
                <th>Sezon nazwa</th>
                <th>Sezon rozpoczyna</th>
                <th>Sezon kończy</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($this->klasy as $key => $val) : ?>
                <tr>
                    <td><?php echo $val['sezon_nazwa']; ?></td>
                    <td><?php echo $val['sezon_start']; ?></td>
                    <td><?php echo $val['sezon_koniec']; ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>