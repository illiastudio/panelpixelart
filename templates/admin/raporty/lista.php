<div class="content-full">
  <h3>Generuj raporty</h3>

  <form method="post" action="">
    <table collapse class="raporty-table">
      <thead>
        <tr>
          <th>Nazwa</th>
          <th>Sezon</th>
          <th>Dodatkowe parametry</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      <?php if ($_SESSION['role'] == 'admin'): ?>
        <tr>
          <td>Zgłoszenia</td>
          <td>
            <select class="input" name="zgloszenia_sezon">
            <?php foreach ($this->sezony as $key => $val): ?>
              <?php
                if ($val['sezon_nazwa'] == SEZON) {
                  $sel = 'selected="selected"';
                } else {
                  $sel = '';
                }
              ?>
              <option value="<?php echo $val['sezon_nazwa']; ?>" <?php echo $sel; ?>><?php echo $val['sezon_nazwa']; ?></option>
            <?php endforeach ?>
            </select>
          </td>
          <td style="text-align: center;">Brak</td>
          <td><button class="button" name="zgloszenia_generuj">Generuj</button></td>
        </tr>

        <tr>
          <td>Należności</td>
          <td>
            <select class="input" name="platnosci_v2_sezon">
            <?php foreach ($this->sezony as $key => $val): ?>
              <?php
                if ($val['sezon_nazwa'] == SEZON) {
                  $sel = 'selected="selected"';
                } else {
                  $sel = '';
                }
              ?>
              <option value="<?php echo $val['sezon_nazwa']; ?>" <?php echo $sel; ?>><?php echo $val['sezon_nazwa']; ?></option>
            <?php endforeach ?>
            </select>
          </td>
          <td style="text-align: center;">Brak</td>
          <td><button class="button" name="platnosci_v2_generuj">Generuj</button></td>
        </tr>

        <tr>
          <td>Salda</td>
          <td>
            <select class="input" name="salda_sezon">
            <?php foreach ($this->sezony as $key => $val): ?>
              <?php
                if ($val['sezon_nazwa'] == SEZON) {
                  $sel = 'selected="selected"';
                } else {
                  $sel = '';
                }
              ?>
              <option value="<?php echo $val['sezon_nazwa']; ?>" <?php echo $sel; ?>><?php echo $val['sezon_nazwa']; ?></option>
            <?php endforeach ?>
            </select>
          </td>
          <td style="text-align: center;">Brak</td>
          <td><button class="button" name="salda_generuj">Generuj</button></td>
        </tr>
      <?php endif; ?>
        <tr>
          <td>Obecności</td>
          <td>
            <select class="input" name="obecnosci_sezon">
            <?php foreach ($this->sezony as $key => $val): ?>
              <?php
                if ($val['sezon_nazwa'] == SEZON) {
                  $sel = 'selected="selected"';
                } else {
                  $sel = '';
                }
              ?>
              <option value="<?php echo $val['sezon_nazwa']; ?>" <?php echo $sel; ?>><?php echo $val['sezon_nazwa']; ?></option>
            <?php endforeach ?>
            </select>
          </td>
          <td>
            <div>
              <select class="input" name="obecnosci_druzyna">
                <option value="">--- Wybierz drużynę ---</option>
              <?php foreach ($this->druzyny as $key => $val): ?>
                <option value="<?php echo $val['druzyna_id']; ?>"><?php echo $val['druzyna_nazwa'].' '.$val['sekcja_nazwa']; ?></option>
              <?php endforeach ?>
              </select>
            </div>
            <div>
              <select class="input" name="obecnosci_miesiac">
                <option value="">--- Wybierz miesiąc ---</option>
                <?php foreach ($this->miesiace as $key => $val): ?>
                  <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </td>
          <td><button class="button" name="obecnosci_generuj">Generuj</button></td>
        </tr>

        <tr>
          <td>Dzieci</td>
          <td>
            <select class="input" name="lista_dzieci_sezon">
            <?php foreach ($this->sezony as $key => $val): ?>
              <?php
                if ($val['sezon_nazwa'] == SEZON) {
                  $sel = 'selected="selected"';
                } else {
                  $sel = '';
                }
              ?>
              <option value="<?php echo $val['sezon_nazwa']; ?>" <?php echo $sel; ?>><?php echo $val['sezon_nazwa']; ?></option>
            <?php endforeach ?>
            </select>
          </td>
          <td>
            <div>
              <select class="input" name="lista_dzieci_sekcja">
                <option value="">--- Wybierz sekcję ---</option>
              <?php foreach ($this->sekcje as $key => $val): ?>
                <option value="<?php echo $val['sekcja_id']; ?>"><?php echo $val['sekcja_nazwa']; ?></option>
              <?php endforeach ?>
              </select>
            </div>
            <div>
              <select class="input" name="lista_dzieci_klasa">
                <option value="">--- Wybierz klasę ---</option>
                <?php foreach ($this->klasy as $key => $val): ?>
                  <option value="<?php echo $val['klasa_id']; ?>"><?php echo $val['klasa_nazwa']; ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </td>
          <td><button class="button" name="lista_dzieci_generuj">Generuj</button></td>
        </tr>

      </tbody>
    </table>
  </form>
</div>