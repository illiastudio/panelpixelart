<div class="content-full raporty">
  <h3>Należności za sezon: <span class="important"><?php echo $this->sezon; ?></span></h3>
  <form class="raport-eksport" action="" method="post">
    <button class="save-button-2" name="eksport"><span></span>Eksport do xls</button>
  </form>
  <table id="js-raport-naleznosci" class="list-table">
    <thead>
      <tr>
        <td>Id rodziny</td>
        <td>Id syna</td>
        <td>Nazwisko</td>
        <td>Imię syna</td>
        <td>Klasa</td>
        <td>Sekcja</td>
        <td width="6%">Składka wg taryfikatora</td>
        <td width="6%">Propozycja składki</td>
        <td>Data zgłoszenia</td>
        <td>Wrzesień</td>
        <td>Październik</td>
        <td>Listopad</td>
        <td>Grudzień</td>
        <td>Styczeń</td>
        <td>Luty</td>
        <td>Marzec</td>
        <td>Kwiecień</td>
        <td>Maj</td>
        <td>Czerwiec</td>
      </tr>
    </thead>
    <tbody>

    <?php
      $sumArr = array();

      $t = explode('-', $this->sezon);

      $okresy = array();
      $okresy[] = '20'.$t[0].'-09-01';
      $okresy[] = '20'.$t[0].'-10-01';
      $okresy[] = '20'.$t[0].'-11-01';
      $okresy[] = '20'.$t[0].'-12-01';
      $okresy[] = '20'.$t[1].'-01-01';
      $okresy[] = '20'.$t[1].'-02-01';
      $okresy[] = '20'.$t[1].'-03-01';
      $okresy[] = '20'.$t[1].'-04-01';
      $okresy[] = '20'.$t[1].'-05-01';
      $okresy[] = '20'.$t[1].'-06-01';

    ?>

    <?php foreach ($this->arr as $key => $val): ?>

      <?php foreach ($val['sekcje'] as $kSekcja => $vSekcja): ?>

      <tr>
        <td><?php echo $val['rodzina_id']; ?></td>
        <td><?php echo $key; ?></td>
        <td><?php echo $val['nazwisko']; ?></td>
        <td><?php echo $val['imie']; ?></td>
        <td><?php echo $val['klasa']; ?></td>
        <td><?php echo $kSekcja; ?></td>
        <td><?php echo $vSekcja['taryfikator']; ?></td>
        <td><?php echo $vSekcja['propozycja']; ?></td>
        <td><?php echo $vSekcja['data']; ?></td>

        <?php foreach ($okresy as $monthKey => $monthVal): ?>

        <td>
        <?php
            if (isset($vSekcja['platnosci'][$monthVal])) {
              $ile =  $vSekcja['platnosci'][$monthVal];
            } else {
              $ile = 0;
            }

            echo $ile;

            $sumArr[$monthKey] += $ile;
        ?>
        </td>

        <?php endforeach; ?>

      </tr>

      <?php endforeach; ?>
    <?php endforeach; ?>

    </tbody>
    <tfoot>
      <tr class="razem">
        <td colspan="9">RAZEM</td>

        <?php foreach ($sumArr as $k => $v): ?>
        <td><?php echo $v; ?></td>
        <?php endforeach ?>

      </tr>
    </tfoot>
  </table>

</div>