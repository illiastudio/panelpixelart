<div class="content-full raporty">
  <h3>
    Obecności za sezon: <span class="important"><?php echo $this->sezon; ?></span>
    Drużyna: <span class="important"><?php echo $this->druzyna; ?></span>
    Miesiąc: <span class="important"><?php echo $this->miesiac; ?></span>
  </h3>
  <form class="raport-eksport" action="" method="post">
    <button class="save-button-2" name="eksport"><span></span>Eksport do xls</button>
  </form>

  <?php $sumArr = array(); ?>

  <table id="js-raport-obecnosci" class="list-table">
    <thead>
      <tr>
        <td>Id rodziny</td>
        <td>Id syna</td>
        <td>Nazwisko</td>
        <td>Imię syna</td>
        <td>Klasa</td>
        <td>Sekcja</td>
        <td>Drużyna</td>

        <?php foreach ($this->arr['treningi'] as $key => $val): ?>
          <td class="nosort"><?php echo $val['dzien'].'-'.$val['miesiac']; ?></td>
          <?php $sumArr[$key]['bylo'] = 0; ?>
          <?php $sumArr[$key]['max'] = 0; ?>
        <?php endforeach ?>

      </tr>
    </thead>
    <tbody>

    <?php foreach ($this->arr['dzieci'] as $key => $val): ?>

      <tr>
        <td><?php echo $val['rodzina_id']; ?></td>
        <td><?php echo $val['dziecko_id']; ?></td>
        <td><?php echo $val['rodzina_nazwisko']; ?></td>
        <td><?php echo $val['dziecko_imie']; ?></td>
        <td><?php echo $val['klasa_nazwa']; ?></td>
        <td><?php echo $val['sekcja_nazwa']; ?></td>
        <td><?php echo $val['druzyna_nazwa']; ?></td>
        <?php foreach ($this->arr['treningi'] as $key2 => $val2): ?>
        <td>
          <?php
            $status = 0;
          ?>
          <?php foreach ($this->arr['obecnosci'] as $key3 => $val3): ?>
          <?php
            if($val3['obecnosc_dziecko'] == $val['dziecko_id']) {
              if ($val3['obecnosc_trening'] == $val2['trening_id']) {
                $status = intval($val3['obecnosc_status']);
              }
            }
          ?>

          <?php endforeach ?>
          <?php
            if ($status == 0) {
              echo 'b/d';
            } elseif ($status == 1) {
              echo 'ob.';
            } elseif ($status == 2) {
              echo 'nb';
              $status = 0;
            }


            $sumArr[$key2]['bylo'] += $status;
            $sumArr[$key2]['max'] += 1;
          ?>
        </td>
        <?php endforeach ?>
      </tr>

    <?php endforeach; ?>

    </tbody>
    <tfoot>
      <tr class="razem">
        <td colspan="7">RAZEM</td>

        <?php foreach ($sumArr as $k => $v): ?>
        <td><?php echo $v['bylo'].'/'.$v['max']; ?></td>
        <?php endforeach; ?>

      </tr>
    </tfoot>
  </table>

</div>