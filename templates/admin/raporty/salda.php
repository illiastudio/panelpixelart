<div class="content-full raporty">
  <h3>Salda za sezon: <span class="important"><?php echo $this->sezon; ?></span></h3>
  <form class="raport-eksport" action="" method="post">
    <button class="save-button-2" name="eksport"><span></span>Eksport do xls</button>
  </form>
  <table id="js-raport-salda" class="list-table">
    <thead>
      <tr>
        <td>Id rodziny</td>
        <td>Nazwisko</td>
        <td>Ojciec mail</td>
        <td>Ojciec telefon</td>
        <td>Matka mail</td>
        <td>Matka telefon</td>
        <td>Suma należności</td>
        <td>Suma wpłat</td>
        <td>Saldo</td>
        <td>Status salda</td>
        <td>Status salda sort</td>
      </tr>
    </thead>
    <tbody>

    <?php
      $razem = array(
        'naleznosci' => 0,
        'zaplacono' => 0,
        'bilans' => 0
      );
      $sum = 0;
    ?>

    <?php foreach ($this->arr as $key => $val): ?>

      <tr>
        <td><?php echo $val['rodzina_id']; ?></td>
        <td><?php echo $val['rodzina_nazwisko']; ?></td>
        <td><?php echo $val['ojciec_mail']; ?></td>
        <td><?php echo $val['ojciec_telefon']; ?></td>
        <td><?php echo $val['matka_mail']; ?></td>
        <td><?php echo $val['matka_telefon']; ?></td>
        <td>
        <?php
          echo $val['naleznosc'];
          $razem['naleznosci'] += $val['naleznosc'];
        ?>
        </td>
        <td>
        <?php
          echo $val['zaplacono'];
          $razem['zaplacono'] += $val['zaplacono'];
        ?>
        </td>
        <td>
        <?php
          echo $val['pb_bilans'];
          $razem['bilans'] += $val['pb_bilans'];
        ?>
        </td>
        <td>
        <?php
          switch ($val['status_sort']) {
            case 0:
              echo 'Niedopłata';
              break;
            case 1:
              echo 'Nadpłata';
              break;
            case 2:
              echo 'Saldo ok';
              break;
            case 3:
              echo 'Brak należności';
              break;
          }
        ?>
        </td>
        <td>
          <?php echo $val['status_sort']; ?>
        </td>
      </tr>

    <?php endforeach; ?>

    </tbody>

    <tfoot>
      <tr class="razem">
        <td colspan="6">RAZEM</td>
        <td><?php echo $razem['naleznosci']; ?></td>
        <td><?php echo $razem['zaplacono']; ?></td>
        <td><?php echo $razem['bilans']; ?></td>
        <td></td>
      </tr>
    </tfoot>

  </table>

</div>