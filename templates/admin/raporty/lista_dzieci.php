<div class="content-full raporty">
  <h3>
    Lista dzieci: <span class="important"><?php echo $this->sezon; ?></span>
    <?php if (!empty($this->sekcja)): ?>
    Sekcja: <span class="important"><?php echo $this->sekcja[0]['sekcja_nazwa']; ?></span>
    <?php endif; ?>
    <?php if (!empty($this->klasa)): ?>
    Klasa: <span class="important"><?php echo $this->klasa[0]['klasa_nazwa']; ?></span>
    <?php endif; ?>
  </h3>
  <form class="raport-eksport" action="" method="post">
    <button class="save-button-2" name="eksport"><span></span>Eksport do xls</button>
  </form>

  <table id="js-rodziny-list-table" class="list-table" width="100%">
    <thead>
      <tr>
        <th width="5%">Id rodziny</th>
        <th width="5%">Id syna</th>
        <th>Nazwisko</th>
        <th>Imię syna</th>
        <th>Klasa</th>
        <th>Sekcja</th>
        <th>Ojciec mail</th>
        <th>Ojciec telefon</th>
        <th>Matka mail</th>
        <th>Matka telefon</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($this->lista as $key => $val): ?>
      <tr>
        <td><?php echo $val['rodzina_id']; ?></td>
        <td><?php echo $val['dziecko_id']; ?></td>
        <td><?php echo $val['rodzina_nazwisko']; ?></td>
        <td><?php echo $val['dziecko_imie']; ?></td>
        <td><?php echo $val['klasa_nazwa']; ?></td>
        <td><?php echo $val['sekcja_nazwa']; ?></td>
        <td><?php echo $val['ojciec_mail']; ?></td>
        <td><?php echo $val['ojciec_telefon']; ?></td>
        <td><?php echo $val['matka_mail']; ?></td>
        <td><?php echo $val['matka_telefon']; ?></td>
        <td>
        <?php
          switch ($val['zgloszenie_status']) {
            case '1':
              echo 'Aktualne';
              break;
            case '2':
            case '3':
            case '0':
              echo 'Nieaktualne';
              break;
            default:
              echo '';
              break;
          }
        ?>
        </td>
      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
</div>