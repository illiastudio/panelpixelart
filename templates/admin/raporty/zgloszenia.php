<div class="content-full raporty">
  <h3>Zgłoszenia za sezon: <span class="important"><?php echo $this->sezon; ?></span></h3>
  <form class="raport-eksport" action="" method="post">
    <button class="save-button-2" name="eksport"><span></span>Eksport do xls</button>
  </form>
  <table id="js-raport-zgloszenia" class="list-table">
    <thead>
      <tr>
        <th>Id rodziny</th>
        <th>Id syna</th>
        <th>Nazwisko</th>
        <th>Imię syna</th>
        <th>Klasa</th>
        <th>Sekcja</th>
        <th>Składka wg taryfikatora</th>
        <th>Propozycja składki</th>
        <th>Data zgłoszenia</th>
        <th>Data zgłoszenia sort</th>
        <th>Status</th>
        <th width="10%">Uwagi</th>
        <th>Status odpowiedzi</th>
        <th>Status wysyłki</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($this->zgloszenia as $key => $val): ?>
    <?php
      if ($val['zgloszenie_status'] == '0') {
        $val['zgloszenie_status'] = 'nieaktualne';
      }

      if ($val['zgloszenie_status'] == '1') {
        $val['zgloszenie_status'] = 'aktualne';
      }

      if ($val['zgloszenie_status'] == '2') {
        $val['zgloszenie_status'] = 'rezygnacja';
      }

      if ($val['zgloszenie_status'] == '3') {
        $val['zgloszenie_status'] = 'anulowane';
      }

      if ($val['potwierdzone'] == '1') {
        $val['potwierdzone'] = 'zaakceptowane';
      }

      if ($val['potwierdzone'] == '2') {
        $val['potwierdzone'] = 'odrzucone';
      }

      if ($val['wyslane'] == '0') {
        $val['wyslane'] = 'niewyslane';
      }

      if ($val['wyslane'] == '1') {
        $val['wyslane'] = 'wyslane';
      }
    ?>

      <tr>
        <td><?php echo $val['rodzina_id']; ?></td>
        <td><?php echo $val['dziecko_id']; ?></td>
        <td><?php echo $val['rodzina_nazwisko']; ?></td>
        <td><?php echo $val['dziecko_imie']; ?></td>
        <td><?php echo $val['klasa_nazwa']; ?></td>
        <td><?php echo $val['sekcja_nazwa']; ?></td>
        <td><?php echo $val['zgloszenie_skladka']; ?></td>
        <td><?php echo $val['zgloszenie_propozycja']; ?></td>
        <td><?php echo $val['zgloszenie_date_add']; ?></td>
        <td><?php echo $val['data_sort']; ?></td>
        <td><?php echo $val['zgloszenie_status']; ?></td>
        <td><?php echo $val['uwagi_tresc']; ?></td>
        <td><?php echo $val['potwierdzone']; ?></td>
        <td><?php echo $val['wyslane']; ?></td>
      </tr>

    <?php endforeach; ?>

    </tbody>
  </table>

</div>