<div class="content-full">
  <form class="import-form" action="" method="post" enctype="multipart/form-data">
    <input type="file" name="plik"><br>
    <span style="font-size: 1.4em;">Zgłoszenia z sezonu: </span>
    <select name="sezon">
    <?php foreach ($this->sezony as $key => $value): ?>
      <?php
        if ($value['sezon_nazwa'] == SEZON) {
          $sel = 'selected="selected"';
        } else {
          $sel = '';
        }
      ?>
      <option value="<?php echo $value['sezon_nazwa']; ?>" <?php echo $sel; ?>>
        <?php echo $value['sezon_nazwa']; ?>
      </option>
    <?php endforeach; ?>
    </select><br>
    <input type="submit" name="import" value="Importuj">
  </form>
</div>