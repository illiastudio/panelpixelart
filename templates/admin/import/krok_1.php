<div class="content-full">
  <h3>Import zgłoszeń z pliku: <span class="important"><?php echo $_SESSION['zgloszenia_filename']; ?></span></h3>
  <h4>Krok 1: Sprawdź dane rodziców</h4>

  <div class="rekordy-info">
  <?php if ($this->rekord > 1): ?>
    <a class="prev prev-button" href="<?php echo URL.'admin/import/krok/1/rekord/'.($this->rekord - 1); ?>"><span></span>Poprzednie</a>
  <?php else: ?>
    <span class="prev prev-button disabled"><span></span>Poprzednie</span>
  <?php endif ?>
  <span class="now">Rekord: <?php echo $this->rekord; ?> z <?php echo $this->ile; ?></span>
  <?php if ($this->rekord < $this->ile): ?>
    <a class="next next-button" href="<?php echo URL.'admin/import/krok/1/rekord/'.($this->rekord + 1); ?>"><span></span>Następne</a>
  <?php else: ?>
    <span class="next next-button disabled"><span></span>Następne</span>
  <?php endif ?>
  </div>

  <table cellpadding="0" width="100%" class="import-krok">
    <thead>
      <tr>
        <th>Id: <?php echo $this->arr[$this->rekord]['id_rodziny']; ?></td>
        <th colspan="2"><?php echo $this->arr[$this->rekord]['nazwisko']; ?></td>
      </tr>
    </thead>
    <tbody>
      <tr class="head-row">
        <td width="14%" ></td>
        <td width="43%">MAMA</td>
        <td width="43%">TATA</td>
      </tr>
      <tr class="row">
        <td>E-MAIL</td>
        <td>
          <span><?php echo $this->arr[$this->rekord]['mail_matki']; ?></span>
        </td>
        <td>
          <span><?php echo $this->arr[$this->rekord]['mail_ojca']; ?></span>
        </td>
      </tr>
      <tr class="row">
        <td>TELEFON</td>
        <td>
          <span><?php echo $this->arr[$this->rekord]['telefon_matki']; ?></span>
        </td>
        <td>
          <span><?php echo $this->arr[$this->rekord]['telefon_ojca']; ?></span>
        </td>
      </tr>
    </tbody>
  </table>

  <div class="submit-buttons">
    <form method="post" action="">
      <input type="hidden" name="rekord" value="<?php echo $this->rekord; ?>">
      <input type="hidden" name="rodzina" value="<?php echo $this->arr[$this->rekord]['id_rodziny']; ?>">
      <button class="no-button" type="submit" name="przerwij"><span></span>Przerwij procedurę importu</button>
      <button class="save-button" type="submit" name="dodaj_nowa_rodzine"><span></span>Wpisuję nową rodzinę</button>
      <button class="add-button" type="submit" name="przejdz_dalej"><span></span>Chcę dopisać do tej rodziny</button>
    </form>
  </div>

  <div style="margin: 25px 0;">
    <h4>Sprawdź czy dana rodzina już jest w bazie</h4>

    <table id="js-rodziny-table" class="list-table" cellpadding="0" width="100%">
      <thead>
        <th width="10%">Id rodziny</th>
        <th width="30%">Nazwisko</th>
        <th width="15%">Mail matki</th>
        <th width="15%">Tel. matki</th>
        <th width="15%">Mail ojca</th>
        <th width="15%">Tel. ojca</th>
      </thead>
      <tbody>
      <?php foreach ($this->rodziny as $key => $val): ?>
        <tr>
          <td><?php echo $val['rodzina_id']; ?></td>
          <td><?php echo $val['rodzina_nazwisko']; ?></td>
          <td><?php echo $val['matka_mail']; ?></td>
          <td><?php echo $val['matka_telefon']; ?></td>
          <td><?php echo $val['ojciec_mail']; ?></td>
          <td><?php echo $val['ojciec_telefon']; ?></td>
        </tr>
      <?php endforeach ?>
      </tbody>
    </table>

  </div>
</div>