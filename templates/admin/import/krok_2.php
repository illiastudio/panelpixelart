<?php
  $arr = $this->arr;
  $rekord = $this->rekord;
?>

  <div class="content-full">
    <h3>Import zgłoszeń z pliku: <span class="important"><?php echo $_SESSION['zgloszenia_filename']; ?></span></h3>
    <h4>Krok 2: Dopisz dziecko</h4>

    <table id="zgloszenia" cellpadding="0" width="100%" class="import-krok">
      <thead>
        <tr>
          <th colspan="3">Nazwisko</th>
          <th>Dziecko Id</th>
        </tr>
      </thead>
      <tbody>
        <tr class="head-row">
          <td colspan="3"><?php echo $arr[$rekord]['nazwisko']; ?></td>
          <td><?php echo $arr[$rekord]['id_dziecka']; ?></td>
        </tr>
        <tr class="head-second">
          <th>Imię syna</th>
          <th>Klasa</th>
          <th>Sekcja</th>
          <th>Kwota</th>
        </tr>
        <?php if (isset($arr[$rekord]['zgloszenia'])): ?>
        <?php foreach ($arr[$rekord]['zgloszenia'] as $key => $val): ?>
        <tr class="row">
          <?php
            switch ($key) {
              case 'pilka_nozna':
                $sekcja = 'Piłka nożna';
                break;
              case 'judo':
                $sekcja = 'Judo';
                break;
              case 'szachy':
                $sekcja = 'Szachy';
                break;
              case 'plywanie':
                $sekcja = 'Pływanie';
                break;
              case 'szermierka':
                $sekcja = 'Szermierka';
                break;
            }
          ?>
          <td><?php echo $arr[$rekord]['imie']; ?></td>
          <td><?php echo $arr[$rekord]['klasa']; ?></td>
          <td><?php echo $sekcja; ?></td>
          <td><?php echo $val; ?></td>
        </tr>
        <?php endforeach ?>
        <?php else: ?>
        <tr class="row">
          <td colspan="4">Brak zgłoszeń do zaimportowania</td>
        </tr>
        <?php endif; ?>
      </tbody>
    </table>

    <div class="submit-buttons">
      <form method="post" action="">
        <input type="hidden" name="rekord" value="<?php echo $rekord; ?>">
        <input type="hidden" name="rodzina" value="<?php echo $arr[$rekord]['id_rodziny']; ?>">
        <button class="no-button" type="submit" name="przerwij"><span></span>Przerwij procedurę importu</button>
        <button class="add-button" type="submit" name="dodaj_syna"><span></span>Chcę dopisać syna</button>
        <button class="save-button" type="submit" name="importuj"><span></span>Chcę zaimportować nowe zgłoszenie</button>
        <button class="save-button-2" type="submit" name="importuj_do_istniejacego"><span></span>Chcę dodać zgłoszenie do istniejącego</button>
        <button class="next-button" type="submit" name="przejdz_dalej"><span></span>Przejdź do następnego rekordu z importu</button>
      </form>
    </div>

    <div style="margin: 50px 0">
      <h4>Sprawdź czy dziecko jest już w bazie</h4>

      <table cellpading="0" width="100%" class="import-krok">
        <thead>
          <th width="15%">Dziecko Id</th>
          <th>Imię syna</th>
          <th>Klasa</th>
        </thead>
        <tbody>
        <?php if (!empty($this->dzieci)): ?>
        <?php foreach ($this->dzieci as $key => $val): ?>
          <tr class="row">
            <td><?php echo $val['dziecko_id']; ?></td>
            <td><?php echo $val['dziecko_imie']; ?></td>
            <td><?php echo $val['klasa_nazwa']; ?></td>
          </tr>
        <?php endforeach ?>
        <?php else: ?>
          <tr class="row">
            <td colspan="3">Brak dzieci w bazie danych</td>
          </tr>
        <?php endif ?>
        </tbody>
      </table>
    </div>

    <div style="margin: 50px 0;">
      <h4>Sprawdź czy zgłoszenie już jest w bazie</h4>

      <table cellpadding="0" width="100%" class="import-krok">
        <thead>
          <tr>
            <th>Imię i Nazwisko syna</th>
            <th>Klasa</th>
            <th>Sekcja</th>
            <th>Kwota</th>
            <th>Propozycja</th>
          </tr>
        </thead>
        <tbody>
        <?php if (!empty($this->zgloszeniaArr)): ?>
        <?php foreach ($this->zgloszeniaArr as $key => $value): ?>
          <?php if (count($value['zgloszenia']) > 0): ?>
          <?php
          $razem = 0;
          $propozycja = 0;

          foreach ($value['zgloszenia'] as $key2 => $value2): ?>
            <tr class="row" id="<?php echo $value2['zgloszenie_id']; ?>">
              <td><?php echo $value2['dziecko_imie']; ?></td>
              <td><?php echo $value2['klasa']; ?></td>
              <td><?php echo $value2['sekcja_nazwa']; ?></td>
              <td><?php echo $value2['zgloszenie_skladka']; ?></td>
              <td><?php echo $value2['zgloszenie_propozycja']; ?></td>
            </tr>
          <?php endforeach ?>
          <?php endif; ?>
        <?php endforeach; ?>
        <?php else: ?>
          <tr class="row">
            <td colspan="5">Brak istniejących zgłoszeń w bazie danych</td>
          </tr>
        <?php endif ?>
        </tbody>
      </table>
    </div>
  </div>