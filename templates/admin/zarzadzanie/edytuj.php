<div class="content-full">
  <h3>Zgłoszenia rodziny: <span class="important"><?php echo $this->rodzina[0]['rodzina_nazwisko']; ?> (id: <?php echo $this->rodzina[0]['rodzina_id']; ?>)</span></h3>

  <?php if ($this->issetNew): ?>
    <h3 class="important center">Uwaga: Istnieje zgłoszenie które czeka na akceptacje!</h3>
  <?php endif ?>

  <div class="zarzadzaj-menu">
    <ul>
      <li>
        <a href="<?php echo URL; ?>admin/salda/edytuj/<?php echo $this->rodzina[0]['rodzina_id']; ?>">Saldo</a>
      </li>
      <li>
        <a href="<?php echo URL; ?>admin/zarzadzanie/zarzadzaj/<?php echo $this->rodzina[0]['rodzina_id']; ?>">Zarządzaj</a>
      </li>
      <li>
        <span>Zgłoszenia</span>
      </li>
    </ul>
  </div>

  <div class="sezon-buttons">
    <ul>
    <?php foreach ($this->sezony as $sezon): ?>
      <?php
        if ($this->thisSezon == $sezon['sezon_nazwa']) {
          $class = 'class="active"';
        } else {
          $class = '';
        }
      ?>

      <li>
        <a <?php echo $class; ?> href="<?php echo URL.'admin/zarzadzanie/zgloszenia/'.$this->rodzinaId.'/'.$sezon['sezon_nazwa']; ?>"><?php echo $sezon['sezon_nazwa']; ?></a>
      </li>
    <?php endforeach; ?>
    </ul>
  </div>

  <?php if (!empty($this->zgloszenia)): ?>

  <form id="js-form" action="" method="post">

  <?php $i = 0; ?>
  <?php foreach ($this->zgloszenia as $key => $value): ?>

    <table cellpadding="0" width="100%"  class="zgloszenia">
      <thead>
        <tr>
          <th>Imię</th>
          <th width="10%">Klasa</th>
          <th width="10%">Sekcja</th>
          <th width="10%">Składka</th>
          <th width="15%">Propozycja</th>
          <th width="15%">Obowiązuje od</th>
          <th width="15%">Data końca</th>
          <th width="3%">Aktualne</th>
          <th width="3%">Anuluj</th>
        </tr>
      </thead>
      <tbody>
      <?php
        $razem = 0;
        $propozycja = 0;
      ?>
      <?php foreach ($value as $key => $val): ?>
        <tr class="row">
          <td><?php echo $val['dziecko_imie']; ?></td>
          <td><?php echo $val['klasa_nazwa']; ?></td>
          <td><?php echo $val['sekcja_nazwa']; ?></td>
          <td>
            <?php echo $val['skladka']; ?>
            <?php $razem += $val['skladka']; ?>
          </td>
          <td>
            <input class="input" type="text" name="zgloszenie_propozycja[<?php echo $val['zgloszenie_id']; ?>]" value="<?php echo $val['zgloszenie_propozycja']; ?>">
            <?php $propozycja += $val['zgloszenie_propozycja']; ?>
          </td>
          <td>
            <input class="input js-date-start" readonly="readonly" type="text" name="zgloszenie_date[<?php echo $val['zgloszenie_id']; ?>]" value="<?php echo $val['zgloszenie_date']; ?>" data-id="<?php echo $val['zgloszenie_id']; ?>">
          </td>
          <td>
            <?php
              if ($val['zgloszenie_date_end'] != '0000-00-00') {
                $dEnd = $val['zgloszenie_date_end'];
              } else {
                $dEnd = '';
              }
            ?>

            <input class="input js-date-end" readonly="readonly" type="text" name="zgloszenie_date_end[<?php echo $val['zgloszenie_id']; ?>]" value="<?php echo $dEnd; ?>" data-id="<?php echo $val['zgloszenie_id']; ?>">
          </td>
          <td>
            <input type="hidden" name="aktualne[<?php echo $val['zgloszenie_id']; ?>]" value="0">
            <?php if ($val['zgloszenie_status'] == '1'): ?>
              <input type="checkbox" name="aktualne[<?php echo $val['zgloszenie_id']; ?>]" value="1" checked>
            <?php else: ?>
              <input type="checkbox" name="aktualne[<?php echo $val['zgloszenie_id']; ?>]" value="1">
            <?php endif; ?>
          </td>
          <td>
            <input type="hidden" name="anuluj[<?php echo $val['zgloszenie_id']; ?>]" value="0">
            <?php if ($val['zgloszenie_status'] == '3'): ?>
              <input type="checkbox" name="anuluj[<?php echo $val['zgloszenie_id']; ?>]" value="1" checked>
            <?php else: ?>
              <input type="checkbox" name="anuluj[<?php echo $val['zgloszenie_id']; ?>]" value="1">
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach ?>
        <tr class="razem">
          <td colspan="3">RAZEM:</td>
          <td><?php echo $razem; ?> zł</td>
          <td><?php echo $propozycja; ?> zł</td>
          <td colspan="4"></td>
        </tr>
      </tbody>
    </table>

    <hr>

  <?php $i++; ?>
  <?php endforeach; ?>

    <div class="submit-buttons">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
    </div>

  </form>
  <?php else: ?>

  <p class="no-result-txt">Brak zgłoszeń do wyświetlenia</p>

  <?php endif; ?>
</div>