<div class="content-full">
  <div class="title-container">
    <h3>Lista rodzin</h3>
    <a id="js-add-family" class="button add-button" href="<?php echo URL; ?>admin/zarzadzanie/dodajrodzine/"><span></span>Dodaj rodzine</a>
  </div>
  <table id="js-rodziny-table" class="list-table" width="100%">
    <thead>
      <tr>
        <th width="5%">Id</th>
        <th>Nazwisko</th>
        <th>Ojciec mail</th>
        <th>Ojciec telefon</th>
        <th>Matka mail</th>
        <th>Matka telefon</th>
        <th width="10%">Status płatności</th>
        <th width="5%">Status sort</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach ($this->rodzina as $key => $val) : ?>
        <tr>
          <td><?php echo $val['rodzina_id']; ?></td>
          <td><?php echo $val['rodzina_nazwisko']; ?></td>
          <td><?php echo $val['namedad']; ?></td>
          <td><?php echo $val['teldad']; ?></td>
          <td><?php echo $val['namemom']; ?></td>
          <td><?php echo $val['telmom']; ?></td>
        </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</div>