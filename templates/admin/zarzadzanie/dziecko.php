<div class="edit-box">
  <h3>Edycja: <?php echo $this->dziecko[0]['dziecko_imie']; ?> <?php echo $this->rodzina[0]['rodzina_nazwisko']; ?></h3>

  <form method="post" action="<?php echo URL; ?>admin/zarzadzanie/edytujdziecko/<?php echo $this->dziecko[0]['dziecko_id']; ?>">
    <div class="form-line">
      <label class="form-label" for="dziecko_imie">Imię</label>
      <input class="input" type="text" name="dziecko_imie" value="<?php echo $this->dziecko[0]['dziecko_imie']; ?>">
    </div>

    <div class="form-line">
      <label for="dziecko_klasa" class="form-label">Klasa</label>
      <select name="dziecko_klasa">
      <?php foreach ($this->klasy as $key => $val): ?>
        <?php
          if ($val['klasa_id'] == $this->dziecko[0]['dziecko_klasa']) {
            $select = 'selected="selected"';
          } else {
            $select = '';
          }
        ?>

        <option value="<?php echo $val['klasa_id']; ?>" <?php echo $select; ?>><?php echo $val['klasa_nazwa']; ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-line">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
    </div>
  </form>
</div>