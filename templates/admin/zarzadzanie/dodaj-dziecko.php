<div class="edit-box">
  <h3>Dodaj dziecko do rodziny</h3>

  <form method="post" action="<?php echo URL; ?>admin/zarzadzanie/dodajdziecko/<?php echo $this->rodzinaId; ?>">
    <div class="form-line">
      <label class="form-label" for="dziecko_imie">Imię</label>
      <input class="input" type="text" name="dziecko_imie">
    </div>

    <div class="form-line">
      <label for="dziecko_klasa" class="form-label">Klasa</label>
      <select name="dziecko_klasa">
      <?php foreach ($this->klasy as $key => $val): ?>
        <option value="<?php echo $val['klasa_id']; ?>"><?php echo $val['klasa_nazwa']; ?></option>
      <?php endforeach ?>
      </select>
    </div>

    <div class="form-line">
      <button class="add-button" type="submit" name="dodaj"><span></span>Dodaj</button>
    </div>
  </form>
</div>