<div class="content-full">
  <h3>Zarządzaj rodziną: <span class="important"><?php echo $this->rodzina[0]['rodzina_nazwisko']; ?> (id: <?php echo $this->rodzina[0]['rodzina_id']; ?>)</span></h3>

  <div class="zarzadzaj-menu">
    <ul>
      <li>
        <a href="<?php echo URL; ?>admin/salda/edytuj/<?php echo $this->rodzina[0]['rodzina_id']; ?>">Saldo</a>
      </li>
      <li>
        <span>Zarządzaj</span>
      </li>
      <li>
        <a href="<?php echo URL; ?>admin/zarzadzanie/zgloszenia/<?php echo $this->rodzina[0]['rodzina_id']; ?>">Zgłoszenia</a>
      </li>
    </ul>
  </div>

  <h3>Opiekunowie</h3>

  <?php if (!empty($this->opiekunowie)): ?>

  <table class="list-table" width="100%" cellpadding="0">
    <thead>
      <tr>
        <th>Id</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Mail</th>
        <th>Telefon</th>
        <th width="80px">Opcje</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($this->opiekunowie as $key => $val): ?>
      <?php
        if ($key % 2 == 0) {
          $class = 'even';
        } else {
          $class = 'odd';
        }
      ?>
      <tr class="<?php echo $class; ?>">
        <td><?php echo $val['opiekun_id']; ?></td>
        <td><?php echo $val['opiekun_imie']; ?></td>
        <td><?php echo $this->rodzina[0]['rodzina_nazwisko']; ?></td>
        <td><?php echo $val['opiekun_mail']; ?></td>
        <td><?php echo $val['opiekun_telefon']; ?></td>
        <td>
          <div class="menu-buttons">
            <a class="edit js-edit-opiekun" title="Edytuj opiekuna" href="<?php echo URL; ?>admin/zarzadzanie/edytujopiekuna/<?php echo $val['opiekun_id']; ?>"><span class="none">Edytuj rodzica</span></a>
            <a class="password js-new-password" title="Zmień hasło" href="<?php echo URL; ?>admin/zarzadzanie/opiekunhaslo/<?php echo $val['opiekun_id']; ?>"><span class="none">Zmień hasło</span></a>
          </div>
        </td>
      </tr>
    <?php endforeach ?>
    </tbody>
  </table>

  <?php endif; ?>

  <div class="title-container">
    <h3>Dzieci</h3>
    <a id="js-add-child" class="button add-button" href="<?php echo URL; ?>admin/zarzadzanie/dodajdziecko/<?php echo $this->rodzina[0]['rodzina_id']; ?>"><span></span>Dodaj dziecko</a>
  </div>

  <?php if (!empty($this->dzieci)): ?>

  <table class="list-table" width="100%" cellpadding="0">
    <thead>
      <tr>
        <th>Id</th>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Klasa</th>
        <th width="80px">Opcje</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($this->dzieci as $key => $val): ?>
      <?php
        if ($key % 2 == 0) {
          $class = 'even';
        } else {
          $class = 'odd';
        }
      ?>
      <tr class="<?php echo $class; ?>">
        <td><?php echo $val['dziecko_id']; ?></td>
        <td><?php echo $val['dziecko_imie']; ?></td>
        <td><?php echo $this->rodzina[0]['rodzina_nazwisko']; ?></td>
        <td><?php echo $val['klasa_nazwa']; ?></td>
        <td>
          <div class="menu-buttons">
            <a class="edit js-edit-dziecko" href="<?php echo URL; ?>admin/zarzadzanie/edytujdziecko/<?php echo $val['dziecko_id']; ?>"><span class="none">Edytuj dziecko</span></a>
          </div>
        </td>
      </tr>
    <?php endforeach ?>
    </tbody>
  </table>
  <?php endif ?>
</div>