<div class="edit-box">
  <h3>Nowe hasło dla: <?php echo $this->opiekun[0]['opiekun_imie']; ?> <?php echo $this->rodzina[0]['rodzina_nazwisko']; ?></h3>

  <form method="post" action="<?php echo URL; ?>admin/zarzadzanie/opiekunhaslo/<?php echo $this->opiekun[0]['opiekun_id']; ?>">

    <div class="form-line">
      <label class="form-label" for="opiekun_haslo">Nowe hasło</label>
      <input class="input" type="text" name="opiekun_haslo" value="">
    </div>

    <div class="form-line">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zmień i wyślij hasło</button>
    </div>

    <div class="form-line">
      <button class="save-button-2" type="submit" name="generuj"><span></span>Wygeneruj i wyślij hasło</button>
    </div>

  </form>

</div>