<div class="edit-box">
  <h3>Dodaj rodzine</h3>

  <form method="post" action="<?php echo URL; ?>admin/zarzadzanie/dodajrodzine/">
    <div class="form-line">
      <label class="form-label" for="nazwisko">Nazwisko</label>
      <input class="input" type="text" name="nazwisko">
    </div>

    <div class="form-line">
      <label class="form-label" for="matka_imie">Imię matki</label>
      <input class="input" type="text" name="matka_imie">
    </div>
    <div class="form-line">
      <label class="form-label" for="matka_mail">Mail matki</label>
      <input class="input" type="text" name="matka_mail">
    </div>
    <div class="form-line">
      <label class="form-label" for="matka_telefon">Telefon matki</label>
      <input class="input" type="text" name="matka_telefon">
    </div>

    <div class="form-line">
      <label class="form-label" for="ojciec_imie">Imię ojca</label>
      <input class="input" type="text" name="ojciec_imie">
    </div>
    <div class="form-line">
      <label class="form-label" for="ojciec_mail">Mail ojca</label>
      <input class="input" type="text" name="ojciec_mail">
    </div>
    <div class="form-line">
      <label class="form-label" for="ojciec_telefon">Telefon ojca</label>
      <input class="input" type="text" name="ojciec_telefon">
    </div>


    <div class="form-line">
      <button class="add-button" type="submit" name="dodaj"><span></span>Dodaj</button>
    </div>
  </form>
</div>