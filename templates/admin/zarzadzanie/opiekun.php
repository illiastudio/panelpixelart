<div class="edit-box">
  <h3>Edycja: <?php echo $this->opiekun[0]['opiekun_imie']; ?> <?php echo $this->rodzina[0]['rodzina_nazwisko']; ?></h3>

  <form method="post" action="<?php echo URL; ?>admin/zarzadzanie/edytujopiekuna/<?php echo $this->opiekun[0]['opiekun_id']; ?>">
    <div class="form-line">
      <label class="form-label" for="opiekun_imie">Imię</label>
      <input class="input" type="text" name="opiekun_imie" value="<?php echo $this->opiekun[0]['opiekun_imie']; ?>">
    </div>

    <div class="form-line">
      <label class="form-label" for="opiekun_mail">Mail</label>
      <input class="input" type="text" name="opiekun_mail" value="<?php echo $this->opiekun[0]['opiekun_mail']; ?>">
    </div>

    <div class="form-line">
      <label class="form-label" for="opiekun_telefon">Telefon</label>
      <input class="input" type="text" name="opiekun_telefon" value="<?php echo $this->opiekun[0]['opiekun_telefon']; ?>">
    </div>

    <div class="form-line">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
    </div>
  </form>
</div>