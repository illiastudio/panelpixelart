<div class="rodzina-wrapper">
  <h3>Przeglądaj saldo rodziny: <span class="important"><?php echo $this->rodzina['rodzina_nazwisko'].' (id: '.$this->rodzina['rodzina_id'].')'; ?></span></h3>

  <div class="zarzadzaj-menu">
    <ul>
      <li>
        <span>Saldo</span>
      </li>
      <li>
        <a href="<?php echo URL; ?>admin/zarzadzanie/zarzadzaj/<?php echo $this->rodzina['rodzina_id']; ?>">Zarządzaj</a>
      </li>
      <li>
        <a href="<?php echo URL; ?>admin/zarzadzanie/zgloszenia/<?php echo $this->rodzina['rodzina_id']; ?>">Zgłoszenia</a>
      </li>
    </ul>
  </div>

  <div class="sezon-buttons">
    <ul>
    <?php foreach ($this->sezony as $sezon): ?>
      <?php
        if ($this->thisSezon == $sezon['sezon_nazwa']) {
          $class = 'class="active"';
        } else {
          $class = '';
        }
      ?>

      <li>
        <a <?php echo $class; ?> href="<?php echo URL.'admin/salda/edytuj/'.$this->rodzinaId.'/'.$sezon['sezon_nazwa']; ?>"><?php echo $sezon['sezon_nazwa']; ?></a>
      </li>
    <?php endforeach; ?>
    </ul>
  </div>

  <div>
    <div class="header-plus">
      <h3>Saldo za sezon: <?php echo $this->thisSezon; ?></h3>
      <button id="js-send-saldo" type="submit" name="wyslij" class="button"><span></span>Wyślij wiadomość o saldzie</button>
    </div>
    <form id="js-send-saldo-form" action="" method="post">
      <input type="hidden" name="rodzina_id" value="<?php echo $this->rodzinaId; ?>">
      <input type="hidden" name="status" value="<?php echo $this->status; ?>">
    </form>
    <table class="platnosci" width="100%" cellpadding="0">
      <thead>
        <tr>
          <th>Rok</th>
          <th>Miesiąc</th>
          <th>Do zapłaty</th>
        </tr>
      </thead>
      <tbody>
      <?php if (!empty($this->saldo) || !empty($this->poprzednieSezony)): ?>
      <?php $suma = 0; ?>

      <?php if (!empty($this->saldo)): ?>
        <?php foreach ($this->saldo as $key => $val): ?>
          <tr class="row">
            <td><?php echo $val['ps_rok']; ?></td>
            <td><?php echo $this->months[$val['ps_miesiac']]; ?></td>
            <td><?php echo $val['ps_do_zaplaty']; ?></td>
            <?php $suma += $val['ps_do_zaplaty']; ?>
          </tr>
        <?php endforeach ?>
      <?php endif; ?>

<?php /*
      <?php if (!empty($this->poprzednieSezony)): ?>
        <?php foreach ($this->poprzednieSezony as $key2 => $val2): ?>
          <tr class="row">
            <td colspan="2">Saldo z sezono <?php echo $key2; ?></td>
            <td>
            <?php
              if ($val2 < 0) {
                echo $val2 * -1;
              } elseif ($val2 > 0) {
                echo '-'.$val2;
              } else {
                echo $val2;
              }
            ?>
            </td>
            <?php $suma -= $val2; ?>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
*/ ?>

        <tr class="razem <?php echo $this->statusClass; ?>">
          <td colspan="2">Saldo</td>
          <td>
          <?php
          $saldo = $this->zaplacono - $suma;

          if ($saldo > 0) {
            echo '+';
          }

          echo $saldo;
          ?>
          </td>
        </tr>
        <tr class="razem">
          <td colspan="2">Rozliczone na dzień:</td>
          <?php if ($this->last != 0): ?>
          <td><?php echo $this->last; ?></td>
          <?php else: ?>
          <td>Brak płatności</td>
          <?php endif ?>
        </tr>
      <?php else: ?>
        <tr class="row">
          <td colspan="5">Brak należności</td>
        </tr>
      <?php endif ?>
      </tbody>
    </table>
  </div>

  <?php if (!empty($this->poprzednieSezony)): ?>
  <div>
    <h3>Saldo z uwzględnieniem poprzednich sezonów</h3>

    <table class="platnosci" width="100%" cellpadding="0">
      <thead>
        <tr>
          <th>Sezon</th>
          <th>Saldo</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>

      <?php $sumaPoprzednie = 0; ?>
      <?php foreach ($this->poprzednieSezony as $key => $val): ?>
        <?php
          if ($val < 0) {
            // $class = 'platnosc-bg-err';
            $status = 'Niedopłata';
          } elseif ($val > 0) {
            // $class = 'platnosc-bg-plus';
            $status = 'Nadpłata';
          } else {
            // $class = 'platnosc-bg-ok';
            $status = 'Ok';
          }
        ?>

        <tr class="row">
          <td><?php echo $key; ?></td>
          <td>
          <?php
            echo $val;
            $sumaPoprzednie += $val;
          ?>
          </td>
          <td>
            <?php echo $status; ?>
          </td>
        </tr>
      <?php endforeach; ?>

    <?php
      $saldoPoprzednie = $this->zaplacono - $suma + $sumaPoprzednie;

      if ($saldoPoprzednie < 0) {
        $classSuma = 'platnosc-bg-err';
      } elseif ($saldoPoprzednie > 0) {
        $classSuma = 'platnosc-bg-plus';
      } else {
        $classSuma = 'platnosc-bg-ok';
      }
    ?>

    <tr class="razem <?php echo $classSuma; ?>">
      <td>Saldo</td>
      <td colspan="2">
      <?php

      if ($saldoPoprzednie > 0) {
        echo '+';
      }

      echo $saldoPoprzednie;
      ?>
      </td>
    </tr>

    </tbody>
    </table>
  </div>
  <?php endif; ?>

  <div>
    <h3>Wpłaty</h3>
    <table id="js-wplaty" class="platnosci list-table" width="100%" cellpadding="0">
      <thead>
        <tr>
          <th>Nr dokumentu</th>
          <th>Nazwa</th>
          <th>Opis</th>
          <th>Kwota</th>
          <th>Typ</th>
          <th>Data</th>
          <th>Data sort</th>
          <th>Opcje</th>
        </tr>
      </thead>
      <tbody>
      <?php if (!empty($this->platnosci)): ?>
      <?php foreach ($this->platnosci as $key => $val): ?>
        <tr class="row">
          <td><?php echo $val['platnosc_nr_dokumentu']; ?></td>
          <td><?php echo $val['platnosc_nazwa']; ?></td>
          <td><?php echo $val['platnosc_opis']; ?></td>
          <td>
          <?php
            if ($val['platnosc_zaplacono'] < 0) {
              echo $val['platnosc_zaplacono'] * -1;
            } else {
              echo $val['platnosc_zaplacono'];
            }
          ?>
          </td>
          <td>
          <?php
            if ($val['platnosc_zaplacono'] < 0) {
              echo 'Korekta';
            } else {
              echo 'Wpłata';
            }
          ?>
          </td>
          <td><?php echo $val['platnosc_data_operacji']; ?></td>
          <td><?php echo $val['data_operacji_sort']; ?></td>
          <td width="80px">
            <div class="menu-buttons">
                <a class="delete js-delete-platonsc" href="<?php echo URL; ?>admin/salda/usunplatnosc/<?php echo $val['platnosc_id']; ?>" title="Usuń płatność"><span class="none">Usuń płatność</span></a>
            </div>
          </td>
        </tr>
      <?php endforeach ?>
      <?php endif ?>
      </tbody>
    </table>
  </div>

</div>