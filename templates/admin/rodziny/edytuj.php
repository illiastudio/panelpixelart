<div class="hidden-list">
  <?php echo $this->panel; ?>
</div>
<form action="" method="post">
<span id="js-show-hide-list" class="list-button"></span>
<div class="left">
  <div class="opcje">
  <h3 class="title-bottom-line">Opcje</h3>
  <div class="form-line">
    <span class="form-label"><label for="nazwa">Nazwa</label></span>
    <input type="text" name="nazwa" value="<?php echo $this->page['link_name'] ?>">
  </div>
  <div class="form-line">
    <span class="form-label"><label for="wlaczona">Włączona</label></span>
    <select name="wlaczona">
      <?php if ($this->page['page_status'] == 0): ?>
      <option value="0" selected>Nie</option>
      <option value="1">Tak</option>
      <?php else: ?>
      <option value="0">Nie</option>
      <option value="1" selected>Tak</option>
      <?php endif; ?>
    </select>
  </div>
  <div class="form-line">
    <span class="form-label"><label for="pokazuj">Pokazuj w menu</label></span>
    <select name="pokazuj">
      <?php if ($this->page['page_show'] == 0): ?>
      <option value="0" selected>Nie</option>
      <option value="1">Tak</option>
      <?php else: ?>
      <option value="0">Nie</option>
      <option value="1" selected>Tak</option>
      <?php endif; ?>
    </select>
  </div>
  <div class="form-line">
    <span class="form-label"></span>
    <input type="hidden" name="adres-old" value="<?php echo $this->page['link_url'] ?>">
    <input type="submit" class="zapisz" name="zapisz" value="Zapisz">
  </div>
</div>
  <div id="miniaturka">
    <h3 class="title-bottom-line">Miniaturka</h3>

  </div>
</div>
<div class="content">
  <h3 class="title-bottom-line">Zarządzanie aktualnościami: <?php echo $this->page['link_name']; ?></h3>
  <textarea name="tresc" id="tresc"><?php echo $this->page['text_full']; ?></textarea>
  <script>
    CKEDITOR.replace('tresc');
    CKEDITOR.config.resize_minWidth = 600;
    openElfinderModalInCKeditor();
  </script>
</div>
<div class="clear"></div>
</form>