<div class="content-full">
  <h3>Zarządzanie stronami</h3>
  <table class="list-table" width="100%">
    <thead>
      <tr>
        <th>Tytuł</th>
        <th width="7%">Opcje</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->lista as $key => $val): ?>
      <?php $class = ($key % 2 == 0) ? 'odd' : 'even'; ?>
      <tr class="<?php echo $class; ?>">
        <td>
          <?php echo $val['cms_title']; ?>
        </td>
        <td>
          <div class="menu-buttons">
            <a class="edit" href="<?php echo URL; ?>admin/cms/edytuj/<?php echo $val['cms_id']; ?>"><span class="none">Edytuj stronę</span></a>
          </div>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

</div>