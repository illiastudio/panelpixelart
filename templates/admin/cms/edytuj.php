<div class="content-full">

  <h3>Edytuj stronę: <?php echo $this->item[0]['cms_title']; ?></h3>

  <form class="centered-form" action="" method="post">
    <div class="form-line">
      <label class="form-label" for="cms_title">Tytuł</label>
      <input type="text" id="cms_title" name="cms_title" value="<?php echo $this->item[0]['cms_title']; ?>">
    </div>

    <div class="form-line">
      <label class="form-label form-label-line" for="cms_content">Treść</label>
      <div class="ckeditor-container">
        <textarea name="cms_content" id="cms_content"><?php echo stripslashes($this->item[0]['cms_content']); ?></textarea>
        <script>CKEDITOR.replace('cms_content');</script>
      </div>
    </div>

    <div class="form-line form-line-last">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
    </div>
  </form>
</div>