<div class="edit-box">
  <h3>Edytuj sekcje: <?php echo $this->item[0]['sekcja_nazwa']; ?></h3>

  <form method="post" action="<?php echo URL; ?>admin/sekcje/edytuj/<?php echo $this->item[0]['sekcja_id']; ?>">
    <div class="form-line">
      <label class="form-label" for="sekcja_nazwa">Nazwa</label>
      <input class="input" type="text" name="sekcja_nazwa" value="<?php echo $this->item[0]['sekcja_nazwa']; ?>">
      <input type="hidden" name="sekcja_nazwa_old" value="<?php echo $this->item[0]['sekcja_nazwa']; ?>">
    </div>
    <div class="form-line">
      <label class="form-label" for="sekcja_nazwa">Status</label>
      <select name="sekcja_status">
        <?php if ($this->item[0]['sekcja_status'] == '1'): ?>
        <option value="1" selected>Aktywna</option>
        <option value="0">Nieaktywna</option>
        <?php else: ?>
        <option value="1">Aktywna</option>
        <option value="0" selected>Nieaktywna</option>
        <?php endif; ?>
      </select>
    </div>

    <div class="form-line">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
    </div>
  </form>
</div>