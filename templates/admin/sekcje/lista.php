<div class="content-full">
  <div class="title-container">
    <h3>Zarządzanie sekcjami</h3>
    <a id="js-add-section" class="button add-button" href="<?php echo URL; ?>admin/sekcje/dodaj/"><span></span>Dodaj sekcje</a>
  </div>
  <h3>Sekcje aktywne</h3>
  <table class="list-table" width="100%">
    <thead>
      <tr>
        <th width="5%">Id</th>
        <th>Nazwa</th>
        <th width="7%">Opcje</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->lista as $key => $val): ?>
      <?php $class = ($key % 2 == 0) ? 'odd' : 'even'; ?>
      <tr class="<?php echo $class; ?>">
        <td>
          <?php echo $val['sekcja_id']; ?>
        </td>
        <td>
          <?php echo $val['sekcja_nazwa']; ?>
        </td>
        <td>
          <div class="menu-buttons">
            <a class="js-section-edit edit" href="<?php echo URL; ?>admin/sekcje/edytuj/<?php echo $val['sekcja_id']; ?>"><span class="none">Edytuj sekcje</span></a>
          </div>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <h3>Sekcje nieaktywne</h3>
  <table class="list-table" width="100%">
    <thead>
      <tr>
        <th width="5%">Id</th>
        <th>Nazwa</th>
        <th width="7%">Opcje</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->listaNieaktywne as $key => $val): ?>
      <?php $class = ($key % 2 == 0) ? 'odd' : 'even'; ?>
      <tr class="<?php echo $class; ?>">
        <td>
          <?php echo $val['sekcja_id']; ?>
        </td>
        <td>
          <?php echo $val['sekcja_nazwa']; ?>
        </td>
        <td>
          <div class="menu-buttons">
            <a class="js-section-edit edit" href="<?php echo URL; ?>admin/sekcje/edytuj/<?php echo $val['sekcja_id']; ?>"><span class="none">Edytuj sekcje</span></a>
          </div>
        </td>
      </tr>
      <?php endforeach; ?>
      <?php if (empty($this->listaNieaktywne)): ?>
      <tr class="odd"><td colspan="3">Brak sekcji</td></tr>
      <?php endif; ?>
    </tbody>
  </table>

</div>