<div class="content-full">
  <div class="title-container">
    <h3>Zarządzanie klasami</h3>
    <a id="js-add-class" class="button add-button" href="<?php echo URL; ?>admin/klasy/dodaj/"><span></span>Dodaj klasę</a>
  </div>
  <h3>Klasy aktywne</h3>
  <table class="list-table" width="100%">
    <thead>
      <tr>
        <th width="5%">Id</th>
        <th>Nazwa</th>
        <th>Klasa cyfra</th>
        <th>Klasa znak</th>
        <th width="7%">Opcje</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->lista as $key => $val): ?>
      <?php $class = ($key % 2 == 0) ? 'odd' : 'even'; ?>
      <tr class="<?php echo $class; ?>">
        <td>
          <?php echo $val['klasa_id']; ?>
        </td>
        <td>
          <?php echo $val['klasa_nazwa']; ?>
        </td>
        <td>
          <?php echo $val['klasa_kod_num']; ?>
        </td>
        <td>
          <?php echo $val['klasa_kod_str']; ?>
        </td>
        <td>
          <div class="menu-buttons">
            <a class="js-class-edit edit" href="<?php echo URL; ?>admin/klasy/edytuj/<?php echo $val['klasa_id']; ?>"><span class="none">Edytuj klase</span></a>
          </div>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <h3>Klasy nieaktywne</h3>
  <table class="list-table" width="100%">
    <thead>
      <tr>
        <th width="5%">Id</th>
        <th>Nazwa</th>
        <th>Klasa cyfra</th>
        <th>Klasa znak</th>
        <th width="7%">Opcje</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($this->listaNieaktywne as $key => $val): ?>
      <?php $class = ($key % 2 == 0) ? 'odd' : 'even'; ?>
      <tr class="<?php echo $class; ?>">
        <td>
          <?php echo $val['klasa_id']; ?>
        </td>
        <td>
          <?php echo $val['klasa_nazwa']; ?>
        </td>
        <td>
          <?php echo $val['klasa_kod_num']; ?>
        </td>
        <td>
          <?php echo $val['klasa_kod_str']; ?>
        </td>
        <td>
          <div class="menu-buttons">
            <a class="js-class-edit edit" href="<?php echo URL; ?>admin/klasy/edytuj/<?php echo $val['klasa_id']; ?>"><span class="none">Edytuj klase</span></a>
          </div>
        </td>
      </tr>
      <?php endforeach; ?>
      <?php if (empty($this->listaNieaktywne)): ?>
      <tr class="odd"><td colspan="5">Brak klas</td></tr>
      <?php endif; ?>
    </tbody>
  </table>

</div>