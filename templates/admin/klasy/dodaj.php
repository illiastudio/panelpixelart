<div class="edit-box">
  <h3>Dodaj klasę</h3>

  <form method="post" action="<?php echo URL; ?>admin/klasy/dodaj/">
    <div class="form-line">
      <label class="form-label" for="klasa_nazwa">Nazwa</label>
      <input class="input" type="text" name="klasa_nazwa">
    </div>
    <div class="form-line">
      <label class="form-label" for="klasa_kod_num">Cyfra</label>
      <input class="input" type="text" name="klasa_kod_num">
    </div>
    <div class="form-line">
      <label class="form-label" for="klasa_kod_str">Znak</label>
      <input class="input" type="text" name="klasa_kod_str">
    </div>

    <div class="form-line">
      <button class="add-button" type="submit" name="dodaj"><span></span>Dodaj</button>
    </div>
  </form>
</div>