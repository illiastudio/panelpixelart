<div class="edit-box">
  <h3>Edytuj klasę: <?php echo $this->item[0]['klasa_nazwa']; ?></h3>

  <form method="post" action="<?php echo URL; ?>admin/klasy/edytuj/<?php echo $this->item[0]['klasa_id']; ?>">
    <div class="form-line">
      <label class="form-label" for="klasa_nazwa">Nazwa</label>
      <input class="input" type="text" name="klasa_nazwa" value="<?php echo $this->item[0]['klasa_nazwa']; ?>">
      <input type="hidden" name="klasa_nazwa_old" value="<?php echo $this->item[0]['klasa_nazwa']; ?>">
    </div>
    <div class="form-line">
      <label class="form-label" for="klasa_kod_num">Cyfra</label>
      <input class="input" type="text" name="klasa_kod_num" value="<?php echo $this->item[0]['klasa_kod_num']; ?>">
    </div>
    <div class="form-line">
      <label class="form-label" for="klasa_kod_str">Znak</label>
      <input class="input" type="text" name="klasa_kod_str" value="<?php echo $this->item[0]['klasa_kod_str']; ?>">
    </div>
    <div class="form-line">
      <label class="form-label" for="klasa_status">Status</label>
      <select name="klasa_status">
        <?php if ($this->item[0]['klasa_status'] == '1'): ?>
        <option value="1" selected>Aktywna</option>
        <option value="0">Nieaktywna</option>
        <?php else: ?>
        <option value="1">Aktywna</option>
        <option value="0" selected>Nieaktywna</option>
        <?php endif; ?>
      </select>
    </div>

    <div class="form-line">
      <button class="save-button" type="submit" name="zapisz"><span></span>Zapisz</button>
    </div>
  </form>
</div>