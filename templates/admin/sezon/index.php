<div class="left">
    <h3>Dodaj Sezon</h3>
    <form action="" method="post">
        <div class="form-line">
            <span class="form-label"><label for="sezon-start">Sezon rozpoczyna</label></span>
            <input type="date" name="sezon-start">
        </div>
        <div class="form-line">
            <span class="form-label"><label for="do">Sezon kończy</label></span>
            <input type="date" name="sezon-finish">
        </div>
        <div class="form-line">
            <span class="form-label"></span>
            <button class="add-button" type="submit" name="dodaj-sezon"><span></span>Dodaj sezon</button>
        </div>
    </form>
</div>
<div class="content">
    <h3>Lista sezonów </h3>
    <!--  -->
    <table width="100%" id="js-sezon-table_last" class="list-table">
        <thead>
            <tr>
                <th>Sezon nazwa</th>
                <th>Sezon rozpoczyna</th>
                <th>Sezon kończy</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($this->klasy as $key => $val) : ?>
                <tr>
                    <td><?php echo $val['sezon_nazwa']; ?></td>
                    <td><?php echo $val['sezon_start']; ?></td>
                    <td><?php echo $val['sezon_koniec']; ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>
</div>