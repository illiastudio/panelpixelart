<?php

class Page_Model extends Model {
  public function checkIsset($table, $column, $where) {
    $whereString = '';
    $whereBindArray = array();

    foreach ($where as $key => $value) {
      $whereString .= $key.' = :'.$key;
      $whereBindArray[':'.$key] = $value;
    }

    $r = $this->select('SELECT '.$column.' FROM '.$table.' WHERE '.$whereString.' LIMIT 1', $whereBindArray);

    if (!empty($r[0][$column])) {
      return true;
    }

    return false;
  }

  public function getValue($table, $column, $where) {
    $whereString = '';
    $whereBindArray = array();

    foreach ($where as $key => $value) {
      $whereString .= $key.' = :'.$key;
      $whereBindArray[':'.$key] = $value;
    }

    $r = $this->select('SELECT '.$column.' FROM '.$table.' WHERE '.$whereString.' LIMIT 1', $whereBindArray);

    if (!empty($r[0])) {
      return $r[0][$column];
    }
  }

  public function changeOpiekunPassword($id, $password) {
    return $this->update('opiekunowie', array('opiekun_password' => $password, 'opiekun_password_changed' => '1'), 'opiekun_id = :id', array(':id' => $id));
  }

  public function changeOpiekunImie($id, $imie) {
    $_SESSION['opiekun_imie'] = htmlspecialchars($imie);

    return $this->update('opiekunowie', array('opiekun_imie' => $imie), 'opiekun_id = :id', array(':id' => $id));
  }

  public function updateOpiekunPassowrdCode($login, $code) {
    $this->update('opiekunowie', array('opiekun_password_reset' => $code), 'opiekun_mail = :login', array(':login' => $login));
    // die;
  }

  public function checkOpiekunPasswordCodeAndGetUser($code) {
    $r = $this->select('SELECT opiekun_mail FROM opiekunowie WHERE opiekun_password_reset = :code', array(':code' => $code));

    if (!empty($r)) {
      return $r[0];
    }
  }

  public function resetOpiekunPassword($login, $newPassword) {
    $this->update('opiekunowie', array('opiekun_password' => $newPassword, 'opiekun_password_reset' => '""'), 'opiekun_mail = :login', array(':login' => $login));
  }

  public function getPlatnosci($rodzinaId) {
    $tmp = $this->select('SELECT *, DATE_FORMAT(platnosc_data_operacji, "%Y%m%d%H%i%s") AS data_operacji_sort FROM platnosci WHERE platnosc_rodzina_id = :id ORDER BY platnosc_data_operacji', array(':id' => $rodzinaId));

    if (!empty($tmp)) {
      return $tmp;
    }
  }

  public function getHistory($rodzinaId, $status = 'history') {
    if ($status == 'active') {
      $zgloszenieStatus = 'zgloszenie_status = "1"';
    } else if ($status == 'history') {
      $zgloszenieStatus = 'zgloszenie_status != "1"';
    }

    return $this->select('SELECT zgloszenie_date_add, GROUP_CONCAT(zgloszenie_id) AS zgloszenia FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id WHERE dziecko_rodzina_id = :id AND '.$zgloszenieStatus.' AND zgloszenie_skladka > 0 GROUP BY zgloszenie_group_id ORDER BY zgloszenie_date_add DESC', array(':id' => $rodzinaId));
  }

  public function getZgloszenie($zgloszenieId) {
    $tmp = $this->select('SELECT zgloszenie_id, dziecko_id, dziecko_imie, klasa_nazwa, klasa_id, zgloszenie_sekcja, taryfikator_oplata AS skladka, zgloszenie_propozycja, zgloszenie_uwagi_id, sekcja_nazwa, uwagi_tresc, zgloszenie_date_add, zgloszenie_data_mod, zgloszenie_status, zgloszenie_date FROM zgloszenia z INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON rodzina_id = dziecko_rodzina_id INNER JOIN klasy ON klasa_id = dziecko_klasa INNER JOIN taryfikator t ON (taryfikator_sekcja = zgloszenie_sekcja AND taryfikator_klasa = dziecko_klasa) INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id LEFT JOIN uwagi ON zgloszenie_uwagi_id = uwagi_id WHERE zgloszenie_id = :id LIMIT 1', array(':id' => $zgloszenieId));

    if (!empty($tmp)) {
      return $tmp[0];
    }
  }

  public function getSezony() {
    $tmp = $this->select('SELECT * FROM sezony');

    if (!empty($tmp)) {
      return $tmp;
    }
  }
}