<?php

class Platnosci_Admin_Model extends Admin_Model {
  public function checkRodzinaByPodmiot($podmiotId) {
    $tmp = $this->select('SELECT pp_rodzina_id FROM platnosci_platnicy WHERE pp_platnik_id = :id LIMIT 1', array(':id' => $podmiotId));

    if (!empty($tmp)) {
      return $tmp[0]['pp_rodzina_id'];
    }
  }

  public function checkPlatnosc($id) {
    $tmp = $this->select('SELECT 1 FROM platnosci WHERE platnosc_nr_dokumentu = :id LIMIT 1', array(':id' => $id));

    if (!empty($tmp)) {
      return true;
    }

    return false;
  }

  public function rodzinyList() {
    $tmp = $this->select('SELECT rodzina_id, LTRIM(CONCAT(COALESCE(o.opiekun_imie, ""), " ", rodzina_nazwisko)) ojciec, o.opiekun_mail ojciec_mail, LTRIM(CONCAT(COALESCE(m.opiekun_imie, ""), " ", rodzina_nazwisko)) matka, m.opiekun_mail matka_mail FROM rodziny INNER JOIN opiekunowie o ON o.opiekun_rodzina_id = rodzina_id AND o.opiekun_typ = 1 INNER JOIN opiekunowie m ON m.opiekun_rodzina_id = rodzina_id AND m.opiekun_typ = 2 GROUP BY o.opiekun_rodzina_id');

    if (!empty($tmp)) {
      return $tmp;
    }
  }

  public function issetRodzina($id) {
    $tmp = $this->select('SELECT 1 FROM rodziny WHERE rodzina_id = :id LIMIT 1', array(':id' => $id));

    if (!empty($tmp)) {
      return true;
    }

    return false;
  }

  public function issetPlatnik($rodzinaId) {
    $tmp = $this->select('SELECT 1 FROM platnosci_platnicy WHERE pp_rodzina_id = :id LIMIT 1', array(':id' => $rodzinaId));

    if (!empty($tmp)) {
      return true;
    }

    return false;
  }

  public function issetPlatnosc($nr) {
    $tmp = $this->select('SELECT 1 FROM platnosci WHERE platnosc_nr_dokumentu = :nr LIMIT 1', array(':nr' => $nr));

    if (!empty($tmp)) {
      return true;
    }

    return false;
  }

  public function selectPlatnikRodzina($id) {
    $tmp = $this->select('SELECT pp_rodzina_id FROM platnosci_platnicy WHERE pp_platnik_id = :id LIMIT 1', array(':id' => $id));

    if (!empty($tmp)) {
      return $tmp[0]['pp_rodzina_id'];
    } else {
      return '';
    }
  }

}