<?php

class Ajax_Admin_Model extends Admin_Model {
  public function changeStatus($id, $column, $value) {
    return $this->update('pages', array($column => $value), 'page_id = :id', array(':id' => $id));
  }
}