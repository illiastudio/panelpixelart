<?php

class Rodziny_Admin_Model extends Admin_Model {
  public function getRodzina($id) {
    $tmp = $this->select('SELECT * FROM rodziny WHERE rodzina_id = :id LIMIT 1', array(':id' => $id));

    if (!empty($tmp)) {
      return $tmp[0];
    }
  }

  public function getSaldo($id, $sezon = 'index') {
    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $daty = $this->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => $sezon));

    if (empty($daty)) {
      return array();
    }

    $daty = $daty[0];

    $tmp = $this->select('SELECT ps_rodzina_id, MONTH(ps_okres) ps_miesiac, YEAR(ps_okres) ps_rok, ps_do_zaplaty FROM platnosci_saldo WHERE ps_rodzina_id = :id AND (ps_okres BETWEEN :start AND :koniec) ORDER BY ps_rok DESC, ps_miesiac DESC', array(':id' => $id, ':start' => $daty['sezon_start'], ':koniec' => $daty['sezon_koniec']));

    if (!empty($tmp)) {
      return $tmp;
    }
  }

  public function getPoprzednieSezony($id, $sezon) {
    // $sez = explode('-', $sezon);
    // $sez = ($sez[0] - 1).'-'.($sez[1] - 1);

    // $daty = $this->select('SELECT * FROM sezony WHERE sezon_nazwa = :thisSezon', array(':thisSezon' => $sezon));

    $daty = $this->select('SELECT * FROM sezony WHERE sezon_koniec < (SELECT sezon_koniec FROM sezony WHERE sezon_nazwa = :thisSezon)', array(':thisSezon' => $sezon));

    if (empty($daty)) {
      return array();
    }

    $arr = array();
    $zaplacono = 0;
    foreach ($daty as $key => $val) {
      // $tmp = $this->select('SELECT COALESCE(SUM(ps_do_zaplaty), 0) AS saldo, :sezon AS sezon FROM platnosci_saldo WHERE ps_rodzina_id = :id AND (ps_okres BETWEEN :start AND :koniec)', array(':id' => $id, ':sezon' => $val['sezon_nazwa'], ':start' => $val['sezon_start'], ':koniec' => $val['sezon_koniec']));
      $start = $this->select('SELECT MIN(sezon_start) AS sezon_start FROM sezony LIMIT 1');
      $start = $start[0]['sezon_start'];

      $tmp = $this->select('SELECT COALESCE(SUM(ps_do_zaplaty), 0) AS saldo, :sezon AS sezon FROM platnosci_saldo WHERE ps_rodzina_id = :id AND (ps_okres BETWEEN :start AND :koniec) AND MONTH(ps_okres) NOT IN (7, 8)', array(':id' => $id, ':sezon' => $val['sezon_nazwa'], ':start' => $start, ':koniec' => $val['sezon_koniec']));

      $zaplacono += $this->getZaplacono($id, $val['sezon_nazwa']);

      $arr[$val['sezon_nazwa']] = $zaplacono - $tmp[0]['saldo'];
    }

    return $arr;
  }

  public function getZaplacono($id, $sezon) {
    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $daty = $this->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => $sezon));

    if (empty($daty)) {
      return array();
    }

    $daty = $daty[0];

    $tmp = $this->select('SELECT SUM(platnosc_zaplacono) AS zaplacono FROM platnosci WHERE platnosc_rodzina_id = :id AND (platnosc_data_operacji BETWEEN :start AND :koniec + INTERVAL 2 MONTH) GROUP BY platnosc_rodzina_id', array(':id' => $id, ':start' => $daty['sezon_start'], ':koniec' => $daty['sezon_koniec']));

    if (!empty($tmp)) {
      return $tmp[0]['zaplacono'];
    }
  }

  public function getPlatnosci($rodzinaId, $sezon) {
    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $daty = $this->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => $sezon));

    if (empty($daty)) {
      return array();
    }

    $daty = $daty[0];

    $tmp = $this->select('SELECT *, DATE_FORMAT(platnosc_data_operacji, "%Y%m%d%H%i%s") AS data_operacji_sort FROM platnosci WHERE platnosc_rodzina_id = :id AND (platnosc_data_operacji BETWEEN :start AND :koniec + INTERVAL 2 MONTH) ORDER BY platnosc_data_operacji', array(':id' => $rodzinaId, ':start' => $daty['sezon_start'], ':koniec' => $daty['sezon_koniec']));

    if (!empty($tmp)) {
      return $tmp;
    }
  }

/* Z PODLICZENIEM POPRZEDNICH SEZONOW */
  public function checkStatusPlatnosciPoprzednieSezony($id, $sezon) {
    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $daty = $this->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => $sezon));

    if (empty($daty)) {
      return array();
    }

    $daty = $daty[0];

    // $tmp = $this->select('SELECT (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort FROM rodziny LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id LEFT JOIN (SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty FROM (SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty FROM platnosci_saldo WHERE ps_okres <= :koniec GROUP BY ps_rodzina_id) ps LEFT JOIN (SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono FROM platnosci WHERE (platnosc_data_operacji <= :koniec + INTERVAL 2 MONTH) GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id WHERE rodzina_id = :id GROUP BY rodzina_id', array(':id' => $id, ':start' => $daty['sezon_start'], ':koniec' => $daty['sezon_koniec']));

    $tmp = $this->select('
      SELECT (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND zaplacono_sezon IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort
      FROM rodziny
      LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id
      LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id
      LEFT JOIN (
      SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty, zaplacono_sezon
      FROM (
      SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty
      FROM platnosci_saldo
      WHERE ps_okres <= :koniec
      GROUP BY ps_rodzina_id) ps
      LEFT JOIN (
      SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono
      FROM platnosci
      WHERE (platnosc_data_operacji <= :koniec + INTERVAL 2 MONTH)
      GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id
      LEFT JOIN (
      SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono_sezon
      FROM platnosci
      WHERE (platnosc_data_operacji BETWEEN :start AND :koniec + INTERVAL 2 MONTH)
      GROUP BY platnosc_rodzina_id) p2 ON ps.ps_rodzina_id = p2.platnosc_rodzina_id
      ) tmp ON rodzina_id = pb_rodzina_id
      WHERE rodzina_id = :id
      GROUP BY rodzina_id', array(':id' => $id, ':start' => $daty['sezon_start'], ':koniec' => $daty['sezon_koniec']));

    if (!empty($tmp)) {
      return $tmp[0]['status_sort'];
    }
  }

  /* BEZ PODLICZANIA POPRZEDNICH SEZONOW */
  public function checkStatusPlatnosci($id, $sezon) {
    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $daty = $this->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => $sezon));

    if (empty($daty)) {
      return array();
    }

    $daty = $daty[0];

    $tmp = $this->select('SELECT (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort FROM rodziny LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id LEFT JOIN (SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty FROM (SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty FROM platnosci_saldo WHERE (ps_okres BETWEEN :start AND :koniec) GROUP BY ps_rodzina_id) ps LEFT JOIN (SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono FROM platnosci WHERE (platnosc_data_operacji BETWEEN :start AND :koniec + INTERVAL 2 MONTH) GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id WHERE rodzina_id = :id GROUP BY rodzina_id', array(':id' => $id, ':start' => $daty['sezon_start'], ':koniec' => $daty['sezon_koniec']));

    if (!empty($tmp)) {
      return $tmp[0]['status_sort'];
    }
  }

  public function checkLastPlatnosc($id, $sezon) {
    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $daty = $this->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => $sezon));

    if (empty($daty)) {
      return array();
    }

    $daty = $daty[0];

    $tmp = $this->select('SELECT MAX(platnosc_data_operacji) AS last FROM platnosci WHERE platnosc_rodzina_id = :id AND (platnosc_data_operacji BETWEEN :start AND :koniec + INTERVAL 2 MONTH) LIMIT 1', array('id' => $id, ':start' => $daty['sezon_start'], ':koniec' => $daty['sezon_koniec']));

    if (!empty($tmp)) {
      return $tmp[0]['last'];
    } else {
      return 0;
    }
  }


}