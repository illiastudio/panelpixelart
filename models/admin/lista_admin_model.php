<?php

class Lista_Admin_Model extends Admin_Model {
  public function getZgloszeniaList() {
    $tmp = $this->select('SELECT zgloszenie_id, zgloszenie_dziecko_id, zgloszenie_group_id, sekcja_nazwa, zgloszenie_skladka, zgloszenie_propozycja, zgloszenie_date_add, zgloszenie_date, dziecko_imie, klasa_nazwa, rodzina_nazwisko, rodzina_id, DATE_FORMAT(zgloszenie_date_add, "%Y%m%d%H%i%s") AS zgloszenie_date_add_sort, DATE_FORMAT(zgloszenie_date, "%Y%m%d%H%i%s") AS zgloszenie_date_sort
     FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON rodzina_id = dziecko_rodzina_id INNER JOIN klasy ON dziecko_klasa = klasa_id INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id INNER JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id WHERE zgloszenie_status = "1" AND zgloszenie_skladka > 0 AND potwierdzenie_status = "1" AND potwierdzenie_wyslane = "1"');

    if (!empty($tmp)) {
      return $tmp;
    } else {
      return array();
    }
  }
}