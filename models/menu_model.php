<?php

class Menu_Model extends Model {
  public function multiLevelMenu($typ) {
    $r = $this->select('SELECT relation_id, relation_element_id, link_name, link_url, relation_parent_id, page_type, page_status, page_show FROM relations INNER JOIN pages ON relation_element_id = page_id INNER JOIN links ON link_page_id = relation_element_id WHERE relation_type = :typ ORDER BY relation_parent_id, relation_sort, link_name',array(':typ' => $typ));

    $menu = array(
      'items' => array(),
      'parents' => array()
    );

    if (!empty($r)) {
      foreach ($r as $item) {
        $menu['items'][$item['relation_element_id']] = $item;
        $menu['parents'][$item['relation_parent_id']][] = $item['relation_element_id'];
      }
    }

    return $menu;
  }

}