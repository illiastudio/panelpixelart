<?php

class Admin_Model extends Model {
  public function getMail($login) {
    $r = $this->select('SELECT user_mail FROM users WHERE user_login = :login', array(':login' => $login));

    if (!empty($r)) {
      return $r[0]['user_mail'];
    }
  }

  public function resetPassword($login, $newPassword) {
    $this->update('users', array('user_password' => $newPassword, 'user_password_reset' => ''), 'user_login = :login', array(':login' => $login));
  }

  public function updatePassowrdCode($login, $code) {
    $this->update('users', array('user_password_reset' => $code), 'user_login = :login', array(':login' => $login));
  }

  public function checkPasswordCodeAndGetUser($code) {
    $r = $this->select('SELECT user_login, user_mail FROM users WHERE user_password_reset = :code', array(':code' => $code));

    if (!empty($r)) {
      return $r[0];
    }
  }

  public function getUserMail($id) {
    $r = $this->select('SELECT user_mail FROM users WHERE user_id = :id LIMIT 1', array(':id' => $id));

    if (!empty($r)) {
      return $r[0]['user_mail'];
    }
  }

  public function changeUserMail($id, $mail) {
    return $this->update('users', array('user_mail' => $mail), 'user_id = :id', array(':id' => $id));
  }

  public function changeUserPassword($id, $password) {
    return $this->update('users', array('user_password' => $password, 'user_password_changed' => '1'), 'user_id = :id', array(':id' => $id));
  }

  public function checkIsset($table, $column, $where) {
    $whereString = '';
    $whereBindArray = array();

    foreach ($where as $key => $value) {
      $whereString .= $key.' = :'.$key;
      $whereBindArray[':'.$key] = $value;
    }

    $r = $this->select('SELECT '.$column.' FROM '.$table.' WHERE '.$whereString.' LIMIT 1', $whereBindArray);

    if (!empty($r[0][$column])) {
      return true;
    }

    return false;
  }

  public function getValue($table, $column, $where) {
    $whereString = '';
    $whereBindArray = array();

    foreach ($where as $key => $value) {
      $whereString .= $key.' = :'.$key;
      $whereBindArray[':'.$key] = $value;
    }

    $r = $this->select('SELECT '.$column.' FROM '.$table.' WHERE '.$whereString.' LIMIT 1', $whereBindArray);

    if (!empty($r[0])) {
      return $r[0][$column];
    }
  }

  public function getPage($id) {
    $r = $this->select('SELECT page_id, page_type, text_short, text_full, link_name, link_url, page_status, page_show, page_params FROM pages LEFT JOIN texts ON page_id = text_page_id AND text_lang = :lang INNER JOIN links ON page_id = link_page_id AND link_lang = :lang WHERE page_id = :id LIMIT 1', array(':lang' => LANG, ':id' => $id));

    if (!empty($r)) {
      return $r[0];
    }
  }

  public function getList($table, $column, $where) {
    $whereString = '';
    $whereBindArray = array();

    foreach ($where as $key => $value) {
      $whereString .= $key.' = :'.$key;
      $whereBindArray[':'.$key] = $value;
    }

    $r = $this->select('SELECT '.$column.' FROM '.$table.' WHERE '.$whereString.' ', $whereBindArray);

    if (!empty($r)) {
      return $r;
    }
  }

  public function savePage($id, $data) {
    return $this->update('pages', $data, 'page_id = :id', array(':id' => $id));
  }

  public function saveTexts($id, $data) {
    return $this->update('texts', $data, 'text_page_id = :id', array(':id' => $id));
  }

  public function saveLinks($id, $data) {
    return $this->update('links', $data, 'link_page_id = :id', array(':id' => $id));
  }

  public function insertPage($data) {
    return $this->insert('pages', $data);
  }

  public function insertTexts($data) {
    return $this->insert('texts', $data);
  }

  public function insertLinks($data) {
    return $this->insert('links', $data);
  }

  public function insertMenuRelations($data, $newColumn) {
    if ($newColumn) {
      $r = $this->select('SELECT COALESCE(MAX(relation_sort), 0) + 1 AS next FROM relations WHERE relation_type = :menu AND relation_parent_id = 0', array(':menu' => $data['relation_type']));

      $data['relation_sort'] = $r[0]['next'];
      $data['relation_parent_id'] = 0;
    } else {
      $r = $this->select('SELECT COALESCE(MAX(relation_sort), 0) + 1 AS next, relation_parent_id FROM relations WHERE relation_parent_id IN (SELECT relation_element_id FROM relations WHERE relation_sort IN (SELECT MAX(relation_sort) FROM relations WHERE relation_type = :menu AND relation_parent_id = 0))', array(':menu' => $data['relation_type']));

      $data['relation_sort'] = $r[0]['next'];
      $data['relation_parent_id'] = $r[0]['relation_parent_id'];
    }

    return $this->insert('relations', $data);
  }

  public function insertRelations($data)  {
    return $this->insert('relations', $data);
  }

  public function checkAdres($adres) {
    $r = $this->select('SELECT 1 FROM links WHERE link_url = :url LIMIT 1', array(':url' => $adres));

    if (!empty($r)) {
      return true;
    }

    return false;
  }

  public function checkIfPageTypeInMenu($pageType, $menuType) {
    $r = $this->select('SELECT 1 FROM menu_page_types INNER JOIN menu_types ON mpt_menu_type_id = mt_id INNER JOIN page_types ON mpt_page_type_id = pt_id WHERE pt_code = :pageType AND mt_code = :menuType LIMIT 1', array(':pageType' => $pageType, ':menuType' => $menuType));

    if (!empty($r)) {
      return true;
    }

    return false;
  }

  public function getRelationTypeByPageId($id) {
    $r = $this->select('SELECT relation_type FROM relations WHERE relation_element_id = :id OR relation_parent_id = :id LIMIT 1', array(':id' => $id));

    if (!empty($r)) {
      return $r[0]['relation_type'];
    }
  }

  public function getPageTypes($menuType) {
    $id = 0;

    $r = $this->getValue('menu_types', 'mt_id', array('mt_code' => $menuType));

    if (!empty($r)) {
      $id = $r;
    }

    return $this->select('SELECT pt_code, pt_name FROM page_types WHERE pt_id IN (SELECT mpt_page_type_id FROM menu_page_types WHERE mpt_menu_type_id = :id)', array(':id' => $id));
  }

  public function getMetatags($id) {
    $r = $this->select('SELECT * FROM metatags WHERE metatag_page_id = :id LIMIT 1', array(':id' => $id));

    if (!empty($r)) {
      return $r[0];
    }
  }

  public function saveMetatags($data, $id) {
    $r = $this->select('SELECT 1 FROM metatags WHERE metatag_page_id = :id', array(':id' => $id));

    if (!empty($r[0])) {
      if (empty($data['metatag_title']) && empty($data['metatag_keywords']) && empty($data['metatag_description'])) {
        return $this->delete('metatags', 'metatag_page_id = :id', array(':id' => $id));
      } else {
        return $this->update('metatags', $data, 'metatag_page_id = :id', array(':id' => $id));
      }
    } else {
      $data['metatag_page_id'] = $id;

      return $this->insert('metatags', $data);
    }
  }

  public function getPagesWithRelations($type) {
    $r = $this->select('SELECT page_id, link_name AS title, link_url AS url, relation_parent_id AS parent, page_status, page_show, page_date_add AS data, DATE_FORMAT(page_date_add, "%Y%m%d%H%i%s") AS data_sort FROM relations INNER JOIN pages ON relation_element_id = page_id INNER JOIN links ON link_page_id = relation_element_id WHERE relation_type = :type ORDER BY relation_parent_id, relation_sort, link_name', array(':type' => $type));

    if (!empty($r)) {
      return $r;
    }
  }

  public function getPageIdByType($type) {
    $r = $this->select('SELECT page_id FROM pages WHERE page_type = :type', array(':type' => $type));

    if (!empty($r[0])) {
      return $r[0]['page_id'];
    }
  }

  public function checkIfType($id, $type) {
    $r = $this->select('SELECT 1 FROM pages WHERE page_id = :id AND page_type = :type LIMIT 1', array(':id' => $id, ':type' => $type));

    if (!empty($r)) {
      return true;
    }

    return false;
  }

  public function getModul($type, $limit = true) {
    if ($limit) {
      $limitStr = 'LIMIT 1';
    } else {
      $limitStr = '';
    }

    $r = $this->select('SELECT page_id, page_type, link_name, link_url FROM pages INNER JOIN links ON page_id = link_page_id WHERE page_type = :type '.$limitStr.'', array(':type' => $type));

    if (!empty($r)) {
      return $r[0];
    }
  }

  public function getChildrensUrl($id) {
    $r = $this->select('SELECT relation_element_id, link_url FROM relations INNER JOIN links ON relation_element_id = link_page_id WHERE relation_parent_id = :id OR relation_parent_id IN (SELECT relation_element_id FROM relations WHERE relation_parent_id = :id)', array(':id' => $id));

    if (!empty($r)) {
      return $r;
    }
  }


  public function getOpiekunowie($id) {
    $tmp = $this->select('SELECT * FROM opiekunowie WHERE opiekun_rodzina_id = :id', array(':id' => $id));

    if (!empty($tmp)) {
      return $tmp;
    }
  }

  public function getSezony($nazwa = null) {
    if (empty($nazwa)) {
      $tmp = $this->select('SELECT * FROM sezony');
    } else {
      $tmp = $this->select('SELECT * FROM sezony WHERE sezon_nazwa = :nazwa', array(':nazwa' => $nazwa));
    }

    if (!empty($tmp)) {
      return $tmp;
    }
  }

  public function updateSaldo($id, $start = null, $end = null) {
    $date = START_DATE;

    if (!empty($start)) {
      $date = $start;
    }

    $end_date = date('Y-m-01', time());

    if (!empty($end)) {
      $end_date = $end;
    }

    while (strtotime($date) <= strtotime($end_date)) {
      $tmp = $this->select('
        SELECT dziecko_rodzina_id AS ps_rodzina_id, sum(zgloszenie_propozycja) AS ps_do_zaplaty, GROUP_CONCAT(zgloszenie_id) AS ps_zgloszenia
        FROM zgloszenia
        INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
        LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id
        WHERE dziecko_rodzina_id = :id
        AND (zgloszenie_skladka > 0)
        AND (potwierdzenie_status = "1")
        AND (potwierdzenie_wyslane = "1")
        AND (
          (zgloszenie_status = "1"
            AND zgloszenie_date <= :data
            AND zgloszenie_date != "0000-00-00"
          )
          OR (zgloszenie_status IN ("0", "2")
            AND (zgloszenie_date <= :data AND zgloszenie_date != "0000-00-00")
            AND (
              (zgloszenie_date_end >= :data AND zgloszenie_date_end != "0000-00-00")
              OR (YEAR(zgloszenie_date_end) = YEAR(:data - INTERVAL 1 MONTH)
                AND MONTH(zgloszenie_date_end) = MONTH(:data - INTERVAL 1 MONTH)
                AND DAYOFMONTH(zgloszenie_date_end) > 20
              )
            )
          )
        )
        GROUP BY dziecko_rodzina_id',
      array(':id' => $id, ':data' => $date));

      $arr = array();

      if (!empty($tmp)) {
        $arr = $tmp[0];
      } else {
        $arr['ps_rodzina_id'] = $id;
        $arr['ps_do_zaplaty'] = 0;
      }

      $arr['ps_okres'] = $date;

      if (in_array(date('m', strtotime($date)), array('07', '08'))) {
        $date = date ("Y-m-d", strtotime("+1 month", strtotime($date)));
       } else {
        $this->delete('platnosci_saldo', 'ps_rodzina_id = :id AND ps_okres = :data', array(':id' => $id, ':data' => $date));
        $this->insert('platnosci_saldo', $arr);

        $date = date ("Y-m-d", strtotime("+1 month", strtotime($date)));
       }
    }
  }
  public function getPrevSezon($sezon) {
    $tmp = $this->select('SELECT * FROM sezony WHERE YEAR(sezon_start) IN (
      SELECT YEAR(sezon_start) - 1
      FROM sezony
      WHERE sezon_nazwa = :sezon) LIMIT 1', array(':sezon' => $sezon));

    return $tmp;
  }
}