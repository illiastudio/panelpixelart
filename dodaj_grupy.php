<?php

die;

require 'config.php';

try {
  $pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
  header('Content-Type: text/html; charset=utf-8');
  die('Nie można nawiązać połączenia z bazą danych.');
}

$sth = $pdo->prepare('SELECT dziecko_rodzina_id, zgloszenie_date_add, GROUP_CONCAT(zgloszenie_id) AS zgloszenia FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id WHERE zgloszenie_status = "1" GROUP BY dziecko_rodzina_id, DATE_FORMAT(zgloszenie_date_add, "%Y-%m-%d %H:%i:%s")');
$sth->execute();

$resultArr = $sth->fetchAll(PDO::FETCH_ASSOC);

$sth = $pdo->prepare('SELECT MAX(zgloszenie_group_id) AS max FROM zgloszenia');
$sth->execute();

$group = $sth->fetchAll(PDO::FETCH_ASSOC);
$group = $group[0]['max'] + 1;

foreach ($resultArr as $key => $val) {
  // echo 'rodzina_id = '.$val['dziecko_rodzina_id'].' | group_id = '.$group.' | '.$val['zgloszenia'].'<br>';

  // $sth = $pdo->prepare('SELECT zgloszenie_id, zgloszenie_dziecko_id, zgloszenie_status, zgloszenie_uwagi_id, dziecko_imie, zgloszenie_date_add, dziecko_rodzina_id FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id WHERE zgloszenie_id in ('.$val['zgloszenia'].')');
  // $sth->execute();

  // $res = $sth->fetchAll(PDO::FETCH_ASSOC);
  // $res['grupa'] = $group;

  // echo '<pre>';
  // print_r($res);
  // echo '</pre>';

  $sth = $pdo->prepare('UPDATE zgloszenia SET zgloszenie_group_id = '.$group.' WHERE zgloszenie_id IN ('.$val['zgloszenia'].')');
  $sth->execute();

  $group++;
}