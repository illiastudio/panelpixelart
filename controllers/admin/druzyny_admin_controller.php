<?php

class Druzyny_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista() {
    if (isset($_POST['dodaj-druzyne'])) {
      $this->dodaj();
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/druzyny.js');

    $this->_header->adminHeader();

    $this->_top->adminTop('druzyny');
    $this->_view->trenerzy = $this->_model->select('SELECT user_id, user_name FROM users WHERE user_level = "1"');
    $this->_view->sekcje = $this->_model->select('SELECT * FROM sekcje ORDER BY sekcja_nazwa ASC');


    if ($_SESSION['role'] == 'admin') {
      $this->_view->druzyny = $this->_model->select('
        SELECT
          druzyna_id,
          druzyna_nazwa,
          sekcja_id,
          sekcja_nazwa,
          user_id,
          user_name
        FROM druzyny
        INNER JOIN users ON user_id = druzyna_trener
        INNER JOIN sekcje ON druzyna_sekcja = sekcja_id
      ');
    } else {
      $this->_view->druzyny = $this->_model->select('
        SELECT
          druzyna_id,
          druzyna_nazwa,
          sekcja_id,
          sekcja_nazwa,
          user_id,
          user_name
        FROM druzyny
        INNER JOIN users ON user_id = druzyna_trener
        INNER JOIN sekcje ON druzyna_sekcja = sekcja_id
        WHERE druzyna_trener = :id
      ', array(':id' => $_SESSION['user_id']));
    }

    $this->_view->renderPage('admin/druzyny/index');

    $this->_footer->adminFooter();
  }

  public function dodaj() {
    $form = new Form();
    $form->post('nazwa')->val('notEmpty', 'nazwa')
         ->post('trener')->val('notEmpty', 'trener')
         ->post('sekcja')->val('notEmpty', 'sekcja');

    if ($form->errorCheck()) {
      $data = $form->fetch();

      $tmp = $this->_model->select('SELECT 1 FROM druzyny WHERE druzyna_nazwa = :nazwa AND druzyna_trener = :trener', array(':nazwa' => $data['nazwa'], ':klasa' => $data['klasa']));

      if (!empty($tmp)) {
        $this->_msg->add('error', 'Taka drużyna już istnieje', false, 'admin/druzyny');
      }

      $arr = array(
        'druzyna_nazwa' => $data['nazwa'],
        'druzyna_trener' => $data['trener'],
        'druzyna_sekcja' => $data['sekcja']
      );

      $test = $this->_model->insert('druzyny', $arr);

      if ($test) {
        $this->_msg->add('success', 'Dodano drużynę', false, 'admin/druzyny');
      }
    } else {
      $errors = $form->getErrors();
      $errorsArray = array();
      $errorsCount = 0;

      foreach ($errors as $err) {
        $this->_msg->add('error', $err, false);
      }
    }
  }

  public function usun($params) {
    $id = $params[0];

    $test = $this->_model->delete('druzyny', 'druzyna_id = :id', array(':id' => $id));

    if ($test) {
      $this->_msg->add('success', 'Usunięto drużynę', false, 'admin/druzyny');
    } else {
      $this->_msg->add('error', 'Nie udało się usunąć drużyny', false, 'admin/druzyny');
    }
  }

  public function edytuj($params) {
    $id = $params[0];

    if (isset($_POST['zapisz-druzyne'])) {
      $this->_zapiszDruzyne($id);
    }

    if (isset($_POST['dodaj-dziecko'])) {
      $this->_dodajDziecko($id);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/multiselect.js');
    $this->_header->addStyles('file', 'public/css/multiselect.css');
    $this->_header->addScripts('file', 'public/js/admin/druzyny.js');

    $this->_header->adminHeader();

    $this->_top->adminTop('druzyny');

    $this->_view->trenerzy = $this->_model->select('SELECT user_id, user_name FROM users WHERE user_level = "1"');
    $this->_view->sekcje = $this->_model->select('SELECT * FROM sekcje ORDER BY sekcja_nazwa ASC');

    $druzyna = $this->_model->select('
      SELECT
        druzyna_id,
        druzyna_nazwa,
        sekcja_id,
        sekcja_nazwa,
        user_id,
        user_name
      FROM druzyny
      INNER JOIN users ON user_id = druzyna_trener
      INNER JOIN sekcje ON druzyna_sekcja = sekcja_id
      WHERE druzyna_id = :id
      LIMIT 1
    ', array(':id' => $id));
    $this->_view->druzyna = $druzyna[0];

    $sezon = $this->_model->getSezony(SEZON);
    $this->_view->dzieciLista = $this->_model->select('
      SELECT DISTINCT
        dziecko_id,
        dziecko_imie,
        rodzina_nazwisko,
        klasa_nazwa
      FROM zgloszenia
      INNER JOIN dzieci ON dziecko_id = zgloszenie_dziecko_id
      INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id
      INNER JOIN klasy ON dziecko_klasa = klasa_id
      WHERE zgloszenie_sekcja = :sekcja
      /*
        AND zgloszenie_status IN ("1", "2")
        AND zgloszenie_date >= :start AND zgloszenie_date < :end
        AND (zgloszenie_date_end <= :end OR zgloszenie_date_end = "0000-00-00")
      */
      ORDER BY rodzina_nazwisko ASC, dziecko_imie ASC
    ', array(':sekcja' => $druzyna[0]['sekcja_id'], ':start' => $sezon[0]['sezon_start'], ':end' => $sezon[0]['sezon_koniec']));

    $this->_view->dzieci = $this->_model->select('
      SELECT
        dziecko_id,
        dziecko_imie,
        rodzina_nazwisko,
        klasa_nazwa
      FROM dzieci
      INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id
      INNER JOIN klasy ON dziecko_klasa = klasa_id
      INNER JOIN druzyny_dzieci ON dd_dziecko = dziecko_id
      WHERE dd_druzyna = :id
    ', array(':id' => $id));

    $this->_view->renderPage('admin/druzyny/edytuj');

    $this->_footer->adminFooter();
  }

  private function _zapiszDruzyne($id) {
    $form = new Form();
    $form->post('nazwa')->val('notEmpty', 'nazwa')
         ->post('trener')->val('notEmpty', 'trener')
         ->post('sekcja')->val('notEmpty', 'sekcja')
         ->post('nazwa-old')->val('notEmpty', 'nazwa-old');

    if ($form->errorCheck()) {
      $data = $form->fetch();

      if ($data['nazwa'] != $data['nazwa-old']) {
        $tmp = $this->_model->select('SELECT 1 FROM druzyny WHERE druzyna_nazwa = :nazwa AND druzyna_trener = :trener', array(':nazwa' => $data['nazwa'], ':klasa' => $data['klasa']));

        if (!empty($tmp)) {
          $this->_msg->add('error', 'Taka drużyna już istnieje', false, 'admin/druzyny/edytuj/'.$id);
        }
      }


      $arr = array(
        'druzyna_nazwa' => $data['nazwa'],
        'druzyna_trener' => $data['trener'],
        'druzyna_sekcja' => $data['sekcja']
      );

      $test = $this->_model->update('druzyny', $arr, 'druzyna_id = :id', array(':id' => $id));

      if ($test) {
        $this->_msg->add('success', 'Zapisano drużynę', false, 'admin/druzyny/edytuj/'.$id);
      }
    } else {
      $errors = $form->getErrors();
      $errorsArray = array();
      $errorsCount = 0;

      foreach ($errors as $err) {
        $this->_msg->add('error', $err, false);
      }
    }
  }

  private function _dodajDziecko($id) {
    if (!isset($_POST['dzieci-lista'])) {
      $this->_msg->add('error', 'Nie zaznaczyłeś żadnego dziecka', false, 'admin/druzyny/edytuj/'.$id);
    }

    $dzieci = $_POST['dzieci-lista'];

    foreach ($dzieci as $key => $val) {
      $this->_model->delete('druzyny_dzieci', 'dd_druzyna = :id AND dd_dziecko = :dziecko', array(':id' => $id, ':dziecko' => $val));

      $arr = array(
        'dd_druzyna' => $id,
        'dd_dziecko' => $val
      );

      $test = $this->_model->insert('druzyny_dzieci', $arr);
    }
  }

  public function usunDziecko($params) {
    $druzyna = $params[0];
    $dziecko = $params[1];

    $test = $this->_model->delete('druzyny_dzieci', 'dd_druzyna = :id AND dd_dziecko = :dziecko', array(':id' => $druzyna, ':dziecko' => $dziecko));

    if ($test) {
      $this->_msg->add('success', 'Usunięto dziecko z drużyny', false, 'admin/druzyny/edytuj/'.$druzyna);
    } else {
      $this->_msg->add('error', 'Nie udało się usunąć', false, 'admin/druzyny/edytuj/'.$druzyna);
    }

  }

}