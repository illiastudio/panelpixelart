<?php

class Raport_Admin_Controller extends Admin_Controller {
  private $_miesiace = array();

  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $this->_miesiace[1] = 'Styczeń';
    $this->_miesiace[2] = 'Luty';
    $this->_miesiace[3] = 'Marzec';
    $this->_miesiace[4] = 'Kwiecień';
    $this->_miesiace[5] = 'Maj';
    $this->_miesiace[6] = 'Czerwiec';
    $this->_miesiace[9] = 'Wrzesień';
    $this->_miesiace[10] = 'Październik';
    $this->_miesiace[11] = 'Listopad';
    $this->_miesiace[12] = 'Grudzień';
  }

  public function lista() {
    if (isset($_POST['zgloszenia_generuj'])) {
      $this->redirect('admin/raport/zgloszenia/'.$_POST['zgloszenia_sezon']);
    }

    if (isset($_POST['platnosci_generuj'])) {
      $this->platnosci($_POST['platnosci_sezon']);
    }

    if (isset($_POST['platnosci_v2_generuj'])) {
      $this->redirect('admin/raport/naleznosci/'.$_POST['platnosci_v2_sezon']);
    }

    if (isset($_POST['salda_generuj'])) {
      $this->redirect('admin/raport/salda/'.$_POST['salda_sezon']);
    }

    if (isset($_POST['obecnosci_generuj']) && !empty($_POST['obecnosci_druzyna']) && !empty($_POST['obecnosci_miesiac'])) {
      $this->redirect('admin/raport/obecnosci/'.$_POST['obecnosci_sezon'].'/'.$_POST['obecnosci_druzyna'].'/'.$_POST['obecnosci_miesiac']);
    }

    if (isset($_POST['lista_dzieci_generuj'])) {
      $sekcja = '/0';
      $klasa = '/0';

      if (isset($_POST['lista_dzieci_sekcja']) && !empty($_POST['lista_dzieci_sekcja'])) {
        $sekcja = '/'.$_POST['lista_dzieci_sekcja'];
      }

      if (isset($_POST['lista_dzieci_klasa']) && !empty($_POST['lista_dzieci_klasa'])) {
        $klasa = '/'.$_POST['lista_dzieci_klasa'];
      }

      $this->redirect('admin/raport/listadzieci/'.$_POST['lista_dzieci_sezon'].$sekcja.$klasa);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop('raporty');

    $this->_view->sezony = $this->_model->getSezony();

    if ($_SESSION['role'] == 'admin') {
      $this->_view->druzyny = $this->_model->select('
        SELECT
          druzyna_id,
          druzyna_nazwa,
          sekcja_nazwa
        FROM druzyny
        INNER JOIN sekcje ON druzyna_sekcja = sekcja_id
      ');
    } else {
      $this->_view->druzyny = $this->_model->select('
        SELECT
          druzyna_id,
          druzyna_nazwa,
          sekcja_nazwa
        FROM druzyny
        INNER JOIN sekcje ON druzyna_sekcja = sekcja_id
        WHERE druzyna_trener = :id
      ', array(':id' => $_SESSION['user_id']));
    }

    $this->_view->miesiace = $this->_miesiace;

    $this->_view->sekcje = $this->_model->select('SELECT * FROM sekcje ORDER BY sekcja_nazwa ASC');
    $this->_view->klasy = $this->_model->select('SELECT klasa_id, klasa_nazwa FROM klasy ORDER BY klasa_nazwa ASC');

    $this->_view->renderPage('admin/raporty/lista');

    $this->_footer->adminFooter();
  }

  public function zgloszenia($sezon) {
    $sezon = $sezon[0];
    $tmp = $this->_model->getSezony($sezon);

    if (empty($tmp)) {
      echo 'Nie ma takiego sezonu';
      die;
    }

    if (isset($_POST['eksport'])) {
      $this->_zgloszeniaExport($tmp);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');

    $this->_header->addScripts('text', '
      $(document).ready(function() {
        var dataTable = $("#js-raport-zgloszenia").dataTable({
          "paging": false,
          "searching": false,
          "info": false
        });
      });
    ');

    $this->_header->adminHeader();

    $this->_top->adminTop('raporty');

    $this->_view->sezon = $sezon;
    $this->_view->zgloszenia = $this->_zgloszeniaData($tmp);
    $this->_view->renderPage('admin/raporty/zgloszenia');

    $this->_footer->adminFooter();

    die();
  }

  private function _zgloszeniaData($tmp) {
    $zgloszenia = $this->_model->select('SELECT *, DATE_FORMAT(zgloszenie_date_add, "%Y%m%d%H%i%s") AS data_sort FROM raport INNER JOIN rodziny ON rodzina_id = dziecko_rodzina_id
      WHERE (
        (
          ((zgloszenie_date BETWEEN :start AND :end) AND (zgloszenie_date_end BETWEEN :start AND :end))
          OR ((zgloszenie_date BETWEEN :start AND :end) AND zgloszenie_date_end = "0000-00-00")
        ) OR zgloszenie_date_add BETWEEN :start AND :end
      )
     ORDER BY zgloszenie_date_add', array(':start' => $tmp[0]['sezon_start'], ':end' => $tmp[0]['sezon_koniec']));

    return $zgloszenia;
  }

  private function _zgloszeniaExport($sezon) {
    include './libs/PHPExcel.php';
    include './libs/PHPExcel/Writer/Excel2007.php';

    $zgloszenia = $this->_zgloszeniaData($sezon);

    $objPHPExcel = new PHPExcel();

    $start = explode('-', $sezon[0]['sezon_start']);
    $end = explode('-', $sezon[0]['sezon_koniec']);

    $titleSezon = $start[0].'-'.$end[0];

    $filename = "raport-zgloszenia-".$titleSezon."-stan-".date('d-m-y_H-i').".xlsx";

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header('Cache-Control: max-age=0');

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Id rodziny');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Id syna');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Nazwisko');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Imię syna');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Klasa');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Sekcja');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Składka wg taryfikatora');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Propozycja składki');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Data zgłoszenia');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Status');
    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Uwagi');
    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Status odpowiedzi');
    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Status wysyłki');

    foreach ($zgloszenia as $key => $val) {
      $row = $key + 2;

      if ($val['zgloszenie_status'] == '0') {
        $val['zgloszenie_status'] = 'nieaktualne';
      }

      if ($val['zgloszenie_status'] == '1') {
        $val['zgloszenie_status'] = 'aktualne';
      }

      if ($val['zgloszenie_status'] == '2') {
        $val['zgloszenie_status'] = 'rezygnacja';
      }

      if ($val['zgloszenie_status'] == '3') {
        $val['zgloszenie_status'] = 'anulowane';
      }

      if ($val['potwierdzone'] == '1') {
        $val['potwierdzone'] = 'zaakceptowane';
      }

      if ($val['potwierdzone'] == '2') {
        $val['potwierdzone'] = 'odrzucone';
      }

      if ($val['wyslane'] == '0') {
        $val['wyslane'] = 'niewyslane';
      }

      if ($val['wyslane'] == '1') {
        $val['wyslane'] = 'wyslane';
      }

      $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $val['rodzina_id']);
      $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $val['dziecko_id']);
      $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $val['rodzina_nazwisko']);
      $objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $val['dziecko_imie']);
      $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $val['klasa_nazwa']);
      $objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $val['sekcja_nazwa']);
      $objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $val['zgloszenie_skladka']);
      $objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $val['zgloszenie_propozycja']);
      $objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $val['zgloszenie_date_add']);
      $objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $val['zgloszenie_status']);
      $objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $val['uwagi_tresc']);
      $objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $val['potwierdzone']);
      $objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, $val['wyslane']);
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

    die;
  }

  public function platnosci($sezon) {
    $tmp = $this->_model->getSezony($sezon);

    if (empty($tmp)) {
      echo 'Nie ma takiego sezonu';
      die;
    }

    $arr = $this->_model->select('
      SELECT rodzina_id, rodzina_nazwisko, o.opiekun_imie AS ojciec_imie, o.opiekun_telefon AS ojeciec_telefon, o.opiekun_mail AS ojciec_mail, m.opiekun_imie AS matka_imie, m.opiekun_telefon AS matka_telefon, m.opiekun_mail AS matka_mail, coalesce(SUM(platnosc_zaplacono), 0) AS zaplacono
      , status_sort
      FROM platnosci

      RIGHT JOIN rodziny ON rodzina_id = platnosc_rodzina_id
      INNER JOIN opiekunowie o ON o.opiekun_rodzina_id = rodzina_id AND o.opiekun_typ = 1
      INNER JOIN opiekunowie m ON m.opiekun_rodzina_id = rodzina_id AND m.opiekun_typ = 2

      INNER join (
        SELECT rodzina_id AS rodzina, (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort
        FROM rodziny
        LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id
        LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id
        LEFT JOIN (
          SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty
          FROM (
            SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty
            FROM platnosci_saldo
            WHERE (ps_okres BETWEEN :start AND :end)
            GROUP BY ps_rodzina_id) ps
            LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono
            FROM platnosci
            WHERE (platnosc_data_operacji BETWEEN :start AND :end + INTERVAL 2 MONTH)
          GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id
        GROUP BY rodzina_id
      ) tmp on rodzina_id = rodzina

      WHERE (platnosc_data_operacji BETWEEN :start AND :end + INTERVAL 2 MONTH)

      GROUP BY rodzina_id
    ', array(':start' => $tmp[0]['sezon_start'], ':end' => $tmp[0]['sezon_koniec']));

    $raport = "rodzina_id;nazwisko;ojciec_imie;ojeciec_telefon;ojciec_mail;matka_imie;matka_telefon;matka_mail;zaplacono;status \n";

    foreach ($arr as $key => $val) {

      switch ($val['status_sort']) {
        case 0:
          $status = 'NIEDOPŁATA';
          break;
        case 1:
          $status = 'NADPŁATA';
          break;
        case 2:
          $status = 'SALDO OK';
          break;
        case 3:
          $status = 'PROSIMY O KONTAKT Z BIUREM KLUBU';
          break;
      }

      $raport .= $val['rodzina_id'].';'.$val['rodzina_nazwisko'].';'.$val['ojciec_imie'].';'.$val['ojeciec_telefon'].';'.$val['ojciec_mail'].';'.$val['matka_imie'].';'.$val['matka_telefon'].';'.$val['matka_mail'].';'.$val['zaplacono'].';'.$status."\n";
    }

    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename=raport-platnosci.txt");
    header("Content-Transfer-Encoding: binary");

    header('Content-Type: text/html; charset=utf-8');

    echo $raport;
    die();
  }

  public function naleznosci($sezon) {
    error_reporting(0);

    $sezon = $sezon[0];
    $tmp = $this->_model->getSezony($sezon);

    if (empty($tmp)) {
      echo 'Nie ma takiego sezonu';
      die;
    }

    $array = $this->_naleznosciData($sezon);

    if (isset($_POST['eksport'])) {
      $this->_naleznosciEksport($sezon);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');

    $this->_header->addScripts('text', '
      $(document).ready(function() {
        var dataTable = $("#js-raport-naleznosci").dataTable({
          "paging": false,
          "searching": false,
          "info": false
        });
      });
    ');

    $this->_header->adminHeader();

    $this->_top->adminTop('raporty');

    $this->_view->sezon = $sezon;
    // $this->_view->okresy = $okresy;
    $this->_view->arr = $this->_naleznosciData($sezon);
    $this->_view->renderPage('admin/raporty/naleznosci');

    $this->_footer->adminFooter();

    die();

  }

  private function _naleznosciData($sezon) {
    $tmp = $this->_model->getSezony($sezon);

    if (empty($tmp)) {
      echo 'Nie ma takiego sezonu';
      die;
    }

    // $date = START_DATE;
    // $end_date = date('Y-m-01', time());

    // $dzieci = $this->_model->select('SELECT dziecko_id, rodzina_id, rodzina_nazwisko, dziecko_imie, klasa_nazwa FROM rodziny r INNER JOIN dzieci d ON r.rodzina_id = d.dziecko_rodzina_id INNER JOIN klasy k ON k.klasa_id = d.dziecko_klasa');

    $dzieci = $this->_model->select('
      SELECT
        dziecko_id,
        rodzina_id,
        rodzina_nazwisko,
        dziecko_imie,
        klasa_nazwa
      FROM rodziny
      INNER JOIN dzieci ON rodzina_id = dziecko_rodzina_id
      INNER JOIN klasy ON klasa_id = dziecko_klasa
      INNER JOIN zgloszenia ON zgloszenie_dziecko_id = dziecko_id
      WHERE zgloszenie_status IN ("1", "2")
        AND zgloszenie_date >= :start AND zgloszenie_date < :end
        AND (zgloszenie_date_end <= :end OR zgloszenie_date_end = "0000-00-00")
    ', array(':start' => $tmp[0]['sezon_start'], ':end' => $tmp[0]['sezon_koniec']));

    $array = array();

    foreach ($dzieci as $key => $val) {

      $array[$val['dziecko_id']] = array();
      $array[$val['dziecko_id']]['nazwisko'] = $val['rodzina_nazwisko'];
      $array[$val['dziecko_id']]['rodzina_id'] = $val['rodzina_id'];
      $array[$val['dziecko_id']]['imie'] = $val['dziecko_imie'];
      $array[$val['dziecko_id']]['klasa'] = $val['klasa_nazwa'];
      $array[$val['dziecko_id']]['sekcje'] = array();

      $sekcje = $this->_model->select('SELECT DISTINCT(sekcja_nazwa) nazwa, sekcja_id, klasa_nazwa,
      zgloszenie_date_add, zgloszenie_skladka, zgloszenie_propozycja FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON rodzina_id = dziecko_rodzina_id LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id INNER JOIN klasy ON klasa_id = zgloszenie_dziecko_klasa_id INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id INNER JOIN taryfikator ON (taryfikator_sekcja = zgloszenie_sekcja AND taryfikator_klasa = zgloszenie_dziecko_klasa_id AND taryfikator_sezon = :sezon) WHERE dziecko_id = :id AND zgloszenie_skladka > 0 AND zgloszenie_status IN ("1", "2") AND zgloszenie_date >= :start AND zgloszenie_date < :end
        AND (zgloszenie_date_end <= :end OR zgloszenie_date_end = "0000-00-00")', array(':id' => $val['dziecko_id'], ':sezon' => $sezon, ':start' => $tmp[0]['sezon_start'], ':end' => $tmp[0]['sezon_koniec']));

      foreach ($sekcje as $key2 => $val2) {
        $array[$val['dziecko_id']]['klasa'] = $val2['klasa_nazwa'];
        $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']]['taryfikator'] = $val2['zgloszenie_skladka'];
        $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']]['propozycja'] = $val2['zgloszenie_propozycja'];
        $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']]['data'] = $val2['zgloszenie_date_add'];
        $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']]['platnosci'] = array();

        $platnosci = $this->_model->select('SELECT ps_okres, ps_zgloszenia FROM platnosci_saldo WHERE (ps_okres BETWEEN :start AND :end) AND ps_rodzina_id = :rodzina ORDER BY ps_okres ASC', array(':start' => $tmp[0]['sezon_start'], ':end' => $tmp[0]['sezon_koniec'], ':rodzina' => $val['rodzina_id']));

        // $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']] = $platnosci;

        if (!empty($platnosci)) {
          foreach ($platnosci as $key3 => $val3) {
            if (!empty($val3['ps_zgloszenia'])) {
              $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']]['platnosci'][$val3['ps_okres']] = 0;

              $ile = $this->_model->select('select zgloszenie_id, zgloszenie_sekcja, sum(zgloszenie_propozycja) as suma from zgloszenia where zgloszenie_id IN ('.$val3['ps_zgloszenia'].') AND zgloszenie_sekcja = :sekcja AND zgloszenie_dziecko_id = :dziecko', array(':sekcja' => $val2['sekcja_id'], ':dziecko' => $val['dziecko_id']));

                if (!empty($ile[0]['suma'])) {
                  $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']]['platnosci'][$val3['ps_okres']] = $ile[0]['suma'];
                } else {
                  $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']]['platnosci'][$val3['ps_okres']] = 0;
                }

            }
          }
        } else {
          $mies = $this->_model->select('SELECT distinct(ps_okres) as d FROM platnosci_saldo WHERE (ps_okres BETWEEN :start AND :end) ORDER BY ps_okres ASC', array(':start' => $tmp[0]['sezon_start'], ':end' => $tmp[0]['sezon_koniec']));

          foreach ($mies as $key4 => $val4) {
            $array[$val['dziecko_id']]['sekcje'][$val2['nazwa']]['platnosci'][$val4['d']] = 0;
          }
        }
      }
    }

    return $array;
  }

  private function _naleznosciEksport($sezon) {
    include './libs/PHPExcel.php';
    include './libs/PHPExcel/Writer/Excel2007.php';

    $arr = $this->_naleznosciData($sezon);

    $objPHPExcel = new PHPExcel();

    $t = explode('-', $sezon);
    $start = $t[0];
    $end = $t[1];

    $titleSezon = $start.'-'.$end;

    $filename = "raport-naleznosci-".$titleSezon."-stan-".date('d-m-y_H-i').".xlsx";

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header('Cache-Control: max-age=0');

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Id rodziny');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Id syna');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Nazwisko');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Imię syna');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Klasa');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Sekcja');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Składka wg taryfikatora');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Propozycja składki');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Data zgłoszenia');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Wrzesień');
    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Październik');
    $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Listopad');
    $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Grudzień');
    $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Styczeń');
    $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Luty');
    $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Marzec');
    $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Kwiecień');
    $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Maj');
    $objPHPExcel->getActiveSheet()->SetCellValue('S1', 'Czerwiec');

    $row = 2;

    $okresy = array();
    $okresy[0]['d'] = '20'.$start.'-09-01';
    $okresy[0]['kol'] = 'J';
    $okresy[1]['d'] = '20'.$start.'-10-01';
    $okresy[1]['kol'] = 'K';
    $okresy[2]['d'] = '20'.$start.'-11-01';
    $okresy[2]['kol'] = 'L';
    $okresy[3]['d'] = '20'.$start.'-12-01';
    $okresy[3]['kol'] = 'M';
    $okresy[4]['d'] = '20'.$end.'-01-01';
    $okresy[4]['kol'] = 'N';
    $okresy[5]['d'] = '20'.$end.'-02-01';
    $okresy[5]['kol'] = 'O';
    $okresy[6]['d'] = '20'.$end.'-03-01';
    $okresy[6]['kol'] = 'P';
    $okresy[7]['d'] = '20'.$end.'-04-01';
    $okresy[7]['kol'] = 'Q';
    $okresy[8]['d'] = '20'.$end.'-05-01';
    $okresy[8]['kol'] = 'R';
    $okresy[9]['d'] = '20'.$end.'-06-01';
    $okresy[9]['kol'] = 'S';

    $sumArr = array();

    foreach ($arr as $key => $val) {
      foreach ($val['sekcje'] as $kSekcja => $vSekcja) {
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $val['rodzina_id']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $key);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $val['nazwisko']);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $val['imie']);
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $val['klasa']);
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $kSekcja);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $vSekcja['taryfikator']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $vSekcja['propozycja']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $vSekcja['data']);

        foreach ($okresy as $monthKey => $monthVal) {

          if (isset($vSekcja['platnosci'][$monthVal['d']])) {
            $ile =  $vSekcja['platnosci'][$monthVal['d']];
          } else {
            $ile = 0;
          }

          $objPHPExcel->getActiveSheet()->SetCellValue($monthVal['kol'].$row, $ile);
          $sumArr[$monthVal['kol']] += $ile;
        }

        $row++;
      }
    }

    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'RAZEM');

    foreach ($sumArr as $k => $v) {
      $objPHPExcel->getActiveSheet()->SetCellValue($k.$row, $v);
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

    die;
  }

  public function salda($sezon) {
    $sezon = $sezon[0];
    $tmp = $this->_model->getSezony($sezon);

    if (empty($tmp)) {
      echo 'Nie ma takiego sezonu';
      die;
    }

    if (isset($_POST['eksport'])) {
      $this->_saldaEksport($tmp);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');

    $this->_header->addScripts('text', '
      $(document).ready(function() {
        var dataTable = $("#js-raport-salda").dataTable({
          "paging": false,
          "searching": false,
          "info": false,
          "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            { "iDataSort": 10 },
            { "bVisible": false }
          ],
        });
      });
    ');

    $this->_header->adminHeader();

    $this->_top->adminTop('raporty');

    $this->_view->sezon = $sezon;
    $this->_view->arr = $this->_saldaData($tmp);
    $this->_view->renderPage('admin/raporty/salda');

    $this->_footer->adminFooter();

    die();
  }

  private function _saldaData($sezon) {
    $arr = $this->_model->select('
      SELECT
        SQL_CALC_FOUND_ROWS
        rodzina_id,
        rodzina_nazwisko,
        m1.opiekun_mail AS ojciec_mail,
        m1.opiekun_telefon AS ojciec_telefon,
        m2.opiekun_mail AS matka_mail,
        m2.opiekun_telefon AS matka_telefon,
        COALESCE(NULL, do_zaplaty, 0) AS naleznosc,
      zaplacono,
        COALESCE(pb_bilans, 0) pb_bilans,
        (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort
      FROM
       rodziny
        LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id
        LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id
        INNER JOIN opiekunowie m1 ON m1.opiekun_rodzina_id = rodzina_id AND m1.opiekun_typ = 1
        INNER JOIN opiekunowie m2 ON m2.opiekun_rodzina_id = rodzina_id AND m2.opiekun_typ = 2
        LEFT JOIN (
          SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty, COALESCE(zaplacono, 0) AS zaplacono
          FROM (
            SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty
            FROM platnosci_saldo
            WHERE (ps_okres BETWEEN :start AND :end)
            GROUP BY ps_rodzina_id) ps
          LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono
            FROM platnosci
            WHERE (platnosc_data_operacji BETWEEN :start AND :end + INTERVAL 2 MONTH)
            GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id
      WHERE rodzina_id IN (
        SELECT DISTINCT dziecko_rodzina_id
        FROM zgloszenia
        INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
        WHERE zgloszenie_status IN ("1", "2")
          AND zgloszenie_date >= :start
          AND zgloszenie_date < :end
          AND (zgloszenie_date_end <= :end OR zgloszenie_date_end = "0000-00-00")
      )
      GROUP BY rodzina_id
    ', array(':start' => $sezon[0]['sezon_start'], ':end' => $sezon[0]['sezon_koniec']));

    return $arr;
  }

  private function _saldaEksport($sezon) {
    include './libs/PHPExcel.php';
    include './libs/PHPExcel/Writer/Excel2007.php';

    $arr = $this->_saldaData($sezon);

    $objPHPExcel = new PHPExcel();

    $start = explode('-', $sezon[0]['sezon_start']);
    $start = $start[0];
    $end = explode('-', $sezon[0]['sezon_koniec']);
    $end = $end[0];

    $titleSezon = $start.'-'.$end;

    $filename = "raport-salda-".$titleSezon."-stan-".date('d-m-y_H-i').".xlsx";

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header('Cache-Control: max-age=0');

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Id rodziny');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nazwisko');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Ojciec mail');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Ojciec telefon');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Matka mail');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Matka telefon');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Suma należności');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Suma wpłat');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Saldo');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Status salda');

    $row = 2;
    $sumArr = array(
      'G' => 0,
      'H' => 0,
      'I' => 0
    );

    foreach ($arr as $key => $val) {
      $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $val['rodzina_id']);
      $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $val['rodzina_nazwisko']);
      $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $val['ojciec_mail']);
      $objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $val['ojciec_telefon']);
      $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $val['matka_mail']);
      $objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $val['matka_telefon']);
      $objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $val['naleznosc']);
      $objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $val['zaplacono']);
      $objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $val['pb_bilans']);

      switch ($val['status_sort']) {
        case 0:
          $status = 'Niedopłata';
          break;
        case 1:
          $status = 'Nadpłata';
          break;
        case 2:
          $status = 'Saldo ok';
          break;
        case 3:
          $status = 'Brak należności';
          break;
      }

      $objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $status);

      $sumArr['G'] += $val['naleznosc'];
      $sumArr['H'] += $val['zaplacono'];
      $sumArr['I'] += $val['pb_bilans'];

      $row++;
    }

    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'RAZEM');

    foreach ($sumArr as $k => $v) {
      $objPHPExcel->getActiveSheet()->SetCellValue($k.$row, $v);
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

    die;
  }

  public function obecnosci($params) {
    if (isset($_POST['eksport'])) {
      $this->_obecnosciEksport($params);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');

    $this->_header->addScripts('text', '
      $(document).ready(function() {
        var dataTable = $("#js-raport-obecnosci").dataTable({
          "paging": false,
          "searching": false,
          "info": false,
          "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": ["nosort"]
          }]
        });
      });
    ');

    $this->_header->adminHeader();

    $this->_top->adminTop('raporty');

    $this->_view->sezon = $params[0];

    $druzyna = $this->_model->select('SELECT druzyna_nazwa FROM druzyny WHERE druzyna_id = :id', array(':id' => $params[1]));

    $this->_view->druzyna = $druzyna[0]['druzyna_nazwa'];
    $this->_view->miesiac = $this->_miesiace[$params[2]];
    $this->_view->arr = $this->_obecnosciData($params);
    $this->_view->renderPage('admin/raporty/obecnosci');

    $this->_footer->adminFooter();

    die();
  }

  private function _obecnosciData($params) {
    $sezon = $params[0];
    $tmp = $this->_model->getSezony($sezon);

    if (empty($tmp)) {
      echo 'Nie ma takiego sezonu';
      die;
    }

    $sezArr = explode('-', $sezon);

    $druzyna = $params[1];
    $miesiac = $params[2];

    $arr = array();

    $arr['dzieci'] = $this->_model->select('
      SELECT
        rodzina_id,
        dziecko_id,
        rodzina_nazwisko,
        dziecko_imie,
        klasa_nazwa,
        sekcja_nazwa,
        druzyna_nazwa
      FROM rodziny
      INNER JOIN dzieci ON rodzina_id = dziecko_rodzina_id
      INNER JOIN klasy ON dziecko_klasa = klasa_id
      INNER JOIN druzyny_dzieci ON dd_dziecko = dziecko_id
      INNER JOIN druzyny ON druzyna_id = dd_druzyna
      INNER JOIN sekcje ON sekcja_id = druzyna_sekcja
      WHERE druzyna_id = :druzyna
      ORDER BY rodzina_nazwisko ASC, dziecko_imie ASC
    ', array(':druzyna' => $druzyna));

    $arr['treningi'] = $this->_model->select('
      SELECT
        trening_id,
        MONTH(trening_data) AS miesiac,
        DAY(trening_data) AS dzien
      FROM treningi
      WHERE trening_druzyna = :druzyna
        AND (YEAR(trening_data) BETWEEN :start AND :koniec)
        AND MONTH(trening_data) = :miesiac
        ORDER BY trening_data
    ', array(':druzyna' => $druzyna, ':start' => '20'.$sezArr[0], ':koniec' => '20'.$sezArr[1], ':miesiac' => $miesiac));

    $arr['obecnosci'] = $this->_model->select('
      SELECT
        obecnosc_dziecko,
        obecnosc_status,
        obecnosc_trening
      FROM treningi
      INNER JOIN obecnosci ON trening_id = obecnosc_trening
      WHERE trening_druzyna = :druzyna
        AND (YEAR(trening_data) BETWEEN :start AND :koniec)
        AND MONTH(trening_data) = :miesiac
    ', array(':druzyna' => $druzyna, ':start' => '20'.$sezArr[0], ':koniec' => '20'.$sezArr[1], ':miesiac' => $miesiac));

    return $arr;
  }

  private function _obecnosciEksport($params) {
    include './libs/PHPExcel.php';
    include './libs/PHPExcel/Writer/Excel2007.php';

    $arr = $this->_obecnosciData($params);

    $objPHPExcel = new PHPExcel();

    $sezon = $this->_model->getSezony($params[0]);
    $start = explode('-', $sezon[0]['sezon_start']);
    $start = $start[0];
    $end = explode('-', $sezon[0]['sezon_koniec']);
    $end = $end[0];

    $titleSezon = $start.'-'.$end;

    $filename = "raport-salda-".$titleSezon."-stan-".date('d-m-y_H-i').".xlsx";

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header('Cache-Control: max-age=0');

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Id rodziny');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Id syna');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Nazwisko');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Imię syna');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Klasa');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Sekcja');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Drużyna');

    $cols = array('H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'W');
    $sumArr = array();

    foreach ($arr['treningi'] as $key => $val) {
      $objPHPExcel->getActiveSheet()->SetCellValue($cols[$key].'1', $val['dzien'].'-'.$val['miesiac']);

      $sumArr[$cols[$key]]['bylo'] = 0;
      $sumArr[$cols[$key]]['max'] = 0;
    }

    $row = 2;

    foreach ($arr['dzieci'] as $key => $val) {
      $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $val['rodzina_id']);
      $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $val['dziecko_id']);
      $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $val['rodzina_nazwisko']);
      $objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $val['dziecko_imie']);
      $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $val['klasa_nazwa']);
      $objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $val['sekcja_nazwa']);
      $objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $val['druzyna_nazwa']);

      foreach ($arr['treningi'] as $key2 => $val2) {
        $status = 0;

        foreach ($arr['obecnosci'] as $key3 => $val3) {
          if($val3['obecnosc_dziecko'] == $val['dziecko_id'] && $val3['obecnosc_trening'] == $val2['trening_id']) {
            $status = intval($val3['obecnosc_status']);
          }
        }

        if ($status == 0) {
          $statusShow = 'b/d';
        } elseif ($status == 1) {
          $statusShow = 'ob.';
        } elseif ($status == 2) {
          $statusShow = 'nb';
          $status = 0;
        }

        $objPHPExcel->getActiveSheet()->SetCellValue($cols[$key2].$row, $statusShow);

        $sumArr[$cols[$key2]]['bylo'] += $status;
        $sumArr[$cols[$key2]]['max'] += 1;
      }

      $row++;
    }

    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, 'RAZEM');

    foreach ($sumArr as $k => $v) {
      $objPHPExcel->getActiveSheet()->SetCellValue($k.$row, $v['bylo'].'/'.$v['max']);
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

    die;
  }

  public function listaDzieci($params) {
    $sezon = $params[0];
    $tmp = $this->_model->getSezony($sezon);

    unset($params[0]);

    $params = array_values($params);

    if (empty($tmp)) {
      echo 'Nie ma takiego sezonu';
      die;
    }

    if (isset($_POST['eksport'])) {
      $this->_listaDzieciEksport($tmp, $params);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('text', '
      $(document).ready(function() {
        var dataTable = $("#js-rodziny-list-table").dataTable({
          "paging": false,
          "searching": false,
          "info": false,
          "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": ["nosort"]
          }]
        });
      });
    ');

    $this->_header->adminHeader();

    $this->_top->adminTop('raporty');

    $this->_view->sezon = $sezon;

    if ($params[0] != '0') {
      $this->_view->sekcja = $this->_model->select('SELECT sekcja_nazwa FROM sekcje WHERE sekcja_id = :id LIMIT 1', array('id' => $params[0]));
    } else {
      $this->_view->sekcja = null;
    }

    if ($params[1] != '0') {
      $this->_view->klasa = $this->_model->select('SELECT klasa_nazwa FROM klasy WHERE klasa_id = :id LIMIT 1', array('id' => $params[1]));
    } else {
      $this->_view->klasa = null;
    }

    $this->_view->lista = $this->_listaDzieciData($tmp, $params);

    $this->_view->renderPage('admin/raporty/lista_dzieci');

    $this->_footer->adminFooter();

    die();
  }

  private function _listaDzieciData($sezon, $params) {
    $sekcja = $params[0];
    $klasa = $params[1];

    if ($sekcja != '0') {
      $sekcjaWhere = ' AND sekcja_id = '.$sekcja;
    } else {
      $sekcjaWhere = '';
    }

    if ($klasa != '0') {
      $klasaWhere = ' AND klasa_id = '.$klasa;
    } else {
      $klasaWhere = '';
    }

    $arr = $this->_model->select('
      SELECT * FROM (
        SELECT
          rodzina_id,
          dziecko_id,
          rodzina_nazwisko,
          dziecko_imie,
          klasa_nazwa,
          sekcja_nazwa,
          m1.opiekun_mail AS ojciec_mail,
          m1.opiekun_telefon AS ojciec_telefon,
          m2.opiekun_mail AS matka_mail,
          m2.opiekun_telefon AS matka_telefon,
          zgloszenie_status
        FROM zgloszenia
          INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
          INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id
          INNER JOIN klasy ON dziecko_klasa = klasa_id
          INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id
          INNER JOIN opiekunowie m1 ON m1.opiekun_rodzina_id = rodzina_id AND m1.opiekun_typ = 1
          INNER JOIN opiekunowie m2 ON m2.opiekun_rodzina_id = rodzina_id AND m2.opiekun_typ = 2
        WHERE zgloszenie_status IN ("1", "2")
          AND zgloszenie_date >= "'.$sezon[0]['sezon_start'].'"
          AND zgloszenie_date < "'.$sezon[0]['sezon_koniec'].'"
          AND (zgloszenie_date_end <= "'.$sezon[0]['sezon_koniec'].'" OR zgloszenie_date_end = "0000-00-00")
          '.$klasaWhere.$sekcjaWhere.'
        ORDER BY zgloszenie_status ASC
      ) AS temp
      GROUP BY rodzina_id, dziecko_id, sekcja_nazwa
      ORDER BY dziecko_id ASC
    ');

    return $arr;
  }

  private function _listaDzieciEksport($sezon, $params) {
    include './libs/PHPExcel.php';
    include './libs/PHPExcel/Writer/Excel2007.php';

    $objPHPExcel = new PHPExcel();

    $start = explode('-', $sezon[0]['sezon_start']);
    $start = $start[0];
    $end = explode('-', $sezon[0]['sezon_koniec']);
    $end = $end[0];

    $titleSezon = $start.'-'.$end;

    if ($params[0] != '0') {
      $sekcjaFile = $this->_model->select('SELECT sekcja_nazwa FROM sekcje WHERE sekcja_id = :id LIMIT 1', array('id' => $params[0]));
      $sekcjaFile = '-'.$sekcjaFile[0]['sekcja_nazwa'];
    } else {
      $sekcjaFile = '';
    }

    if ($params[1] != '0') {
      $klasaFile = $this->_model->select('SELECT klasa_nazwa FROM klasy WHERE klasa_id = :id LIMIT 1', array('id' => $params[1]));
      $klasaFile = '-'.$klasaFile[0]['klasa_nazwa'];
    } else {
      $klasaFile = '';
    }

    $arr = $this->_listaDzieciData($sezon, $params);

    $filename = "lista-dzieci-".$titleSezon.$sekcjaFile.$klasaFile.date('d-m-y_H-i').".xlsx";

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '"');
    header('Cache-Control: max-age=0');

    $objPHPExcel->setActiveSheetIndex(0);

    $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Id rodziny');
    $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Id syna');
    $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Nazwisko');
    $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Imię syna');
    $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Klasa');
    $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Sekcja');
    $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Ojciec mail');
    $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Ojciec telefon');
    $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Matka mail');
    $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Matka telefon');
    $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Status');

    $row = 2;

    foreach ($arr as $key => $val) {
      $objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $val['rodzina_id']);
      $objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $val['dziecko_id']);
      $objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $val['rodzina_nazwisko']);
      $objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $val['dziecko_imie']);
      $objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $val['klasa_nazwa']);
      $objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $val['sekcja_nazwa']);
      $objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $val['ojciec_mail']);
      $objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $val['ojciec_telefon']);
      $objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $val['matka_mail']);
      $objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $val['matka_telefon']);

      switch ($val['zgloszenie_status']) {
        case '1':
          $status = 'Aktualne';
          break;
        case '2':
        case '3':
        case '0':
          $status = 'Nieaktualne';
          break;
        default:
          $status = '';
          break;
      }

      $objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $status);

      $row++;
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'"');
    header('Cache-Control: max-age=0');

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');

    die;
  }

}