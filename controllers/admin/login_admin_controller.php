<?php

class Login_Admin_Controller extends Admin_Controller {
  private $_auth;

  public function __construct() {
    parent::__construct();

    $this->_auth = new Auth();
  }

  public function panel() {
    $this->_login();

    ob_start();
    $this->_msg->render();
    $this->_view->msg = ob_get_contents();
    ob_end_clean();

    $this->_view->renderPage('admin/login');
  }

  private function _login() {
    if (isset($_POST['zaloguj'])) {

      $form = new Form();
      $form->post('login')->val('notEmpty', 'login')
           ->post('haslo')->val('notEmpty', 'haslo');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $logged = $this->_auth->login($data['login'], $data['haslo']);

        if ($logged) {
          $this->_checkIssetMail();
          $this->_checkPasswordChanged();

          $this->redirect('admin/');
        } else {
          $this->_msg->add('error', 'Nieprawidłowy login lub hasło', false, 'admin/login');
        }
      } else {
        $form->showErrors();
        $this->reload();
      }
    }
  }

  public function wyloguj() {
    $this->_auth->logout();

    $this->redirect('admin/login/');
  }

  private function _checkIssetMail() {
    $test = $this->_model->checkIsset('users', 'user_mail', array('user_login' => $_SESSION['user_login']));

    if (!$test) {
      $this->_msg->add('info', 'Twoje konto nie ma przypisanego adresu e-mail. W przypadku utraty hasła, niemożliwe będzie jego samodzielne odzyskanie.<br><br>Aby ustawić adres e-mail wejdź: <a href="'.URL.'admin/user">w edycję profilu</a>.', true);
    }
  }

  private function _checkPasswordChanged() {
    $test = $this->_model->getValue('users', 'user_password_changed', array('user_login' => $_SESSION['user_login']));

    if ($test == 0) {
      $this->_msg->add('info', 'Twoje konto posiada domyślne hasło. Znacznie zwiększa to ryzyko dostępu do niego dla osób nieupoważnionych.<br><br>Aby zmienić hasło wejdź: <a href="'.URL.'admin/user">w edycję profilu</a>.', true);
    }
  }

}