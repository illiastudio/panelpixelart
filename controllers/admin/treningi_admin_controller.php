<?php

class Treningi_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista($params) {
    $id = $params[0];

    if (isset($_POST['dodaj-treningi'])) {
      $this->dodaj($id);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/jquery-ui.multidatespicker.js');
    $this->_header->addScripts('file', 'public/js/admin/treningi.js');

    $this->_header->adminHeader();

    $this->_top->adminTop('treningi');

    $this->_view->treningi = $this->_model->select('
      SELECT
        trening_id,
        trening_data,
        DATE_FORMAT(trening_data, "%Y%m%d%H%i%s") AS data_sort
      FROM treningi
      WHERE trening_druzyna = :id
    ', array(':id' => $id));

    $this->_view->renderPage('admin/treningi/index');

    $this->_footer->adminFooter();
  }

  public function dodaj($id) {
    $form = new Form();
    $form->post('daty-treningow')->val('notEmpty', 'daty treningow');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $treningi = explode(',', $data['daty-treningow']);
        $err = 0;

        foreach ($treningi as $key => $val) {
          $arr = array(
            'trening_druzyna' => $id,
            'trening_data' => trim($val)
          );

          $test = $this->_model->insert('treningi', $arr);

          if (!$test) {
            $err++;
          }
        }

        if ($err == 0) {
          $this->_msg->add('success', 'Dodano treningi', false, 'admin/treningi/lista/'.$id);
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }
      }
  }

  public function usun($params) {
    $id = $params[0];

    $druzyna = $this->_model->select('SELECT trening_druzyna FROM treningi WHERE trening_id = :id LIMIT 1', array(':id' => $id));
    $druzyna = $druzyna[0]['trening_druzyna'];

    $test = $this->_model->delete('treningi', 'trening_id = :id', array(':id' => $id));
    $this->_model->delete('obecnosci', 'obecnosc_trening = :id', array(':id' => $id), false);

    if ($test) {
      $this->_msg->add('success', 'Usunięto trening', false, 'admin/treningi/lista/'.$druzyna);
    } else {
      $this->_msg->add('error', 'Nie udało się usunąć treningu', false, 'admin/treningi/lista/'.$druzyna);
    }
  }

  public function obecnosci($params) {
    $id = $params[0];

    if (isset($_POST['zapisz-obecnosci'])) {
      $this->_zapiszObecnosci($id);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/admin/treningi.js');

    $this->_header->adminHeader();

    $this->_top->adminTop('treningi');

    $this->_view->trening = $this->_model->select('
      SELECT
        druzyna_id,
        druzyna_nazwa,
        trening_data
      FROM treningi
      INNER JOIN druzyny ON trening_druzyna = druzyna_id
      WHERE trening_id = :id LIMIT 1',
    array(':id' => $id));

    $druzynaId = $this->_view->trening[0]['druzyna_id'];

    $this->_view->obecnosci = $this->_model->select('
      SELECT
        obecnosc_dziecko,
        obecnosc_status
      FROM
        obecnosci
      WHERE obecnosc_trening = :id',
    array(':id' => $id));

    $this->_view->lista = $this->_model->select('
      SELECT
        dziecko_id,
        dziecko_imie,
        klasa_nazwa,
        rodzina_nazwisko
      FROM druzyny
      INNER JOIN druzyny_dzieci ON druzyna_id = dd_druzyna
      INNER JOIN dzieci ON dziecko_id = dd_dziecko
      INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id
      INNER JOIN klasy ON dziecko_klasa = klasa_id
      WHERE druzyna_id = :id
      ORDER BY rodzina_nazwisko, dziecko_imie
    ', array(':id' => $druzynaId));

    $this->_view->renderPage('admin/treningi/obecnosci');

    $this->_footer->adminFooter();
  }

  private function _zapiszObecnosci($id) {
    if (isset($_POST['obecnosc'])) {
      $obecnosci = $_POST['obecnosc'];

      $this->_model->delete('obecnosci', 'obecnosc_trening = :id', array(':id' => $id), false);
      $err = 0;

      foreach ($obecnosci as $key => $val) {
        $arr = array(
          'obecnosc_trening' => $id,
          'obecnosc_dziecko' => $key,
          'obecnosc_status' => (string)$val
        );

        $test = $this->_model->insert('obecnosci', $arr);

        if (!$test) {
          $err++;
        }
      }

      if ($test) {
        $this->_msg->add('success', 'Zapisano obecności', false, 'admin/treningi/obecnosci/'.$id);
      } else {
        $this->_msg->add('error', 'Nie udało się zapisać wszystkich obecności', false, 'admin/treningi/obecnosci/'.$id);
      }
    }
  }

}