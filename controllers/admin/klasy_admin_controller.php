<?php

class Klasy_Admin_Controller extends Admin_Controller {

  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista() {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/admin/klasy.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('klasy');

    $this->_view->lista = $this->_model->select('SELECT * FROM klasy WHERE klasa_status = "1"');
    $this->_view->listaNieaktywne = $this->_model->select('SELECT * FROM klasy WHERE klasa_status = "0"');


    $this->_view->renderPage('admin/klasy/lista');
    $this->_footer->adminFooter();
  }

  public function dodaj() {
    if (isset($_POST['dodaj'])) {
      $form = new Form();
      $form->post('klasa_nazwa')->val('notEmpty', 'nazwa')
           ->post('klasa_kod_num')->val('notEmpty', 'cyfra')
           ->post('klasa_kod_str')->val('notEmpty', 'znak');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $ifExists = $this->_model->select('SELECT 1 FROM klasy WHERE klasa_nazwa = :nazwa LIMIT 1', array(':nazwa' => $data['klasa_nazwa']));

        if (!empty($ifExists)) {
          $this->_msg->add('error', 'Klasa o takiej nazwie już istnieje', false, 'admin/klasy/');
        }

        $test = $this->_model->insert('klasy', $data);

        if ($test) {
          $this->_msg->add('success', 'Dodano klasę', false, 'admin/klasy/');
        } else {
          $this->_msg->add('error', 'Nie udało się dodać klasy', false, 'admin/klasy/');
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }

        $this->_msg->add('error', 'Nie udało się dodać klasy', false, 'admin/klasy/');
      }
    }

    $this->_view->renderPage('admin/klasy/dodaj');
  }

  public function edytuj($params) {
    $klasaId = $params[0];

    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('klasa_nazwa')->val('notEmpty', 'nazwa')
           ->post('klasa_status')->val('notEmpty', 'status')
           ->post('klasa_kod_num')->val('notEmpty', 'cyfra')
           ->post('klasa_kod_str')->val('notEmpty', 'znak')
           ->post('klasa_nazwa_old');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        if ($data['klasa_nazwa_old'] != $data['klasa_nazwa']) {
          $ifExists = $this->_model->select('SELECT 1 FROM klasy WHERE klasa_nazwa = :nazwa LIMIT 1', array(':nazwa' => $data['klasa_nazwa']));

          if (!empty($ifExists)) {
            $this->_msg->add('error', 'Klasa o takiej nazwie już istnieje', false, 'admin/klasy/');
          }
        }
        unset($data['klasa_nazwa_old']);

        $test = $this->_model->update('klasy', $data, 'klasa_id = :id', array(':id' => $klasaId));

        if ($test) {
          $this->_msg->add('success', 'Zapisano klasę', false, 'admin/klasy/');
        } else {
          $this->_msg->add('error', 'Nie udało się edytować klasy', false, 'admin/klasy/');
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }

        $this->_msg->add('error', 'Nie udało się edytować klasy', false, 'admin/klasy/');
      }
    }

    $this->_view->item = $this->_model->select('SELECT * FROM klasy WHERE klasa_id = :id LIMIT 1', array(':id' => $klasaId));
    $this->_view->renderPage('admin/klasy/edytuj');
  }
}