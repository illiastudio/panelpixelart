<?php

class Salda_Admin_Controller extends Admin_Controller
{
  public function __construct()
  {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista()
  {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/rodziny.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('rodziny');

    $this->_view->panel = $this->_view->renderPage('admin/rodziny/panel', false);
    $this->_view->content = $this->_view->renderPage('admin/rodziny/lista', false);

    $this->_view->renderPage('admin/rodziny/index');

    $this->_footer->adminFooter();
  }

  public function edytuj($params)
  {
    $paramsNumber = count($params);

    $rodzinaObj = new Rodzina_Admin_Controller();
    $rodzinaObj->init($params);
  }

  public function getList()
  {
    parent::_isAjax();

    $model = new Rodziny_Admin_Model();

    $aColumns = array(
      'rodzina_id',
      'rodzina_nazwisko',
      'ojciec_mail',
      'ojciec_telefon',
      'matka_mail',
      'matka_telefon',
      'status',
      'status_sort',
      'last_send',
      'last_send_sort',
      'opcje'
    );

    /* Paging */
    $sLimit = '';

    if (isset($_GET['start']) && $_GET['length'] != -1) {
      $sLimit = "LIMIT " . intval($_GET['start']) . ", " . intval($_GET['length']);
    }

    /* Ordering */
    $sOrder = '';

    if (isset($_GET['order'])) {
      foreach ($_GET['order'] as $key => $value) {
        $sOrder .= $aColumns[$value['column']] . ' ' . $value['dir'] . ', ';
      }

      if ($sOrder != '') {
        $sOrder = rtrim($sOrder, ', ');
        $sOrder = 'ORDER BY ' . $sOrder;
      }
    } else {
      // $sOrder = 'ORDER BY data_sort DESC';
    }

    $daty = $this->_model->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => SEZON));

    if (empty($daty)) {
      return array();
    }
    // echo  var_dump($daty);

    $daty = $daty[0];

    $sezonStart = $daty['sezon_start'];
    $sezonKoniec = $daty['sezon_koniec'];

    /* Filtering */
    $sWhere = '';

    if (isset($_GET['search']) && $_GET['search']['value'] != '') {
      $sWhere = 'WHERE (';
      $str = htmlspecialchars($_GET['search']['value']);

      for ($i = 0; $i < count($aColumns); $i++) {
        if (in_array($aColumns[$i], array('opcje', 'naleznosc', 'status', 'status_sort', 'last_send', 'last_send_sort'))) {
          continue;
        } else if ($aColumns[$i] == 'ojciec_mail') {
          $sWhere .= "m1.opiekun_mail LIKE '%" . $str . "%' OR ";
        } else if ($aColumns[$i] == 'matka_mail') {
          $sWhere .= "m2.opiekun_mail LIKE '%" . $str . "%' OR ";
        } else if ($aColumns[$i] == 'ojciec_telefon') {
          $sWhere .= "m1.opiekun_telefon LIKE '%" . $str . "%' OR ";
        } else if ($aColumns[$i] == 'matka_telefon') {
          $sWhere .= "m2.opiekun_telefon LIKE '%" . $str . "%' OR ";
        } else {
          $sWhere .= $aColumns[$i] . " LIKE '%" . $str . "%' OR ";
        }
      }

      $sWhere = substr_replace($sWhere, "", -3);
      $sWhere .= ')';
    }

    /* SQL queries */
    /*
    $sQuery = "
      SELECT
        SQL_CALC_FOUND_ROWS
        rodzina_id,
        rodzina_nazwisko,
        m1.opiekun_mail AS ojciec_mail,
        m1.opiekun_telefon AS ojciec_telefon,
        m2.opiekun_mail AS matka_mail,
        m2.opiekun_telefon AS matka_telefon,
        COALESCE(NULL, do_zaplaty, 0) AS naleznosc,
        COALESCE(pb_bilans, 0) pb_bilans,
        (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND zaplacono_sezon IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort,
        COALESCE(DATE_FORMAT(MAX(pm_date_send), '%Y-%m-%d'), 'Nie wysyłano') AS last_send,
        DATE_FORMAT(MAX(pm_date_send), '%Y%m%d%H%i%s') AS last_send_sort
      FROM
       rodziny
        LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id
        LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id
        INNER JOIN opiekunowie m1 ON m1.opiekun_rodzina_id = rodzina_id AND m1.opiekun_typ = 1
        INNER JOIN opiekunowie m2 ON m2.opiekun_rodzina_id = rodzina_id AND m2.opiekun_typ = 2
        LEFT JOIN (
          SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty, zaplacono_sezon
          FROM (
            SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty
            FROM platnosci_saldo
            WHERE (ps_okres <= '".$sezonKoniec."')
            GROUP BY ps_rodzina_id) ps
          LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono
            FROM platnosci
            WHERE (platnosc_data_operacji <= '".$sezonKoniec."' + INTERVAL 2 MONTH)
            GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id
          LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono_sezon
            FROM platnosci
            WHERE (platnosc_data_operacji BETWEEN '".$sezonStart."' AND '".$sezonKoniec."' + INTERVAL 2 MONTH)
            GROUP BY platnosc_rodzina_id) p2 ON ps.ps_rodzina_id = p2.platnosc_rodzina_id
        ) tmp ON rodzina_id = pb_rodzina_id
        LEFT JOIN platnosci_msg ON rodzina_id = pm_rodzina_id
      $sWhere
        GROUP BY rodzina_id
      $sOrder
      $sLimit
    ";
*/
    $sQuery = "
      SELECT
        SQL_CALC_FOUND_ROWS
        rodzina_id,
        rodzina_nazwisko,
        m1.opiekun_mail AS ojciec_mail,
        m1.opiekun_telefon AS ojciec_telefon,
        m2.opiekun_mail AS matka_mail,
        m2.opiekun_telefon AS matka_telefon,
        COALESCE(NULL, do_zaplaty, 0) AS naleznosc,
        COALESCE(pb_bilans, 0) pb_bilans,
        (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort,
        COALESCE(DATE_FORMAT(MAX(pm_date_send), '%Y-%m-%d'), 'Nie wysyłano') AS last_send,
        DATE_FORMAT(MAX(pm_date_send), '%Y%m%d%H%i%s') AS last_send_sort
      FROM
       rodziny
        LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id
        LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id
        INNER JOIN opiekunowie m1 ON m1.opiekun_rodzina_id = rodzina_id AND m1.opiekun_typ = 1
        INNER JOIN opiekunowie m2 ON m2.opiekun_rodzina_id = rodzina_id AND m2.opiekun_typ = 2
        LEFT JOIN (
          SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty
          FROM (
            SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty
            FROM platnosci_saldo
            WHERE (ps_okres BETWEEN '" . $sezonStart . "' AND '" . $sezonKoniec . "')
            GROUP BY ps_rodzina_id) ps
          LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono
            FROM platnosci
            WHERE (platnosc_data_operacji BETWEEN '" . $sezonStart . "' AND '" . $sezonKoniec . "' + INTERVAL 2 MONTH)
            GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id
        LEFT JOIN platnosci_msg ON rodzina_id = pm_rodzina_id
      $sWhere
        GROUP BY rodzina_id
      $sOrder
      $sLimit
    ";

    $rResult = $this->_model->select($sQuery);

    /* Data set length after filtering */
    $sQuery = "
      SELECT FOUND_ROWS() AS ile
    ";
    $rResultFilterTotal = $this->_model->select($sQuery);
    $iFilteredTotal = $rResultFilterTotal[0]['ile'];

    /* Total data set length */
    $sQuery = "
      SELECT COUNT(rodzina_id) AS ile
      FROM
        rodziny
    ";
    $rResultTotal = $this->_model->select($sQuery);
    $iTotal = $rResultTotal[0]['ile'];

    /*
     * Output
    */
    if (!isset($_GET['sEcho'])) {
      $_GET['sEcho'] = 0;
    }

    $output = array(
      "sEcho" => intval($_GET['sEcho']),
      "iTotalRecords" => $iTotal,
      "iTotalDisplayRecords" => $iFilteredTotal,
      "aaData" => array()
    );


    foreach ($rResult as $key => $value) {
      $row = array();

      for ($i = 0; $i < count($aColumns); $i++) {
        if ($aColumns[$i] == 'opcje') {
          $row[] = '<div class="menu-buttons">
            <a class="send js-send" href="" title="Wyślij wiadomość"><span class="none">Wyślij wiadomość</span></a>
          </div>';
        } elseif ($aColumns[$i] == 'status' || $aColumns[$i] == 'status_sort') {
          // STARA KOLEJNOSC 0-1-2-3
          // NOWA 0-3-1-2

          switch ($value['status_sort']) {
            case 0:
              $row[] = '<span title="Niedopłata" data-status="0" class="platnosc-err"></span>';
              break;
            case 1:
              $row[] = '<span title="Nadpłata" data-status="1" class="platnosc-plus"></span>';
              break;
            case 2:
              $row[] = '<span title="Saldo ok" data-status="2" class="platnosc-ok"></span>';
              break;
            case 3:
              $row[] = '<span title="Brak należności" data-status="3" class="platnosc-empty"></span>';
              break;
          }
        } else {
          $row[] = $value[$aColumns[$i]];
        }
      }

      $output['aaData'][] = $row;
    }

    echo json_encode($output);
  }


  public function usunplatnosc($params)
  {
    $id = $params[0];

    $tmp = $this->_model->select('SELECT platnosc_rodzina_id FROM platnosci WHERE platnosc_id = :id', array(':id' => $id));

    if (!empty($tmp)) {
      $rodzinaId = $tmp[0]['platnosc_rodzina_id'];
    } else {
      return false;
    }

    $test = $this->_model->delete('platnosci', 'platnosc_id = :id', array(':id' => $id));

    if ($test) {
      $this->_msg->add('success', 'Usunięto płatność', false, 'admin/salda/edytuj/' . $rodzinaId);
    } else {
      $this->_msg->add('error', 'Nie udało się usunąć płatności', false, 'admin/salda/edytuj/' . $rodzinaId);
    }
  }
}
