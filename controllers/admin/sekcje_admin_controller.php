<?php

class Sekcje_Admin_Controller extends Admin_Controller {

  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista() {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/admin/sekcje.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('sekcje');

    $this->_view->lista = $this->_model->select('SELECT * FROM sekcje WHERE sekcja_status = "1"');
    $this->_view->listaNieaktywne = $this->_model->select('SELECT * FROM sekcje WHERE sekcja_status = "0"');


    $this->_view->renderPage('admin/sekcje/lista');
    $this->_footer->adminFooter();
  }

  public function dodaj() {
    if (isset($_POST['dodaj'])) {
      $form = new Form();
      $form->post('sekcja_nazwa')->val('notEmpty', 'nazwa');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $ifExists = $this->_model->select('SELECT 1 FROM sekcje WHERE sekcja_nazwa = :nazwa LIMIT 1', array(':nazwa' => $data['sekcja_nazwa']));

        if (!empty($ifExists)) {
          $this->_msg->add('error', 'Sekcja o takiej nazwie już istnieje', false, 'admin/sekcje/');
        }

        $test = $this->_model->insert('sekcje', $data);

        if ($test) {
          $this->_msg->add('success', 'Dodano sekcje', false, 'admin/sekcje/');
        } else {
          $this->_msg->add('error', 'Nie udało się dodać sekcji', false, 'admin/sekcje/');
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }

        $this->_msg->add('error', 'Nie udało się dodać sekcji', false, 'admin/sekcje/');
      }
    }

    $this->_view->renderPage('admin/sekcje/dodaj');
  }

  public function edytuj($params) {
    $sekcjaId = $params[0];

    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('sekcja_nazwa')->val('notEmpty', 'nazwa')
           ->post('sekcja_status')->val('notEmpty', 'status')
           ->post('sekcja_nazwa_old');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        if ($data['sekcja_nazwa_old'] != $data['sekcja_nazwa']) {


          $ifExists = $this->_model->select('SELECT 1 FROM sekcje WHERE sekcja_nazwa = :nazwa LIMIT 1', array(':nazwa' => $data['sekcja_nazwa']));

          if (!empty($ifExists)) {
            $this->_msg->add('error', 'Sekcja o takiej nazwie już istnieje', false, 'admin/sekcje/');
          }
        }
        unset($data['sekcja_nazwa_old']);

        $test = $this->_model->update('sekcje', $data, 'sekcja_id = :id', array(':id' => $sekcjaId));

        if ($test) {
          $this->_msg->add('success', 'Zapisano sekcje', false, 'admin/sekcje/');
        } else {
          $this->_msg->add('error', 'Nie udało się edytować sekcji', false, 'admin/sekcje/');
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }

        $this->_msg->add('error', 'Nie udało się edytować sekcji', false, 'admin/sekcje/');
      }
    }

    $this->_view->item = $this->_model->select('SELECT * FROM sekcje WHERE sekcja_id = :id LIMIT 1', array(':id' => $sekcjaId));
    $this->_view->renderPage('admin/sekcje/edytuj');
  }
}