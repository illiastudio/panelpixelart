<?php

class Password_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
  }

  public function reset($params) {
    if (isset($params[0]) && !empty($params[0])) {
      $code = $params[0];

      $this->_resetPassowrd($code);
      $this->_view->renderPage('admin/password_reset');
      exit();
    }

    if (isset($_POST['wyslij'])) {
      $form = new Form();
      $form->post('login')->val('notEmpty', 'login');

      if ($form->errorCheck()) {
        $data = $form->fetch();
      } else {
        $form->showErrors();
        $this->reload();
      }

      $mailAdres = $this->_model->getMail($data['login']);

      if (!empty($mailAdres)) {
        $random = new Random();
        $haslo = $random->create(6);
        $hasloHash = Hash::create('sha512', HASH_PASSWORD_KEY.$haslo, HASH_SITE_KEY);

        $mail = new phpmailer();
        $mail->CharSet = "UTF-8";
        $mail->SetLanguage("pl", "libs/");
        $mail->IsHTML(true);

        // SMTP DO TESTOW Z LOCALHOSTA
        $mail->Mailer = 'smtp';
        $mail->addCustomHeader('MIME-Version: 1.0');
        $mail->addCustomHeader('Content-Type: text/html; charset=UTF-8');
        $mail->IsSMTP();
        $mail->SMTPDebug = 2;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAutoTLS = false;
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPKeepAlive = true;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;

        $mail->From = $mailAdres;
        $mail->FromName = $data['login'];
        $mail->Subject = 'Panel Administratora - reset hasła';
        $mail->Body = 'Nowe hasło: <br>'.trim($haslo);
        $mail->AddAddress($mailAdres);

        if($mail->Send()) {
          $this->_model->resetPassword($data['login'], $hasloHash);

          $this->_msg->add('success', 'Wysłano nowe hasło na mail ' . $mailAdres, false, 'admin/login');
        } else {
          $this->_msg->add('error', 'Nie udało się wysłać wiadomości.', false, 'admin/password/reset');
        }

        $mail->ClearAddresses();
        $mail->ClearAttachments();
        $mail->SmtpClose();
      }
    }

    ob_start();
    $this->_msg->render();
    $this->_view->msg = ob_get_contents();
    ob_end_clean();

    $this->_view->renderPage('admin/password_reset');
  }

  private function _resetPassowrd($code) {
    $code = htmlspecialchars($code);

    $user = $this->_model->checkPasswordCodeAndGetUser($code);

    if (!empty($user)) {
      $random = new Random();
      $haslo = $random->create(6);
      $hasloHash = Hash::create('sha512', HASH_PASSWORD_KEY.$haslo, HASH_SITE_KEY);

      $mail = new phpmailer();
      $mail->CharSet = "UTF-8";
      $mail->SetLanguage("pl", "libs/");
      $mail->IsHTML(true);

      // SMTP DO TESTOW Z LOCALHOSTA
      $mail->Mailer = 'smtp';
      $mail->addCustomHeader('MIME-Version: 1.0');
      $mail->addCustomHeader('Content-Type: text/html; charset=UTF-8');
      $mail->IsSMTP();
      $mail->SMTPDebug = 2;
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'tls';
      $mail->SMTPAutoTLS = false;
      $mail->Host = MAIL_HOST;
      $mail->Port = MAIL_PORT;
      $mail->SMTPKeepAlive = true;
      $mail->Username = MAIL_USER;
      $mail->Password = MAIL_PASS;

      $mail->From = $user['user_mail'];
      $mail->FromName = $user['user_login'];
      $mail->Subject = 'Panel Administratora - reset hasła';
      $mail->Body = 'Nowe hasło: <br>'.trim($haslo);
      $mail->AddAddress($user['user_mail']);
      $mail->send();

      if($mail->Send()) {
        $this->_model->resetPassword($user['user_login'], $hasloHash);

        $this->_msg->add('success', 'Wysłano nowe hasło.', false, 'admin/login');
      } else {
        $this->_msg->add('error', 'Nie udało się wysłać wiadomości.', false, 'admin/password/reset');
      }

      $mail->ClearAddresses();
      $mail->ClearAttachments();
      $mail->SmtpClose();
    }

    ob_start();
    $this->_msg->render();
    $this->_view->msg = ob_get_contents();
    ob_end_clean();

    $this->redirect('admin/login');
  }

}
