<?php

class Index_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function index() {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop();

    $this->_view->renderPage('admin/index');

    $this->_footer->adminFooter();
  }

}