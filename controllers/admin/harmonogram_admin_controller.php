<?php

class Harmonogram_Admin_Controller extends Admin_Controller
{
  public function __construct()
  {
    parent::__construct();
    parent::_isLogged();
  }

  public function index()
  {
    if (isset($_POST['dodaj-school'])) {
      $this->addScholl();
    }
    if (isset($_POST['remove'])) {
      $this->remove($_POST['id']);
    }
    
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/admin/harmonogram.js');
    $this->_header->adminHeader();
    $this->_top->adminTop('harmonogram');

    $this->_view->schools = $this->_model->select('SELECT * FROM harmonogram ORDER BY ID ASC');

    $this->_view->renderPage('admin/harmonogram/index');

    $this->_footer->adminFooter();
    
  }

  public function addScholl()
  {

    $form = new Form();
    $form->post('name')->val('notEmpty', 'name')
      ->post('title')->val('Empty', 'title');

    if ($form->errorCheck()) {
      $data = $form->fetch();

      $name = $data['name'];
      $title = $data['title'];

      $checkSezon = $this->_model->select('SELECT 1 FROM harmonogram WHERE names =  ' . '"' . $name . '"');

      echo $checkSezon;
      if (!empty($checkSezon)) {
        $this->_msg->add('error', 'Taka szkoła już istnieje', false, 'admin/harmonogram/index');
      }
      $dataTable = array(
        'names' => $name,
        'title' => $title,
      );

      $insertToTable = $this->_model->insert('harmonogram', $dataTable);

      if ($insertToTable) {
        $this->_msg->add('success', "Dodano szkołę: " . $name, false, 'admin/harmonogram/index');
      } else {
        echo $insertToTable;
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }
      }
    } else {
      $errors = $form->getErrors();
      $errorsArray = array();
      $errorsCount = 0;

      foreach ($errors as $err) {
        $this->_msg->add('error', $err, false);
      }
    }
   
  }

  function remove(){
    // parent::_isAjax();
    $id = $_POST['id'];
    // echo $argc;
   $delete = $this->_model->delete('harmonogram', 'id = :id ', array(':id' => $id));
    if(!empty($delete)){
      $newHarmonogram = $this->_model->select('SELECT id, names, title FROM harmonogram');

      foreach($newHarmonogram as $key => $val) : ?>
       <tr>
            <td style="display: flex;flex-direction: column;">
                <?php echo $val['names']; ?>
                <span><?php echo $val['title']; ?></span>
                <div>
                    <button class="removeSchool" data-id="<?php echo $val['id']; ?>" style="cursor:pointer; padding: 5px 15px !important;">remove</button>
                    <button class="editSchool" style="cursor:pointer; padding: 5px 15px !important;">edit</button>
                </div>
            </td>
        </tr>
      <?php endforeach;
    
    }else {
      echo json_encode('Error');
    }
    
  }
}
