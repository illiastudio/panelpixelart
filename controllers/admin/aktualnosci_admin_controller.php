<?php

class Aktualnosci_Admin_Controller extends Admin_Controller {
  private $_modul;

  public function  __construct() {
    parent::__construct();
    parent::_isLogged();

    $this->_modul = $this->_model->getModul('AM');
  }

  public function lista() {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/aktualnosci.js');
    $this->_header->adminHeader();

    $this->_top->adminTop();

    $this->_view->panel = $this->_view->renderPage('admin/aktualnosci/panel', false);
    $this->_view->content = $this->_view->renderPage('admin/aktualnosci/lista', false);

    $this->_view->renderPage('admin/aktualnosci/index');

    $this->_footer->adminFooter();
  }

  public function dodaj() {
    if (isset($_POST['dodaj'])) {
      $type = 'A';

      $form = new Form();
      $form->post('tytul')->val('notEmpty', 'tytuł');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $data['adres'] = $this->_modul['link_url'].'/'.$this->urlCharset($data['tytul']);

        if ($this->_model->checkAdres($data['adres'])) {
          header('Content-Type: application/json');
          echo json_encode(array(array('type' => 'error', 'msg' => 'Taka strona już istnieje')));
          die();
        }

        $dataPage = array();
        $dataPage['page_type'] = $type;

        $testPage = $this->_model->insertPage($dataPage);
        $id = $this->_model->getlastInsertId();

        $dataLinks = array();
        $dataLinks['link_page_id'] = $id;
        $dataLinks['link_name'] = $data['tytul'];
        $dataLinks['link_url'] = $data['adres'];
        $dataLinks['link_lang'] = $_SESSION['lang'];
        $dataLinks['link_type'] = $type;

        $testLinks = $this->_model->insertLinks($dataLinks);

        $dataTexts = array();
        $dataTexts['text_page_id'] = $id;
        $dataTexts['text_lang'] = $_SESSION['lang'];

        $testTexts = $this->_model->insertTexts($dataTexts);

        $dataRelations = array();
        $dataRelations['relation_parent_id'] = $this->_model->getPageIdByType('AM');
        $dataRelations['relation_element_id'] = $id;
        $dataRelations['relation_type'] = $type;

        $testRelations = $this->_model->insertRelations($dataRelations, false);

        if ($testPage && $testLinks && $testTexts && $testRelations) {
          header('Content-Type: application/json');
          echo json_encode(array(array('type' => 'success', 'msg' => 'Dodano nową stronę')));
        } else {
          header('Content-Type: application/json');
          echo json_encode(array(array('type' => 'error', 'msg' => 'Wystąpił błąd')));
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $errorsArray[] = array('type' => 'error', 'msg' => $err);
        }

        header('Content-Type: application/json');
        echo json_encode($errorsArray);
      }
    }
  }

  public function edytuj($params) {
    parent::_checkParams($params, 1, 'admin/aktualnosci/lista');
    $id = $params[0];

    parent::_checkType($id, 'A', 'admin/aktualnosci/');

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/aktualnosci.js');
    $this->_header->addScripts('file', 'ckeditor/ckeditor.js');
    $this->_header->adminHeader();
    $this->_top->adminTop();
    $this->_view->panel = $this->_view->renderPage('admin/aktualnosci/lista', false);

    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('tresc', true)
           ->post('nazwa')->val('notEmpty', 'nazwa')
           ->post('wlaczona')->val('notEmpty', 'włączona')
           ->post('pokazuj')->val('notEmpty', 'pokazuj w menu')
           ->post('adres-old');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $data['adres'] = $this->_modul['link_url'].'/'.$this->urlCharset($data['nazwa']);

        if ($data['adres'] != $data['adres-old'] && $this->_model->checkAdres($data['adres'])) {
          $this->_msg->add('error', 'Taka strona już istnieje');
          $this->reload();
        }

        $dataPage = array();
        $dataPage['page_status'] = $data['wlaczona'];
        $dataPage['page_show'] = $data['pokazuj'];

        $testPage = $this->_model->savePage($id, $dataPage);

        $dataTexts = array();
        $dataTexts['text_full'] = $data['tresc'];

        $testTexts = $this->_model->saveTexts($id, $dataTexts);

        $dataLinks = array();
        $dataLinks['link_name'] = $data['nazwa'];
        $dataLinks['link_url'] = $data['adres'];

        $testLinks = $this->_model->saveLinks($id, $dataLinks);

        if ($testPage && $testTexts && $testLinks) {
          $this->_msg->add('success', 'Zapisano zmiany.');
        } else {
          $this->_msg->add('error', 'Nie udało się zapisać wszystkich zmian.');
        }

        $this->reload();
      } else {
        $form->showErrors();
        $this->reload();
      }
    }

    $menuType = $this->_model->getRelationTypeByPageId($id);
    $this->_view->pageTypes = $this->_model->getPageTypes($menuType);
    $this->_view->page = $this->_model->getPage($id);

    $this->_view->renderPage('admin/aktualnosci/edytuj');

    $this->_footer->adminFooter();
  }

  public function usun($params) {
    parent::_checkParams($params, 1, 'admin/aktualnosci/lista');
    $id = $params[0];

    $test = $this->_model->delete('pages', 'page_id = :id', array(':id' => $id));

    if ($test) {
      header('Content-Type: application/json');
      echo json_encode(array(array('type' => 'success', 'msg' => 'Usunięto stronę')));
    } else {
      header('Content-Type: application/json');
      echo json_encode(array(array('type' => 'error', 'msg' => 'Wystąpił błąd')));
    }
  }

  public function getList() {
    parent::_isAjax();

    $aColumns = array('status', 'title', 'data', 'data_sort', 'opcje');

    /* Paging */
    $sLimit = '';

    if (isset($_GET['start']) && $_GET['length'] != -1) {
      $sLimit = "LIMIT ".intval($_GET['start']).", ".intval($_GET['length']);
    }

    /* Ordering */
    $sOrder = '';

    if (isset($_GET['order'])) {
      foreach ($_GET['order'] as $key => $value) {
        $sOrder .= $aColumns[$value['column']].' '.$value['dir'].', ';
      }

      if ($sOrder != '') {
        $sOrder = rtrim($sOrder, ', ');
        $sOrder = 'ORDER BY '.$sOrder;
      }
    } else {
      $sOrder = 'ORDER BY data_sort DESC';
    }

    /* Filtering */
    $sWhere = '';

    if (isset($_GET['search']) && $_GET['search']['value'] != '') {
      $sWhere = 'AND (';
      $str = htmlspecialchars($_GET['search']['value']);

      for ($i = 0; $i < count($aColumns); $i++) {
        if ($aColumns[$i] == 'status') {
          continue;
        } elseif ($aColumns[$i] == 'title') {
          $sWhere .= "link_name LIKE '%".$str."%' OR ";
        } elseif ($aColumns[$i] == 'data') {
          $sWhere .= "page_date_add LIKE '%".$str."%' OR ";
        }
      }

      $sWhere = substr_replace($sWhere, "", -3);
      $sWhere .= ')';
    }

    /* SQL queries */
    $sQuery = "
      SELECT
        SQL_CALC_FOUND_ROWS  page_id,
        link_name AS title,
        link_url AS url,
        relation_parent_id AS parent,
        page_status,
        page_show,
        page_date_add AS data,
        DATE_FORMAT(page_date_add, '%Y%m%d%H%i%s') AS data_sort
      FROM
        relations
          INNER JOIN pages ON relation_element_id = page_id
              INNER JOIN links ON link_page_id = relation_element_id
      WHERE
        relation_type = 'A'
      $sWhere
      $sOrder
      $sLimit
    ";
    $rResult = $this->_model->select($sQuery);

    /* Data set length after filtering */
    $sQuery = "
      SELECT FOUND_ROWS() AS ile
    ";
    $rResultFilterTotal = $this->_model->select($sQuery);
    $iFilteredTotal = $rResultFilterTotal[0]['ile'];

    /* Total data set length */
    $sQuery = "
      SELECT COUNT(page_id) AS ile
      FROM
        relations
          INNER JOIN pages ON relation_element_id = page_id
              INNER JOIN links ON link_page_id = relation_element_id
      WHERE
        relation_type = 'A'
    ";
    $rResultTotal = $this->_model->select($sQuery);
    $iTotal = $rResultTotal[0]['ile'];


    /*
     * Output
    */
    if (!isset($_GET['sEcho'])) {
      $_GET['sEcho'] = 0;
    }

    $output = array(
      "sEcho" => intval($_GET['sEcho']),
      "iTotalRecords" => $iTotal,
      "iTotalDisplayRecords" => $iFilteredTotal,
      "aaData" => array()
    );

    foreach ($rResult as $key => $value) {
      $row = array();

      for ($i = 0; $i < count($aColumns); $i++) {
        if ($aColumns[$i] == 'status') {
          $status = ($value['page_status'] == 0) ? 'N' : 'Y';
          $show = ($value['page_show'] == 0) ? 'N' : 'Y';

          $row[] = '<span id="status_'.$value['page_id'].'" class="status'.$status.'" title="Strona włączona"></span><span id="show_'.$value['page_id'].'" class="status'.$show.'" title="Strona wyswietlana w menu"></span>';
        } elseif ($aColumns[$i] == 'opcje') {
          $row[] = '<div class="opcje"><a class="edit" href="'.URL.'admin/aktualnosci/edytuj/'.$value['page_id'].'" title="Edytuj"><span class="none">Edycja</span></a><a class="delete" href="'.URL.'admin/aktualnosci/usun/'.$value['page_id'].'" title="Usuń"><span class="none">Usuń</span></a><a class="metatag" href="'.URL.'admin/metatagi/edytuj/'.$value['page_id'].'" title="Metatagi"><span class="none">Metatagi</span></a></div>';
        } else {
          $row[] = $value[$aColumns[$i]];
        }

      }

      $output['aaData'][] = $row;
    }

    echo json_encode($output);
  }
}