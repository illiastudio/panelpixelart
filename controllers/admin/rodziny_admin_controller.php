<?php

class Rodziny_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista() {

    $this->_header->adminDefaultScriptsAndStyles($display = true);
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/rodziny-lista.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('trodziny');

    $this->_view->display = $display;
    $this->_view->renderPage('admin/rodziny-lista/lista');

    $this->_footer->adminFooter();

  }

  public function getList() {
    parent::_isAjax();

    $model = new Rodziny_Admin_Model();

    $aColumns = array(
      'rodzina_id',
      'dziecko_id',
      'rodzina_nazwisko',
      'dziecko_imie',
      'klasa_nazwa',
      'sekcja_nazwa',
      'ojciec_mail',
      'ojciec_telefon',
      'matka_mail',
      'matka_telefon'
    );

    /* Paging */
    $sLimit = '';

    if (isset($_GET['start']) && $_GET['length'] != -1) {
      $sLimit = "LIMIT ".intval($_GET['start']).", ".intval($_GET['length']);
    }

    /* Ordering */
    $sOrder = '';

    if (isset($_GET['order'])) {
      foreach ($_GET['order'] as $key => $value) {
        $sOrder .= $aColumns[$value['column']].' '.$value['dir'].', ';
      }

      if ($sOrder != '') {
        $sOrder = rtrim($sOrder, ', ');
        $sOrder = 'ORDER BY '.$sOrder;
      }
    } else {
      $sOrder = 'ORDER BY data_sort DESC';
    }

    /* Filtering */
    $sWhere = '';

    if (isset($_GET['search']) && $_GET['search']['value'] != '') {
      $sWhere = 'WHERE (';
      $str = htmlspecialchars($_GET['search']['value']);

      for ($i = 0; $i < count($aColumns); $i++) {
        if (in_array($aColumns[$i], array('opcje', 'naleznosc', 'status', 'status_sort'))) {
          continue;
        } else if ($aColumns[$i] == 'ojciec_mail') {
          $sWhere .= "m1.opiekun_mail LIKE '%".$str."%' OR ";
        } else if ($aColumns[$i] == 'matka_mail') {
          $sWhere .= "m2.opiekun_mail LIKE '%".$str."%' OR ";
        } else if ($aColumns[$i] == 'ojciec_telefon') {
          $sWhere .= "m1.opiekun_telefon LIKE '%".$str."%' OR ";
        } else if ($aColumns[$i] == 'matka_telefon') {
          $sWhere .= "m2.opiekun_telefon LIKE '%".$str."%' OR ";
        } else {
          $sWhere .= $aColumns[$i]." LIKE '%".$str."%' OR ";
        }
      }

      $sWhere = substr_replace($sWhere, "", -3);
      $sWhere .= ')';
    }

    $sezon = $this->_model->getSezony(SEZON);

    if (empty($sWhere)) {
      $sWhere .= '
        WHERE zgloszenie_status IN ("1", "2")
          AND zgloszenie_date >= "'.$sezon[0]['sezon_start'].'"
          AND zgloszenie_date < "'.$sezon[0]['sezon_koniec'].'"
          AND (zgloszenie_date_end <= "'.$sezon[0]['sezon_koniec'].'" OR zgloszenie_date_end = "0000-00-00")
      ';
    } else {
      $sWhere .= '
          AND zgloszenie_status IN ("1", "2")
          AND zgloszenie_date >= "'.$sezon[0]['sezon_start'].'"
          AND zgloszenie_date < "'.$sezon[0]['sezon_koniec'].'"
          AND (zgloszenie_date_end <= "'.$sezon[0]['sezon_koniec'].'" OR zgloszenie_date_end = "0000-00-00")
      ';
    }

    /* SQL queries */
    $sQuery = "
      SELECT
        SQL_CALC_FOUND_ROWS
        rodzina_id,
        dziecko_id,
        rodzina_nazwisko,
        dziecko_imie,
        klasa_nazwa,
        sekcja_nazwa,
        m1.opiekun_mail AS ojciec_mail,
        m1.opiekun_telefon AS ojciec_telefon,
        m2.opiekun_mail AS matka_mail,
        m2.opiekun_telefon AS matka_telefon
      FROM zgloszenia
        INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
        INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id
        INNER JOIN klasy ON dziecko_klasa = klasa_id
        INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id
        INNER JOIN opiekunowie m1 ON m1.opiekun_rodzina_id = rodzina_id AND m1.opiekun_typ = 1
        INNER JOIN opiekunowie m2 ON m2.opiekun_rodzina_id = rodzina_id AND m2.opiekun_typ = 2
      $sWhere
        GROUP BY rodzina_id, dziecko_id, sekcja_nazwa
      $sOrder
      $sLimit
    ";
    $rResult = $this->_model->select($sQuery);

    /* Data set length after filtering */
    $sQuery = "
      SELECT FOUND_ROWS() AS ile
    ";
    $rResultFilterTotal = $this->_model->select($sQuery);
    $iFilteredTotal = $rResultFilterTotal[0]['ile'];

    /* Total data set length */
    $sQuery = "
      SELECT COUNT(rodzina_id) AS ile
      FROM
        rodziny
    ";
    $rResultTotal = $this->_model->select($sQuery);
    $iTotal = $rResultTotal[0]['ile'];


    /*
     * Output
    */
    if (!isset($_GET['sEcho'])) {
      $_GET['sEcho'] = 0;
    }

    $output = array(
      "sEcho" => intval($_GET['sEcho']),
      "iTotalRecords" => $iTotal,
      "iTotalDisplayRecords" => $iFilteredTotal,
      "aaData" => array()
    );

    foreach ($rResult as $key => $value) {
      $row = array();

      for ($i = 0; $i < count($aColumns); $i++) {
        $row[] = $value[$aColumns[$i]];
      }

      $output['aaData'][] = $row;
    }

    echo json_encode($output);
  }
}