<?php

class Sezon_Admin_Controller extends Admin_Controller
{
  public function __construct()
  {
    parent::__construct();
    parent::_isLogged();
  }

  public function index()
  {
    if (isset($_POST['dodaj-sezon'])) {
      $this->add();
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop('sezon');

    $this->_view->klasy = $this->_model->select('SELECT * FROM sezony ORDER BY sezon_nazwa ASC');

    $this->_view->renderPage('admin/sezon/index');

    $this->_footer->adminFooter();
  }
  public function add()
  {
    $form = new Form();

    $form
      ->post('sezon-start')->val('notEmpty', 'sezon-start')
      ->post('sezon-finish')->val('notEmpty', 'sezon-finish');

    if ($form->errorCheck()) {

      $data = $form->fetch();

      $dataStart =  $data['sezon-start'];
      $dataFinish =  $data['sezon-finish'];
      $fromAndTo = substr($data['sezon-start'], 2, 2) . "-" . substr($data['sezon-finish'], 2, 2);

      $checkSezon =
        $this->_model->select('SELECT 1 FROM sezony WHERE sezon_nazwa = ' . "'" . $fromAndTo . "'");

      if (!empty($checkSezon)) {
        $this->_msg->add('error', 'Taki rekord już istnieje', false, 'admin/sezon');
      }

      $dataTable = array(
        'sezon_nazwa' => $fromAndTo,
        'sezon_start' => $dataStart,
        'sezon_koniec' => $dataFinish
      );

      $insertToTable = $this->_model->insert('sezony', $dataTable);
      if ($insertToTable) {
        $this->_msg->add('success', "Dodano sezon: " . $fromAndTo, false, 'admin/sezon');
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }
      }
    }
  }
}
