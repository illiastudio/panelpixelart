<?php

class Zarzadzanie_Admin_Controller extends Admin_Controller
{
  public function __construct()
  {
    parent::__construct();
    parent::_isLogged();
    parent::_isRole('admin');
  }

  public function lista($display = true)
  {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/zarzadzanie.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('zarzadzanie');

    $this->_view->display = $display;

    $this->_view->rodzina = $this->_model->select('SELECT r.*,
     o.opiekun_imie AS namedad, o.opiekun_telefon AS teldad,
     o2.opiekun_imie AS namemom, o2.opiekun_telefon AS telmom
     FROM `rodziny` r 
     RIGHT JOIN `opiekunowie` o ON r.rodzina_id = o.opiekun_rodzina_id AND o.opiekun_typ = 1
     RIGHT JOIN `opiekunowie` o2 ON r.rodzina_id = o2.opiekun_rodzina_id AND o2.opiekun_typ = 2
     WHERE r.rodzina_id IS NOT NULL
     GROUP BY r.rodzina_id');

    $this->_view->renderPage('admin/zarzadzanie/lista');

    $this->_footer->adminFooter();
  }

  public function zarzadzaj($params)
  {
    $id = $params[0];

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/admin/zarzadzanie.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('zarzadzanie');

    $this->_view->rodzina = $this->_model->select('SELECT rodzina_id, rodzina_nazwisko FROM rodziny WHERE rodzina_id = :id LIMIT 1', array(':id' => $id));

    $this->_view->opiekunowie = $this->_model->getList('opiekunowie', 'opiekun_id, opiekun_rodzina_id, opiekun_imie, opiekun_mail, opiekun_telefon', array('opiekun_rodzina_id' => $id));

    $this->_view->dzieci = $this->_model->select('SELECT * FROM dzieci INNER JOIN klasy ON dziecko_klasa = klasa_id WHERE dziecko_rodzina_id = :id', array(':id' => $id));

    $this->_view->renderPage('admin/zarzadzanie/zarzadzaj');

    $this->_footer->adminFooter();
  }

  public function edytujopiekuna($params)
  {
    $id = $params[0];

    $this->_view->opiekun = $this->_model->select('SELECT opiekun_id, opiekun_rodzina_id, opiekun_rodzina_id, opiekun_imie, opiekun_mail, opiekun_telefon FROM opiekunowie WHERE opiekun_id = :id', array(':id' => $id));
    $this->_view->rodzina = $this->_model->select('SELECT rodzina_nazwisko FROM rodziny WHERE rodzina_id = :id', array(':id' => $this->_view->opiekun[0]['opiekun_rodzina_id']));

    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('opiekun_imie')->val('notEmpty', 'imię')
        ->post('opiekun_mail')->val('notEmpty', 'mail')->val('isMail', 'mail')
        ->post('opiekun_telefon')->val('notEmpty', 'telefon');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $this->_model->update('opiekunowie', $data, 'opiekun_id = :id', array(':id' => $id));
        $this->_msg->add('success', 'Zapisano zmiany', false, 'admin/zarzadzanie/zarzadzaj/' . $this->_view->opiekun[0]['opiekun_rodzina_id']);
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }

        $this->_msg->add('error', 'Nie udało się wprowadzić zmian', false, 'admin/zarzadzanie/zarzadzaj/' . $this->_view->opiekun[0]['opiekun_rodzina_id']);
      }
    }

    $this->_view->renderPage('admin/zarzadzanie/opiekun');
  }

  public function edytujdziecko($params)
  {
    $id = $params[0];

    $this->_view->dziecko = $this->_model->select('SELECT * FROM dzieci INNER JOIN klasy ON dziecko_klasa = klasa_id WHERE dziecko_id = :id', array(':id' => $id));

    $this->_view->rodzina = $this->_model->select('SELECT rodzina_nazwisko FROM rodziny WHERE rodzina_id = :id', array(':id' => $this->_view->dziecko[0]['dziecko_rodzina_id']));

    $this->_view->klasy = $this->_model->select('SELECT * FROM klasy');

    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('dziecko_imie')->val('notEmpty', 'imię')
        ->post('dziecko_klasa')->val('notEmpty', 'klasa');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $this->_model->update('dzieci', $data, 'dziecko_id = :id', array(':id' => $id));
        $this->_msg->add('success', 'Zapisano zmiany', false, 'admin/zarzadzanie/zarzadzaj/' . $this->_view->dziecko[0]['dziecko_rodzina_id']);
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }

        $this->_msg->add('error', 'Nie udało się wprowadzić zmian', false, 'admin/zarzadzanie/zarzadzaj/' . $this->_view->dziecko[0]['dziecko_rodzina_id']);
      }
    }

    $this->_view->renderPage('admin/zarzadzanie/dziecko');
  }

  public function opiekunhaslo($params)
  {
    $id = $params[0];

    $this->_view->opiekun = $this->_model->select('SELECT opiekun_id, opiekun_rodzina_id, opiekun_rodzina_id, opiekun_imie, opiekun_mail, opiekun_telefon FROM opiekunowie WHERE opiekun_id = :id', array(':id' => $id));
    $this->_view->rodzina = $this->_model->select('SELECT rodzina_nazwisko FROM rodziny WHERE rodzina_id = :id', array(':id' => $this->_view->opiekun[0]['opiekun_rodzina_id']));

    if (isset($_POST['zapisz']) || isset($_POST['generuj'])) {
      if (isset($_POST['zapisz'])) {
        $form = new Form();
        $form->post('opiekun_haslo')->val('notEmpty', 'hasło')->val('minlength', 6, 'hasło');

        if ($form->errorCheck()) {
          $data = $form->fetch();
          $haslo = $data['opiekun_haslo'];
        } else {
          $errors = $form->getErrors();
          $errorsArray = array();
          $errorsCount = 0;

          foreach ($errors as $err) {
            $this->_msg->add('error', $err, false);
          }

          $this->_msg->add('error', 'Nie udało się wprowadzić zmian', false, 'admin/zarzadzanie/zarzadzaj/' . $this->_view->opiekun[0]['opiekun_rodzina_id']);
        }
      }

      if (isset($_POST['generuj'])) {
        $random = new Random();
        $haslo = $random->create(6);
      }

      $hasloHash = Hash::create('sha512', HASH_OPIEKUN_PASSWORD_KEY . $haslo, HASH_SITE_KEY);

      $mail = new phpmailer();
      $mail->CharSet = "UTF-8";
      $mail->SetLanguage("pl", "libs/");
      $mail->IsHTML(true);

      // SMTP DO TESTOW Z LOCALHOSTA
      $mail->Mailer = 'smtp';
      $mail->addCustomHeader('MIME-Version: 1.0');
      $mail->addCustomHeader('Content-Type: text/html; charset=UTF-8');
      $mail->IsSMTP();
      $mail->SMTPDebug = 2;
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'tls';
      $mail->SMTPAutoTLS = false;
      $mail->Host = MAIL_HOST;
      $mail->Port = MAIL_PORT;
      $mail->SMTPKeepAlive = true;
      $mail->Username = MAIL_USER;
      $mail->Password = MAIL_PASS;

      // $mail->From = $user['opiekun_mail'];
      $mail->From = MAIL_FROM;
      // $mail->FromName = $user['opiekun_mail'];
      $mail->FromName = MAIL_FROM;
      $mail->Subject = 'Panel Rodzica - nowe hasło';
      $mail->Body = 'Nowe hasło: <br>' . trim($haslo);
      $mail->AddAddress($this->_view->opiekun[0]['opiekun_mail']);

      // $mail->AddAddress(MAIL_TEST);
      if ($mail->Send()) {
        $this->_model->update('opiekunowie', array('opiekun_password' => $hasloHash, 'opiekun_password_reset' => '""'), 'opiekun_mail = :login', array(':login' => $this->_view->opiekun[0]['opiekun_mail']));

        $this->_msg->add('success', 'Wysłano nowe hasło.', false, 'admin/zarzadzanie/zarzadzaj/' . $this->_view->opiekun[0]['opiekun_rodzina_id']);
      } else {
        $this->_msg->add('error', 'Nie udało się wysłać wiadomości.' . MAIL_USER, false, 'admin/zarzadzanie/zarzadzaj/' . $this->_view->opiekun[0]['opiekun_rodzina_id']);
      }

      $mail->ClearAddresses();
      $mail->ClearAttachments();
      $mail->SmtpClose();
    }

    $this->_view->renderPage('admin/zarzadzanie/haslo');
  }

  public function zgloszenia($params)
  {
    // parent::_checkParams($params, 1, 'admin/lista/');
    $id = $params[0];

    if (!empty($params[1])) {
      $sezon = $params[1];
    } else {
      $sezon = SEZON;
    }

    if (isset($_POST['zapisz'])) {
      $this->_save($id, $sezon);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/admin/zarzadzanie.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('zarzadzanie');
    $this->_view->sezony = $this->_model->getSezony();
    $this->_view->thisSezon = $sezon;
    $this->_view->rodzinaId = $id;

    $this->_view->rodzina = $this->_model->select('SELECT rodzina_id, rodzina_nazwisko FROM rodziny WHERE rodzina_id = :id LIMIT 1', array(':id' => $id));

    $sezonInfo = $this->_model->getSezony($sezon);
    $prevSezon = $this->_model->getPrevSezon($sezon);

    $zgloszenia = $this->_model->select(
      '
      SELECT zgloszenie_id, zgloszenie_group_id, dziecko_id, dziecko_imie, klasa_nazwa, klasa_id, zgloszenie_sekcja, sekcja_nazwa, taryfikator_oplata AS skladka, zgloszenie_propozycja, zgloszenie_uwagi_id, zgloszenie_date, zgloszenie_date_end, zgloszenie_status
      FROM zgloszenia
      INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
      INNER JOIN rodziny ON rodzina_id = dziecko_rodzina_id
      #INNER JOIN klasy ON klasa_id = dziecko_klasa
      INNER JOIN klasy ON klasa_id = zgloszenie_dziecko_klasa_id
      INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id
      INNER JOIN taryfikator ON (taryfikator_sekcja = zgloszenie_sekcja AND taryfikator_klasa = zgloszenie_dziecko_klasa_id AND taryfikator_sezon = :sezon)
      LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id
      WHERE rodzina_id = :id
      AND (zgloszenie_skladka > 0)
      AND (potwierdzenie_status = "1")
      AND (potwierdzenie_wyslane = "1")

      AND (
        (
          ((zgloszenie_date BETWEEN :start AND :end) AND (zgloszenie_date_end BETWEEN :start AND :end))
          OR ((zgloszenie_date BETWEEN :start AND :end) AND zgloszenie_date_end = "0000-00-00")
        ) OR zgloszenie_date_add BETWEEN :start AND :end
		OR (zgloszenie_date >= :start AND zgloszenie_date > zgloszenie_date_end AND zgloszenie_date < :end)
		OR (zgloszenie_date < :start AND zgloszenie_date > :prevEnd)
      )

      ORDER BY zgloszenie_status ASC, zgloszenie_date DESC, zgloszenie_date_end DESC',
      array(':id' => $id, ':start' => $sezonInfo[0]['sezon_start'], ':end' => $sezonInfo[0]['sezon_koniec'], ':sezon' => $sezon, ':prevEnd' => $prevSezon[0]['sezon_koniec'])
    );

    $arr = array();

    foreach ($zgloszenia as $key => $val) {
      $arr[$val['zgloszenie_group_id']][] = $val;
    }

    $this->_view->zgloszenia = $arr;

    /* TYLKO OSTATNIE ZGLOSZENIE
          AND zgloszenie_group_id IN (
            SELECT MAX(zgloszenie_group_id)
            FROM zgloszenia
            INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
            WHERE dziecko_rodzina_id = :id)
    */

    $test = $this->_model->select('SELECT 1 FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id WHERE dziecko_rodzina_id = :id AND zgloszenie_status = "1" AND (zgloszenie_skladka > 0) AND zgloszenie_id NOT IN (SELECT potwierdzenie_zgloszenie_id FROM potwierdzenia)', array(':id' => $id));

    if (!empty($test)) {
      $this->_view->issetNew = true;
    } else {
      $this->_view->issetNew = false;
    }

    $this->_view->renderPage('admin/zarzadzanie/edytuj');

    $this->_footer->adminFooter();
  }

  private function _save($id, $sezon)
  {
    $test = 0;

    foreach ($_POST['zgloszenie_date'] as $key => $val) {
      $arr = array();
      $arr['zgloszenie_date'] = $_POST['zgloszenie_date'][$key];
      $arr['zgloszenie_propozycja'] = intval($_POST['zgloszenie_propozycja'][$key]);

      if (!empty($_POST['zgloszenie_date_end'][$key])) {
        $arr['zgloszenie_date_end'] = $_POST['zgloszenie_date_end'][$key];
        $arr['zgloszenie_status'] = '2';
      } else {
        if ($_POST['aktualne'][$key]) {
          $arr['zgloszenie_date_end'] = '0000-00-00';
          $arr['zgloszenie_status'] = '1';
        } else {
          $arr['zgloszenie_date_end'] = '0000-00-00';
          $arr['zgloszenie_status'] = '0';
        }
      }

      if ($_POST['anuluj'][$key] == 1) {
        $arr['zgloszenie_status'] = '3';
      }

      $this->_model->update('zgloszenia', $arr, 'zgloszenie_id = :id', array(':id' => $key));
    }

    $this->_updateSaldo($id);

    $this->_msg->add('success', 'Zapisano zmiany', false, 'admin/zarzadzanie/zgloszenia/' . $id . '/' . $sezon);
  }

  private function _updateSaldo($id)
  {
    $date = START_DATE;
    $end_date = date('Y-m-01', time());

    while (strtotime($date) <= strtotime($end_date)) {
      $tmp = $this->_model->select(
        '
        SELECT dziecko_rodzina_id AS ps_rodzina_id, sum(zgloszenie_propozycja) AS ps_do_zaplaty, GROUP_CONCAT(zgloszenie_id) AS ps_zgloszenia
        FROM zgloszenia
        INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
        LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id
        WHERE dziecko_rodzina_id = :id
        AND (zgloszenie_skladka > 0)
        AND (potwierdzenie_status = "1")
        AND (potwierdzenie_wyslane = "1")
        AND (
          (zgloszenie_status = "1"
            AND zgloszenie_date <= :data
            AND zgloszenie_date != "0000-00-00"
          )
          OR (zgloszenie_status IN ("0", "2")
            AND (zgloszenie_date <= :data AND zgloszenie_date != "0000-00-00")
            AND (
              (zgloszenie_date_end >= :data AND zgloszenie_date_end != "0000-00-00")
              OR (YEAR(zgloszenie_date_end) = YEAR(:data - INTERVAL 1 MONTH)
                AND MONTH(zgloszenie_date_end) = MONTH(:data - INTERVAL 1 MONTH)
                AND DAYOFMONTH(zgloszenie_date_end) > 20
              )
            )
          )
        )
        GROUP BY dziecko_rodzina_id',
        array(':id' => $id, ':data' => $date)
      );

      $arr = array();

      if (!empty($tmp)) {
        $arr = $tmp[0];
      } else {
        $arr['ps_rodzina_id'] = $id;
        $arr['ps_do_zaplaty'] = 0;
      }

      $arr['ps_okres'] = $date;

      if (in_array(date('m', strtotime($date)), array('07', '08'))) {
        $date = date("Y-m-d", strtotime("+1 month", strtotime($date)));
      } else {
        $this->_model->delete('platnosci_saldo', 'ps_rodzina_id = :id AND ps_okres = :data', array(':id' => $id, ':data' => $date));
        $this->_model->insert('platnosci_saldo', $arr);

        $date = date("Y-m-d", strtotime("+1 month", strtotime($date)));
      }
    }
  }

  public function getList()
  {
    parent::_isAjax();

    $model = new Rodziny_Admin_Model();

    $aColumns = array(
      'rodzina_id',
      'rodzina_nazwisko',
      'ojciec_mail',
      'ojciec_telefon',
      'matka_mail',
      'matka_telefon',
      'status',
      'status_sort'
    );

    /* Paging */
    $sLimit = '';

    if (isset($_GET['start']) && $_GET['length'] != -1) {
      $sLimit = "LIMIT " . intval($_GET['start']) . ", " . intval($_GET['length']);
    }

    /* Ordering */
    $sOrder = '';

    if (isset($_GET['order'])) {
      foreach ($_GET['order'] as $key => $value) {
        $sOrder .= $aColumns[$value['column']] . ' ' . $value['dir'] . ', ';
      }

      if ($sOrder != '') {
        $sOrder = rtrim($sOrder, ', ');
        $sOrder = 'ORDER BY ' . $sOrder;
      }
    } else {
      $sOrder = 'ORDER BY data_sort DESC';
    }

    $daty = $this->_model->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => SEZON));

    if (empty($daty)) {
      return array();
    }

    $daty = $daty[0];

    $sezonStart = $daty['sezon_start'];
    $sezonKoniec = $daty['sezon_koniec'];

    /* Filtering */
    $sWhere = '';

    if (isset($_GET['search']) && $_GET['search']['value'] != '') {
      $sWhere = 'WHERE (';
      $str = htmlspecialchars($_GET['search']['value']);

      for ($i = 0; $i < count($aColumns); $i++) {
        if (in_array($aColumns[$i], array('opcje', 'naleznosc', 'status', 'status_sort'))) {
          continue;
        } else if ($aColumns[$i] == 'ojciec_mail') {
          $sWhere .= "m1.opiekun_mail LIKE '%" . $str . "%' OR ";
        } else if ($aColumns[$i] == 'matka_mail') {
          $sWhere .= "m2.opiekun_mail LIKE '%" . $str . "%' OR ";
        } else if ($aColumns[$i] == 'ojciec_telefon') {
          $sWhere .= "m1.opiekun_telefon LIKE '%" . $str . "%' OR ";
        } else if ($aColumns[$i] == 'matka_telefon') {
          $sWhere .= "m2.opiekun_telefon LIKE '%" . $str . "%' OR ";
        } else {
          $sWhere .= $aColumns[$i] . " LIKE '%" . $str . "%' OR ";
        }
      }

      $sWhere = substr_replace($sWhere, "", -3);
      $sWhere .= ')';
    }

    /* SQL queries */
    /*
    $sQuery = "
      SELECT
        SQL_CALC_FOUND_ROWS
        rodzina_id,
        rodzina_nazwisko,
        m1.opiekun_mail AS ojciec_mail,
        m1.opiekun_telefon AS ojciec_telefon,
        m2.opiekun_mail AS matka_mail,
        m2.opiekun_telefon AS matka_telefon,
        COALESCE(NULL, do_zaplaty, 0) AS naleznosc,
        COALESCE(pb_bilans, 0) pb_bilans,
        (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND zaplacono_sezon IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort
      FROM
       rodziny
        LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id
        LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id
        INNER JOIN opiekunowie m1 ON m1.opiekun_rodzina_id = rodzina_id AND m1.opiekun_typ = 1
        INNER JOIN opiekunowie m2 ON m2.opiekun_rodzina_id = rodzina_id AND m2.opiekun_typ = 2
        LEFT JOIN (
          SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty, zaplacono_sezon
          FROM (
            SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty
            FROM platnosci_saldo
            WHERE (ps_okres <= '".$sezonKoniec."')
            GROUP BY ps_rodzina_id) ps
          LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono
            FROM platnosci
            WHERE (platnosc_data_operacji <= '".$sezonKoniec."' + INTERVAL 2 MONTH)
            GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id
          LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono_sezon
            FROM platnosci
            WHERE (platnosc_data_operacji BETWEEN '".$sezonStart."' AND '".$sezonKoniec."' + INTERVAL 2 MONTH)
            GROUP BY platnosc_rodzina_id) p2 ON ps.ps_rodzina_id = p2.platnosc_rodzina_id
      ) tmp ON rodzina_id = pb_rodzina_id
      $sWhere
        GROUP BY rodzina_id
      $sOrder
      $sLimit
    ";
    */
    /* SQL queries */
    $sQuery = "
      SELECT
        SQL_CALC_FOUND_ROWS
        rodzina_id,
        rodzina_nazwisko,
        m1.opiekun_mail AS ojciec_mail,
        m1.opiekun_telefon AS ojciec_telefon,
        m2.opiekun_mail AS matka_mail,
        m2.opiekun_telefon AS matka_telefon,
        COALESCE(NULL, do_zaplaty, 0) AS naleznosc,
        COALESCE(pb_bilans, 0) pb_bilans,
        (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort
      FROM
       rodziny
        LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id
        LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id
        INNER JOIN opiekunowie m1 ON m1.opiekun_rodzina_id = rodzina_id AND m1.opiekun_typ = 1
        INNER JOIN opiekunowie m2 ON m2.opiekun_rodzina_id = rodzina_id AND m2.opiekun_typ = 2
        LEFT JOIN (
          SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty
          FROM (
            SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty
            FROM platnosci_saldo
            WHERE (ps_okres BETWEEN '" . $sezonStart . "' AND '" . $sezonKoniec . "')
            GROUP BY ps_rodzina_id) ps
          LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono
            FROM platnosci
            WHERE (platnosc_data_operacji BETWEEN '" . $sezonStart . "' AND '" . $sezonKoniec . "' + INTERVAL 2 MONTH)
            GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id
      $sWhere
        GROUP BY rodzina_id
      $sOrder
      $sLimit
    ";
    $rResult = $this->_model->select($sQuery);

    /* Data set length after filtering */
    $sQuery = "
      SELECT FOUND_ROWS() AS ile
    ";
    $rResultFilterTotal = $this->_model->select($sQuery);
    $iFilteredTotal = $rResultFilterTotal[0]['ile'];

    /* Total data set length */
    $sQuery = "
      SELECT COUNT(rodzina_id) AS ile
      FROM
        rodziny
    ";
    $rResultTotal = $this->_model->select($sQuery);
    $iTotal = $rResultTotal[0]['ile'];


    /*
     * Output
    */
    if (!isset($_GET['sEcho'])) {
      $_GET['sEcho'] = 0;
    }

    $output = array(
      "sEcho" => intval($_GET['sEcho']),
      "iTotalRecords" => $iTotal,
      "iTotalDisplayRecords" => $iFilteredTotal,
      "aaData" => array()
    );

    foreach ($rResult as $key => $value) {
      $row = array();

      for ($i = 0; $i < count($aColumns); $i++) {
        if ($aColumns[$i] == 'status') {
          switch ($value['status_sort']) {
            case 0:
              $row[] = '<span title="Niedopłata" data-status="0" class="platnosc-err"></span>';
              break;
            case 1:
              $row[] = '<span title="Nadpłata" data-status="2" class="platnosc-plus"></span>';
              break;
            case 2:
              $row[] = '<span title="Saldo ok" data-status="3" class="platnosc-ok"></span>';
              break;
            case 3:
              $row[] = '<span title="Brak należności" data-status="1" class="platnosc-empty"></span>';
              break;
          }
        } else {
          $row[] = $value[$aColumns[$i]];
        }
      }

      $output['aaData'][] = $row;
    }

    echo json_encode($output);
  }

  public function dodajdziecko($params)
  {
    if (isset($_POST['dodaj'])) {
      $form = new Form();
      $form->post('dziecko_imie')->val('notEmpty', 'imię')
        ->post('dziecko_klasa')->val('notEmpty', 'klasa');

      if ($form->errorCheck()) {
        $data = $form->fetch();
        $data['dziecko_rodzina_id'] = $params[0];
        // $this->_model->update('dzieci', $data, 'dziecko_id = :id', array(':id' => $id));
        $this->_model->insert('dzieci', $data);
        $this->_msg->add('success', 'Zapisano zmiany', false, 'admin/zarzadzanie/zarzadzaj/' . $params[0]);
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }

        $this->_msg->add('error', 'Nie udało się wprowadzić zmian', false, 'admin/zarzadzanie/zarzadzaj/' . $params[0]);
      }
    }

    $this->_view->klasy = $this->_model->select('SELECT * FROM klasy');
    $this->_view->rodzinaId = $params[0];
    $this->_view->renderPage('admin/zarzadzanie/dodaj-dziecko');
  }

  public function dodajrodzine()
  {
    if (isset($_POST['dodaj'])) {
      $form = new Form();
      $form->post('nazwisko')->val('notEmpty', 'nazwisko')
        ->post('matka_imie')->val('notEmpty', 'imię matki')
        ->post('matka_mail')->val('notEmpty', 'mail matki')
        ->post('matka_telefon')->val('notEmpty', 'telefon matki')
        ->post('ojciec_imie')->val('notEmpty', 'imię ojca')
        ->post('ojciec_mail')->val('notEmpty', 'mail ojca')
        ->post('ojciec_telefon')->val('notEmpty', 'telefon ojca');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $insertRodzina = array();
        $insertRodzina['rodzina_nazwisko'] = $data['nazwisko'];
        $insertRodzina['rodzina_date_add'] = date("Y-m-d H:i:s");
        unset($data['nazwisko']);

        $insTest = $this->_model->insert('rodziny', $insertRodzina);
        $rodzinaId = $this->_model->getlastInsertId();

        if ($insTest) {
          $random = new Random();
          $_password1 = $random->create(6);
          $password1 = Hash::create('sha512', HASH_OPIEKUN_PASSWORD_KEY . $_password1, HASH_SITE_KEY);
          $mailPapa = $data['ojciec_mail'];
          $insTest2 = $this->_model->insert('opiekunowie', array(
            'opiekun_rodzina_id' => $rodzinaId,
            'opiekun_mail' => $data['ojciec_mail'],
            'opiekun_imie' => $data['ojciec_imie'],
            'opiekun_telefon' => $data['ojciec_telefon'],
            'opiekun_date_add' => date("Y-m-d H:i:s"),
            'opiekun_typ' => 1,
            'opiekun_password' => $password1
          ));

          $random = new Random();
          $_password2 = $random->create(6);
          $password2 = Hash::create('sha512', HASH_OPIEKUN_PASSWORD_KEY .  $_password2, HASH_SITE_KEY);
          $mailMama = $data['matka_mail'];
          $insTest3 = $this->_model->insert('opiekunowie', array(
            'opiekun_rodzina_id' => $rodzinaId,
            'opiekun_mail' => $data['matka_mail'],
            'opiekun_imie' => $data['matka_imie'],
            'opiekun_telefon' => $data['matka_telefon'],
            'opiekun_date_add' => date("Y-m-d H:i:s"),
            'opiekun_typ' => 2,
            'opiekun_password' => $password2
          ));

          if (!$insTest2 || !$insTest3) {
            $this->_msg->add('error', 'Nie udało się prawidłowo dodać rodziny', false, 'admin/zarzadzanie/');
          }
        } else {
          $this->_msg->add('error', 'Nie udało się dodać rodziny', false, 'admin/zarzadzanie/');
        }

        $this->sendMail($mailPapa, 'Dane logowania do panelu rodzica <br> Login: '
          . $mailPapa . ' <br> Hasło: ' . $_password1 . '<br> link: ' . DOMAIN . '/login');

        $this->sendMail($mailMama, ' Dane logowania do panelu rodzica <br> Login: '
          . $mailMama . ' <br>Hasło: ' . $_password2 . '<br> link: ' . DOMAIN . '/login');

        $this->_msg->add('success', 'Zapisano zmiany', false, 'admin/zarzadzanie/');
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }

        $this->_msg->add('error', 'Nie udało się dodać rodziny', false, 'admin/zarzadzanie/');
      }
    }
    $this->_view->renderPage('admin/zarzadzanie/dodaj-rodzine');
  }
  public function sendMail($sendAdress, $body, $FromName = '')
  {
    $mail = new phpmailer();
    $mail->CharSet = "UTF-8";
    $mail->SetLanguage("pl", "libs/");
    $mail->IsHTML(true);

    // SMTP DO TESTOW Z LOCALHOSTA
    $mail->Mailer = 'smtp';
    $mail->addCustomHeader('MIME-Version: 1.0');
    $mail->addCustomHeader('Content-Type: text/html; charset=UTF-8');
    $mail->IsSMTP();
    $mail->SMTPDebug = 2;
    $mail->SMTPAuth = true;
    // $mail->SMTPSecure = 'tls';
    $mail->SMTPAutoTLS = false;
    $mail->Host = MAIL_HOST;
    $mail->Port = MAIL_PORT;
    $mail->SMTPKeepAlive = true;
    $mail->Username = MAIL_USER;
    $mail->Password = MAIL_PASS;


    $mail->From = "illia@studiograficzne.com";
    $mail->FromName = $FromName;
    $mail->Subject = MAIL_SUBJECT;
    $mail->Body = $body;
    $mail->AddAddress($sendAdress);

    if ($mail->Send()) {
      $this->_msg->add('success', 'Wysłano nowe hasło na mail');
    }

    $mail->ClearAddresses();
    $mail->ClearAttachments();
    $mail->SmtpClose();
    return true;
  }
}
