<?php

class Cms_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
    parent::_isRole('admin');
  }

  public function lista() {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop('cms');

    $this->_view->lista = $this->_model->select('SELECT * FROM cms');

    $this->_view->renderPage('admin/cms/lista');
    $this->_footer->adminFooter();
  }

  public function edytuj($params) {
    if (isset($_POST['zapisz'])) {
      $this->_zapisz($params);
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/ckeditor/ckeditor.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('cms');

    $this->_view->id = $params[0];
    $this->_view->item = $this->_model->select('SELECT * FROM cms WHERE cms_id = :id', array(':id' => $params[0]));

    $this->_view->renderPage('admin/cms/edytuj');
    $this->_footer->adminFooter();
  }

  private function _zapisz($params) {
      $form = new Form();
      $form->post('cms_title')->val('notEmpty', 'tytul')
           ->post('cms_content');

    if ($form->errorCheck()) {
      $data = $form->fetch();
      $data['cms_content'] = $data['cms_content'];
      $test = $this->_model->update('cms', $data, 'cms_id = :id', array(':id' => $params[0]));

      if ($test) {
        $this->_msg->add('success', 'Zapisano treść', false, 'admin/cms/edytuj/'.$params[0]);
      }
    } else {
      $errors = $form->getErrors();
      $errorsArray = array();
      $errorsCount = 0;

      foreach ($errors as $err) {
        $this->_msg->add('error', $err, false);
      }
    }
  }
}