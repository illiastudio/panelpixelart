<?php

class Lista_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $this->_model = new Lista_Admin_Model();
  }

  public function lista() {
    if (isset($_POST['zmien_date'])) {
      if (!empty($_POST['date'])) {
        $data = $_POST['date'];

        $regex = "/^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$/";

        if (preg_match($regex, $data)) {
          $zgloszeniaStr = '';

          foreach ($_POST['check'] as $key => $val) {
            $zgloszeniaStr .= $val.', ';
          }

          $zgloszeniaStr = rtrim($zgloszeniaStr, ', ');

          if (empty($zgloszeniaStr)) {
            $_SESSION['selectedDate'] = $data;

            $this->_msg->add('error', 'Nie zaznaczyłeś żadnych zgłoszeń', false, 'admin/lista/');
          }

          $this->_model->update('zgloszenia', array('zgloszenie_date' => $data), 'zgloszenie_id IN ('.$zgloszeniaStr.')');

          // $okresy = $this->_model->select('select ps_okres from platnosci_saldo where ps_rodzina_id = ')

          $this->_msg->add('success', 'Zmieniono datę wejścia w życie wybranych zgłoszeń.', false, 'admin/lista/');
        } else {
          $this->_msg->add('error', 'Podana data jest nieprawidłowa.', false, 'admin/lista/');
        }
      } else {
        $this->_msg->add('error', 'Musisz podać datę.', false, 'admin/lista/');
      }
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/lista_zgloszen.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('lista');

    $this->_view->lista = $this->_model->getZgloszeniaList();

    $this->_view->renderPage('admin/lista_zgloszen/lista');

    $this->_footer->adminFooter();
  }
}