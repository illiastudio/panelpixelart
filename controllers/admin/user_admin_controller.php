<?php

class User_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function profil() {
    $this->_zapisz();

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop('user');

    $this->_view->mail = $this->_model->getUserMail($_SESSION['user_id']);
    $this->_view->renderPage('admin/user');

    $this->_footer->adminFooter();
  }

  private function _zapisz() {
    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('mail')->val('isMail', 'e-mail')
           ->post('old-mail')
           ->post('new-password')->val('minlength', 6, 'hasło')
           ->post('confirm-password')->val('equal', 'new-password', 'Hasła nie są takie same.');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        if ($data['mail'] != $data['old-mail']) {
          $testMail = $this->_model->changeUserMail($_SESSION['user_id'], $data['mail']);
        }

        if (!empty($data['new-password'])) {
          $password = HASH_PASSWORD_KEY.$data['new-password'];
          $password = Hash::create('sha512', $password, HASH_SITE_KEY);

          $testPassword = $this->_model->changeUserPassword($_SESSION['user_id'], $password);
        }

        if (isset($testMail)) {
          if ($testMail) {
            $this->_msg->add('success', 'Zmieniono adres e-mail.');
          } else {
            $this->_msg->add('error', 'Nie udało się zmienić adresu e-mail.');
          }
        }

        if (isset($testPassword)) {
          if ($testPassword) {
            $this->_msg->add('success', 'Zmieniono hasło.');
          } else {
            $this->_msg->add('error', 'Nie udało się zmienić hasła.');
          }
        }

        $this->reload();
      } else {
        $form->showErrors();
        $this->reload();
      }
    }
  }

}