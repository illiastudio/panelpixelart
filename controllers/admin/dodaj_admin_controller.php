<?php

class Dodaj_Admin_Controller extends Admin_Controller {

  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista() {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addStyles('file', 'public/css/select2.min.css');
    $this->_header->addStyles('file', 'public/css/styles.css');

    $this->_header->addScripts('file', 'public/js/select2.full.min.js');
    $this->_header->addScripts('file', 'public/js/admin/dodaj.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('dodaj');

    $sezon = $this->_model->getSezony(SEZON);
    $this->_view->dzieciLista = $this->_model->select('
      SELECT DISTINCT
        dziecko_id,
        dziecko_imie,
        rodzina_nazwisko,
        klasa_nazwa
      FROM dzieci
      INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id
      INNER JOIN klasy ON dziecko_klasa = klasa_id
      ORDER BY rodzina_nazwisko ASC, dziecko_imie ASC
    ', array(':start' => $sezon[0]['sezon_start'], ':end' => $sezon[0]['sezon_koniec']));

    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('imie_nazwisko')->val('notEmpty', 'imie i nazwisko')
           ->post('klasa')->val('notEmpty', 'klasa')
           ->post('sekcja_sportowa')->val('notEmpty', 'sekcja sportowa')
           ->post('taryfa')->val('notEmpty', 'taryfa')
           ->post('propozycja')->val('notEmpty', 'propozycja')
           ->post('zgloszenie_date')->val('notEmpty', 'data');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $tmp = $this->_model->select('SELECT zgloszenie_group_id
          FROM zgloszenia
          WHERE zgloszenie_dziecko_id IN (
          SELECT dziecko_rodzina_id
          FROM dzieci
          WHERE dziecko_id = :id) AND zgloszenie_status = "1"
          LIMIT 1',
        array(':id' => $data['imie_nazwisko']));

        if (!empty($tmp)) {
          $grupa = $tmp[0]['zgloszenie_group_id'];
        } else {
          $tmpMax = $this->_model->select('SELECT MAX(zgloszenie_group_id) AS max FROM zgloszenia');
          $grupa = $tmpMax[0]['max'] + 1;
        }

        if (empty($data['propozycja'])) {
          $propozycja = $data['taryfa'];
        } else {
          $propozycja = $data['propozycja'];
        }

        $zgloszenieData = date("Y-m-d H:i:s", time());

        $arr = array(
          'zgloszenie_dziecko_id' => $data['imie_nazwisko'],
          'zgloszenie_dziecko_klasa_id' => $data['klasa'],
          'zgloszenie_sekcja' => $data['sekcja_sportowa'],
          'zgloszenie_skladka' => $data['taryfa'],
          'zgloszenie_propozycja' => $propozycja,
          'zgloszenie_uwagi_id' => 0,
          'zgloszenie_date_add' => $zgloszenieData,
          'zgloszenie_group_id' => $grupa,
          'zgloszenie_date' => $data['zgloszenie_date'],
          'zgloszenie_status' => '1'
        );

        $test = $this->_model->insert('zgloszenia', $arr);
        $zgloszenie_id = $this->_model->getlastInsertId();

        if ($test) {
          // $this->_model->insert('potwierdzenia', array('potwierdzenie_zgloszenie_id' => $zgloszenie_id, 'potwierdzenie_status' => 1, 'potwierdzenie_wyslane' => 1));
          $this->_msg->add('success', 'Dodano zgłoszenie', false, 'admin/dodaj');
        } else {
          $this->_msg->add('error', 'Nie udało się dodać zgłoszenia', false, 'admin/dodaj');
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }
      }
      // die;
    }

    $this->_view->renderPage('admin/dodaj/zgloszenie');

    $this->_footer->adminFooter();
  }

  public function ustawKlase() {
    parent::_isAjax();

    $id = (int) $_POST['dziecko_id'];

    $klasa = $this->_model->select('SELECT klasa_nazwa AS dziecko_klasa, klasa_id FROM dzieci INNER JOIN klasy ON dziecko_klasa = klasa_id WHERE dziecko_id = :id', array(':id' => $id));

    $sekcje = $this->_model->select('SELECT sekcja_id, sekcja_nazwa FROM sekcje INNER JOIN taryfikator ON sekcja_id = taryfikator_sekcja WHERE taryfikator_klasa = :klasa AND taryfikator_sezon = :sezon ORDER BY sekcja_nazwa ASC', array(':klasa' => $klasa[0]['klasa_id'], ':sezon' => SEZON));

    $sekcjeTxt = '<option value="">--- Wybierz sekcję ---</option>';

    foreach ($sekcje as $k => $v) {
      $sekcjeTxt .= '<option value="'.$v['sekcja_id'].'">'.$v['sekcja_nazwa'].'</option>';
    }

    header('Content-Type: application/json');
    echo json_encode(array(array(
      'klasa' => $klasa[0]['dziecko_klasa'],
      'klasa_id' => $klasa[0]['klasa_id'],
      'sekcje' => $sekcjeTxt
    )));

    die();
  }

  public function ustawCene() {
    parent::_isAjax();

    $sekcja = $_POST['sekcja'];
    $klasa = $_POST['klasa'];

    $cena = $this->_model->select('SELECT taryfikator_oplata FROM taryfikator WHERE taryfikator_klasa = :klasa AND taryfikator_sekcja = :sekcja AND taryfikator_sezon = :sezon', array(':klasa' => $klasa, ':sekcja' => $sekcja, ':sezon' => SEZON));

    $cena = $cena[0]['taryfikator_oplata'];

    header('Content-Type: application/json');
    echo json_encode(array(array(
      'cena' => $cena
    )));

    die();
  }
}