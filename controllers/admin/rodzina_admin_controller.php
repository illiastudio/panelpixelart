<?php

class Rodzina_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $this->_model = new Rodziny_Admin_Model();
  }

  public function init($params) {
    $paramsNumber = count($params);

    switch ($paramsNumber) {
      case 1:
        $id = $params[0];

        $this->_przegladaj($id);

        break;
      case 2:
        $id = $params[0];
        $sezon = $params[1];

        $this->_przegladaj($id, $sezon);

        break;
    }
  }

  private function _przegladaj($id, $sezon = 'index') {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/rodziny.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('rodziny');

    $this->_view->months = array(
      1 => 'Styczeń',
      2 => 'Luty',
      3 => 'Marzec',
      4 => 'Kwiecień',
      5 => 'Maj',
      6 => 'Czerwiec',
      7 => 'Lipiec',
      8 => 'Sierpień',
      9 => 'Wrzesień',
      10 => 'Październik',
      11 => 'Listopad',
      12 => 'Grudzień'
    );

    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $this->_view->sezony = $this->_model->getSezony();
    $this->_view->thisSezon = $sezon;
    $this->_view->platnosci = $this->_model->getPlatnosci($id, $sezon);
    $this->_view->rodzina = $this->_model->getRodzina($id);
    $this->_view->saldo = $this->_model->getSaldo($id, $sezon);
    $this->_view->zaplacono = $this->_model->getZaplacono($id, $sezon);
    $this->_view->rodzinaId = $id;
    $this->_view->status = $this->_model->checkStatusPlatnosci($id, $sezon);
    $this->_view->last = $this->_model->checkLastPlatnosc($id, $sezon);
    $this->_view->poprzednieSezony = $this->_model->getPoprzednieSezony($id, $sezon);

    switch ($this->_model->checkStatusPlatnosci($id, $sezon)) {
      case 0:
        $status = 'platnosc-bg-err';
        break;
      case 1:
        $status = 'platnosc-bg-plus';
        break;
      case 2:
        $status = 'platnosc-bg-ok';
        break;
      case 3:
        $status = 'platnosc-bg-empty';
        break;
      default:
        $status = '';
        break;
    }

    $this->_view->statusClass = $status;

    $this->_view->renderPage('admin/rodziny/przegladaj');

    $this->_footer->adminFooter();
  }

  public function sendSaldo() {
    parent::_isAjax();

    if (isset($_POST)) {
      $id = intval($_POST['rodzina_id']);

      $mail = new phpmailer();
      $mail->CharSet = "UTF-8";
      $mail->SetLanguage("pl", "libs/");
      $mail->IsHTML(true);

      // SMTP
      $mail->Mailer = 'smtp';
      $mail->addCustomHeader('MIME-Version: 1.0');
      $mail->addCustomHeader('Content-Type: text/html; charset=UTF-8');
      $mail->IsSMTP();
      $mail->SMTPDebug = 2;
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'tls';
      $mail->SMTPAutoTLS = false;
      $mail->Host = MAIL_HOST;
      $mail->Port = MAIL_PORT;
      $mail->SMTPKeepAlive = true;
      $mail->Username = MAIL_USER;
      $mail->Password = MAIL_PASS;

      $mail->From = MAIL_FROM;
      $mail->FromName = MAIL_FROM_NAME;
      $mail->Subject = 'Portal Rodzica UKS Żagle – PŁATNOŚCI – prosimy o sprawdzenie aktualnego salda';

      $opiekunowie = $this->_model->getOpiekunowie($id);

      $status = '';

      switch ($_POST['status']) {
        case 0:
          $status = 'NIEDOPŁATA';
          $styleColor = 'style="color: #F64747;"';
          break;
        case 1:
          $status = 'NADPŁATA';
          $styleColor = 'style="color: #4183D7;"';
          break;
        case 2:
          $status = 'SALDO OK';
          $styleColor = 'style="color: #2ECC71;"';
          break;
        case 3:
          $status = 'DO WYJAŚNIENIA';
          $styleColor = 'style="color: #F5AB35;"';
          break;
      }

      $bodyMsg = '
        <p>Drodzy Rodzice,</p>

        <p>Po wczytaniu kolejnego pliku z przelewami otrzymanego z banku status Waszych rozliczeń jest: </p>

        <center><h1 '.$styleColor.'>'.$status.'</h1></center>

        <p>Uprzejmie prosimy o zalogowanie się do Portalu Rodzica i dokonanie sprawdzenia, że wszystko zostało poprawnie odnotowane.</p>

        <p><u><strong>Gdyby okazało się, że są jakieś błędy – prosimy o kontakt poprzez: <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a></strong></u></p>

        <center><h2 style="color: #F64747;"><u>Jeśli wszystko się zgadza prosimy nic nam nie wysyłać!</u></h2></center>

        <p>Ze sportowym pozdrowieniem,<br>
        Biuro UKS Żagle<br>
        <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a></p>
      ';

      foreach ($opiekunowie as $key => $val) {
        $mail->AddAddress($val['opiekun_mail']);
      }

      $ukrytaKopia = 'testportaluuks@gmail.com';

      $mail->Body = $bodyMsg;
      $mail->AddBCC(MAIL_TEST);
      // $mail->AddAddress(MAIL_TEST);
      // $mail->AddAddress($ukrytaKopia);

      if ($mail->Send()) {
        $this->_model->insert('platnosci_msg', array('pm_rodzina_id' => $id, 'pm_status' => $_POST['status']));

        header('Content-Type: application/json');
        echo json_encode(array(
          'type' => 'success',
          'msg' => 'Wysłano wiadomość'
        ));
      } else {
        header('Content-Type: application/json');
        echo json_encode(array(
          'type' => 'error',
          'msg' => 'Nie udało się wysłać wiadomości'
        ));
      }

      $mail->ClearAddresses();
      $mail->ClearAttachments();

      $mail->SmtpClose();

      die;
    }
  }

}