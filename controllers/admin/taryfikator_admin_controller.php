<?php

class Taryfikator_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
    parent::_isRole('admin');
  }

  public function lista($params) {
    if (isset($_POST['dodaj-taryfikator'])) {
      $this->dodaj();
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/jquery-ui.multidatespicker.js');
    $this->_header->addScripts('file', 'public/js/admin/taryfikator.js');

    $this->_header->adminHeader();

    $this->_top->adminTop('taryfikator');

    $this->_view->klasy = $this->_model->select('SELECT * FROM klasy WHERE klasa_status="1" ORDER BY klasa_nazwa ASC');
    $this->_view->sekcje = $this->_model->select('SELECT * FROM sekcje WHERE sekcja_status="1" ORDER BY sekcja_nazwa ASC');
    $this->_view->taryfikator = $this->_model->select('
      SELECT
        taryfikator_id,
        klasa_nazwa,
        sekcja_nazwa,
        taryfikator_sezon,
        taryfikator_oplata
      FROM taryfikator
      INNER JOIN klasy ON klasa_id = taryfikator_klasa
      INNER JOIN sekcje ON sekcja_id = taryfikator_sekcja
    ');
    $this->_view->sezon = $this->_model->select('SELECT sezon_nazwa FROM sezony ORDER BY sezon_nazwa ASC');

    $this->_view->renderPage('admin/taryfikator/index');

    $this->_footer->adminFooter();
  }

  public function dodaj() {
    $form = new Form();
    $form->post('klasa')->val('notEmpty', 'klasa')
         ->post('sekcja')->val('notEmpty', 'sekcja')
         ->post('sezon')->val('notEmpty', 'sezon')
         ->post('oplata')->val('notEmpty', 'opłata');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $tmp = $this->_model->select('SELECT 1 FROM taryfikator WHERE taryfikator_sekcja = :sekcja AND taryfikator_klasa = :klasa AND taryfikator_sezon = :sezon', array(':sekcja' => $data['sekcja'], ':klasa' => $data['klasa'], 'sezon' => $data['sezon']));

        if (!empty($tmp)) {
          $this->_msg->add('error', 'Taki rekord już istnieje', false, 'admin/taryfikator');
        }

        $arr = array(
          'taryfikator_sekcja' => $data['sekcja'],
          'taryfikator_klasa' => $data['klasa'],
          'taryfikator_sezon' => $data['sezon'],
          'taryfikator_oplata' => $data['oplata']
        );

        $test = $this->_model->insert('taryfikator', $arr);

        if ($test) {
          $this->_msg->add('success', "Dodano taryfikator", false, 'admin/taryfikator');
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }
      }
  }

  public function usun($params) {
    $id = $params[0];

    $test = $this->_model->delete('taryfikator', 'taryfikator_id = :id', array(':id' => $id));

    if ($test) {
      $this->_msg->add('success', 'Usunięto taryfikator', false, 'admin/taryfikator');
    } else {
      $this->_msg->add('error', 'Nie udało się usunąć taryfikatora', false, 'admin/taryfikator');
    }
  }

  public function edytuj($params) {
    $id = $params[0];

    $tmp = $this->_model->select('SELECT taryfikator_id, sekcja_nazwa, klasa_nazwa, taryfikator_oplata, taryfikator_sezon, sekcja_id, klasa_id FROM taryfikator INNER JOIN sekcje ON taryfikator_sekcja = sekcja_id INNER JOIN klasy ON taryfikator_klasa = klasa_id WHERE taryfikator_id = :id LIMIT 1', array('id' => $id));
    $this->_view->tmp = $tmp;

    if (isset($_POST['zapisz'])) {
      if (isset($_POST['taryfikator_oplata']) && !empty($_POST['taryfikator_oplata'])) {
        $oplata = intval($_POST['taryfikator_oplata']);
        $oplataOld = intval($_POST['taryfikator_oplata_old']);

        if ($oplata != $oplataOld) {
          $arr = array('taryfikator_oplata' => $_POST['taryfikator_oplata']);
          $this->_model->update('taryfikator', $arr, 'taryfikator_id = :id', array(':id' => $id));

          $sezonArr = explode('-', $tmp[0]['taryfikator_sezon']);
          $start = '20'.$sezonArr[0].'-09-01';
          $end = '20'.$sezonArr[1].'-06-30';

          $zgloszenia = $this->_model->select('
            SELECT zgloszenie_id, dziecko_rodzina_id
            FROM zgloszenia
            INNER JOIN dzieci ON dziecko_id = zgloszenie_dziecko_id
            WHERE zgloszenie_sekcja = :sekcja
              AND zgloszenie_dziecko_klasa_id = :klasa
              AND zgloszenie_date >= :start
              AND (zgloszenie_date_end <= :end OR (zgloszenie_date_end = "0000-00-00" AND zgloszenie_status = "1"))
              AND zgloszenie_propozycja = zgloszenie_skladka
          ', array(':start' => $start, 'end' => $end, ':sekcja' => $tmp[0]['sekcja_id'], ':klasa' => $tmp[0]['klasa_id']));

          $rodziny = array();
          foreach ($zgloszenia as $key => $val) {
            // update zgloszenie_skladka i zgloszenie_propozycja
            $arr = array('zgloszenie_skladka' => $oplata, 'zgloszenie_propozycja' => $oplata);
            $this->_model->update('zgloszenia', $arr, 'zgloszenie_id = :id', array(':id' => $val['zgloszenie_id'] ));
            $rodziny[] = $val['dziecko_rodzina_id'];
          }

          $rodziny = array_values(array_unique($rodziny));

          foreach ($rodziny as $key => $val) {
            // przemielenie platnosci
            $this->_updateSaldo($start, $val);
          }

          $this->_msg->add('success', 'Zapisano zmiany', false, 'admin/taryfikator/');
        } else {
          $this->_msg->add('info', 'Wprowadzona opłata jest taka sama jak poprzednia. Nic nie zmienion.', false, 'admin/taryfikator/');
        }
      } else {
        $this->_msg->add('error', 'Pole opłata nie może byc puste', false, 'admin/taryfikator/');
      }
    }

    $this->_view->renderPage('admin/taryfikator/edytuj');
  }

  private function _updateSaldo($start, $rodzina) {
    $date = $start;
    $end_date = date('Y-m-01', time());
    $id = $rodzina;

    while (strtotime($date) <= strtotime($end_date)) {
      $tmp = $this->_model->select('
        SELECT dziecko_rodzina_id AS ps_rodzina_id, sum(zgloszenie_propozycja) AS ps_do_zaplaty, GROUP_CONCAT(zgloszenie_id) AS ps_zgloszenia
        FROM zgloszenia
        INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
        LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id
        WHERE dziecko_rodzina_id = :id
        AND (zgloszenie_skladka > 0)
        AND (potwierdzenie_status = "1")
        AND (potwierdzenie_wyslane = "1")
        AND (
          (zgloszenie_status = "1"
            AND zgloszenie_date <= :data
            AND zgloszenie_date != "0000-00-00"
          )
          OR (zgloszenie_status IN ("0", "2")
            AND (zgloszenie_date <= :data AND zgloszenie_date != "0000-00-00")
            AND (
              (zgloszenie_date_end >= :data AND zgloszenie_date_end != "0000-00-00")
              OR (YEAR(zgloszenie_date_end) = YEAR(:data - INTERVAL 1 MONTH)
                AND MONTH(zgloszenie_date_end) = MONTH(:data - INTERVAL 1 MONTH)
                AND DAYOFMONTH(zgloszenie_date_end) > 20
              )
            )
          )
        )
        GROUP BY dziecko_rodzina_id',
      array(':id' => $id, ':data' => $date));

      $arr = array();

      if (!empty($tmp)) {
        $arr = $tmp[0];
      } else {
        $arr['ps_rodzina_id'] = $id;
        $arr['ps_do_zaplaty'] = 0;
      }

      $arr['ps_okres'] = $date;

      if (in_array(date('m', strtotime($date)), array('07', '08'))) {
        $date = date ("Y-m-d", strtotime("+1 month", strtotime($date)));
       } else {
        $this->_model->delete('platnosci_saldo', 'ps_rodzina_id = :id AND ps_okres = :data', array(':id' => $id, ':data' => $date));
        $this->_model->insert('platnosci_saldo', $arr);

        $date = date ("Y-m-d", strtotime("+1 month", strtotime($date)));
       }
    }
  }
}