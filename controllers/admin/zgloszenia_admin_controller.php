<?php

class Zgloszenia_Admin_Controller extends Admin_Controller {
  private $_zgloszenia;

  public function __construct() {
    parent::__construct();
    parent::_isLogged();
    parent::_isRole('admin');
  }

  public function lista($params) {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop('zgloszenia');

    $zgloszeniaArr = array();

    if (!empty($params[0])) {
      $strona = $params[0];
    } else {
      $this->redirect('admin/zgloszenia/lista/1');
    }

    $zgloszenia = $this->_model->select('SELECT rodzina_id, rodzina_nazwisko, zgloszenie_date_add, GROUP_CONCAT(zgloszenie_id) AS zgloszenia, uwagi_id, uwagi_tresc, zgloszenie_date_add FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id LEFT JOIN uwagi ON uwagi_id = zgloszenie_uwagi_id LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id WHERE zgloszenie_status = "1" AND zgloszenie_skladka > 0 AND STR_TO_DATE(zgloszenie_date_add, "%Y-%m-%d %H:%i:%s") > STR_TO_DATE("2014-08-26 08:23:46", "%Y-%m-%d %H:%i:%s") AND (COALESCE(potwierdzenie_status, 0) = 0 || potwierdzenie_wyslane = "0") GROUP BY rodzina_id ORDER BY zgloszenie_date_add LIMIT '.($strona - 1).', 1');

    $ile = $this->_model->select('SELECT COUNT(rodzina_id) AS ile FROM (SELECT rodzina_id FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id LEFT JOIN uwagi ON uwagi_id = zgloszenie_uwagi_id LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id WHERE zgloszenie_status = "1" AND zgloszenie_skladka > 0 AND zgloszenie_date_add > "2014-08-26 08:23:46" AND (COALESCE(potwierdzenie_status, 0) = 0 || potwierdzenie_wyslane = "0") GROUP BY rodzina_id) AS tmp');

    $ile = $ile[0]['ile'];

    foreach ($zgloszenia as $k => $v) {
      $tmp = $this->_model->select('SELECT zgloszenie_id, rodzina_id, rodzina_nazwisko, dziecko_imie, klasa_nazwa AS klasa, sekcja_nazwa, zgloszenie_date_add, uwagi_id, uwagi_tresc, zgloszenie_skladka, zgloszenie_propozycja, zgloszenie_date FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id INNER JOIN klasy ON dziecko_klasa = klasa_id INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id LEFT JOIN uwagi ON uwagi_id = zgloszenie_uwagi_id LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id WHERE zgloszenie_status = "1" AND zgloszenie_id IN ('.$v['zgloszenia'].') AND (COALESCE(potwierdzenie_status, 0) = 0 || potwierdzenie_wyslane = "0")');

      $arr = array();

      foreach ($tmp as $k2 => $v2) {
        $arr[$v2['zgloszenie_id']] = $v2;
      }

      $v['zgloszenia'] = $arr;

      $zgloszeniaArr[$v['rodzina_id']] = $v;
    }

    $this->_view->strona = $strona;
    $this->_view->ile = $ile;
    $this->_view->zgloszenia = $zgloszeniaArr;
    $this->_zgloszenia = $zgloszeniaArr;

    $this->_send($strona, $ile);

    $this->_view->renderPage('admin/zgloszenia/lista');

    $this->_footer->adminFooter();
  }

  private function _send($strona, $ile) {
    if (isset($_POST['wyslij'])) {
      $err = 0;

      $mail = new phpmailer(true);
      $mail->CharSet = "UTF-8";
      $mail->SetLanguage("pl", "libs/");
      $mail->IsHTML(true);


      // SMTP
      $mail->Mailer = 'smtp';
        $mail->addCustomHeader('MIME-Version: 1.0');
        $mail->addCustomHeader('Content-Type: text/html; charset=UTF-8');
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAutoTLS = false;
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPKeepAlive = true;
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;

      $mail->From = MAIL_FROM;
      $mail->FromName = MAIL_FROM_NAME;
      $mail->Subject = 'Portal Rodzica UKS Żagle – ZAPISANIE/ZMIANA/REZYGNACJA – potwierdzenie i aktualizacja danych do płatności';

      foreach ($_POST['zgloszenie'] as $key => $value) {
        $array = array();
        $zgloszeniaTxt = '';

        foreach ($value as $zgloszenie_id => $status) {
          if ($status > 0) {
            $rodzina = $_POST['rodzina'][$zgloszenie_id];
            $tmp = $this->_zgloszenia[$rodzina]['zgloszenia'][$zgloszenie_id];

            $this->_model->delete('potwierdzenia', 'potwierdzenie_zgloszenie_id = '.$zgloszenie_id.'');
            $this->_model->insert('potwierdzenia', array('potwierdzenie_zgloszenie_id' => $zgloszenie_id, 'potwierdzenie_status' => $status));

            if ($status == 1) {
              $statusTxt = 'Zaakceptowane';

              if ($_POST['taryfa'][$zgloszenie_id] == $_POST['propozycja'][$zgloszenie_id] || $_POST['propozycja'][$zgloszenie_id] == 0) {
                $kwota = $kwota = $_POST['taryfa'][$zgloszenie_id];
              } else {
                $kwota = $_POST['propozycja'][$zgloszenie_id];
              }

              $this->_model->update('zgloszenia', array('zgloszenie_date' => $_POST['zgloszenie_date'][$zgloszenie_id], 'zgloszenie_propozycja' => $_POST['propozycja'][$zgloszenie_id]), 'zgloszenie_id = :id', array(':id' => $zgloszenie_id));
            } elseif ($status == 2) {
              $statusTxt = 'Odrzucone';

              $kwota = $_POST['propozycja'][$zgloszenie_id];
            }

            $array[$rodzina][$zgloszenie_id]['imie_nazwisko'] = $tmp['dziecko_imie'];
            $array[$rodzina][$zgloszenie_id]['klasa'] = $tmp['klasa'];
            $array[$rodzina][$zgloszenie_id]['sekcja'] = $tmp['sekcja_nazwa'];
            $array[$rodzina][$zgloszenie_id]['status'] = $statusTxt;
            $array[$rodzina][$zgloszenie_id]['kwota'] = $kwota;
            $array[$rodzina][$zgloszenie_id]['data'] = $_POST['zgloszenie_date'][$zgloszenie_id];
            $zgloszeniaTxt .= $zgloszenie_id.',';

          }
        }

        if (!empty($array)) {
          $zgloszeniaTxt = rtrim($zgloszeniaTxt, ',');

          $maile = $this->_model->select('SELECT opiekun_mail, opiekun_active_link FROM opiekunowie WHERE opiekun_rodzina_id = :rodzina_id', array(':rodzina_id' => $tmp['rodzina_id']));

          foreach ($maile as $m) {
            $mail->AddAddress($m['opiekun_mail']);

            $this->_view->activeLink = $m['opiekun_active_link'];
          }

          if (!empty($_POST['odpowiedz'])) {
            $odpArr = array(
              'odpowiedz_uwaga' => $zgloszenie_id,
              'odpowiedz_tresc' => $_POST['odpowiedz'],
              'odpowiedz_autor' => $_SESSION['user_id']
            );

            $this->_model->insert('odpowiedzi', $odpArr);

            if (isset($_POST['uwaga-tresc'])) {
              $this->_view->uwaga = $_POST['uwaga-tresc'];
            }

            $this->_view->odpowiedz = $_POST['odpowiedz'];
          }

          ob_start();
          $this->_view->array = $array;
          $this->_view->renderPage('admin/zgloszenia/potwierdzenie');
          $cont = ob_get_contents();
          ob_end_clean();

          $mail->Body = $cont;

          // $mail->AddBCC(MAIL_KOPIA);
          // $mail->AddAddress('testportaluuks@gmail.com');
          // $mail->AddBCC(MAIL_TEST);

          if ($mail->Send()) {
            $this->_model->update('potwierdzenia', array('potwierdzenie_wyslane' => 1), 'potwierdzenie_zgloszenie_id IN ('.$zgloszeniaTxt.')');

            // $date = START_DATE;
            // $end_date = date('Y-m-01', time());

            $sezon = $this->_model->select('SELECT * FROM sezony WHERE sezon_nazwa = :nazwa', array(':nazwa' => SEZON));

            $date = $sezon[0]['sezon_start'];
            $end_date = date('Y-m-01', time());

            if ($date > $end_date) {
              $end_date = date ("Y-m-d", strtotime("+1 month", strtotime($end_date)));
            }

            while (strtotime($date) <= strtotime($end_date)) {
              $temp = $this->_model->select('
                SELECT dziecko_rodzina_id AS ps_rodzina_id, sum(zgloszenie_propozycja) AS ps_do_zaplaty, GROUP_CONCAT(zgloszenie_id) AS ps_zgloszenia
                FROM zgloszenia
                INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
                LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id
                WHERE dziecko_rodzina_id = :id
                AND (zgloszenie_skladka > 0)
                AND (potwierdzenie_status = "1")
                AND (potwierdzenie_wyslane = "1")
                AND (
                  (zgloszenie_status = "1"
                    AND zgloszenie_date <= :data
                    AND zgloszenie_date != "0000-00-00"
                  )
                  OR (zgloszenie_status IN ("0", "2")
                    AND (zgloszenie_date <= :data AND zgloszenie_date != "0000-00-00")
                    AND (
                      (zgloszenie_date_end >= :data AND zgloszenie_date_end != "0000-00-00")
                      OR (YEAR(zgloszenie_date_end) = YEAR(:data - INTERVAL 1 MONTH)
                        AND MONTH(zgloszenie_date_end) = MONTH(:data - INTERVAL 1 MONTH)
                        AND DAYOFMONTH(zgloszenie_date_end) > 20
                      )
                    )
                  )
                )
                GROUP BY dziecko_rodzina_id',
              array(':id' => $tmp['rodzina_id'], ':data' => $date));

              $tmpArr = array();

              if (!empty($temp)) {
                $tmpArr = $temp[0];
              } else {
                $tmpArr['ps_rodzina_id'] = $tmp['rodzina_id'];
                $tmpArr['ps_do_zaplaty'] = 0;
              }

              $tmpArr['ps_okres'] = $date;

              $this->_model->delete('platnosci_saldo', 'ps_rodzina_id = :id AND ps_okres = :data', array(':id' => $tmp['rodzina_id'], ':data' => $date));
              $this->_model->insert('platnosci_saldo', $tmpArr);

              $date = date ("Y-m-d", strtotime("+1 month", strtotime($date)));
            }

          } else {
            $err++;
          }
        }
      }

      if ($err == 0) {
        $this->_msg->add('success', 'Wysłano potwierdzenia.');
      } else {
        $this->_msg->add('error', 'Wystąpił błąd, część potwierdzeń mogła zostać nie wysłana.');

        $this->reload();
      }
      $mail->ClearAddresses();
      $mail->ClearAttachments();
      $mail->SmtpClose();
    }
  }
}