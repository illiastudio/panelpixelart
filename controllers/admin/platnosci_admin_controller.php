<?php

class Platnosci_Admin_Controller extends Admin_Controller {
  private $_importArr;

  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $this->_model = new Platnosci_Admin_Model();
  }

  public function index() {
    unset($_SESSION['platnosci_import']);

    $this->_import();

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop('platnosci');

    if (empty($this->_importArr)) {
      $this->_view->renderPage('admin/import/platnosci');
    }

    $this->_footer->adminFooter();
  }

  private function _import() {
    if (isset($_POST['import'])) {
      $upl = false;

      if (isset($_FILES['plik'])) {
        $tmp_name = $_FILES['plik']["tmp_name"];
        $name = $_FILES['plik']["name"];

        if (empty($name)) {
          $this->_msg->add('error', 'Nie wybrano pliku', true, 'admin/import/');
        }

        $nameArr = explode('.', $name);
        $ext = end($nameArr);

        if (strtolower($ext) != 'xls') {
          $this->_msg->add('error', 'Błędne rozszerzenie pliku, dozwolone jedynie pliki .xls', true, 'admin/import/');
        }

        move_uploaded_file($tmp_name, './'.$name);

        $upl = true;
      }

      if ($upl) {
        error_reporting(0);

        $xls = './'.$name;

        $_SESSION['platnosci_filename'] = $name;

        require_once './libs/excel/reader.php';
        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('UTF-8');
        $data->read($xls);

        $colspan = array();
        $naglowki = array('nazwa produktu');
        $r_naglowki = array_flip($naglowki);
        $pomin_zakladki = array();
        $r_pomin_zakladki = array_flip($pomin_zakladki);
        $pomin_wiersze = array();
        $r_pomin_wiersze = array_flip($pomin_wiersze);
        $pokazuj_kolumny = array(1,2,3);
        $r_pokazuj_kolumny = array_flip($pokazuj_kolumny);
        $pomin_puste_wiersze = true;
        $musi_byc = array(1);
        $r_musi_byc = array_flip($musi_byc);
        $kolumna_id = '1';
        $tytul = "lista";
        $parametry = array($colspan,$r_naglowki,$r_pomin_zakladki,$r_pomin_wiersze,$r_pokazuj_kolumny,$pomin_puste_wiersze,$r_musi_byc,$kolumna_id);

        $arr = array();
        $ilosc_zakl = 1;

        for ($zakl = 0; $zakl <= ($ilosc_zakl - 1); $zakl++) {
          $maxRow = $data->sheets[$zakl]['numRows'];

          for ($i = 2; $i <= $maxRow; $i++) {
            for ($j = 1; $j < $data->sheets[$zakl]['numCols']; $j++) {
              if (!empty($data->sheets[$zakl]['cells'][$i][1])) {
                ini_set('mbstring.substitute_character', "none");
                $komorka = mb_convert_encoding($data->sheets[$zakl]['cells'][$i][$j], 'UTF-8', 'UTF-8');

                // $komorka = $data->sheets[$zakl]['cells'][$i][$j];
                $komorka = str_replace(chr(243), 'ó', $komorka);
                $komorka = str_replace(chr(211), 'Ó', $komorka);
                $komorka = str_replace(chr(160), ' ', $komorka);
                $komorka = trim($komorka);

                if ($j == 1) {
                  $arr[$i - 2]['id'] = $komorka;
                }

                if ($j == 2) {
                  $arr[$i - 2]['nr_dokumentu'] = $komorka;
                }

                if ($j == 3) {
                  if (substr_count($komorka, '.') > 0) {
                    $t = explode('.', $komorka);

                    if ($t[0] < 10) {
                      $t[0] = '0'.$t[0];
                    }

                    $arr[$i - 2]['data'] = $t[2].'-'.$t[1].'-'.$t[0];
                  } else if (substr_count($komorka, '/') > 0) {
                    $t = explode('/', $komorka);

                    $arr[$i - 2]['data'] = $t[2].'-'.$t[1].'-'.$t[0];
                  }
                }

                if ($j == 4) {
                  $arr[$i - 2]['platnik_id'] = $komorka;
                }

                if ($j == 5) {
                  $arr[$i - 2]['nazwa'] = $komorka;
                }

                $nextCell = mb_convert_encoding($data->sheets[$zakl]['cells'][$i][7], 'UTF-8', 'UTF-8');
                if ($j == 6 && empty($nextCell)) {
                  $arr[$i - 2]['kwota'] = $komorka;
                }

                $prevCell = mb_convert_encoding($data->sheets[$zakl]['cells'][$i][6], 'UTF-8', 'UTF-8');
                if ($j == 7 && empty($prevCell)) {
                  $arr[$i - 2]['kwota'] = $komorka * -1;
                }

                if ($j == 9) {
                  $arr[$i - 2]['opis'] = $komorka;
                }
              }
            }
          }
        }

        $this->_importArr = $arr;

        $this->_unsetImported();

        $this->_importArr = array_values($this->_importArr);

        $_SESSION['platnosci_import'] = json_encode(array_filter(array_merge(array(0), $this->_importArr)));

        $this->redirect('admin/platnosci/rekord/1');
      }
    }
  }

  private function _unsetImported() {
    foreach ($this->_importArr as $key => $val) {
      $test = $this->_model->issetPlatnosc($val['nr_dokumentu']);

      if ($test) {
        unset($this->_importArr[$key]);
      }
    }
  }

  private function _checkPodmiotRodzina($rekord) {
    if (!empty($this->_importArr)) {
      $count = 0;

      $val = $this->_importArr[($rekord - 1)];

      $rodzinaId = $this->_model->checkRodzinaByPodmiot($val['platnik_id']);

      if (!empty($rodzinaId)) {
        $data = array(
          'platnosc_rodzina_id' => $rodzinaId,
          'platnosc_nr_dokumentu' => $val['nr_dokumentu'],
          'platnosc_zaplacono' => $val['kwota'],
          'platnosc_nazwa' => $val['nazwa'],
          'platnosc_opis' => $val['opis'],
          'platnosc_data_operacji' => $val['data']
        );

        $empty = 0;
        foreach ($data as $k => $v) {
          if (empty($v)) {
            $empty++;
          }
        }

        if ($empty != 0) {
          $this->_msg->add('error', 'Wszystkie pola muszą być wypełnione.', false);
        }

        if (!$this->_model->checkPlatnosc($val['nr_dokumentu']) && $empty == 0) {
          $this->_model->insert('platnosci', $data);

          $count++;
          unset($this->_importArr[($rekord - 1)]);
        } else {
          $this->_msg->add('error', 'Nie udało się zaimportować płatności.', false);
        }

      }

      $this->_importArr = array_values($this->_importArr);
      $_SESSION['platnosci_import'] = json_encode(array_filter(array_merge(array(0), $this->_importArr)));

      if ($count > 0) {
        $this->_msg->add('success', 'Zaimportowano płatności', false);
      }

      if (($rekord - 1) == count($this->_importArr)) {
        $this->redirect('admin/platnosci/rekord/'.($rekord - 1));
      } else {
        $this->reload();
      }
    }
  }

  public function rekord($params) {
    parent::_checkParams($params, 1, 'admin/rekordy/1');
    $number = $params[0];

    if (empty($_SESSION['platnosci_import'])) {
      $this->redirect('admin/platnosci');
    }

    if (isset($_POST['przerwij'])) {
      unset($_SESSION['platnosci_import']);

      $this->redirect('admin/platnosci');
    }

    $this->_zapiszPlatnika();

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/platnosci.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('platnosci');

    $rekordy = json_decode($_SESSION['platnosci_import'], true);
    $rekordy = array_values($rekordy);

    $this->_view->ile = count($rekordy);

    if (!empty($rekordy)) {
      $this->_view->rekord = $rekordy[$number - 1];
      $this->_view->platnikRodzina = $this->_model->selectPlatnikRodzina($rekordy[$number - 1]['platnik_id']);
    } else {
      $this->_view->rekord = 1;
      $this->_view->platnikRodzina = array();
    }

    $this->_view->number = $number;
    $this->_view->rodziny = $this->_model->rodzinyList();


    $this->_view->renderPage('admin/platnosci/lista');

    $this->_footer->adminFooter();
  }

  private function _zapiszPlatnika() {
    if (isset($_POST['zaimportuj_platnosc'])) {
      $form = new Form();
      $form->post('rodzina_id')->val('notEmpty', 'Rodzina Id')
           ->post('number')->val('notEmpty', 'numer')
           ->post('platnik_id')->val('notEmpty', 'płatnik');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $testRodzina = $this->_model->issetRodzina($data['rodzina_id']);

        if (!$testRodzina) {
          $this->_msg->add('error', 'Rodzina o takim Id nie istnieje w bazie.', false, 'admin/platnosci/rekord/'.$data['number']);
        }

        $issetPlatnik = $this->_model->issetPlatnik($data['rodzina_id']);

        if (!$issetPlatnik && $data['platnik_id']) {
          $rodzina = $this->_model->select('SELECT pp_rodzina_id FROM platnosci_platnicy WHERE pp_platnik_id = :id LIMIT 1', array(':id' => $data['platnik_id']));

            if (!empty($rodzina) && $rodzina[0]['pp_rodzina_id'] != $data['rodzina_id']) {
              $this->_msg->add('error', 'Ten podmiot jest już przypisany do innej rodziny.', false, 'admin/platnosci/rekord/'.$data['number']);
            } else {
              $testInsertPlatnik = $this->_model->insert('platnosci_platnicy', array('pp_rodzina_id' => $data['rodzina_id'], 'pp_platnik_id' => $data['platnik_id']));

              if ($testInsertPlatnik) {
                $this->_msg->add('success', 'Powiązano płatnika z rodziną', false);
              }
            }
        } else {
          $platnik = $this->_model->select('SELECT pp_platnik_id FROM platnosci_platnicy WHERE pp_rodzina_id = :id LIMIT 1', array(':id' => $data['rodzina_id']));

          if ($platnik[0]['pp_platnik_id'] != $data['platnik_id']) {
            $this->_msg->add('error', 'Do tej rodziny przypisany jest już inny płatnik.', false, 'admin/platnosci/rekord/'.$data['number']);
          }
        }

        $rekordy = json_decode($_SESSION['platnosci_import'], true);
        $this->_importArr = array_values($rekordy);

        $this->_checkPodmiotRodzina($data['number']);
      } else {
        $form->showErrors();
      }
    }
  }
}