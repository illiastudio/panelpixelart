<?php

class Import_Admin_Controller extends Admin_Controller {
  private $_rekord;
  private $_arr;
  private $_ile;

  public function __construct() {
    parent::__construct();
    parent::_isLogged();
    parent::_isRole('admin');
  }

  public function index() {
    $this->_import();

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop('import');

    $this->_view->sezony = $this->_model->select('SELECT sezon_nazwa FROM sezony WHERE sezon_koniec <= (SELECT sezon_koniec FROM sezony WHERE sezon_nazwa = :nazwa)', array(':nazwa' => SEZON));

    $this->_view->renderPage('admin/import/index');

    $this->_footer->adminFooter();
  }

  private function _import() {
    if (isset($_POST['import'])) {
      $upl = false;

      if (isset($_POST['sezon'])) {
        $_SESSION['import_sezon'] = $_POST['sezon'];

        $sezon = $this->_model->select('SELECT * FROM sezony WHERE sezon_nazwa = :nazwa', array(':nazwa' => $_POST['sezon']));

        $_SESSION['sezon_start'] = $sezon[0]['sezon_start'];
        $_SESSION['sezon_koniec'] = $sezon[0]['sezon_koniec'];
      }

      if (isset($_FILES['plik'])) {
        $tmp_name = $_FILES['plik']["tmp_name"];
        $name = $_FILES['plik']["name"];

        if (empty($name)) {
          $this->_msg->add('error', 'Nie wybrano pliku', true, 'admin/import/');
        }

        $nameArr = explode('.', $name);
        $ext = end($nameArr);

        if (strtolower($ext) != 'xls') {
          $this->_msg->add('error', 'Błędne rozszerzenie pliku, dozwolone jedynie pliki .xls', true, 'admin/import/');
        }

        move_uploaded_file($tmp_name, './'.$name);

        $upl = true;
      }

      if ($upl) {
        error_reporting(0);

        $xls = './'.$name;

        $_SESSION['zgloszenia_filename'] = $name;

        require_once './libs/excel/reader.php';
        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('UTF-8');
        $data->read($xls);

        $colspan = array();
        $naglowki = array('nazwa produktu');
        $r_naglowki = array_flip($naglowki);
        $pomin_zakladki = array();
        $r_pomin_zakladki = array_flip($pomin_zakladki);
        $pomin_wiersze = array();
        $r_pomin_wiersze = array_flip($pomin_wiersze);
        $pokazuj_kolumny = array(1,2,3);
        $r_pokazuj_kolumny = array_flip($pokazuj_kolumny);
        $pomin_puste_wiersze = true;
        $musi_byc = array(1);
        $r_musi_byc = array_flip($musi_byc);
        $kolumna_id = '1';
        $tytul = "lista";
        $parametry = array($colspan,$r_naglowki,$r_pomin_zakladki,$r_pomin_wiersze,$r_pokazuj_kolumny,$pomin_puste_wiersze,$r_musi_byc,$kolumna_id);

        $arr = array();
        $ilosc_zakl = 1;

        for ($zakl = 0; $zakl <= ($ilosc_zakl - 1); $zakl++) {
          $maxRow = $data->sheets[$zakl]['numRows'];

          for ($i = 2; $i <= $maxRow; $i++) {
            for ($j = 1; $j <= $data->sheets[$zakl]['numCols']; $j++) {
              if (!empty($data->sheets[$zakl]['cells'][$i][1])) {
                $komorka = $data->sheets[$zakl]['cells'][$i][$j];
                $komorka = str_replace(chr(243), 'ó', $komorka);
                $komorka = str_replace(chr(160), ' ', $komorka);
                $komorka = trim($komorka);

                if ($j == 1) {
                  $arr[$i - 1]['id_dziecka'] = $komorka;
                }

                if ($j == 3) {
                  $arr[$i - 1]['id_rodziny'] = $komorka;
                }

                if ($j == 2) {
                  $arr[$i - 1]['klasa'] = $komorka;
                }

                if ($j == 5) {
                  $arr[$i - 1]['imie'] = $komorka;
                }

                if ($j == 4) {
                  $arr[$i - 1]['nazwisko'] = $komorka;
                }

                if ($j == 12) {
                  $arr[$i - 1]['mail_matki'] = $komorka;
                }

                if ($j == 13) {
                  $arr[$i - 1]['telefon_matki'] = $komorka;
                }

                if ($j == 14) {
                  $arr[$i - 1]['mail_ojca'] = $komorka;
                }

                if ($j == 15) {
                  $arr[$i - 1]['telefon_ojca'] = $komorka;
                }

                if ($j == 6) {
                  if (intval($komorka) > 0) {
                    $arr[$i - 1]['zgloszenia']['pilka_nozna'] = $komorka;
                  }
                }

                if ($j == 7) {
                  if (intval($komorka) > 0) {
                    $arr[$i - 1]['zgloszenia']['judo'] = $komorka;
                  }
                }

                if ($j == 8) {
                  if (intval($komorka) > 0) {
                    $arr[$i - 1]['zgloszenia']['szachy'] = $komorka;
                  }
                }

                if ($j == 9) {
                  if (intval($komorka) > 0) {
                    $arr[$i - 1]['zgloszenia']['plywanie'] = $komorka;
                  }
                }

                if ($j == 10) {
                  if (intval($komorka) > 0) {
                    $arr[$i - 1]['zgloszenia']['szermierka'] = $komorka;
                  }
                }

                $arr[$i - 1]['status'] = 0;
              }
            }
          }
        }

        $_SESSION['import'] = json_encode($arr);

        unlink('./'.$name);

        $this->redirect('admin/import/krok/1/rekord/1');
      } else {
        $this->_msg->add('error', 'Nie udało się zaimportować pliku.', true, 'admin/import/');
      }
    }
  }

  public function krok($params) {
    if (!isset($_SESSION['import'])) {
      $this->redirect('admin/import');
    }

    if (isset($params[0])) {
      $krok = $params[0];

      unset($params[0]);
      $params = array_values($params);

      switch ($krok) {
        case 1:
          $this->_krok_1($params);
          break;
        case 2:
          $this->_krok_2($params);
          break;
        default:
          $this->redirect('admin/import');
          break;
      }
    } else {
      $this->redirect('admin/import');
    }
  }

  private function _krok_1($params) {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/import.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('import');

    if (isset($params[0])) {
      if (isset($params[1])) {
        $this->_rekord = $params[1];
        $this->_view->rekord = $params[1];
      } else {
        $this->redirect('admin/import/krok/1/rekord/1');
      }
    } else {
      $this->redirect('admin/import/krok/1/rekord/1');
    }

    $this->_view->rodziny = $this->_model->select('SELECT rodzina_id, rodzina_nazwisko, o1.opiekun_mail AS ojciec_mail, o1.opiekun_telefon AS ojciec_telefon, o2.opiekun_telefon AS matka_telefon, o2.opiekun_mail AS matka_mail FROM rodziny LEFT JOIN opiekunowie o1 ON rodzina_id = o1.opiekun_rodzina_id AND o1.opiekun_typ = 1 LEFT JOIN opiekunowie o2 ON rodzina_id = o2.opiekun_rodzina_id AND o2.opiekun_typ = 2 ORDER BY rodzina_nazwisko ASC, rodzina_id ASC');

    $this->_arr = json_decode($_SESSION['import'], true);
    $this->_ile = count($this->_arr);

    $this->_view->arr = $this->_arr;
    $this->_view->ile = $this->_ile;

    $this->_krok_1_actions($params);

    $this->_view->renderPage('admin/import/krok_1');

    $this->_footer->adminFooter();
  }

  private function _krok_1_actions($params) {
    if (isset($_POST['dodaj_nowa_rodzine']) || isset($_POST['przejdz_dalej'])) {
      $arr = json_decode($_SESSION['import'], true);
      $rekord = $this->_rekord;

      if (isset($_POST['dodaj_nowa_rodzine'])) {

        $test = $this->_model->select('SELECT 1 FROM rodziny WHERE rodzina_id = :id LIMIT 1', array('id' => $_POST['rodzina']));

        if (!empty($test)) {
          $this->_msg->add('error', 'Rodzina o takim ID już istnieje', true, 'admin/import/krok/1/rekord/'.$rekord);
        }

        $testMail = $this->_model->select('SELECT 1 FROM opiekunowie WHERE opiekun_mail != "brak" AND (opiekun_mail = :ojciec OR opiekun_mail = :matka) LIMIT 1', array('ojciec' => $arr[$rekord]['mail_ojca'], 'matka' => $arr[$rekord]['mail_matki']));

        if (!empty($testMail)) {
          $this->_msg->add('error', 'Istnieje już matka lub ojciec z podanym adresem e-mail. Sprawdź czy adresy e-mail w importowanym pliku są poprawne', true, 'admin/import/krok/1/rekord/'.$rekord);
        }

        $insertRodzina = array(
          'rodzina_id' => $arr[$rekord]['id_rodziny'],
          'rodzina_nazwisko' => ucfirst(mb_strtolower($arr[$rekord]['nazwisko'], 'UTF-8')),
          'rodzina_date_add' => date("Y-m-d H:i:s")
        );

        $insTest = $this->_model->insert('rodziny', $insertRodzina);

        $password1 = HASH_OPIEKUN_PASSWORD_KEY.$arr[$rekord]['mail_ojca'];
        $password1 = Hash::create('sha512', $password1, HASH_SITE_KEY);

        $insTest2 = $this->_model->insert('opiekunowie', array(
          'opiekun_rodzina_id' => $arr[$rekord]['id_rodziny'],
          'opiekun_mail' => $arr[$rekord]['mail_ojca'],
          'opiekun_telefon' => $arr[$rekord]['telefon_ojca'],
          'opiekun_date_add' => date("Y-m-d H:i:s"),
          'opiekun_typ' => 1,
          'opiekun_password' => $password1
        ));

        $password2 = HASH_OPIEKUN_PASSWORD_KEY.$arr[$rekord]['mail_matki'];
        $password2 = Hash::create('sha512', $password2, HASH_SITE_KEY);

        $insTest3 = $this->_model->insert('opiekunowie', array(
          'opiekun_rodzina_id' => $arr[$rekord]['id_rodziny'],
          'opiekun_mail' => $arr[$rekord]['mail_matki'],
          'opiekun_telefon' => $arr[$rekord]['telefon_matki'],
          'opiekun_date_add' => date("Y-m-d H:i:s"),
          'opiekun_typ' => 2,
          'opiekun_password' => $password2
        ));

        if ($insTest && $insTest2 && $insTest3) {
          $this->_model->insert('import', array('typ' => 'R', 'id' => $arr[$rekord]['id_rodziny']));

          $rodzice = array(
            $arr[$rekord]['mail_ojca'],
            $arr[$rekord]['mail_matki']
          );

          $ukrytaKopia = 'testportaluuks@gmail.com';

          $mail = new phpmailer();
          $mail->CharSet = "UTF-8";
          $mail->SetLanguage("pl", "libs/");
          $mail->IsHTML(true);

          // SMTP
          $mail->Mailer = 'smtp';
          $mail->Host = MAIL_HOST;
          $mail->Port = MAIL_PORT;
          $mail->SMTPKeepAlive = true;
          $mail->SMTPAuth = true;
          $mail->IsSMTP();
          $mail->Username = MAIL_USER;
          $mail->Password = MAIL_PASS;

          $mail->From = MAIL_FROM;
          $mail->FromName = MAIL_FROM_NAME;
          // $mail->Subject = 'Portal Rodzica UKS Żagle – zapisanie/zmiana/rezygnacja – potwierdzenie dokonanej dyspozycji';
          $mail->Subject = 'UKS Żagle – PORTAL RODZICA UKS ŻAGLE – prosimy o zalogowanie się';

          foreach ($rodzice as $val) {
            $link = MD5($val);

            // $this->_model->insert('uzytkownicy', array('uzytkownik_login' => $val, 'uzytkownik_haslo' => $link, 'uzytkownik_rodzina_id' => $arr[$rekord]['id_rodziny']));
/*
            $wiadomosc = '
              <p>Drodzy Rodzice,</p>
              <p>Informujemy, że <strong>otrzymaliśmy dyspozycję odnośnie zapisania/zmiany/rezygnacji Waszego syna/synów.</strong><br>
              Prosimy oczekiwać (maksymalnie w ciągu 7 dni) na mail zawierający:<br>
              a) potwierdzenie możliwości realizacji Waszej dyspozycji <br>
              b) nowe dane do płatności (w przypadku jej zaakceptowania)</p>
              <p>Szczegóły otrzymanej przez Klub dyspozycji są stale dostępne w zakładce „Zgłoszenia” w Portalu Rodzica (i mogą być w dalszym ciągu zmieniane) a jej status można obserwować w zakładce „Historia zgłoszeń”.</p>
              <p><strong>Prosimy o upewnienie się</stong> czy wszystko zostało poprawnie odnotowane.</p>
              <p>Gdyby okazało się, że są jakieś błędy – prosimy o kontakt poprzez: <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a></p>
              <p>Ze sportowym pozdrowieniem,<br>
              Zarząd UKS Żagle</p>
            ';
*/
            $wiadomosc = '
              <p>Drodzy Rodzice</p>
              <p>Witamy w Portalu Rodzica UKS Żagle. Informujemy, że macie już możliwość zalogowania się.</p>
              <p>Zakładamy, że <strong>dostęp do konta danej rodziny mają dwie osoby: tata i mama. Każdy loguje się podając jako login swój adres mail</strong>. Ze względów bezpieczeństwa system sprawdza czy mail podany przy pierwszym logowaniu zgadza się z mail’em, który nam podaliście.</p>
              <p>Obecnie <strong>prosimy Was o dokonanie pierwszego logowania</strong> (oddzielnie loguje się tata i oddzielnie mama), w którym ustalicie <strong>indywidualne dla każdego (osobne dla taty i osobne dla mamy) hasło</strong>.</p>
              <p>Ci, którzy dokonali już pierwszego logowania proszeni są o sprawdzenie czy ich login i hasło nadal działają poprawnie.</p>
              <p>Ci, którzy otrzymują tą wiadomość po raz pierwszy jako hasła używają adresu e-mail podawanego jako login.</p>
              <center>
                <h2 style="color: red;">LOGIN: '.$val.'</h2>
                <h2 style="color: red;">HASŁO: jw. (dla tych którzy są tu pierwszy raz - '.$val.' dla pozostałych hasło ustalone wcześniej)</h2>
              </center>
              <p>Jeżeli zapomnieliście hasło to zanim powiadomicie biuro UKS spróbujcie proszę poradzic sobie sami. Informacja na ten temat widoczna przy logowaniu.</p>
              <p>Gdyby pojawiły się trudności – prosimy o kontakt mail’owy na <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a></p>
              <center>
                <h1>
                  Wejście do Portalu Rodzica poprzez stronę klubową: <a href="http://ukszagle.pl/">http://ukszagle.pl/</a>
                </h1>
              </center>
              <p>
                Ze sportowym pozdrowieniem,<br>
                Biuro UKS Żagle<br>
                <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a>
              </p>
            ';

            $mail->Body = $wiadomosc;
            $mail->AddBCC(MAIL_TEST);
            $mail->AddAddress($val);
            // $mail->AddAddress(MAIL_TEST);

            $mail->Send();

            $mail->ClearAddresses();
            $mail->ClearAttachments();

          }

          $mail->SmtpClose();

          $this->_msg->add('success', 'Zaimportowano rodzinę');
        } else {
          $this->_msg->add('error', 'Nie udało się zaimportować', true, 'admin/import/krok/1/rekord/'.$rekord);
        }
      }

      $_SESSION['rekord'] = $_POST['rekord'];
      $_SESSION['id_rodziny'] = $_POST['rodzina'];

      header('Location: '.URL.'admin/import/krok/2');
      exit();
    }

    if (isset($_POST['przerwij'])) {
      unset($_SESSION['rekord']);
      unset($_SESSION['import']);

      $this->redirect('admin/import');
    }
  }

  private function _krok_2($params) {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->adminHeader();

    $this->_top->adminTop('import');

    $this->_view->rekord = $_SESSION['rekord'];
    $this->_view->arr = json_decode($_SESSION['import'], true);

    $this->_krok_2_actions($params);

    if (isset($_SESSION['id_rodziny'])) {
      $zgloszeniaArr = array();

      $idRodziny = $_SESSION['id_rodziny'];

      $zgloszenia = $this->_model->select('SELECT rodzina_id, rodzina_nazwisko, zgloszenie_date_add, GROUP_CONCAT(zgloszenie_id) AS zgloszenia, uwagi_tresc AS uwagi FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id LEFT JOIN uwagi ON uwagi_id = zgloszenie_uwagi_id WHERE zgloszenie_status = "1" AND rodzina_id = :rId GROUP BY rodzina_id ORDER BY zgloszenie_date_add', array('rId' => $idRodziny));

      foreach ($zgloszenia as $k => $v) {
        $tmp = $this->_model->select('SELECT zgloszenie_id, rodzina_id, rodzina_nazwisko, dziecko_imie, klasa_nazwa AS klasa, sekcja_nazwa, zgloszenie_date_add, uwagi_tresc, zgloszenie_skladka, zgloszenie_propozycja FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON dziecko_rodzina_id = rodzina_id INNER JOIN klasy ON dziecko_klasa = klasa_id INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id LEFT JOIN uwagi ON uwagi_id = zgloszenie_uwagi_id LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id WHERE zgloszenie_status = "1" AND zgloszenie_id IN ('.$v['zgloszenia'].')');

        $arrTemp = array();

        foreach ($tmp as $k2 => $v2) {
          $arrTemp[$v2['zgloszenie_id']] = $v2;
        }

        $v['zgloszenia'] = $arrTemp;

        $zgloszeniaArr[$v['rodzina_id']] = $v;
      }

      $this->_view->zgloszeniaArr = $zgloszeniaArr;

      $this->_view->dzieci = $this->_model->select('SELECT * FROM dzieci INNER JOIN klasy ON dziecko_klasa = klasa_id WHERE dziecko_rodzina_id = :rId', array('rId' => $idRodziny));
    }


    $this->_view->renderPage('admin/import/krok_2');

    $this->_footer->adminFooter();
  }

  private function _krok_2_actions() {
    $rekord = $_SESSION['rekord'];
    $arr = json_decode($_SESSION['import'], true);

    if (isset($_POST['dodaj_syna'])) {
      $rodzinaTest = $this->_model->select('SELECT 1 FROM rodziny WHERE rodzina_id = :rId', array('rId' => $arr[$rekord]['id_rodziny']));

      if (empty($rodzinaTest)) {
        $this->_msg->add('error', 'W bazie nie ma rodziny o takim Id: '.$arr[$rekord]['id_rodziny'].'', true, 'admin/import/krok/2');
      }

      $test = $this->_model->select('SELECT 1 FROM dzieci WHERE dziecko_id = :id LIMIT 1', array('id' => $arr[$rekord]['id_dziecka']));
      $err = 0;

      if (empty($test)) {
        $klasa = $this->_model->select('SELECT klasa_id FROM klasy WHERE klasa_nazwa = :nazwa LIMIT 1', array(':nazwa' => $arr[$rekord]['klasa']));

        if (empty($klasa)) {
          $this->_msg->add('error', 'Nie ma takiej klasy', true, 'admin/import/krok/2');
        }

        $insertDziecko = array(
          'dziecko_id' => $arr[$rekord]['id_dziecka'],
          'dziecko_rodzina_id' => $arr[$rekord]['id_rodziny'],
          'dziecko_imie' => $arr[$rekord]['imie'],
          'dziecko_klasa' => $klasa[0]['klasa_id'],
          'dziecko_date_add' => date("Y-m-d H:i:s")
        );

        $testDzieckoInsert = $this->_model->insert('dzieci', $insertDziecko);

        $this->_model->insert('import', array('typ' => 'D', 'id' => $arr[$rekord]['id_dziecka']));

        if (!$testDzieckoInsert) {
          $err++;
        }
      } else {
        $this->_msg->add('error', 'W bazie jest już dziecko o takim Id: '.$arr[$rekord]['id_dziecka'].'', true, 'admin/import/krok/2');
      }

      if ($err == 0) {
        $this->_msg->add('success', 'Zaimportowwano dziecko', false, 'admin/import/krok/2');
      }
    }

    if (isset($_POST['importuj']) || isset($_POST['importuj_do_istniejacego'])) {

      $rodzinaTest = $this->_model->select('SELECT 1 FROM rodziny WHERE rodzina_id = :rId', array('rId' => $arr[$rekord]['id_rodziny']));

      if (empty($arr[$rekord]['zgloszenia'])) {
        $this->_msg->add('error', 'Brak zgłoszeń do zaimportowania', false, 'admin/import/krok/2');
      }

      if (empty($rodzinaTest)) {
        $this->_msg->add('error', 'W bazie nie ma rodziny o takim Id: '.$arr[$rekord]['id_rodziny'].'', true, 'admin/import/krok/2');
      }

      $test = $this->_model->select('SELECT 1 FROM dzieci WHERE dziecko_id = :id LIMIT 1', array('id' => $arr[$rekord]['id_dziecka']));
      $err = 0;

      if (empty($test)) {
        $this->_msg->add('error', 'W bazie nie ma dziecka o danym Id: '.$arr[$rekord]['id_dziecka'].'', true, 'admin/import/krok/2');
      }

      if (!isset($_POST['importuj_do_istniejacego'])) {
        if (isset($_SESSION['import_sezon']) && $_SESSION['import_sezon'] != SEZON) {
          $this->_model->update('zgloszenia', array('zgloszenie_status' => '0'), 'zgloszenie_dziecko_id = '.$arr[$rekord]['id_dziecka'].' AND zgloszenie_status = "1" AND (zgloszenie_date BETWEEN "'.$_SESSION['sezon_start'].'" AND "'.$_SESSION['sezon_koniec'].'")');
        } else {
          $this->_model->update('zgloszenia', array('zgloszenie_status' => '0'), 'zgloszenie_dziecko_id = '.$arr[$rekord]['id_dziecka'].' AND zgloszenie_status = "1"');
        }
      }

      $max = $this->_model->select('SELECT MAX(zgloszenie_group_id) as max FROM zgloszenia LIMIT 1');

      $groupId = $max[0]['max'] + 1;

      $zgloszenieData = date("Y-m-d H:i:s");

      foreach ($arr[$rekord]['zgloszenia'] as $key => $val) {
        switch ($key) {
          case 'pilka_nozna':
            $key = 'Piłka nożna';
            break;
          case 'judo':
            $key = 'Judo';
            break;
          case 'szachy':
            $key = 'Szachy';
            break;
          case 'plywanie':
            $key = 'Pływanie';
            break;
          case 'szermierka':
            $key = 'Szermierka';
            break;
        }

        $sekcja = $this->_model->select('SELECT taryfikator_oplata, sekcja_id FROM sekcje INNER JOIN taryfikator ON sekcja_id = taryfikator_sekcja INNER JOIN klasy ON klasa_id = taryfikator_klasa WHERE sekcja_nazwa = :nazwa AND klasa_nazwa = :klasa AND taryfikator_sezon = :sezon LIMIT 1', array('nazwa' => $key, 'klasa' => $arr[$rekord]['klasa'], 'sezon' => $_SESSION['import_sezon']));

        if (!empty($sekcja)) {
          if (isset($_POST['importuj_do_istniejacego'])) {
            $id = $this->_model->select('SELECT zgloszenie_group_id FROM zgloszenia INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id WHERE (zgloszenie_dziecko_id = :id OR dziecko_rodzina_id = :rodzina) AND zgloszenie_status = "1" LIMIT 1', array(':id' => $arr[$rekord]['id_dziecka'], ':rodzina' => $arr[$rekord]['id_rodziny']));

            if (!empty($id)) {
              $groupId = $id[0]['zgloszenie_group_id'];
            } else {
              $this->_msg->add('error', 'Nie ma wcześniejszych zgłoszeń.', false, 'admin/import/krok/2');
            }
          }

          $klasaId = $this->_model->select('SELECT klasa_id FROM klasy WHERE klasa_nazwa = :nazwa LIMIT 1', array(':nazwa' => $arr[$rekord]['klasa']));

          $insertZgloszenia = array(
            'zgloszenie_dziecko_id' => $arr[$rekord]['id_dziecka'],
            'zgloszenie_dziecko_klasa_id' => $klasaId[0]['klasa_id'],
            'zgloszenie_sekcja' => $sekcja[0]['sekcja_id'],
            'zgloszenie_skladka' => $sekcja[0]['taryfikator_oplata'],
            'zgloszenie_propozycja' => $val,
            'zgloszenie_status' => '1',
            'zgloszenie_date_add' => $zgloszenieData,
            'zgloszenie_group_id' => $groupId
          );

          // JEZELI POPRZEDNI SEZON TO USTALAMY DATE POCZATKU I KONCA ZGLOSZENIA (ZALOZENIE ZE ZGLOSZENIE OBOWIAZUJE CALY SEZON) I STATUS 2 CZYLI ZAKONCZONE
          if (isset($_SESSION['import_sezon']) && $_SESSION['import_sezon'] != SEZON) {
            $insertZgloszenia['zgloszenie_date'] = $_SESSION['sezon_start'];
            $insertZgloszenia['zgloszenie_date_end'] = $_SESSION['sezon_koniec'];
            $insertZgloszenia['zgloszenie_status'] = '2';
          }

          $testZgloszeniaInsert = $this->_model->insert('zgloszenia', $insertZgloszenia);
          $lastId = $this->_model->getlastInsertId();

          // JEZELI POPRZEDNI SEZON TO AUTOMATYCZNIE POTWIERDZONE
          if (isset($_SESSION['import_sezon']) && $_SESSION['import_sezon'] != SEZON) {
            $this->_model->delete('potwierdzenia', 'potwierdzenie_zgloszenie_id = '.$lastId.'');
            $this->_model->insert('potwierdzenia', array('potwierdzenie_zgloszenie_id' => $lastId, 'potwierdzenie_status' => '1', 'potwierdzenie_wyslane' => '1'));
          }

          $this->_model->insert('import', array('typ' => 'Z', 'id' => $lastId));

          if (!$testZgloszeniaInsert) {
            $err++;
          } else {
            // JEZELI POPRZEDNI SEZON TO UPDATE SALDO
            if (isset($_SESSION['import_sezon']) && $_SESSION['import_sezon'] != SEZON) {
              $this->_model->updateSaldo($arr[$rekord]['id_rodziny'], $_SESSION['sezon_start'], $_SESSION['sezon_koniec']);
            }
          }
        } else {
          $err++;
          $this->_msg->add('error', 'Sekcja: '.$key.' oraz klasa: '.$arr[$rekord]['klasa'].' nie są ze sobą połączone w taryfikatorze. Sprawdź czy kóreś nie jest błędnie zapisane.', true);
        }

      }

      if ($err == 0) {
        $this->_msg->add('success', 'Zaimportowano zgłoszenie', false, 'admin/import/krok/2');
      } else {
        $this->_msg->add('error', 'Wystąpił błąd, nie wszystkie dane mogły zostać zaimportowane', true, 'admin/import/krok/2');
      }
    }

    if (isset($_POST['przejdz_dalej'])) {
      unset($_SESSION['rekord']);
      unset($_SESSION['id_rodziny']);

      unset($arr[$rekord]);

      $_SESSION['import'] = json_encode(array_filter(array_merge(array(0), $arr)));

      header('Location: '.URL.'admin/import/krok/1/rekord/'.$rekord);
      exit();
    }

    if (isset($_POST['przerwij'])) {
      unset($_SESSION['rekord']);
      unset($_SESSION['id_rodziny']);
      unset($_SESSION['import']);
      unset($_SESSION['sezon']);
      unset($_SESSION['sezon_start']);
      unset($_SESSION['sezon_koniec']);

      $this->redirect('admin/import');
    }
  }
}