<?php

class Uzytkownicy_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
    parent::_isRole('admin');
  }

  public function lista() {
    if (isset($_POST['dodaj-uzytkownika'])) {
      $this->_dodaj();
    }

    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/admin/uzytkownicy.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('uzytkownicy');

    $this->_view->lista = $this->_model->select('
      SELECT user_id, user_name, user_login, user_mail, user_level FROM users WHERE user_login != "studio"
    ');

    $this->_view->renderPage('admin/users/index');

    $this->_footer->adminFooter();
  }

  private function _dodaj() {
    $form = new Form();
    $form->post('login')->val('notEmpty', 'login')
         ->post('mail')->val('notEmpty', 'mail')
         ->post('haslo')->val('notEmpty', 'haslo')
         ->post('imie-nazwisko')->val('notEmpty', 'imię i nazwisko')
         ->post('rola')->val('notEmpty', 'rola');

    if ($form->errorCheck()) {
      $data = $form->fetch();

      $temp = $this->_model->select('SELECT 1 FROM users WHERE user_login = :login OR user_mail = :mail LIMIT 1', array(':login' => $data['login'], ':mail' => $data['mail']));

      if (!empty($temp)) {
        $this->_msg->add('error', 'Użytkownik o takim loginie lub adresie e-mail już istnieje', false, 'admin/uzytkownicy/');
      }

      $arr = array(
        'user_login' => $data['login'],
        'user_mail' => $data['mail'],
        'user_name' => $data['imie-nazwisko'],
        'user_level' => strval($data['rola']),
        'user_password' => Hash::create('sha512', HASH_PASSWORD_KEY.$data['haslo'], HASH_SITE_KEY),
        'user_password_changed' => '1'
      );

      $test = $this->_model->insert('users', $arr);

      if ($test) {
        $this->_msg->add('success', 'Dodano użytkownika', false, 'admin/uzytkownicy/');
      } else {
        $this->_msg->add('error', 'Nie udało się dodać użytkownika', false, 'admin/uzytkownicy/');
      }
    } else {
      $errors = $form->getErrors();
      $errorsArray = array();
      $errorsCount = 0;

      foreach ($errors as $err) {
        $this->_msg->add('error', $err, false);
      }
    }
  }

  public function edytuj($params) {
    $id = $params[0];

    $this->_view->user = $this->_model->select('SELECT user_id, user_login, user_name, user_mail, user_level FROM users WHERE user_id = :id LIMIT 1', array(':id' => $id));

    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('mail')->val('notEmpty', 'mail')
           ->post('mail-old')
           ->post('imie-nazwisko')->val('notEmpty', 'imię i nazwisko')
           ->post('rola')->val('notEmpty', 'rola');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        if ($data['mail'] != $data['mail-old']) {
          $temp = $this->_model->select('SELECT 1 FROM users WHERE user_mail = :mail LIMIT 1', array(':mail' => $data['mail']));

          if (!empty($temp)) {
            $this->_msg->add('error', 'Użytkownik o takim adresie e-mail już istnieje', false, 'admin/uzytkownicy/');
          }
        }

        $arr = array(
          'user_mail' => $data['mail'],
          'user_name' => $data['imie-nazwisko'],
          'user_level' => strval($data['rola'])
        );

        $test = $this->_model->update('users', $arr, 'user_id = :id', array(':id' => $id));

        if ($test) {
          $this->_msg->add('success', 'Zapisano zmiany', false, 'admin/uzytkownicy/');
        } else {
          $this->_msg->add('error', 'Nie udało się zapisać zmian', false, 'admin/uzytkownicy/');
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();
        $errorsCount = 0;

        foreach ($errors as $err) {
          $this->_msg->add('error', $err, false);
        }
      }
    }

    $this->_view->renderPage('admin/users/edytuj');
  }
}