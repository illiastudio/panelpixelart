<?php

class Page_Controller extends Controller
{
  protected $_model;
  protected $_view;
  protected $_header;
  protected $_top;
  protected $_footer;
  protected $_msg;

  public function __construct()
  {
    $session = new Session();
    $session->startSession('page');

    $this->_model = new Page_Model();
    $this->_view = new View();
    $this->_header = new Header_Controller();
    $this->_top = new Top_Controller();
    $this->_footer = new Footer_Controller();
    $this->_msg = new Messages();
  }

  public function loadPage($urlArray)
  {
    if (isset($urlArray[0]) && !empty($urlArray[0])) {
      $modul = $urlArray[0];
      unset($urlArray[0]);
      if (isset($urlArray[1]) && !empty($urlArray[1])) {
        if ($modul == 'login' && $urlArray[1] != 'wyloguj') {
          $action = 'panel';
        } else {
          $action = $urlArray[1];
          $action2 = $urlArray[2];
          unset($urlArray[1]);
        }
      } else {
        switch ($modul) {
          case 'login':
            $action = 'panel';
            break;
          case 'user':
            $action = 'profil';
            break;
          case 'regulamin':
            $modul = 'cms';
            $action = 'index';
            $urlArray = array();
            $urlArray[] = 'regulamin';
            break;
          default:
            $action = 'index';
            break;
        }
      }

      $urlArray = array_values($urlArray);

      $controllerName = $modul . '_Page_Controller';

      $sArray = array('platnosci', 'historia', 'harmonogram');

      if ((class_exists($controllerName) && method_exists($controllerName, $action)) || in_array($modul, $sArray)) {

        if (in_array($modul, $sArray)) {
          $controller = new $controllerName();
          $controller->index($action, $action2);
        } else {
          $reflection = new ReflectionMethod($controllerName, $action);

          if (!$reflection->isPublic()) {
            header('Location: ' . URL);
            exit();
          }

          $controller = new $controllerName();
          $controller->{$action}($urlArray);
        }
      } else {
        header('Location: ' . URL);
        exit();
      }
    } else {
      $this->_homePage();
    }
  }

  private function _homePage()
  {
    $controller = new Home_Page_Controller();
    $controller->index();
    exit();
  }

  protected function _isLogged()
  {
    $auth = new Auth();

    if (!isset($_SESSION['opiekun_id'])) {
      if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        header('Content-Type: application/json');
        echo json_encode(array('type' => 'error', 'msg' => 'Odswież stronę by zalogować się ponownie'));
        exit();
      } else {
        $this->redirect('login/');
      }
    } else {
      $loggedIn = $auth->checkSessionOpiekun();

      if (!$loggedIn) {
        $auth->logout();

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
          header('Content-Type: application/json');
          echo json_encode(array('type' => 'error', 'msg' => 'Odswież stronę by zalogować się ponownie'));
          exit();
        } else {
          $this->redirect('login/');
        }
      } else {
        define("LANG", $_SESSION['page_lang']);

        return true;
      }
    }
  }

  protected function _isAjax()
  {
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      return true;
    } else {
      $this->redirect();
    }
  }

  protected function _checkParams($params, $number, $url)
  {
    $paramsNumber = count($params);
    $hasEmpty = 0;

    foreach ($params as $key => $value) {
      if (empty($value)) {
        $hasEmpty++;
      }
    }

    if ($paramsNumber == $number && $hasEmpty == 0) {
      return true;
    } else {
      $this->redirect($url);
    }
  }

  protected function _checkType($id, $type, $redirectUrl)
  {
    if ($this->_model->checkIfType($id, $type)) {
      return true;
    } else {
      $this->redirect($redirectUrl);
    }
  }
}
