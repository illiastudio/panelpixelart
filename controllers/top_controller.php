<?php

class Top_Controller extends Controller
{
  private $_view;
  protected $_model;

  public function __construct()
  {
    $this->_view = new View();
    $this->_model = new Page_Model();
  }


  public function pageTop($active = null)
  {

    $this->_view->harmonograms = $this->_model->select('SELECT id , names FROM harmonogram');
    
    $this->_view->active = array(
      0 => '',
      1 => '',
      2 => '',
      3 => '',
      4 => '',
      5 => '',
      6 => '',
      7 => '',
      8 => ''
    );

    if (!empty($active)) {
      switch ($active) {
        case 'user':
          $this->_view->active[0] = 'active';
          break;
        case 'zgloszenia':
          $this->_view->active[1] = 'active';
          break;
        case 'platnosci':
          $this->_view->active[2] = 'active';
          break;
        case 'historia':
          $this->_view->active[3] = 'active';
          break;
        case 'dane':
          $this->_view->active[4] = 'active';
          break;
        case 'obecnosci':
          $this->_view->active[5] = 'active';
          break;
        case 'regulamin':
          $this->_view->active[6] = 'active';
          break;
        case 'harmonogram-szkola':
          $this->_view->active[7] = 'active';
          break;
        case 'harmonogram-przedszkole':
          $this->_view->active[8] = 'active';
          break;
      }
    }

    $this->_view->renderPage('page/top');
  }

  public function adminTop($active = null)
  {
    if ($_SESSION['role'] == 'admin') {
      $this->_view->active = array(
        0 => '',
        1 => '',
        2 => '',
        3 => '',
        4 => '',
        5 => '',
        6 => '',
        7 => '',
        8 => '',
        9 => '',
        10 => '',
        11 => '',
        12 => '',
        13 => '',
        14 => '',
        15 => '',
        16 => '',
        17 => '',
        18 => '',
      );

      if (!empty($active)) {
        switch ($active) {
          case 'user':
            $this->_view->active[0] = 'active';
            break;
          case 'rodziny':
            $this->_view->active[1] = 'active';
            break;
          case 'zgloszenia':
            $this->_view->active[2] = 'active';
            break;
          case 'import':
            $this->_view->active[3] = 'active';
            break;
          case 'platnosci':
            $this->_view->active[4] = 'active';
            break;
          case 'zarzadzanie':
            $this->_view->active[5] = 'active';
            break;
          case 'raporty':
            $this->_view->active[6] = 'active';
            break;
          case 'trodziny':
            $this->_view->active[7] = 'active';
            break;
          case 'raport-obecnosci':
            $this->_view->active[8] = 'active';
            break;
          case 'druzyny':
          case 'treningi':
            $this->_view->active[9] = 'active';
            break;
          case 'taryfikator':
            $this->_view->active[10] = 'active';
            break;
          case 'uzytkownicy':
            $this->_view->active[11] = 'active';
            break;
          case 'dodaj':
            $this->_view->active[12] = 'active';
            break;
          case 'strony':
            $this->_view->active[13] = 'active';
            break;
          case 'sekcje':
            $this->_view->active[14] = 'active';
            break;
          case 'klasy':
            $this->_view->active[15] = 'active';
            break;
          case 'cms':
            $this->_view->active[16] = 'active';
            break;
          case 'sezon':
            $this->_view->active[17] = 'active';
            break;
          case 'harmonogram':
            $this->_view->active[18] = 'active';
            break;
        }
      }
    } else if ($_SESSION['role'] == 'trener') {
      $this->_view->active = array(
        0 => '',
        1 => '',
        2 => '',
        3 => '',
      );

      if (!empty($active)) {
        switch ($active) {
          case 'user':
            $this->_view->active[0] = 'active';
            break;
          case 'trodziny':
            $this->_view->active[1] = 'active';
            break;
          case 'raport-obecnosci':
            $this->_view->active[2] = 'active';
            break;
          case 'druzyny':
          case 'treningi':
            $this->_view->active[3] = 'active';
            break;
        }
      }
    }
    $this->_view->renderPage('admin/top');
  }
}
