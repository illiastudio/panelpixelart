<?php

class Menu_Controller extends Controller {
  private $_model;
  private $_view;

  public function __construct() {
    $this->_model = new Menu_Model();
    $this->_view = new View();
  }

  public function buildAdminMenu($name, $nameShow, $pre, $parent) {
    $this->usePageTypesTitle();

    $menu = $this->_model->multiLevelMenu($pre);
    $list = '';

    if (isset($menu['parents'][$parent])) {
      $class = '';

      if ($parent == 0) {
        $class = 'class="'.$name.' nestedSort"';
        $list .= '<h3>'.$nameShow.':</h3>';
       }

      $list .= '<ol '.$class.'>';

      foreach ($menu['parents'][$parent] as $itemId) {
        $item = $menu['items'][$itemId];

        $status = ($item['page_status'] == 0) ? 'N' : 'Y';
        $show = ($item['page_show'] == 0) ? 'N' : 'Y';

        $list .= '
        <li id="'.$pre.$item['link_url'].'_'.$item['relation_element_id'].'">
          <div class="listItem">
            <span class="handle"></span>
            <span id="status_'.$item['relation_element_id'].'" class="status'.$status.'" title="Strona włączona"></span>
            <span id="show_'.$item['relation_element_id'].'" class="status'.$show.'" title="Strona wyswietlana w menu"></span>
            <span class="modul'.$item['page_type'].'" title="'.$this->pageTypesTitle[$item['page_type']].'"></span>
            <p>'.$item['link_name'].'</p>
            <div class="menu-buttons">
              <a class="edit" href="'.URL.'admin/struktura/edytuj/'.$item['relation_element_id'].'" title="Edytuj"><span class="none">Edycja</span></a>
              <a class="delete" href="'.URL.'admin/struktura/usun/'.$item['relation_element_id'].'" title="Usuń"><span class="none">Usuń</span></a>
              <a class="metatag" href="'.URL.'admin/metatagi/edytuj/'.$item['relation_element_id'].'" title="Metatagi"><span class="none">Metatagi</span></a>
            </div>
          </div>';

        if (isset($menu['parents'][$itemId])) {
          $list .= $this->buildAdminMenu($name, $nameShow, $pre, $itemId);
        }

        $list .= '</li>';
      }

      $list .= '</ol>';
    }

    return $list;
  }

}