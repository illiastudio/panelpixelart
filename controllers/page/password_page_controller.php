<?php

class Password_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
  }

  public function reset($params) {
    if (isset($params[0]) && !empty($params[0])) {
      $code = $params[0];

      $this->_resetPassowrd($code);
      $this->_view->renderPage('page/password_reset');
      exit();
    }

    if (isset($_POST['wyslij'])) {
      $form = new Form();
      $form->post('login')->val('notEmpty', 'login');

      if ($form->errorCheck()) {
        $data = $form->fetch();
      } else {
        $form->showErrors();
        $this->reload();
      }

      $test = $this->_model->select('SELECT 1 FROM opiekunowie WHERE opiekun_mail = :login', array(':login' => $data['login']));

      if (!empty($test)) {
        $mailAdres = $data['login'];

        $random = new Random();
        $string = $random->create(50);

        $mail = new phpmailer();
        $mail->CharSet = "UTF-8";
        $mail->SetLanguage("pl", "libs/");
        $mail->IsHTML(true);

        // SMTP DO TESTOW Z LOCALHOSTA
        $mail->Mailer = 'smtp';
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPKeepAlive = true;
        $mail->SMTPAuth = true;
        $mail->IsSMTP();
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;

        $mail->From = MAIL_FROM;
        $mail->FromName = MAIL_FROM_NAME;

        $mail->Subject = 'Panel Rodzica - reset hasła';
        $mail->Body = 'Aby zresetować hasło kliknij w podany niżej link: <br><a href="'.URL.'password/reset/'.$string.'">'.URL.'password/reset/'.$string.'</a>. <br>Po kliknięciu w następnym mailu otrzymasz nowe hasło.';
       $mail->AddAddress($mailAdres);

        if($mail->Send()) {
          $this->_model->updateOpiekunPassowrdCode($data['login'], $string);

          $this->_msg->add('success', 'Wysłano kod resetujący hasło.', false, 'login');
        } else {
          $this->_msg->add('error', 'Nie udało się wysłać wiadomości.', false, 'password/reset');
        }

        $mail->ClearAddresses();
        $mail->ClearAttachments();
        $mail->SmtpClose();
      }
    }

    ob_start();
    $this->_msg->render();
    $this->_view->msg = ob_get_contents();
    ob_end_clean();

    $this->_view->renderPage('page/password_reset');
  }

  private function _resetPassowrd($code) {
    $code = htmlspecialchars($code);

    $user = $this->_model->checkOpiekunPasswordCodeAndGetUser($code);

    if (!empty($user)) {
      $random = new Random();
      $haslo = $random->create(6);
      $hasloHash = Hash::create('sha512', HASH_OPIEKUN_PASSWORD_KEY.$haslo, HASH_SITE_KEY);

      $mail = new phpmailer();
      $mail->CharSet = "UTF-8";
      $mail->SetLanguage("pl", "libs/");
      $mail->IsHTML(true);

      // SMTP DO TESTOW Z LOCALHOSTA
      $mail->Mailer = 'smtp';
      $mail->Host = MAIL_HOST;
      $mail->Port = MAIL_PORT;
      $mail->SMTPKeepAlive = true;
      $mail->SMTPAuth = true;
      $mail->IsSMTP();
      $mail->Username = MAIL_USER;
      $mail->Password = MAIL_PASS;

      $mail->From = MAIL_FROM;
      $mail->FromName = MAIL_FROM_NAME;

      $mail->Subject = 'Panel Rodzica - reset hasła';
      $mail->Body = 'Nowe hasło: <br>'.trim($haslo);
      $mail->AddAddress($user['opiekun_mail']);

      if($mail->Send()) {
        $this->_model->resetOpiekunPassword($user['opiekun_mail'], $hasloHash);

        $this->_msg->add('success', 'Wysłano nowe hasło.', false, 'login');
      } else {
        $this->_msg->add('error', 'Nie udało się wysłać wiadomości.', false, 'password/reset');
      }

      $mail->ClearAddresses();
      $mail->ClearAttachments();
      $mail->SmtpClose();
    }

    ob_start();
    $this->_msg->render();
    $this->_view->msg = ob_get_contents();
    ob_end_clean();

    $this->redirect('page/login');
  }

}