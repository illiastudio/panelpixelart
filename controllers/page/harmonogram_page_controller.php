<?php

class Harmonogram_Page_Controller extends Page_Controller
{
  public function __construct()
  {
    parent::__construct();
    parent::_isLogged();
  }

  public function index($param, $param2)
  {
    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/page/harmonogram.js');
    $this->_header->addStyles('file', 'public/css/page/harmonogram.css');
    $this->_header->pageHeader();

    $this->_top->pageTop('harmonogram-szkola');

    $sections = $this->_model->select('SELECT s.sekcja_id, s.sekcja_nazwa FROM sekcje s 
                                       INNER JOIN harmonogram_sekcje hs ON s.sekcja_id = hs.id_sekcje
                                       WHERE s.sekcja_status = 1');

    $activeItemForSections = $sections[0]['sekcja_id'];

    if (empty($param2)) {
      $param2 = $activeItemForSections;
    }

    $harmonogram = $this->_model->select('SELECT * from harmonogram_sekcje hs
                                          INNER JOIN harmonogram h on h.id = hs.id_harmonogram
                                          INNER JOIN sekcje s ON s.sekcja_id = hs.id_sekcje
                                          WHERE hs.id_sekcje = ' . $param2 . ' AND hs.id_harmonogram = ' . $param);



    // echo var_dump($harmonogram);
    // echo '________';
    // echo var_dump($sections);
    $this->_view->school = $param;
    $this->_view->active = $param2;
    $this->_view->sections = $sections;
    $this->_view->table = $harmonogram;
    $this->_view->renderPage('page/harmonogram');
    $this->_footer->pageFooter();
  }
  //!new


  // !old
  // public function szkola()
  // {
  //   $this->_header->pageDefaultScriptsAndStyles();
  //   $this->_header->addScripts('file', 'public/js/page/harmonogram.js');
  //   $this->_header->addStyles('file', 'public/css/page/harmonogram.css');

  //   $this->_header->pageHeader();

  //   $this->_top->pageTop('harmonogram-szkola');

  //   $doc = new DomDocument;
  //   $doc->validateOnParse = true;
  //   libxml_use_internal_errors(true);
  //    $doc->loadHtml(mb_convert_encoding(file_get_contents('http://ukszagle.pl/szkola-zagle-harmonogram/'), 'HTML-ENTITIES', 'UTF-8'));
  //    libxml_use_internal_errors(false);

  //   $this->_view->content = $this->_getInnerHtml($doc->getElementById('content-section-1'));
  //   $this->_view->renderPage('page/harmonogram');

  //   $this->_footer->pageFooter();
  // }

  public function przedszkole1()
  {
    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/page/harmonogram.js');
    $this->_header->addStyles('file', 'public/css/page/harmonogram.css');

    $this->_header->pageHeader();

    $this->_top->pageTop('harmonogram-przedszkole');

    $doc = new DomDocument;
    $doc->validateOnParse = true;
    libxml_use_internal_errors(true);
    $doc->loadHtml(mb_convert_encoding(file_get_contents('http://ukszagle.pl/przedszkole-strumienie-harmonogram/'), 'HTML-ENTITIES', 'UTF-8'));
    libxml_use_internal_errors(false);

    $this->_view->content = $this->_getInnerHtml($doc->getElementById('content-section-1'));
    $this->_view->renderPage('page/harmonogram');

    $this->_footer->pageFooter();
  }

  private function _getInnerHtml($node)
  {
    $innerHTML = '';
    $children = $node->childNodes;
    foreach ($children as $child) {
      $innerHTML .= $child->ownerDocument->saveXML($child);
    }

    return $innerHTML;
  }
}
