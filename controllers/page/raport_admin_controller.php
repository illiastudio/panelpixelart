<?php

class Raport_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista() {
    $zgloszenia = $this->_model->select('SELECT * FROM raport INNER JOIN rodziny ON rodzina_id = dziecko_rodzina_id ORDER BY zgloszenie_date_add');

    $raport = "imie i nazwisko|klasa|sekcja|skladka wg. taryfikatora|propozycja skladki|data zgloszenia|imie ojca|mail ojca|telefon ojca|imie matki|mail matki|telefon matki|status|uwagi|status odpowiedzi|status wysylki \n";

    foreach ($zgloszenia as $k => $v) {
      if ($v['zgloszenie_status'] == '0') {
        $v['zgloszenie_status'] = 'nieaktualne';
      }

      if ($v['zgloszenie_status'] == '1') {
        $v['zgloszenie_status'] = 'aktualne';
      }

      if ($v['zgloszenie_status'] == '2') {
        $v['zgloszenie_status'] = 'rezygnacja';
      }

      if ($v['potwierdzone'] == '1') {
        $v['potwierdzone'] = 'zaakceptowane';
      }

      if ($v['potwierdzone'] == '2') {
        $v['potwierdzone'] = 'odrzucone';
      }

      if ($v['wyslane'] == '0') {
        $v['wyslane'] = 'niewyslane';
      }

      if ($v['wyslane'] == '1') {
        $v['wyslane'] = 'wyslane';
      }

      $raport .= $v['dziecko_imie'].' '.$v['rodzina_nazwisko'].'|'.$v['klasa_nazwa'].'|'.$v['sekcja_nazwa'].'|'.$v['zgloszenie_skladka'].'|'.$v['zgloszenie_propozycja'].'|'.$v['zgloszenie_date_add'].'|'.$v['ojciec_imie'].'|'.$v['ojciec_mail'].'|'.$v['ojciec_telefon'].'|'.$v['matka_imie'].'|'.$v['matka_mail'].'|'.$v['matka_telefon'].'|'.$v['zgloszenie_status'].'|'.$v['uwagi_tresc'].'|'.$v['potwierdzone'].'|'.$v['wyslane']."\n";
    }

    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename=zagle_raport.txt");
    header("Content-Transfer-Encoding: binary");

    header('Content-Type: text/html; charset=utf-8');

    echo $raport;
    die();
  }
}