<?php

class Rodziny_Admin_Controller extends Admin_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function lista() {
    $this->_header->adminDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/admin/rodziny.js');
    $this->_header->adminHeader();

    $this->_top->adminTop('rodziny');

    $this->_view->panel = $this->_view->renderPage('admin/rodziny/panel', false);
    $this->_view->content = $this->_view->renderPage('admin/rodziny/lista', false);

    $this->_view->renderPage('admin/rodziny/index');

    $this->_footer->adminFooter();
  }

  public function dodaj() {
    if (isset($_POST['dodaj'])) {
      $form = new Form();
      $form->post('rodzina_nazwisko')->val('notEmpty', 'nazwisko');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $data['rodzina_mod_user'] = $_SESSION['user_id'];
        $data['rodzina_date_add'] = date("Y-m-d H:i:s");

        $test = $this->_model->insert('rodziny', $data);

        if ($test) {
          header('Content-Type: application/json');
          echo json_encode(array(array('type' => 'success', 'msg' => 'Dodano nową rodzinę')));
        } else {
          header('Content-Type: application/json');
          echo json_encode(array(array('type' => 'error', 'msg' => 'Wystąpił błąd')));
        }
      } else {
        $errors = $form->getErrors();
        $errorsArray = array();

        foreach ($errors as $err) {
          $errorsArray[] = array('type' => 'error', 'msg' => $err);
        }

        header('Content-Type: application/json');
        echo json_encode($errorsArray);
      }
    }
  }

  public function edytuj($params) {
    $paramsNumber = count($params);

    $rodzinaObj = new Rodzina_Admin_Controller();
    $rodzinaObj->init($params);
  }

  public function usun($params) {
    parent::_checkParams($params, 1, 'admin/rodziny/lista');
    $id = $params[0];

    $test = $this->_model->delete('rodziny', 'rodzina_id = :id', array(':id' => $id));

    if ($test) {
      header('Content-Type: application/json');
      echo json_encode(array(array('type' => 'success', 'msg' => 'Usunięto rodzine')));
    } else {
      header('Content-Type: application/json');
      echo json_encode(array(array('type' => 'error', 'msg' => 'Wystąpił błąd')));
    }
  }

  public function getList() {
    parent::_isAjax();

    $aColumns = array(
      'rodzina_id',
      'rodzina_nazwisko',
      'ojciec_mail',
      'matka_mail',
      'status',
      'status_sort',
      'opcje'
    );

    /* Paging */
    $sLimit = '';

    if (isset($_GET['start']) && $_GET['length'] != -1) {
      $sLimit = "LIMIT ".intval($_GET['start']).", ".intval($_GET['length']);
    }

    /* Ordering */
    $sOrder = '';

    if (isset($_GET['order'])) {
      foreach ($_GET['order'] as $key => $value) {
        $sOrder .= $aColumns[$value['column']].' '.$value['dir'].', ';
      }

      if ($sOrder != '') {
        $sOrder = rtrim($sOrder, ', ');
        $sOrder = 'ORDER BY '.$sOrder;
      }
    } else {
      $sOrder = 'ORDER BY data_sort DESC';
    }

    /* Filtering */
    $sWhere = '';

    if (isset($_GET['search']) && $_GET['search']['value'] != '') {
      $sWhere = 'WHERE (';
      $str = htmlspecialchars($_GET['search']['value']);

      for ($i = 0; $i < count($aColumns); $i++) {
        if (in_array($aColumns[$i], array('opcje', 'naleznosc', 'status', 'status_sort'))) {
          continue;
        } else if ($aColumns[$i] == 'ojciec_mail') {
          $sWhere .= "m1.opiekun_mail LIKE '%".$str."%' OR ";
        } else if ($aColumns[$i] == 'matka_mail') {
          $sWhere .= "m2.opiekun_mail LIKE '%".$str."%' OR ";
        } else {
          $sWhere .= $aColumns[$i]." LIKE '%".$str."%' OR ";
        }
      }

      $sWhere = substr_replace($sWhere, "", -3);
      $sWhere .= ')';
    }

    /* SQL queries */
    $sQuery = "
      SELECT
        SQL_CALC_FOUND_ROWS
        rodzina_id,
        rodzina_nazwisko,
        m1.opiekun_mail AS ojciec_mail,
        m2.opiekun_mail AS matka_mail,
        COALESCE(NULL, do_zaplaty, 0) AS naleznosc,
        COALESCE(pb_bilans, 0) pb_bilans,
        (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort
      FROM
       rodziny
        LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id
        LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id
        INNER JOIN opiekunowie m1 ON m1.opiekun_rodzina_id = rodzina_id AND m1.opiekun_typ = 1
        INNER JOIN opiekunowie m2 ON m2.opiekun_rodzina_id = rodzina_id AND m2.opiekun_typ = 2
        LEFT JOIN (
          SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty
          FROM (
            SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty
            FROM platnosci_saldo
            GROUP BY ps_rodzina_id) ps
          LEFT JOIN (
            SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono
            FROM platnosci
            GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id
      $sWhere
        GROUP BY rodzina_id
      $sOrder
      $sLimit
    ";
    $rResult = $this->_model->select($sQuery);

    /* Data set length after filtering */
    $sQuery = "
      SELECT FOUND_ROWS() AS ile
    ";
    $rResultFilterTotal = $this->_model->select($sQuery);
    $iFilteredTotal = $rResultFilterTotal[0]['ile'];

    /* Total data set length */
    $sQuery = "
      SELECT COUNT(rodzina_id) AS ile
      FROM
        rodziny
    ";
    $rResultTotal = $this->_model->select($sQuery);
    $iTotal = $rResultTotal[0]['ile'];


    /*
     * Output
    */
    if (!isset($_GET['sEcho'])) {
      $_GET['sEcho'] = 0;
    }

    $output = array(
      "sEcho" => intval($_GET['sEcho']),
      "iTotalRecords" => $iTotal,
      "iTotalDisplayRecords" => $iFilteredTotal,
      "aaData" => array()
    );

    foreach ($rResult as $key => $value) {
      $row = array();

      for ($i = 0; $i < count($aColumns); $i++) {
        if ($aColumns[$i] == 'opcje') {
          $row[] = '<div class="menu-buttons">
            <a class="edit" href="'.URL.'admin/rodziny/edytuj/'.$value['rodzina_id'].'" title="Przeglądaj"><span class="none">Edycja</span></a>
          </div>';
        } elseif ($aColumns[$i] == 'status') {
          switch ($value['status_sort']) {
            case 0:
              $row[] = '<span title="Niedopłata" class="platnosc-err"></span>';
              break;
            case 1:
              $row[] = '<span title="Nadpłata" class="platnosc-plus"></span>';
              break;
            case 2:
              $row[] = '<span title="Saldo ok" class="platnosc-ok"></span>';
              break;
            case 3:
              $row[] = '<span title="Brak należności" class="platnosc-empty"></span>';
              break;
          }
        } else {
          $row[] = $value[$aColumns[$i]];
        }

      }

      $output['aaData'][] = $row;
    }

    echo json_encode($output);
  }

}