<?php

class Platnosci_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $login = new Login_Page_Controller();
    $login->checkPasswordChanged();
  }

  public function index($sezon) {
    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/data-tables/js/jquery.dataTables.min.js');
    $this->_header->addScripts('file', 'public/js/page/platnosci.js');
    $this->_header->pageHeader();

    $this->_top->pageTop('platnosci');

    $model = new Rodziny_Admin_Model();

    $this->_view->months = array(
      1 => 'Styczeń',
      2 => 'Luty',
      3 => 'Marzec',
      4 => 'Kwiecień',
      5 => 'Maj',
      6 => 'Czerwiec',
      7 => 'Lipiec',
      8 => 'Sierpień',
      9 => 'Wrzesień',
      10 => 'Październik',
      11 => 'Listopad',
      12 => 'Grudzień'
    );

    $id = $_SESSION['opiekun_rodzina_id'];

    switch ($model->checkStatusPlatnosci($id, $sezon)) {
      case 0:
        $status = 'niedoplata';
        break;
      case 1:
        $status = 'nadplata';
        break;
      case 2:
        $status = 'saldo-ok';
        break;
      case 3:
        $status = 'platnosc-err';
        break;
      default:
        $status = '';
        break;
    }

    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $this->_view->sezony = $this->_model->getSezony();
    $this->_view->thisSezon = $sezon;
    $this->_view->platnosci = $model->getPlatnosci($id, $sezon);
    $this->_view->saldo = $model->getSaldo($id, $sezon);
    $this->_view->zaplacono = $model->getZaplacono($id, $sezon);
    $this->_view->statusClass = $status;
    $this->_view->last = $model->checkLastPlatnosc($id, $sezon);
    $this->_view->poprzednieSezony = $model->getPoprzednieSezony($id, $sezon);
    $this->_view->renderPage('page/platnosci/index');

    $this->_footer->pageFooter();
  }
}