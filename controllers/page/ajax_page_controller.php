<?php

class Ajax_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function ustawKlase() {
    parent::_isAjax();

    $id = (int) $_POST['dziecko_id'];

    $klasa = $this->_model->select('SELECT klasa_nazwa AS dziecko_klasa, klasa_id FROM dzieci INNER JOIN klasy ON dziecko_klasa = klasa_id WHERE dziecko_id = :id', array(':id' => $id));

    $sekcje = $this->_model->select('SELECT sekcja_id, sekcja_nazwa FROM sekcje INNER JOIN taryfikator ON sekcja_id = taryfikator_sekcja WHERE taryfikator_klasa = :klasa AND taryfikator_sezon = :sezon ORDER BY sekcja_nazwa ASC', array(':klasa' => $klasa[0]['klasa_id'], ':sezon' => SEZON));

    $sekcjeTxt = '<option value="0">--- Wybierz sekcję ---</option>';

    foreach ($sekcje as $k => $v) {
      $sekcjeTxt .= '<option value="'.$v['sekcja_id'].'">'.$v['sekcja_nazwa'].'</option>';
    }

    header('Content-Type: application/json');
    echo json_encode(array(array(
      'klasa' => $klasa[0]['dziecko_klasa'],
      'klasa_id' => $klasa[0]['klasa_id'],
      'sekcje' => $sekcjeTxt
    )));

    die();
  }

  public function ustawCene() {
    parent::_isAjax();

    $sekcja = $_POST['sekcja'];
    $klasa = $_POST['klasa'];

    $cena = $this->_model->select('SELECT taryfikator_oplata FROM taryfikator WHERE taryfikator_klasa = :klasa AND taryfikator_sekcja = :sekcja AND taryfikator_sezon = :sezon', array(':klasa' => $klasa, ':sekcja' => $sekcja, ':sezon' => SEZON));

    $cena = $cena[0]['taryfikator_oplata'];

    header('Content-Type: application/json');
    echo json_encode(array(array(
      'cena' => $cena
    )));

    die();
  }

  public function dodajRekord() {
    parent::_isAjax();

    $rodzinaId = (int) $_SESSION['opiekun_rodzina_id'];

    $dzieci = $this->_model->select('SELECT d.*, rodzina_nazwisko FROM dzieci d, rodziny WHERE rodzina_id = dziecko_rodzina_id AND rodzina_id = :id', array(':id' => $rodzinaId));

    $sekcje = $this->_model->select('SELECT * FROM sekcje');

    $id = $_POST['row_id'] + 1;
    $row = '';
    $row = '
      <tr id="'.$id.'" class="row">
        <td>
          <span class="js-usun-rekord"></span>
            <input type="hidden" name="imie_nazwisko['.$id.']">
            <select class="imie_nazwisko input" name="imie_nazwisko['.$id.']">
              <option value="0" selected>--- Wybierz dziecko ---</option>';
              foreach ($dzieci as $k => $v) {
                $row .= '<option value="'.$v['dziecko_id'].'">'.$v['dziecko_imie'].' '.$v['rodzina_nazwisko'].'</option>';
              }
      $row .= '
            </select>
        </td>
        <td class="klasa">
          <span></span>
          <input type="hidden" name="klasa['.$id.']" value="">
        </td>
        <td>
          <input type="hidden" name="sekcja_sportowa['.$id.']">
          <select name="sekcja_sportowa['.$id.']" class="sekcja_sportowa input" disabled>
          </select>
        </td>
        <td class="taryfa">
          <span></span>
          <input type="hidden" name="taryfa['.$id.']" value="0">
        </td>
        <td>
          <input type="hidden" name="rezygnacja['.$id.']" value="0">
          <input class="rezygnuj" type="checkbox" name="rezygnacja['.$id.']" value="1">
        </td>
      </tr>
    ';

    header('Content-Type: application/json');
    echo json_encode(array(array(
      'row' => $row
    )));

    die();
  }

}