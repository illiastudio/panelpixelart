<?php

class Historia_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $login = new Login_Page_Controller();
    $login->checkPasswordChanged();
  }

  public function index($sezon) {
    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->pageHeader();

    $this->_top->pageTop('historia');

    // $date = START_DATE;
    // $end_date = date('Y-m-01', time());

    if ($sezon == 'index' || empty($sezon)) {
      $sezon = SEZON;
    }

    $daty = $this->_model->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => $sezon));
    $daty = $daty[0];

    // $naliczonePlatnosci = $this->_model->select('SELECT ps_okres, ps_zgloszenia FROM platnosci_saldo WHERE ps_rodzina_id = :id AND ps_okres >= :data ORDER BY ps_okres DESC', array(':id' => $_SESSION['opiekun_rodzina_id'], ':data' => $date));

   $naliczonePlatnosci = $this->_model->select('SELECT ps_okres, ps_zgloszenia FROM platnosci_saldo WHERE ps_rodzina_id = :id AND (ps_okres BETWEEN :start AND :koniec) ORDER BY ps_okres DESC', array(':id' => $_SESSION['opiekun_rodzina_id'], ':start' => $daty['sezon_start'], ':koniec' => $daty['sezon_koniec']));

    $arr = array();

    foreach ($naliczonePlatnosci as $key => $val) {
      if (!empty($val['ps_zgloszenia'])) {
        $arr[$val['ps_okres']] = $this->_model->select('
          SELECT zgloszenie_id, zgloszenie_group_id, dziecko_id, dziecko_imie, klasa_nazwa, klasa_id,   zgloszenie_sekcja, sekcja_nazwa, taryfikator_oplata AS skladka, zgloszenie_propozycja,  zgloszenie_uwagi_id, zgloszenie_date, zgloszenie_date_end
          FROM zgloszenia
          INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
          LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id
          INNER JOIN klasy ON klasa_id = zgloszenie_dziecko_klasa_id
          INNER JOIN sekcje ON zgloszenie_sekcja = sekcja_id
          INNER JOIN taryfikator ON (taryfikator_sekcja = zgloszenie_sekcja AND taryfikator_klasa = dziecko_klasa AND taryfikator_sezon = "'.$sezon.'")
          WHERE zgloszenie_id IN ('.$val['ps_zgloszenia'].')
        ');
      }
    }
    $this->_view->sezony = $this->_model->getSezony();
    $this->_view->thisSezon = $sezon;
    $this->_view->arr = $arr;
    $this->_view->months = array('Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień');

    $this->_view->renderPage('page/historia/index');

    $this->_footer->pageFooter();
  }
}