<?php

class Cms_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function index($params) {
    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->pageHeader();

    $this->_top->pageTop($params[0]);

    $this->_view->cms = $this->_model->select('SELECT * FROM cms WHERE cms_slug = :slug LIMIT 1', array(':slug' => $params[0]));
    $this->_view->renderPage('page/cms');

    $this->_footer->pageFooter();
  }
}