<?php

class Obecnosci_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $login = new Login_Page_Controller();
    $login->checkPasswordChanged();
  }

  public function index() {
    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->pageHeader();

    $this->_top->pageTop('obecnosci');

    $this->_view->arr = $this->_getData();

    $miesiace = array();
    $miesiace[1] = 'Styczeń';
    $miesiace[2] = 'Luty';
    $miesiace[3] = 'Marzec';
    $miesiace[4] = 'Kwiecień';
    $miesiace[5] = 'Maj';
    $miesiace[6] = 'Czerwiec';
    $miesiace[9] = 'Wrzesień';
    $miesiace[10] = 'Październik';
    $miesiace[11] = 'Listopad';
    $miesiace[12] = 'Grudzień';
    $this->_view->miesiace = $miesiace;

    $this->_view->renderPage('page/obecnosci/index');

    $this->_footer->pageFooter();
  }

  private function _getData() {
    $arr = array();
    $treningi = array();

    $dzieci = $this->_model->select('
      SELECT
        rodzina_id,
        dziecko_id,
        rodzina_nazwisko,
        dziecko_imie,
        klasa_nazwa,
        druzyna_sekcja,
        druzyna_nazwa,
        sekcja_nazwa,
        druzyna_id
      FROM rodziny
      INNER JOIN dzieci ON rodzina_id = dziecko_rodzina_id
      INNER JOIN klasy ON dziecko_klasa = klasa_id
      INNER JOIN druzyny_dzieci ON dd_dziecko = dziecko_id
      INNER JOIN druzyny ON druzyna_id = dd_druzyna
      INNER JOIN sekcje ON sekcja_id = druzyna_sekcja
      WHERE rodzina_id = :id
      ORDER BY rodzina_nazwisko ASC, dziecko_imie ASC
    ', array(':id' => $_SESSION['opiekun_rodzina_id']));

    foreach ($dzieci as $key => $val) {
      $arr[$val['dziecko_id']]['imie'] = $val['dziecko_imie'];
      $arr[$val['dziecko_id']]['nazwisko'] = $val['rodzina_nazwisko'];
      $arr[$val['dziecko_id']]['klasa'] = $val['klasa_nazwa'];
      $arr[$val['dziecko_id']]['druzyny'][$key]['druzyna'] = $val['druzyna_id'];
      $arr[$val['dziecko_id']]['druzyny'][$key]['sekcja'] = $val['druzyna_sekcja'];
    }

    $sezArr = explode('-', SEZON);

    foreach ($arr as $key => $val) {
      foreach ($val['druzyny'] as $key2 => $val2) {
        $treningi[$key][$val2['druzyna']] = $this->_model->select('
          SELECT
            trening_id,
            MONTH(trening_data) AS miesiac,
            DAY(trening_data) AS dzien,
            sekcja_nazwa,
            druzyna_nazwa
          FROM treningi
          INNER JOIN druzyny ON druzyna_id = trening_druzyna
          INNER JOIN sekcje ON sekcja_id = druzyna_sekcja
          WHERE trening_druzyna = :druzyna
            AND (YEAR(trening_data) BETWEEN :start AND :koniec)
            ORDER BY trening_data
        ', array(':druzyna' => $val2['druzyna'], ':start' => '20'.$sezArr[0], ':koniec' => '20'.$sezArr[1]));
      }
    }

    foreach ($treningi as $key => $val) {
      foreach ($val as $key2 => $val2) {
        foreach ($val2 as $key3 => $val3) {
          $t = $this->_model->select('
            SELECT
              obecnosc_status
            FROM treningi
            INNER JOIN obecnosci ON trening_id = obecnosc_trening
            WHERE obecnosc_trening = :trening AND obecnosc_dziecko = :dziecko LIMIT 1
          ', array(':trening' => $val3['trening_id'], ':dziecko' => $key));

          if (!empty($t)) {
            $status = $t[0]['obecnosc_status'];
          } else {
            $status = 0;
          }

          $arr[$key]['treningi'][$key2]['druzyna'] = $val3['druzyna_nazwa'];
          $arr[$key]['treningi'][$key2]['sekcja'] = $val3['sekcja_nazwa'];
          $arr[$key]['treningi'][$key2]['arr'][$val3['miesiac']][$val3['dzien']] = $status;
        }
      }
    }

    return $arr;
  }
}