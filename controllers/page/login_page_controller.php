<?php

class Login_Page_Controller extends Page_Controller {
  private $_auth;

  public function __construct() {
    parent::__construct();

    $this->_auth = new Auth();
  }

  public function panel($url) {
    $this->_login($url);

    ob_start();
    $this->_msg->render();
    $this->_view->msg = ob_get_contents();
    ob_end_clean();

    $this->_view->renderPage('page/login');
  }

  private function _login($url) {
    if (isset($url[0]) && !empty($url[0])) {
      $this->_loginWithLink($url[0]);
    }

    if (isset($_POST['zaloguj'])) {
      $form = new Form();
      $form->post('login')->val('notEmpty', 'login')
           ->post('haslo')->val('notEmpty', 'haslo');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        $logged = $this->_auth->loginOpiekun($data['login'], $data['haslo']);

        if ($logged) {
          $this->_checkIssetMail();
          $this->checkPasswordChanged();

          $this->redirect('platnosci/');
        } else {
          $this->_msg->add('error', 'Nieprawidłowy login lub hasło', false, 'login');
        }
      } else {
        $form->showErrors();
        $this->reload();
      }
    }
  }

  public function wyloguj() {
    $this->_auth->logout();

    $this->redirect('login/');
  }

  private function _checkIssetMail() {
    $test = $this->_model->checkIsset('opiekunowie', 'opiekun_mail', array('opiekun_mail' => $_SESSION['opiekun_login']));

    if (!$test) {
      $this->_msg->add('info', 'Twoje konto nie ma przypisanego adresu e-mail. W przypadku utraty hasła, niemożliwe będzie jego samodzielne odzyskanie.<br><br>Aby ustawić adres e-mail wejdź <a href="'.URL.'user">w edycję profilu</a>.', true);
    }
  }

  public function checkPasswordChanged() {

    $test = $this->_model->getValue('opiekunowie', 'opiekun_password_changed', array('opiekun_mail' => $_SESSION['opiekun_login']));

    if ($test == 0) {
      $this->_msg->add('info', 'Twoje konto posiada domyślne hasło. Znacznie zwiększa to ryzyko dostępu do niego dla osób nieupoważnionych.<br><br>Aby korzystać z serwisu musisz zmienić hasło.', true);
      $this->redirect('user');
    }

  }

  private function _loginWithLink($activeLink) {
    $logged = $this->_auth->loginOpiekunByActiveLink($activeLink);

    if ($logged) {
      $this->redirect('user/');
    } else {
      $this->_msg->add('error', 'Aktywny link wygasł lub został już wykorzystany. Zaloguj się przy pomocy loginu i hasła.', true, 'login/');
    }
  }
}