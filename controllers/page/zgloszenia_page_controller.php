<?php

class Zgloszenia_Page_Controller extends Page_Controller {
  private $_zgloszenia;
  private $_dzieci;
  private $_sekcje;

  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $login = new Login_Page_Controller();
    $login->checkPasswordChanged();
  }

  public function index() {
    $rodzinaId = $_SESSION['opiekun_rodzina_id'];

    $this->_zgloszenia = $this->_model->select('SELECT zgloszenie_id, dziecko_id, klasa_nazwa, klasa_id, zgloszenie_sekcja, taryfikator_oplata AS skladka, zgloszenie_propozycja, zgloszenie_uwagi_id FROM zgloszenia z INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id INNER JOIN rodziny ON rodzina_id = dziecko_rodzina_id INNER JOIN klasy ON klasa_id = dziecko_klasa INNER JOIN taryfikator t ON (taryfikator_sekcja = zgloszenie_sekcja AND taryfikator_klasa = dziecko_klasa AND taryfikator_sezon = :sezon) WHERE rodzina_id = :rodzinaId AND zgloszenie_status = "1"', array(':rodzinaId' => $rodzinaId, ':sezon' => SEZON));

    $this->_view->zgloszenia = $this->_zgloszenia;

    $this->_dzieci = $this->_model->select('SELECT d.*, rodzina_nazwisko FROM dzieci d, rodziny WHERE rodzina_id = dziecko_rodzina_id AND rodzina_id = :id ORDER BY dziecko_imie ASC', array(':id' => $rodzinaId));

    $this->_view->dzieci = $this->_dzieci;

    $sekcje = $this->_model->select('SELECT sekcja_id, sekcja_nazwa, taryfikator_klasa FROM sekcje INNER JOIN taryfikator ON sekcja_id = taryfikator_sekcja AND taryfikator_sezon = :sezon ORDER BY sekcja_nazwa ASC', array(':sezon' => SEZON));

    $sekcjeArr = array();

    foreach ($sekcje as $key => $value) {
      $sekcjeArr[$value['taryfikator_klasa']][$value['sekcja_id']] = $value['sekcja_nazwa'];
    }

    $this->_sekcje = $sekcjeArr;
    $this->_view->sekcje = $sekcjeArr;

    $uwagi = array();

    if (isset($this->_zgloszenia[0])) {
      $uwagi = $this->_model->select('SELECT uwagi_tresc FROM uwagi WHERE uwagi_id = :id', array(':id' => $this->_zgloszenia[0]['zgloszenie_uwagi_id']));
    }

    if (!empty($uwagi)) {
      $this->_view->uwagi = $uwagi[0]['uwagi_tresc'];
    } else {
      $this->_view->uwagi = '';
    }

    $rodzina = $this->_model->select('SELECT  o.opiekun_id AS ojciec_id, o.opiekun_imie AS ojciec_imie, o.opiekun_mail AS ojciec_mail, o.opiekun_telefon AS ojciec_telefon, m.opiekun_id AS matka_id, m.opiekun_imie AS matka_imie, m.opiekun_mail AS matka_mail, m.opiekun_telefon AS matka_telefon FROM opiekunowie o INNER JOIN opiekunowie m ON o.opiekun_rodzina_id = m.opiekun_rodzina_id AND o.opiekun_typ = 1 AND m.opiekun_typ = 2 WHERE o.opiekun_rodzina_id = :id', array(':id' => $rodzinaId));

    $this->_view->rodzina = $rodzina[0];

    $this->_save();

    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->addScripts('file', 'public/js/admin/zgloszenia.js');
    $this->_header->pageHeader();

    $this->_top->pageTop('zgloszenia');

    $this->_view->renderPage('page/zgloszenia/index');

    $this->_footer->pageFooter();
  }

  private function _save() {
    $rodzina_id = $_SESSION['opiekun_rodzina_id'];

    if (isset($_POST['zapisz'])) {
      $err = 0;
      $uwagiId = 0;

      $dzieciStr = '';

      $now = time();

      $zgloszenieData = date("Y-m-d H:i:s", $now);
      $endData = date("Y-m-d", $now);

      $sezon = $this->_model->select('SELECT * FROM sezony WHERE sezon_nazwa = :sezon LIMIT 1', array(':sezon' => SEZON));

      // if ($endData < $sezon[0]['sezon_start']) {
      //   $endData = date ("Y-m-d", strtotime("-1 year", strtotime($sezon[0]['sezon_koniec'])));
      // }

      foreach ($this->_dzieci as $d) {
        $dzieciStr .= $d['dziecko_id'].',';
      }

      $dzieciStr = rtrim($dzieciStr, ',');

      if (!empty($dzieciStr)) {
        $this->_model->update('zgloszenia', array('zgloszenie_status' => '0', 'zgloszenie_date_end' => $endData), 'zgloszenie_dziecko_id IN ('.$dzieciStr.') AND zgloszenie_status NOT IN  ("2", "3")');
      }

      for ($i = 1; $i <= count($_POST['rezygnacja']); $i++) {
        if (isset($_POST['rezygnacja'][$i]) && $_POST['rezygnacja'][$i] == '1' && isset($_POST['zgloszenie_id'][$i]) && $_POST['zgloszenie_id'][$i] > 0) {

          $this->_model->update('zgloszenia', array('zgloszenie_status' => '2', 'zgloszenie_date_end' => $endData), 'zgloszenie_id = :id', array(':id' => $_POST['zgloszenie_id'][$i]));
        }
      }

      if (!empty($_POST['uwagi'])) {
        $this->_model->insert('uwagi', array('uwagi_tresc' => $_POST['uwagi']));
        $uwagiId = $this->_model->getlastInsertId();
      }

      $max = $this->_model->select('SELECT MAX(zgloszenie_group_id) as max FROM zgloszenia LIMIT 1');

      $groupId = $max[0]['max'] + 1;

      // $zgloszenieData = date("Y-m-d H:i:s");

      $year = date('Y', time());

      if (date('d', time()) > 20) {
        $month = date('m', time()) + 2;
      } else if (date('d', time()) <= 20) {
        $month = date('m', time()) + 1;
      }

      for ($i = 1; $i <= count($_POST['imie_nazwisko']); $i++) {
        if (!empty($_POST['imie_nazwisko'][$i]) && !empty($_POST['sekcja_sportowa'][$i])) {
          if (empty($_POST['taryfa'][$i])) {
            $_POST['taryfa'][$i] = 0;
          }

          // if (empty($_POST['propozycja'][$i])) {
          //   $propozycja = $_POST['taryfa'][$i];
          // } else {
          //   $propozycja = $_POST['propozycja'][$i];
          // }

          $arr = array(
            'zgloszenie_dziecko_id' => $_POST['imie_nazwisko'][$i],
            'zgloszenie_dziecko_klasa_id' => $_POST['klasa'][$i],
            'zgloszenie_sekcja' => $_POST['sekcja_sportowa'][$i],
            'zgloszenie_skladka' => $_POST['taryfa'][$i],
            // 'zgloszenie_propozycja' => $propozycja,
            'zgloszenie_propozycja' => $_POST['taryfa'][$i],
            'zgloszenie_uwagi_id' => $uwagiId,
            'zgloszenie_date_add' => $zgloszenieData,
            'zgloszenie_group_id' => $groupId,
            'zgloszenie_date' => $year.'-'.$month.'-01'
          );

          // if (isset($_POST['rezygnacja'][$i]) && $_POST['rezygnacja'][$i] == '1') {
          //   $this->_model->update('zglosznia', array('zgloszenia_status' => '2'), 'zgloszenie_id = :id', array(':id' => $_POST['zgloszenie_id'][$i]));
          //   $arr['zgloszenie_status'] = 2;
          // }

          $test = $this->_model->insert('zgloszenia', $arr);

          if (!$test) {
            $err++;
          }
        }
      }

      $msg = 'Dokonana zmiana została zarejestrowana w systemie. Na Twój adres został wysłany mail z potwierdzeniem operacji oraz informacją o dalszym trybie postępowania. Prosimy o sprawdzenie<p style="margin-top: 2em;"><a id="js-yes-button" href="'.URL.'" class="yes-button"><span></span>OK</a>';

      if ($err == 0) {
        $mail = new phpmailer();
        $mail->CharSet = "UTF-8";
        $mail->SetLanguage("pl", "libs/");
        $mail->IsHTML(true);

        // SMTP
        $mail->Mailer = 'smtp';
        $mail->Host = MAIL_HOST;
        $mail->Port = MAIL_PORT;
        $mail->SMTPKeepAlive = true;
        $mail->SMTPAuth = true;
        $mail->IsSMTP();
        $mail->Username = MAIL_USER;
        $mail->Password = MAIL_PASS;

        $mail->From = MAIL_FROM;
        $mail->FromName = MAIL_FROM_NAME;

        $link = '<a href="'.URL.'login/'.$_SESSION['opiekun_login'].'">WWW</a>';

        $mail->Subject = 'Portal Rodzica UKS Żagle – zapisanie/zmiana/rezygnacja – potwierdzenie dokonanej dyspozycji';

        $body = '
          <p>Drodzy Rodzice,</p>
          <p>Informujemy, że <strong>otrzymaliśmy dyspozycję odnośnie zapisania/zmiany/rezygnacji Waszego syna/synów.</strong><br>
          Prosimy oczekiwać (maksymalnie w ciągu 7 dni) na mail zawierający:<br>
          a) potwierdzenie możliwości realizacji Waszej dyspozycji <br>
          b) nowe dane do płatności (w przypadku jej zaakceptowania)</p>
          <p>Szczegóły otrzymanej przez Klub dyspozycji są stale dostępne w zakładce „Zgłoszenia” w Portalu Rodzica (i mogą być w dalszym ciągu zmieniane) a jej status można obserwować w zakładce „Historia zgłoszeń”.</p>
          <p><strong>Prosimy o upewnienie się</stong> czy wszystko zostało poprawnie odnotowane.</p>
          <p>Gdyby okazało się, że są jakieś błędy – prosimy o kontakt poprzez: <a href="mailto:biuro@ukszagle.pl">biuro@ukszagle.pl</a></p>
          <center>
            <h1>
              Wejście do Portalu Rodzica poprzez stronę klubową: <a href="http://ukszagle.pl/">http://ukszagle.pl/</a>
            </h1>
          </center>
          <p>Ze sportowym pozdrowieniem,<br>
          Zarząd UKS Żagle</p>
        ';

        $mail->Body = $body;

        // $mail->AddAddress($user->uzytkownik_login);
        $mail->AddAddress($_POST['ojciec_mail']);
        $mail->AddAddress($_POST['matka_mail']);
        //$mail->AddAddress('biuro@ukszagle.pl');
        //$mail->AddAddress('lukasz@studiograficzne.com');
        $mail->AddBCC(MAIL_TEST);

        if ($mail->Send()) {
          header('Content-Type: application/json');
          echo json_encode(array(array(
            'type' => 'ok',
            'msg' => $msg
          )));
        }

        $mail->ClearAddresses();
      }

      exit();
    }
  }
}