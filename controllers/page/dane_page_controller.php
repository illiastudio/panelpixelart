<?php

class Dane_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function index() {
    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->pageHeader();

    $this->_top->pageTop('dane');

    $this->_view->renderPage('page/dane');

    $this->_footer->pageFooter();
  }
}