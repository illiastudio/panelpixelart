<?php

class Historiaa_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();

    $login = new Login_Page_Controller();
    $login->checkPasswordChanged();
  }

  public function index() {
    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->pageHeader();

    $this->_top->pageTop('historia');

    $arrActive = $this->_model->getHistory($_SESSION['opiekun_rodzina_id'], 'active');

    $zglosz = array();

    if (!empty($arrActive)) {
      $zgloszeniaActiveArr = explode(',', $arrActive[0]['zgloszenia']);

      foreach ($zgloszeniaActiveArr as $key => $value) {
        $zglosz[] = $this->_model->getZgloszenie($value);
      }

      $statusCheck = $this->_model->select('SELECT zgloszenie_id, zgloszenie_status, potwierdzenie_status FROM zgloszenia LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id WHERE zgloszenie_id = :id LIMIT 1', array('id' => $zglosz[0]['zgloszenie_id']));
      $statusCheck = $statusCheck[0];
      $statusZgloszenia = $statusCheck['zgloszenie_status'];
      $statusPotwierdzenie = $statusCheck['potwierdzenie_status'];

      if ($statusZgloszenia == '1' && empty($statusPotwierdzenie)) {
        $status = 'ZGŁOSZENIE WYSŁANE';
      } else if ($statusZgloszenia == '1' && $statusPotwierdzenie == '1') {
        $status = 'ZAAKCEPTOWANE';
      } else if ($statusZgloszenia == '1' && $statusPotwierdzenie == '0') {
        $status = 'ODRZUCONE';
      } else {
        $status = '';
      }

      $this->_view->status = $status;
    }

    $this->_view->activeArr = $zglosz;

    $arr = $this->_model->getHistory($_SESSION['opiekun_rodzina_id']);

    $historiaArr = array();

    $i = 0;

    foreach ($arr as $key => $val) {
      $historiaArr[$val['zgloszenie_date_add']] = array();

      $zgloszenia = explode(',', $val['zgloszenia']);

      foreach ($zgloszenia as $k => $v) {
        $historiaArr[$val['zgloszenie_date_add']][$v] = array();
        $historiaArr[$val['zgloszenie_date_add']][$v] = $this->_model->getZgloszenie($v);
      }

      $i++;
    }

    $this->_view->historiaArr = $historiaArr;

    $this->_view->renderPage('page/historia/index_backup');

    $this->_footer->pageFooter();
  }
}