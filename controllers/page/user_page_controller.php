<?php

class User_Page_Controller extends Page_Controller {
  public function __construct() {
    parent::__construct();
    parent::_isLogged();
  }

  public function profil() {
    $this->_zapisz();

    $this->_header->pageDefaultScriptsAndStyles();
    $this->_header->pageHeader();

    $this->_top->pageTop('user');

    $this->_view->imie = $_SESSION['opiekun_imie'];
    $this->_view->telefon = $this->_model->select('SELECT opiekun_telefon FROM opiekunowie WHERE opiekun_id = :id', array(':id' => $_SESSION['opiekun_id']));
    $this->_view->mail = $this->_model->select('SELECT opiekun_mail FROM opiekunowie WHERE opiekun_id = :id', array(':id' => $_SESSION['opiekun_id']));
    $this->_view->renderPage('page/user');

    $this->_footer->pageFooter();
  }

  private function _zapisz() {
    if (isset($_POST['zapisz'])) {
      $form = new Form();
      $form->post('imie')->val('notEmpty', 'imię')
           ->post('telefon')
           ->post('mail')->val('notEmpty', 'e-mail')
           ->post('old-telefon')
           ->post('old-mail')
           ->post('old-imie')
           ->post('new-password')->val('minlength', 6, 'hasło')
           ->post('confirm-password')->val('equal', 'new-password', 'Hasła nie są takie same.');

      if ($form->errorCheck()) {
        $data = $form->fetch();

        if ($data['imie'] != $data['old-imie']) {
          $testDane = $this->_model->changeOpiekunImie($_SESSION['opiekun_id'], $data['imie']);
        }

        if ($data['telefon'] != $data['old-telefon']) {
          $testTel = $this->_model->update('opiekunowie', array('opiekun_telefon' => $data['telefon']), 'opiekun_id = :id', array(':id' => $_SESSION['opiekun_id']));
        }

        if (!empty($data['mail']) && $data['mail'] != $data['old-mail']) {
          $testMail = $this->_model->update('opiekunowie', array('opiekun_mail' => $data['mail']), 'opiekun_id = :id', array(':id' => $_SESSION['opiekun_id']));
        }

        if (!empty($data['new-password'])) {
          $password = HASH_OPIEKUN_PASSWORD_KEY.$data['new-password'];
          $password = Hash::create('sha512', $password, HASH_SITE_KEY);

          $testPassword = $this->_model->changeOpiekunPassword($_SESSION['opiekun_id'], $password);
        }

        if (isset($testDane)) {
          if ($testDane) {
            $this->_msg->add('success', 'Zmieniono dane.');
          } else {
            $this->_msg->add('error', 'Nie udało się zmienić danych.');
          }
        }

        if (isset($testTel)) {
          if ($testTel) {
            $this->_msg->add('success', 'Zmieniono nr telefonu.');
          } else {
            $this->_msg->add('error', 'Nie udało się zmienić nr telefonu.');
          }
        }

        if (isset($testMail)) {
          if ($testMail) {
            $this->_msg->add('success', 'Zmieniono adres e-mail. Uwaga, zmiana adresu e-mail oznacza zmianę loginu.');
          } else {
            $this->_msg->add('error', 'Nie udało się zmienić adresu e-mail.');
          }
        }



        if (isset($testPassword)) {
          if ($testPassword) {
            $this->_msg->add('success', 'Zmieniono hasło.');
          } else {
            $this->_msg->add('error', 'Nie udało się zmienić hasła.');
          }
        }

        $this->reload();
      } else {
        $form->showErrors();
        $this->reload();
      }
    }
  }

}

