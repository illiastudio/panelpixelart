<?php

class Footer_Controller extends Controller {
  private $_view;

  public function __construct() {
    $this->_view = new View();
  }

  public function pageFooter() {
    $messages = new Messages();
    $messages->render();

    $this->_view->renderPage('page/footer');
  }

  public function adminFooter() {
    $messages = new Messages();
    $messages->render();

    $this->_view->renderPage('admin/footer');
  }

}