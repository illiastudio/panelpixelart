<?php

class Header_Controller extends Controller
{
  private $_view;
  private $_scripts = array();
  private $_styles = array();

  public function __construct()
  {
    $this->_view = new View();
  }

  public function pageHeader()
  {
    $this->_view->title = 'Panel Rodzica';
    $this->_view->styles = $this->_styles;

    $this->_view->scripts = $this->_scripts;

    $this->_view->renderPage('page/header');
  }

  public function adminHeader()
  {
    $this->_view->title = 'Panel Administratora';
    $this->_view->styles = $this->_styles;

    $this->_view->scripts = $this->_scripts;

    $this->_view->renderPage('admin/header');
  }

  public function adminDefaultScriptsAndStyles()
  {
    $this->addStyles('file', 'public/css/admin.css');
    $this->addStyles('file', 'public/css/redesign.css');
    // $this->addStyles('file', 'public/css/jquery.fancybox.css');
    $this->addStyles('file', 'public/css/jquery-ui/jquery-ui.min.css');
    // $this->addStyles('file', 'elfinder/css/elfinder.min.css');
    $this->addScripts('file', 'public/js/jquery.min.js');
    $this->addScripts('file', 'public/js/jquery-ui/jquery-ui.min.js');
    // $this->addScripts('file', 'elfinder/js/elfinder.min.js');
    // $this->addScripts('file', 'elfinder/js/i18n/elfinder-pl.js');
    // $this->addScripts('file', 'public/js/nestedSortable.js');
    // $this->addScripts('file', 'public/js/jquery.fancybox.min.js');
    $this->addScripts('file', 'public/js/jquery.simplemodal.min.js');
    $this->addScripts('file', 'public/js/admin.min.js');
    // $this->addScripts('file', 'public/js/preloader.js');
  }

  public function pageDefaultScriptsAndStyles()
  {
    $this->addStyles('file', 'public/css/styles.css');
    $this->addStyles('file', 'public/css/redesign.css');
    // $this->addStyles('file', 'public/css/jquery.fancybox.css');
    // $this->addStyles('file', 'public/css/jqueryui-theme/jquery-ui.min.css');
    // $this->addStyles('file', 'elfinder/css/elfinder.min.css');
    $this->addScripts('file', 'public/js/jquery.min.js');
    // $this->addScripts('file', 'public/js/jquery-ui.min.js');
    // $this->addScripts('file', 'elfinder/js/elfinder.min.js');
    // $this->addScripts('file', 'elfinder/js/i18n/elfinder-pl.js');
    // $this->addScripts('file', 'public/js/nestedSortable.js');
    // $this->addScripts('file', 'public/js/jquery.fancybox.min.js');
    $this->addScripts('file', 'public/js/jquery.simplemodal.min.js');
    $this->addScripts('file', 'public/js/page.min.js');
    // $this->addScripts('file', 'public/js/preloader.js');
  }

  public function addScripts($type, $scriptData)
  {
    $script = '';

    if ($type == 'file') {
      $script = '<script type="text/javascript" src="' . URL . $scriptData . '"></script>';
    } elseif ($type == 'text') {
      $script = '<script type="text/javascript">' . $scriptData . '</script>';
    }

    $this->_scripts[] = $script;
  }

  public function addStyles($type, $styleData)
  {
    $style = '';

    if ($type == 'file') {
      $style = '<link rel="stylesheet" href="' . URL . $styleData . '" />';
    } elseif ($type == 'text') {
    }

    $this->_styles[] = $style;
  }
}
