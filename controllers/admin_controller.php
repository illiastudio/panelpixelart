<?php

class Admin_Controller extends Controller {
  protected $_model;
  protected $_view;
  protected $_header;
  protected $_top;
  protected $_footer;
  protected $_msg;

  public function __construct() {
    $session = new Session();
    $session->startSession('admin');

    $this->_model = new Admin_Model();
    $this->_view = new View();
    $this->_header = new Header_Controller();
    $this->_top = new Top_Controller();
    $this->_footer = new Footer_Controller();
    $this->_msg = new Messages();
  }

  protected function _isLogged() {
    $auth = new Auth();

    if (!isset($_SESSION['user_id'])) {
      if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        header('Content-Type: application/json');
        echo json_encode(array('type' => 'error', 'msg' => 'Odswież stronę by zalogować się ponownie'));
        exit();
      } else {
        $this->redirect('admin/login/');
      }
    } else {
      $loggedIn = $auth->checkSession();

      if (!$loggedIn) {
        $auth->logout();

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
          header('Content-Type: application/json');
          echo json_encode(array('type' => 'error', 'msg' => 'Odswież stronę by zalogować się ponownie'));
          exit();
        } else {
          $this->redirect('admin/login/');
        }
      } else {
        if (!defined('LANG')) {
          define("LANG", $_SESSION['lang']);
        }

        return true;
      }
    }
  }

  protected function _isAjax() {
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      return true;
    } else {
      $this->redirect('admin/');
    }
  }

  protected function _isRole($role) {
    if ($_SESSION['role'] == $role) {
      return true;
    } else {

      $this->_msg->add('error', 'Nie posiadasz wystarczających uprawnień do przeglądania tej strony.', false, 'admin');
    }
  }

  protected function _checkParams($params, $number, $url) {
    $paramsNumber = count($params);
    $hasEmpty = 0;

    foreach ($params as $key => $value) {
      if (empty($value)) {
        $hasEmpty++;
      }
    }

    if ($paramsNumber == $number && $hasEmpty == 0) {
      return true;
    } else {
      $this->redirect($url);
    }
  }

  protected function _checkType($id, $type, $redirectUrl) {
    if ($this->_model->checkIfType($id, $type)) {
      return true;
    } else {
      $this->redirect($redirectUrl);
    }
  }

}