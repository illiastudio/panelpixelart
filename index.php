<?php

require_once 'config.php';

spl_autoload_register(null, false);
spl_autoload_extensions('.php');

function classLoader($class) {
  $subfolder = '';
  $namesArr = explode('_', $class);
  $namesArrCount = count($namesArr);

  $lastElement = end($namesArr);
  $lastElement = strtolower($lastElement);

  if ($lastElement == 'controller' && $namesArrCount > 1) {
    $folder = CONTROLLERS;
  } elseif ($lastElement == 'model' && $namesArrCount > 1) {
    $folder = MODELS;
  } else {
    $folder = LIBS;
  }

  if ($namesArrCount == 3) {
    $subfolder = $namesArr[1].'/';
  }

  $filename = $folder.$subfolder.$class.'.php';
  $filename = strtolower($filename);

  if (!file_exists($filename)) {

    return false;
  }

  require_once $filename;
}

spl_autoload_register('classLoader');

$bootstrap = new Bootstrap();
$bootstrap->init();