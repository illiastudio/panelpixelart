<?php

require_once 'config.php';
require_once 'libs/model.php';

try {
  $pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
  header('Content-Type: text/html; charset=utf-8');
  die('Nie można nawiązać połączenia z bazą danych.');
}

$sth = $pdo->prepare('SELECT setting_value FROM settings WHERE setting_name = "last_saldo_update" LIMIT 1');
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);

$lastSaldoUpdate = $result[0]['setting_value'];

$time = time();
$now = date('d-m-Y', $time);
$nowSql = date('Y-m-d', $time);
$last = date('d-m-Y', strtotime('-1 month', $time));
$nowDay = date('d', $time);

if ($nowDay == '1' && $lastSaldoUpdate == $last) {

  $sthDel = $pdo->prepare('DELETE FROM platnosci_saldo WHERE ps_okres = :data');
  $sthDel->execute(array(':data' => $nowSql));

  $sthIns = $pdo->prepare('INSERT INTO platnosci_saldo (ps_rodzina_id, ps_do_zaplaty, ps_zgloszenia, ps_okres)
    SELECT dziecko_rodzina_id AS ps_rodzina_id, sum(zgloszenie_propozycja) AS ps_do_zaplaty, GROUP_CONCAT(zgloszenie_id) AS ps_zgloszenia, "'.$nowSql.'"
    FROM zgloszenia
    INNER JOIN dzieci ON zgloszenie_dziecko_id = dziecko_id
    LEFT JOIN potwierdzenia ON zgloszenie_id = potwierdzenie_zgloszenie_id
    WHERE (zgloszenie_skladka > 0)
    AND (potwierdzenie_status = "1")
    AND (potwierdzenie_wyslane = "1")
    AND (
     (zgloszenie_status = "1"
       AND zgloszenie_date <= :data
       AND zgloszenie_date != "0000-00-00"
     )
     OR (zgloszenie_status IN ("0", "2")
       AND (zgloszenie_date <= :data AND zgloszenie_date != "0000-00-00")
       AND (
         (zgloszenie_date_end >= :data AND zgloszenie_date_end != "0000-00-00")
         OR (YEAR(zgloszenie_date_end) = YEAR(:data - INTERVAL 1 MONTH)
           AND MONTH(zgloszenie_date_end) = MONTH(:data - INTERVAL 1 MONTH)
           AND DAYOFMONTH(zgloszenie_date_end) > 20
         )
       )
     )
    )
    GROUP BY dziecko_rodzina_id');
  $sthIns->execute(array(':data' => $nowSql));

  if ($sthIns) {
    $sthUp = $pdo->prepare('UPDATE settings SET setting_value = :data WHERE setting_name = "last_saldo_update"');
    $sthUp->execute(array(':data' => $now));
  }
}


die;