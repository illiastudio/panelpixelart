var baseUrl = window.location.protocol + "//" + window.location.host + "/";
// var baseUrl = window.location.protocol + "//" + window.location.host + "/panelpixelart/";
$(document).ready(function () {

    $('#tbody-schools').on("click", '.removeSchool', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');

        $.ajax({
            type: "post",
            url: baseUrl + 'admin/harmonogram/remove',
            data: "id=" + id,
            dataType: "html",
            success: function (response) {
                $('#tbody-schools').html(response);
            },
            error: function (err) {
                console.log('err', err.responseText)
            }
        });

    });

});


