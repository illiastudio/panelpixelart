$(document).ready(function() {
  $(document).on('click', '#js-add-section', function(e) {
    e.preventDefault();

    var $box = $('#js-add-section-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-add-section-box" class="add-section-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function(htmlData) {
        $box = $('#js-add-section-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function() {
            $.modal.close();
          }
        })
      }
    });
  });

  $(document).on('click', '.js-section-edit', function(e) {
    e.preventDefault();

    var $box = $('#js-edit-section-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-edit-section-box" class="edit-section-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function(htmlData) {
        $box = $('#js-edit-section-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function() {
            $.modal.close();
          }
        })
      }
    });
  });
});