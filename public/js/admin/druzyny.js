$(document).ready(function() {
  var dataTable;
  var $table = $('#js-druzyny-table');
  var $table2 = $('#js-druzyny-dzieci-table');

  if ($table.length > 0) {
    dataTable = $table.dataTable({
      'iDisplayLength': 25,
      "stateSave": true,
      'sPaginationTypedialog-box': 'full_numbers',
      "bAutoWidth": false,
      'aaSorting': [
        [ 0, 'asc' ]
      ],
      'aoColumnDefs': [
        { 'bSortable': false, 'aTargets': [3] }
      ],
      'language': {
        'url': baseUrl + 'public/js/data-tables/pl.json'
      }
    });
  }

  if ($table2.length > 0) {
    dataTable = $table2.dataTable({
      'iDisplayLength': 25,
      "stateSave": true,
      'sPaginationTypedialog-box': 'full_numbers',
      "bAutoWidth": false,
      'aaSorting': [
        [ 1, 'asc' ]
      ],
      'aoColumnDefs': [
        { 'bSortable': false, 'aTargets': [3] }
      ],
      'language': {
        'url': baseUrl + 'public/js/data-tables/pl.json'
      }
    });
  }

  $.datepicker.regional["pl"] = {
    closeText: "Zamknij",
    prevText: "&#x3c;Poprzedni",
    nextText: "Następny&#x3e;",
    currentText: "Dziś",
    monthNames: ["Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec",
    "Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"],
    monthNamesShort: ["Sty","Lu","Mar","Kw","Maj","Cze",
    "Lip","Sie","Wrz","Pa","Lis","Gru"],
    dayNames: ["Niedziela","Poniedzialek","Wtorek","Środa","Czwartek","Piątek","Sobota"],
    dayNamesShort: ["Nie","Pn","Wt","Śr","Czw","Pt","So"],
    dayNamesMin: ["N","Pn","Wt","Śr","Cz","Pt","So"],
    weekHeader: "Tydz",
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ""};
  $.datepicker.setDefaults($.datepicker.regional["pl"]);

  $(document).on('click', '.js-delete-druzyny', function(e) {
    if (!confirm('Usunąć drużynę?')) {
      e.preventDefault();
    }
  });

  if ($('select[multiple]').length > 0) {
    $('select[multiple]').multiSelectToCheckboxes({
        filterLabel : 'Szukaj:',
        filterPlaceholder : '...',
        initPlaceholder : 'Wybierz',
        minFilterTextLength : 0,
        selectAllText : 'Zaznacz widoczne',
        deselectAllText : 'Odznacz widoczne',
        maxListToCount : 0, //po przekroczeniu tej wartosci pokaze ponizszy komunikat
        infoText : 'Wybrano {0} z {1}',
        showOnlyCountText : false, //czy pokazywac tylko powyzszy tekst w polu
        closeText : 'X'
    });
  }
});