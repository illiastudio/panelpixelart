// var baseUrl = window.location.protocol + "//" + window.location.host + "/panelpixelart/";
var baseUrl = window.location.protocol + "//" + window.location.host + "/";

$(document).ready(function() {
  podlicz('taryfa');

  $(document).on("click", ".close-msg", function(e) {
    e.preventDefault();

    $.modal.close();
  });

  $(document).on('change', '.imie_nazwisko', function() {
    var $row = $(this).parent().parent(),
    id = $row.attr('id'),
    $span = $('#' + id + ' .klasa span'),
    $input = $('#' + id + ' .klasa input'),
    dziecko = $(this).val(),
    $sekcja = $('#' + id + ' .sekcja_sportowa');

    if (dziecko > 0) {
      $(this).find('option[value=0]').remove();

      $.ajax({
        data: 'dziecko_id=' + dziecko + '&pobierz_klase=true',
        type: 'POST',
        dataType: "json",
        url: baseUrl + 'ajax/ustawKlase',
        success: function(json) {
          if (json[0].type == 'error') {
            for (var i = 0; i < json.length; i++) {
              addMsg(json[i].type, json[i].msg, false);
            }
          } else {
            $span.html(json[0].klasa);
            $input.val(json[0].klasa_id);
            $sekcja.prop('disabled', false);
            $sekcja.val('0');
            $('#' + id + ' .taryfa span').html('');
            $('#' + id + ' .taryfa input').val('0')
            $('#' + id + ' .sekcja_sportowa').html(json[0].sekcje);
            podlicz('taryfa');
          }
        }
      });
    } else {
      $span.html('');
      $input.val('');
      $sekcja.prop('disabled', true);
      $sekcja.val('0');
      $('#' + id + ' .taryfa span').html('');
      $('#' + id + ' .taryfa input').val('0')
      podlicz('taryfa');
    }
  });

  $(document).on('change', '.sekcja_sportowa', function() {
    var $row = $(this).parent().parent(),
    id = $row.attr('id'),
    $span = $('#' + id + ' .taryfa span'),
    $input = $('#' + id + ' .taryfa input'),
    sekcja = $(this).val(),
    klasa = $('#' + id + ' .klasa input').val(),
    test = 0;

    if (sekcja != 0) {

      $('.row').each(function() {
        var rowId = $(this).attr('id');

        if ($('#' + rowId + ' .imie_nazwisko').val() == $('#' + id + ' .imie_nazwisko').val() && id != rowId) {
          if ($('#' + rowId + ' .sekcja_sportowa').val() == $('#' + id + ' .sekcja_sportowa').val() && id != rowId && $('#' + id + ' .sekcja_sportowa').val() != 0) {
            addMsg('error', 'To dziecko jest już zapisane do tej sekcji');

            if ($('.sekcja_sportowa option[value=0]').length === 0) {
              $('.sekcja_sportowa').prepend('<option value="0">--- Wybierz sekcję ---</option>');
            }

            $('#' + id + ' .sekcja_sportowa').val('0');
            $span.html('');
            $input.val('0');
            podlicz('taryfa');
            test++;
          }
        }
      });


      if (test == 0) {
        $(this).find('option[value=0]').remove();

        $.ajax({
          data: 'sekcja=' + sekcja + '&klasa=' + klasa + '&ustaw_taryfe=true',
          type: 'POST',
          dataType: "json",
          url: baseUrl + 'ajax/ustawCene',
          success: function(json) {
            if (json[0].type == 'error') {
              for (var i = 0; i < json.length; i++) {
                addMsg(json[i].type, json[i].msg);
              }
            } else {
              $span.html(json[0].cena);
              $input.val(json[0].cena);
              podlicz('taryfa');
            }
          }
        });
      }
    } else {
      $span.html('');
      $input.val('0');
      podlicz('taryfa');
    }
  });

  $(document).on('click', '#js-dodaj', function(e) {
    e.preventDefault();

    var id = $('#zgloszenia tbody tr:last-child').attr('id');

    $.ajax({
      data: 'row_id=' + id + '&dodaj_row=true',
      type: 'POST',
      dataType: "json",
      url: baseUrl + 'ajax/dodajRekord',
      success: function(json) {
        if (json[0].type == 'error') {
          for (var i = 0; i < json.length; i++) {
            addMsg(json[i].type, json[i].msg);
          }
        } else {
          $('#zgloszenia tbody').append(json[0].row);
        }
      }
    });
  });

  $(document).on('submit', '#form', function(e) {
    e.preventDefault();

    // if (confirm("Zmiany wejdą w życie ...") === true) {
      var imie = 0,
      sekcja = 0,
      err = 0,
      msg = '';

      $('.imie_nazwisko').each(function() {
        if ($(this).val() == 0 && $(this).prop('disabled') == false) {
          imie++;
        }
      });

      $('.sekcja_sportowa').each(function() {
        if ($(this).val() == 0 && $(this).prop('disabled') == false) {
          sekcja++;
        }
      });

      if (imie > 0) {
        err++;
        msg += 'Pole: imię i nazwisko syna nie może pozostać puste<br>';
      }

      if (sekcja > 0) {
        err++;
        msg += 'Pole: sekcja sportowa nie może pozostać puste<br>';
      }

      if (!$('.js-regulamin-checkbox').is(':checked')) {
        err++;
        msg += 'Akceptacja regulaminu jest obowiązkowa.<br>';
      }

      if (err == 0) {
        var data = $(this).serialize();
        $.ajax({
          type: 'POST',
          data: data + '&zapisz=true',
          dataType: 'json',
          success: function(json) {
            if (json[0].type == 'ok') {
              addPopup('info', json[0].msg);
            }
          }
        });
      } else {
        addMsg('error', msg);
      }
    // } else {
    //   return false;
    // }
  });

  $(document).on('change', '.rezygnuj', function() {
    var $row = $(this).parent().parent(),
    id = $row.attr('id'),
    checked = $(this).prop('checked');

    if (checked) {
      $('#' + id + ' .imie_nazwisko').prop('disabled', true);
      $('#' + id + ' .klasa input').prop('disabled', true);
      $('#' + id + ' .sekcja_sportowa').prop('disabled', true);
      $('#' + id + ' .taryfa input').prop('disabled', true);
      podlicz('taryfa');
    } else {
      $('#' + id + ' .imie_nazwisko').prop('disabled', false);
      $('#' + id + ' .klasa input').prop('disabled', false);
      $('#' + id + ' .taryfa input').prop('disabled', false);

      if ($('#' + id + ' .sekcja_sportowa').val() !== null) {
        $('#' + id + ' .sekcja_sportowa').prop('disabled', false);
      }

      podlicz('taryfa');
    }

  });

  // $(document).on('click', '#zaznacz-domyslne', function(e) {
  //   e.preventDefault();

  //   $('.zgloszenie-row').each(function() {
  //     var $this = $(this);
  //     var id = $this.attr('id');
  //     var taryfa = $('#' + id + ' .taryfa-zgl').val();
  //     var propozycja = $('#' + id + ' .propozycja-zgl').val();
  //     var value = $('#' + id + ' .zgloszenie').val();

  //     if (taryfa === propozycja || propozycja === '0') {
  //       if (value === '1') {
  //         $('#' + id + ' .zgloszenie').val('0');
  //       } else {
  //         $('#' + id + ' .zgloszenie').val('1');
  //       }
  //     }
  //   });
  // });

  $(document).on('click', '.js-usun-rekord', function(e) {
    $(this).parent().parent().remove();
  });

});

function podlicz(name) {
  var suma = 0;

  $('.'+ name +' input').each(function() {
    if ($(this).val().trim() !== '' && $(this).prop('disabled') == false) {
      suma += parseInt($(this).val());
    } else {
      suma += 0;
    }
  });

  $('#suma-'+ name +'').html(suma + ' zł');
}

function addMsg(type, msg, sticky) {
  var stickyClass = '';
  var stickyClose = '';

  var msgTxt = "<div class=\"msg-" + type + stickyClass + "\"><span class=\"msg-icon\"></span><p>" + msg + "</p><span class=\"close-msg\"></span></div>";

  if (!sticky) {
    $(msgTxt).prependTo(".messages").slideDown(300).delay(2000).slideUp(300, function() { $(this).remove(); });
  } else {
    $(msgTxt).prependTo(".messages").slideDown(300);
  }
}

function addPopup(type, msg) {
  var close = '';

  if (type != 'info') {
    close = '<span class="close-msg">';
  }

  $('body').append('<div id="popUp" class="' + type + '"><div class="msg-left"><span class="msg-icon"></span></div><div class="msg-right"><p>' + msg + '</p>' + close + '</span></div>');

  $('#popUp').modal({
    close: false,
    width: 400,
    onClose: function() {
      this.close();
      $('#popUp').remove();
    }
  });
}

function isInt(value) {
  return !isNaN(value) && parseInt(Number(value)) == value && (value + "").replace(/ /g,'') !== "";
}