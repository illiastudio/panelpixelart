$(document).ready(function () {
  var dataTable;
  var $table = $('#js-rodziny-table');

  if ($table.length > 0) {
    dataTable = $table.dataTable({
      'iDisplayLength': 25,
      'serverSide': true,
      "stateSave": true,
      'ajax': baseUrl + 'admin/zarzadzanie/getList',
      'sPaginationTypedialog-box': 'full_numbers',
      "bAutoWidth": false,
      'aoColumns': [
        null,
        null,
        null,
        null,
        null,
        null,
        { 'iDataSort': 7 },
        { 'bVisible': false }
      ],
      'aaSorting': [
        [1, 'asc']
      ],
      'language': {
        'url': baseUrl + 'public/js/data-tables/pl.json'
      },
      fnDrawCallback: function () {
        $table.find('tbody tr td').click(function (e) {
          e.preventDefault();

          var targetClass = e.target.className;

          if (targetClass !== 'send js-send') {
            var element = $(this).parent()[0];
            var position = dataTable.fnGetPosition(element);
            var id = dataTable.fnGetData(position)[0];

            document.location.href = "zarzadzaj/" + id;
          }
        });

      }
    });

    function dataTableReload() {
      dataTable.fnDraw();
    }
  }

  $.datepicker.regional["pl"] = {
    closeText: "Zamknij",
    prevText: "&#x3c;Poprzedni",
    nextText: "Następny&#x3e;",
    currentText: "Dziś",
    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec",
      "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
    monthNamesShort: ["Sty", "Lu", "Mar", "Kw", "Maj", "Cze",
      "Lip", "Sie", "Wrz", "Pa", "Lis", "Gru"],
    dayNames: ["Niedziela", "Poniedzialek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
    dayNamesShort: ["Nie", "Pn", "Wt", "Śr", "Czw", "Pt", "So"],
    dayNamesMin: ["N", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
    weekHeader: "Tydz",
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ""
  };
  $.datepicker.setDefaults($.datepicker.regional["pl"]);

  $(".js-date-start").datepicker({
    "dateFormat": "yy-mm-dd",
    onSelect: function (selectedDate) {
      var id = $(this).data('id');

      $(".js-date-end[data-id=" + id + "]").datepicker("option", "minDate", selectedDate);
    }
  });

  $(".js-date-end").datepicker({
    "dateFormat": "yy-mm-dd",
    beforeShow: function () {
      var id = $(this).data('id');
      var selectedDate = $(".js-date-start[data-id=" + id + "]").attr('value');

      $(this).datepicker("option", "minDate", selectedDate);
    }
  }).keyup(function (e) {
    if (e.keyCode == 8 || e.keyCode == 46) {
      $.datepicker._clearDate($(this));
    }
  });

  $("#js-form").on("submit", function (e) {
    var err = 0;

    $(".js-date-end").each(function () {
      var date = $(this).val();

      if (date) {
        if (date.match(/^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$/)) {
          return true;
        } else {
          err++;
        }
      }
    });

    if (err > 0) {
      e.preventDefault();

      addMsg('error', 'Wprowadzona data jest niepoprawna.', false);
    }
  });

  $(document).on('click', '.js-edit-opiekun', function (e) {
    e.preventDefault();

    var $box = $('#js-opiekun-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-opiekun-box" class="opiekun-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function (htmlData) {
        $box = $('#js-opiekun-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function () {
            $.modal.close();
          }
        })
      }
    });
  });

  $(document).on('click', '.js-edit-dziecko', function (e) {
    e.preventDefault();

    var $box = $('#js-dziecko-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-dziecko-box" class="dziecko-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function (htmlData) {
        $box = $('#js-dziecko-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function () {
            $.modal.close();
          }
        })
      }
    });
  });

  $(document).on('click', '.js-new-password', function (e) {
    e.preventDefault();

    var $box = $('#js-new-password');

    if ($box.length === 0) {
      $('body').append('<div id="js-new-password" class="password-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function (htmlData) {
        $box = $('#js-new-password');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function () {
            $.modal.close();
          }
        })
      }
    });
  });

  $(document).on('click', '#js-add-child', function (e) {
    e.preventDefault();

    var $box = $('#js-add-child-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-add-child-box" class="add-child-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function (htmlData) {
        $box = $('#js-add-child-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function () {
            $.modal.close();
          }
        })
      }
    });
  });

  $(document).on('click', '#js-add-family', function (e) {
    e.preventDefault();

    var $box = $('#js-add-family-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-add-family-box" class="add-family-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function (htmlData) {
        $box = $('#js-add-family-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function () {
            $.modal.close();
          }
        })
      }
    });
  });
});