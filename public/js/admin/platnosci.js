$(document).ready(function() {
  var oTable = $('#js-rodziny-list').dataTable({
    'sPaginationTypedialog-box': 'full_numbers',
    "bAutoWidth": false,
    "stateSave": true,
    'aaSorting': [
      [ 1, 'asc' ]
    ],
    'language': {
      'url': baseUrl + 'public/js/data-tables/pl.json'
    }
  });

  $('#dt-search').keyup(function() {
    oTable.fnFilter($(this).val());
  })
});