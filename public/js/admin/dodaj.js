$(document).ready(function() {
  var $dzieckoSelect = $('select.imie_nazwisko');

  $dzieckoSelect.select2();

  $(document).on('change', '.imie_nazwisko', function() {
    var $span = $('.klasa span'),
    $input = $('.klasa input'),
    dziecko = $(this).val(),
    $sekcja = $('.sekcja_sportowa');

    if (dziecko > 0) {
      $('.imie_nazwisko option[value=0]').remove();

      $.ajax({
        data: 'dziecko_id=' + dziecko + '&pobierz_klase=true',
        type: 'POST',
        dataType: "json",
        url: baseUrl + 'admin/dodaj/ustawKlase',
        success: function(json) {
          if (json[0].type == 'error') {
            for (var i = 0; i < json.length; i++) {
              addMsg(json[i].type, json[i].msg, false);
            }
          } else {
            $span.html(json[0].klasa);
            $input.val(json[0].klasa_id);
            $sekcja.prop('disabled', false);
            $sekcja.val('0');
            $('.taryfa span').html('');
            $('.taryfa input').val('0')
            $('.propozycja input').prop('disabled', false);
            $('.propozycja span').html('');
            $('.propozycja input').val('');
            $('.sekcja_sportowa').html(json[0].sekcje);
            // podlicz('taryfa');
            // podlicz('propozycja');
          }
        }
      });
    } else {
      $span.html('');
      $input.val('');
      $sekcja.prop('disabled', true);
      $sekcja.val('0');
      $('.taryfa span').html('');
      $('.taryfa input').val('0')
      $('.propozycja input').prop('disabled', true);
      $('.propozycja span').html('');
      $('.propozycja input').val('');
      // podlicz('taryfa');
      // podlicz('propozycja');
    }
  });


  $(document).on('change', '.sekcja_sportowa', function() {
    var $span = $('.taryfa span'),
    $input = $('.taryfa input'),
    sekcja = $(this).val(),
    klasa = $('.klasa input').val(),
    test = 0;

    if (sekcja != 0) {
      $('.sekcja_sportowa option[value=0]').remove();

      $.ajax({
        data: 'sekcja=' + sekcja + '&klasa=' + klasa + '&ustaw_taryfe=true',
        type: 'POST',
        dataType: "json",
        url: baseUrl + 'admin/dodaj/ustawCene',
        success: function(json) {
          if (json[0].type == 'error') {
            for (var i = 0; i < json.length; i++) {
              addMsg(json[i].type, json[i].msg);
            }
          } else {
            $span.html(json[0].cena);
            $input.val(json[0].cena);
            $('.propozycja span').html(json[0].cena);
            $('.propozycja input').val(json[0].cena);
            // podlicz('taryfa');
            // podlicz('propozycja');
          }
        }
      });
    }
  });

  $(".js-date-start").datepicker({
    "dateFormat": "yy-mm-dd",
    onSelect: function(selectedDate) {
      var id = $(this).data('id');

      $(".js-date-end[data-id=" + id + "]").datepicker("option", "minDate", selectedDate);
    }
  });

});