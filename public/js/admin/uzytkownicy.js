$(document).ready(function() {
  $(document).on('click', '.js-edit-user', function(e) {
    e.preventDefault();

    var $box = $('#js-user-box');

    if ($box .length === 0) {
      $('body').append('<div id="js-user-box" class="user-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function(htmlData) {
        $box = $('#js-user-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function() {
            $.modal.close();
          }
        })
      }
    });
  });
});