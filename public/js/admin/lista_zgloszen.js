$(document).ready(function() {
  var oTable = $('#js-zgloszenia-list').dataTable({
    'iDisplayLength': 25,
    'sPaginationTypedialog-box': 'full_numbers',
    "bAutoWidth": false,
    "stateSave": true,
     'aoColumns': [
      null,
      null,
      null,
      null,
      null,
      null,
      null,
      { 'iDataSort': 8 },
      { 'bVisible': false },
      { 'iDataSort': 10 },
      { 'bVisible': false }
    ],
    'aaSorting': [
      [ 7, 'desc' ]
    ],
    'aoColumnDefs': [
      { 'bSortable': false, 'aTargets': [0] }
    ],
    'language': {
      'url': baseUrl + 'public/js/data-tables/pl.json'
    }
  });

  $('#dt-search').keyup(function() {
    oTable.fnFilter($(this).val());
  })

  $.datepicker.regional["pl"] = {
    closeText: "Zamknij",
    prevText: "&#x3c;Poprzedni",
    nextText: "Następny&#x3e;",
    currentText: "Dziś",
    monthNames: ["Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"],
    monthNamesShort: ["Sty","Lu","Mar","Kw","Maj","Cze","Lip","Sie","Wrz","Pa","Lis","Gru"],
    dayNames: ["Niedziela","Poniedzialek","Wtorek","Środa","Czwartek","Piątek","Sobota"],
    dayNamesShort: ["Nie","Pn","Wt","Śr","Czw","Pt","So"],
    dayNamesMin: ["N","Pn","Wt","Śr","Cz","Pt","So"],
    weekHeader: "Tydz",
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ""
  };
  $.datepicker.setDefaults($.datepicker.regional["pl"]);

  $("#js-datepicker").datepicker({
    "dateFormat": "yy-mm-dd"
  });

  $("#js-ustaw-date-form").on("submit", function(e) {
    var date = $("#js-datepicker").val();

    if (date.match(/^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$/)) {
      return true;
    } else {
      e.preventDefault();

      addMsg('error', 'Wprowadzona data jest niepoprawna.', false);
    }
  });
});