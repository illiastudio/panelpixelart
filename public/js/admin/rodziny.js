$(document).ready(function ($) {
  var dataTable;
  var $table = $('#js-rodziny-table');

  /**
   * !Salda
   */

  $.ajax({
    type: "post",
    url: baseUrl + 'admin/salda/getList',
    dataType: "dataType",
    success: function (response) {
      console.log(response)
    },
    error: function (err) {
      console.log("error", err.responseText)
    }
  });

  dataTable = $table.dataTable({
    'iDisplayLength': 25,
    // 'serverSide': true,
    "stateSave": true,
    'ajax': {
      "url": baseUrl + "admin/salda/getList",
      "dataType": "json"
    },
    'sPaginationTypedialog-box': 'full_numbers',
    "bAutoWidth": false,
    'aoColumns': [
      null,
      null,
      null,
      null,
      null,
      null,
      { 'iDataSort': 7 },
      { 'bVisible': false },
      { 'iDataSort': 9 },
      { 'bVisible': false },
      null
    ],
    'aaSorting': [
      [1, 'asc']
    ],
    'aoColumnDefs': [
      { 'bSortable': false, 'aTargets': [10] }
    ],
    'language': {
      'url': baseUrl + 'public/js/data-tables/pl.json'
    },
    fnDrawCallback: function () {
      $table.find('tbody tr td').click(function (e) {
        e.preventDefault();

        var targetClass = e.target.className;

        if (targetClass !== 'send js-send') {
          var element = $(this).parent()[0];
          var position = dataTable.fnGetPosition(element);
          var id = dataTable.fnGetData(position)[0];

          document.location.href = "edytuj/" + id;
        }
      });

    }
  });

  function dataTableReload() {
    dataTable.fnDraw();
  }

  $(document).on("click", "#js-dodaj-rodzine", function (e) {
    e.preventDefault();

    $.ajax({
      url: baseUrl + "admin/salda/dodaj",
      type: "POST",
      dataType: "json",
      data: $("#dodaj-rodzine").serialize() + "&dodaj=",
      success: function (json) {
        if (json[0].type == 'error') {
          for (var i = 0; i < json.length; i++) {
            addMsg(json[i].type, json[i].msg, true);
          }
        } else {
          addMsg(json[0].type, json[0].msg, false);
          dataTableReload();

          $("#dodaj-rodzine").each(function () {
            this.reset();
          });
        }
      }
    });
  });

  $("body").append("<div class=\"confirm-box\"><h3 class=\"title-bottom-line\">Usunąć element?</h3><p>Czy na pewno chcesz usunąć tą rodzinę?</p><div><button id=\"js-yes\" class=\"yes-button\"><span></span>Tak</button><button id=\"js-no\" class=\"no-button\"><span></span>Nie</button></div><div class=\"clear\"></div></div>");

  $(document).on("click", "#js-delete", function (e) {
    e.preventDefault();

    confirmDialog(this, dataTableReload);
  });

  $(document).on('click', '#js-send-saldo', function (e) {
    e.preventDefault();

    $.ajax({
      url: baseUrl + 'admin/rodzina/sendSaldo',
      type: "post",
      data: $('#js-send-saldo-form').serialize(),
      dataTable: 'json',
      success: function (json) {
        addMsg(json.type, json.msg, false);
      }
    });
  });

  $(document).on('click', '.js-send', function (e) {
    e.preventDefault();

    var $obj = $(this).parent().parent().parent();
    var element = $obj[0];
    var position = dataTable.fnGetPosition(element);
    var id = dataTable.fnGetData(position)[0];
    var status = $obj.find('span[class^="platnosc-"]').data('status');

    var dataString = 'rodzina_id=' + id + '&status=' + status;

    $.ajax({
      url: baseUrl + 'admin/rodzina/sendSaldo',
      type: "post",
      data: dataString,
      dataTable: 'json',
      success: function (json) {
        addMsg(json.type, json.msg, false);
        dataTableReload();
      }
    });
  });

  $("#js-wplaty").dataTable({
    "sPaginationType": "full_numbers",
    "aaSorting": [
      [5, "desc"]
    ],
    "aoColumns": [
      null,
      null,
      null,
      null,
      null,
      { "iDataSort": 6 },
      { "bVisible": false },
      null
    ],
    'aoColumnDefs': [
      { 'bSortable': false, 'aTargets': [7] }
    ],
    'language': {
      "sProcessing": "Przetwarzanie...",
      "sLengthMenu": "Pokaż _MENU_ pozycji",
      "sZeroRecords": "Nie znaleziono pasujących pozycji",
      "sInfoThousands": " ",
      "sInfo": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
      "sInfoEmpty": "Pozycji 0 z 0 dostępnych",
      "sInfoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
      "sInfoPostFix": "",
      "sSearch": "<span class='szukaj'></span>",
      "sUrl": "",
      "oPaginate": {
        "sFirst": "Pierwsza",
        "sPrevious": "Poprzednia",
        "sNext": "Następna",
        "sLast": "Ostatnia"
      },
      "sEmptyTable": "Brak płatności",
      "sLoadingRecords": "Wczytywanie...",
      "oAria": {
        "sSortAscending": ": aktywuj by posortować kolumnę rosnąco",
        "sSortDescending": ": aktywuj by posortować kolumnę malejąco"
      }
    }
  });

  $(document).on('click', '.js-delete-platonsc', function (e) {
    if (!confirm('Usunąć płatność?')) {
      e.preventDefault();
    }
  });

});