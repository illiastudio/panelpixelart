$(document).ready(function () {
  var dataTable;
  var $table = $('#js-taryfikator-table');
  var $tableLast = $('#js-taryfikator-table_last');

  if ($table.length > 0) {
    dataTable = $table.dataTable({
      'iDisplayLength': 25,
      "stateSave": true,
      'sPaginationTypedialog-box': 'full_numbers',
      "bAutoWidth": false,
      'aaSorting': [
        [1, 'asc']
      ],
      'aoColumnDefs': [
        { 'bSortable': false, 'aTargets': [4] }
      ],
      'language': {
        'url': baseUrl + 'public/js/data-tables/pl.json'
      }
    });
  }
  if ($tableLast.length > 0) {
    dataTable = $tableLast.dataTable({
      'iDisplayLength': 25,
      "stateSave": true,
      'sPaginationTypedialog-box': 'full_numbers',
      "bAutoWidth": false,
      'aaSorting': [
        [1, 'asc']
      ],
      'aoColumnDefs': [
        { 'bSortable': false, 'aTargets': [4] }
      ],
      'language': {
        'url': baseUrl + 'public/js/data-tables/pl.json'
      }
    });
  }

  $.datepicker.regional["pl"] = {
    closeText: "Zamknij",
    prevText: "&#x3c;Poprzedni",
    nextText: "Następny&#x3e;",
    currentText: "Dziś",
    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec",
      "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
    monthNamesShort: ["Sty", "Lu", "Mar", "Kw", "Maj", "Cze",
      "Lip", "Sie", "Wrz", "Pa", "Lis", "Gru"],
    dayNames: ["Niedziela", "Poniedzialek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
    dayNamesShort: ["Nie", "Pn", "Wt", "Śr", "Czw", "Pt", "So"],
    dayNamesMin: ["N", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
    weekHeader: "Tydz",
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ""
  };
  $.datepicker.setDefaults($.datepicker.regional["pl"]);

  $(document).on('click', '.js-delete-taryfikator', function (e) {
    if (!confirm('Usunąć taryfikator?')) {
      e.preventDefault();
    }
  });

  $(document).on('click', '.js-edit-taryfikator', function (e) {
    e.preventDefault();

    var $box = $('#js-taryfikator-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-taryfikator-box" class="taryfikator-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function (htmlData) {
        $box = $('#js-taryfikator-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function () {
            $.modal.close();
          }
        })
      }
    });
  });
});