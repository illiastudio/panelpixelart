$(document).ready(function() {
  if ($('#js-daty-treningow').length > 0) {
    $('#js-daty-treningow').multiDatesPicker({
      dateFormat: 'yy-mm-dd',
    });
  }

  var dataTable;
  var $table = $('#js-treningi-table');

  if ($table.length > 0) {
    dataTable = $table.dataTable({
      'iDisplayLength': 25,
      "stateSave": true,
      'sPaginationTypedialog-box': 'full_numbers',
      "bAutoWidth": false,
      'aoColumns': [
        { 'iDataSort': 1 },
        { 'bVisible': false },
        null
      ],
      'aaSorting': [
        [ 0, 'asc' ]
      ],
      'aoColumnDefs': [
        { 'bSortable': false, 'aTargets': [2] }
      ],
      'language': {
        'url': baseUrl + 'public/js/data-tables/pl.json'
      }
    });
  }

  $.datepicker.regional["pl"] = {
    closeText: "Zamknij",
    prevText: "&#x3c;Poprzedni",
    nextText: "Następny&#x3e;",
    currentText: "Dziś",
    monthNames: ["Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec",
    "Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"],
    monthNamesShort: ["Sty","Lu","Mar","Kw","Maj","Cze",
    "Lip","Sie","Wrz","Pa","Lis","Gru"],
    dayNames: ["Niedziela","Poniedzialek","Wtorek","Środa","Czwartek","Piątek","Sobota"],
    dayNamesShort: ["Nie","Pn","Wt","Śr","Czw","Pt","So"],
    dayNamesMin: ["N","Pn","Wt","Śr","Cz","Pt","So"],
    weekHeader: "Tydz",
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ""};
  $.datepicker.setDefaults($.datepicker.regional["pl"]);

  $(document).on('click', '.js-delete-trening', function(e) {
    if (!confirm('Usunąć trening?')) {
      e.preventDefault();
    }
  });
});