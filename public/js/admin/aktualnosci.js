$(document).ready(function() {
  var dataTable;

  dataTable = $('#news_table').dataTable({
    'serverSide': true,
    "stateSave": true,
    'ajax': baseUrl + 'admin/aktualnosci/getList',
    'sPaginationTypedialog-box': 'full_numbers',
    "bAutoWidth": false,
    'aaSorting': [
      [ 2, 'desc' ]
    ],
    'aoColumns': [
      null,
      null,
      { 'iDataSort': 3 },
      { 'bVisible': false },
      null
    ],
    'aoColumnDefs': [
      { 'bSortable': false, 'aTargets': [0] },
      { 'bSortable': false, 'aTargets': [4] }
    ],
    'language': {
      'url': baseUrl + 'public/js/data-tables/pl.json'
    }
  });

  $(document).on("click", "#dodaj_news", function(e) {
    e.preventDefault();

    $.ajax({
      url: baseUrl + "admin/aktualnosci/dodaj",
      type: "POST",
      dataType: "json",
      data: $("#dodaj_strone").serialize() + "&dodaj=",
      success: function(json) {
        if (json[0].type == 'error') {
          for (var i = 0; i < json.length; i++) {
            addMsg(json[i].type, json[i].msg, true);
          }
        } else {
          addMsg(json[0].type, json[0].msg, false);
          dataTable.fnDraw();
          $("#dodaj_strone").each(function(){
            this.reset();
          });
        }
      }
    });
  });

  function dataTableReload() {
    dataTable.fnDraw();
  }

  $("body").append("<div class=\"confirm-box\"><h3 class=\"title-bottom-line\">Usunąć element?</h3><p>Czy na pewno chcesz usunąć tą stronę?</p><div><button class=\"tak\">Tak</button><button class=\"nie\">Nie</button></div><div class=\"clear\"></div></div>");

  $(document).on("click", ".delete", function(e) {
    e.preventDefault();

    confirmDialog(this, dataTableReload);
  });

/*
  var allAccordions = $('.accordion div.data');
  var allAccordionItems = $('.accordion .accordion-item');
  $('.accordion > .accordion-item').click(function() {
    if($(this).hasClass('open'))
    {
      $(this).removeClass('open');
      $(this).next().slideUp("slow");
    }
    else
    {
    allAccordions.slideUp("slow");
    allAccordionItems.removeClass('open');
    $(this).addClass('open');
    $(this).next().slideDown("slow");
    return false;
    }
  });
*/

});