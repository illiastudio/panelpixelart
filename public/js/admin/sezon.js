var $tableLast = $('#js-sezon-table_last');

if ($tableLast.length > 0) {
    dataTable = $tableLast.dataTable({
      'iDisplayLength': 25,
      "stateSave": true,
      'sPaginationTypedialog-box': 'full_numbers',
      "bAutoWidth": false,
      'aaSorting': [
        [1, 'asc']
      ],
      'aoColumnDefs': [
        { 'bSortable': false, 'aTargets': [4] }
      ],
      'language': {
        'url': baseUrl + 'public/js/data-tables/pl.json'
      }
    });
  }