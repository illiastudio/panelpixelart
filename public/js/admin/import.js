$(document).ready(function() {
  var dataTable;

  dataTable = $('#js-rodziny-table').dataTable({
    'sPaginationTypedialog-box': 'full_numbers',
    "bAutoWidth": false,
    'aaSorting': [
      [ 1, 'asc' ]
    ],
    'language': {
      'url': baseUrl + 'public/js/data-tables/pl.json'
    }
  });
});