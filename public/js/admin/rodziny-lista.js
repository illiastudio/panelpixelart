$(document).ready(function() {
  var dataTable;
  var $table = $('#js-rodziny-list-table');

  if ($table.length > 0) {
    dataTable = $table.dataTable({
      'iDisplayLength': 50,
      'serverSide': true,
      "stateSave": true,
      'ajax': baseUrl + 'admin/rodziny/getList',
      'sPaginationTypedialog-box': 'full_numbers',
      "bAutoWidth": false,

      'aaSorting': [
        [ 1, 'asc' ]
      ],
      'language': {
        'url': baseUrl + 'public/js/data-tables/pl.json'
      }
    });
  }

  $.datepicker.regional["pl"] = {
    closeText: "Zamknij",
    prevText: "&#x3c;Poprzedni",
    nextText: "Następny&#x3e;",
    currentText: "Dziś",
    monthNames: ["Styczeń","Luty","Marzec","Kwiecień","Maj","Czerwiec",
    "Lipiec","Sierpień","Wrzesień","Październik","Listopad","Grudzień"],
    monthNamesShort: ["Sty","Lu","Mar","Kw","Maj","Cze",
    "Lip","Sie","Wrz","Pa","Lis","Gru"],
    dayNames: ["Niedziela","Poniedzialek","Wtorek","Środa","Czwartek","Piątek","Sobota"],
    dayNamesShort: ["Nie","Pn","Wt","Śr","Czw","Pt","So"],
    dayNamesMin: ["N","Pn","Wt","Śr","Cz","Pt","So"],
    weekHeader: "Tydz",
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ""};
  $.datepicker.setDefaults($.datepicker.regional["pl"]);

});