$(document).ready(function() {
  $(document).on('click', '#js-add-class', function(e) {
    e.preventDefault();

    var $box = $('#js-add-class-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-add-class-box" class="add-class-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function(htmlData) {
        $box = $('#js-add-class-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function() {
            $.modal.close();
          }
        })
      }
    });
  });

  $(document).on('click', '.js-class-edit', function(e) {
    e.preventDefault();

    var $box = $('#js-edit-class-box');

    if ($box.length === 0) {
      $('body').append('<div id="js-edit-class-box" class="edit-class-box"></div>');
    } else {
      $box.html();
    }

    $.ajax({
      type: 'post',
      dataType: 'html',
      url: $(this).attr('href'),
      success: function(htmlData) {
        $box = $('#js-edit-class-box');

        $box.html(htmlData);
        $box.modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function() {
            $.modal.close();
          }
        })
      }
    });
  });
});