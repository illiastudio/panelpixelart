// var baseUrl = window.location.protocol + "//" + window.location.host + "/panelpixelart/";
var baseUrl = window.location.protocol + "//" + window.location.host + "/";
var $flashMessages;

$(document).ready(function () {
  $flashMessages = $(".messages > div:not(.sticky)");

  if ($('.hidden-list').length > 0) {
    $('.content-wrapper').css('flex-flow', 'row wrap');
  }

  $(document).on('click', '#js-show-hide-list', function (e) {
    e.preventDefault();

    var $list = $('.hidden-list'),
      display = $list.css('display');

    if (display == 'none') {
      $list.slideDown(500);
    } else {
      $list.slideUp(500);
    }
  });

  $(document).on("click", ".tabs-menu li a", function (e) {
    e.preventDefault();

    var tab = $(this).attr('href');

    if ($(tab).length > 0) {
      $(".tabs-menu-active").removeClass("tabs-menu-active");
      $(".tabs-cont-active").removeClass("tabs-cont-active");

      $(tab).addClass("tabs-cont-active");
      $(this).parent().addClass("tabs-menu-active");
    } else {
      alert("Nie znaleziono: " + tab);
    }
  });

  if ($flashMessages.length > 0) {
    $($flashMessages.get().reverse()).each(function (index) {
      var $this = $(this);
      var delayInMilliseconds = 1500;

      setTimeout(function () {
        $this.find('> *').css('visibility', 'hidden');
        $this.slideUp(300, function () {
          $(this).remove();
        });
      }, delayInMilliseconds * (index + 1));
    });
  }

  $.datepicker.regional["pl"] = {
    closeText: "Zamknij",
    prevText: "&#x3c;Poprzedni",
    nextText: "Następny&#x3e;",
    currentText: "Dziś",
    monthNames: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec",
      "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
    monthNamesShort: ["Sty", "Lu", "Mar", "Kw", "Maj", "Cze",
      "Lip", "Sie", "Wrz", "Pa", "Lis", "Gru"],
    dayNames: ["Niedziela", "Poniedzialek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"],
    dayNamesShort: ["Nie", "Pn", "Wt", "Śr", "Czw", "Pt", "So"],
    dayNamesMin: ["N", "Pn", "Wt", "Śr", "Cz", "Pt", "So"],
    weekHeader: "Tydz",
    dateFormat: "yy-mm-dd",
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ""
  };
  $.datepicker.setDefaults($.datepicker.regional["pl"]);

  $(".js-zgloszenie-date").datepicker({
    "dateFormat": "yy-mm-dd"
  });

  $("#js-zgloszenia-form").on("submit", function (e) {

    var date = $(".js-zgloszenie-date").val();

    if (tmp.match(/^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$/)) {
      return true;
    } else {
      e.preventDefault();

      addMsg('error', 'Wprowadzona data jest niepoprawna.', false);
    }


  });

  $(document).on("click", ".close-msg", function (e) {
    e.preventDefault();

    var $this = $(this).parent();

    $this.find('> *').css('visibility', 'hidden');
    $this.slideUp(300, function () {
      $(this).remove();
    });

    // $(this).parent().slideUp(300, function() { $(this).parent().remove(); });
  });

  $(document).on("click", ".statusY, .statusN", function (e) {
    var id = $(this).attr("id");
    var status = $(this).attr("class");
    var $span = $("#" + id);

    $.ajax({
      data: { typ: id, status: status },
      type: "POST",
      dataType: "json",
      url: baseUrl + "admin/ajax/statusChange",
      success: function (json) {
        addMsg(json.type, json.msg, false);

        if (json.type == 'success') {
          if (status == "statusY") {
            $span.attr("class", "statusN");
          } else {
            $span.attr("class", "statusY");
          }
        }
      }
    });
  });

  $(document).on("click", "#pliki", function (e) {
    e.preventDefault();
    $("body").append("<div id=\"elfinder\"></div>");

    $("#elfinder").modal({
      minHeight: 500,
      minWidth: 800,
      onShow: function () {
        var modal = this;


        var elf = $("#elfinder").elfinder({
          lang: "pl",
          url: "/elfinder/php/connector.php",
          height: 500,
          resizable: false
        }).elfinder('instance');
      },
      onClose: function () {
        this.close();
        $("#elfinder").remove();
        clearHash();
      }
    });
  })


  $(document).on("click", ".metatag", function (e) {
    e.preventDefault();

    $("body").append("<div class=\"metatags-box\"></div>");

    $.ajax({
      type: "post",
      url: $(this).attr("href"),
      dataType: "html",
      success: function (data) {
        $(".metatags-box").html(data);

        $(".metatags-box").modal({
          minHeight: 360,
          minWidth: 600,
          onClose: function () {
            $.modal.close();
            $(".metatags-box").remove();
          }
        });
      }
    });
  });

  $(document).on("submit", "#edytuj_metatagi", function (e) {
    e.preventDefault();

    var postData = $(this).serialize();
    var id = $(this).attr('class');

    $.ajax({
      type: "post",
      url: baseUrl + "admin/metatagi/edytuj/" + id,
      dataType: "json",
      data: postData,
      success: function (json) {
        $.modal.close();
        addMsg(json[0].type, json[0].msg, false);
      }
    });
  });

});

function addMsg(type, msg, sticky) {
  var stickyClass = '';
  var stickyClose = '';

  // if (sticky) {
  //   stickyClass = ' sticky';
  //   stickyClose = '<span class="close-msg"></span>';
  // }

  var msgTxt = "<div class=\"msg-" + type + stickyClass + "\"><span class=\"msg-icon\"></span><p>" + msg + "</p><span class=\"close-msg\"></span></div>";

  if (!sticky) {
    $(msgTxt).prependTo(".messages").slideDown(300).delay(1500).slideUp(300, function () { $(this).remove(); });
  } else {
    $(msgTxt).prependTo(".messages").slideDown(300);
  }
}

function createCookie(name, value, days) {
  var expires;

  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toGMTString();
  } else {
    expires = "";
  }

  document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
}

function readCookie(name) {
  var nameEQ = escape(name) + "=";
  var ca = document.cookie.split(';');

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
  }

  return null;
}

function eraseCookie(name) {
  createCookie(name, "", -1);
}

function confirmDialog(link, callback) {
  $(".confirm-box").modal({
    minHeight: 150,
    minWidth: 300,
    onShow: function (dialog) {
      var modal = this;

      $('#js-yes', dialog.data[0]).click(function () {
        $.ajax({
          type: "POST",
          url: link.href,
          success: function (json) {
            addMsg(json[0].type, json[0].msg, false);

            if (callback != false) {
              callback();
            }
          }
        });

        modal.close();
      });

      $('#js-no', dialog.data[0]).click(function () {
        modal.close();
      });
    }
  });
}

function openElfinderModalInCKeditor() {
  CKEDITOR.on('dialogDefinition', function (event) {
    var editor = event.editor;
    var dialogDefinition = event.data.definition;
    var dialogName = event.data.name;

    var cleanUpFuncRef = CKEDITOR.tools.addFunction(function () {
      $('#filemanager_iframe').remove();
      $("body").css("overflow-y", "scroll");
    });

    var tabCount = dialogDefinition.contents.length;

    for (var i = 0; i < tabCount; i++) {
      var browseButton = dialogDefinition.contents[i].get('browse');

      if (browseButton !== null) {
        browseButton.hidden = false;
        browseButton.onClick = function (dialog, i) {
          editor._.filebrowserSe = this;
          $("#elfinder-cont").remove();
          $("body").append("<div id='elfinder-cont'></div>");

          $("#elfinder-cont").modal({
            minHeight: 500,
            minWidth: 800,
            onShow: function () {
              var modal = this;

              $("#elfinder-cont").elfinder({
                resizable: false,
                lang: 'pl',
                height: 500,
                url: '/elfinder/php/connector.php?mode=' + dialogName,
                getFileCallback: function (url) {
                  var dialog = CKEDITOR.dialog.getCurrent();

                  if (dialog._.name == "image") {
                    var urlObj = 'txtUrl'
                  } else if (dialog._.name == "flash") {
                    var urlObj = 'src'
                  } else if (dialog._.name == "link") {
                    var urlObj = 'url'
                  } else {
                    return false
                  };

                  dialog.setValueOf(dialog._.currentTabId, urlObj, url.url);
                  modal.close();
                  return false;
                }
              }).elfinder('instance');
            }
          });
        }
      }
    }
  });
};

function clearHash() {
  var hash = location.hash.replace('#', '');

  if (hash != '') {
    location.hash = '';
  }
}