// var baseUrl = window.location.protocol + "//" + window.location.host + "/panelpixelart/";
var baseUrl = window.location.protocol + "//" + window.location.host + "/";
var $flashMessages;

$(document).ready(function() {
  $flashMessages = $(".messages > div:not(.sticky)");

  if ($flashMessages.length > 0) {
    $($flashMessages.get().reverse()).each(function(index) {
      var $this = $(this);
      var delayInMilliseconds = 1500;

      setTimeout(function() {
        $this.find('> *').css('visibility', 'hidden');
        $this.slideUp(300, function() {
          $(this).remove();
        });
      }, delayInMilliseconds * (index + 1));
    });
  }

  $(document).on("click", ".close-msg", function(e) {
    e.preventDefault();

    var $this = $(this).parent();

    $this.find('> *').css('visibility', 'hidden');
    $this.slideUp(300, function() {
      $(this).remove();
    });

    // $(this).parent().slideUp(300, function() { $(this).parent().remove(); });
  });
});