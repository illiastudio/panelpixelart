$(document).ready(function() {
  $('.tab-title-wrapper').children().click(function(){
    $(this).addClass('active');
    $(this).siblings().removeClass('active');

    var selected_index = $(this).index() + 1;
    $(this).parent().siblings('.tab-content-wrapper').children(':nth-child(' + selected_index + ')').each(function(){
      $(this).siblings().removeClass('active').hide();
      $(this).fadeIn(function(){ $(this).addClass('active'); });
    })
  });
});