$(document).ready(function() {
  $("#js-wplaty").dataTable({
    "sPaginationType": "full_numbers",
    "stateSave": true,
    "aaSorting": [
      [ 5, "desc" ]
    ],
    "aoColumns": [
      null,
      null,
      null,
      null,
      null,
      { "iDataSort": 6 },
      { "bVisible": false }
    ],
    'language': {
      "sProcessing":   "Przetwarzanie...",
      "sLengthMenu":   "Pokaż _MENU_ pozycji",
      "sZeroRecords":  "Nie znaleziono pasujących pozycji",
      "sInfoThousands":  " ",
      "sInfo":         "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
      "sInfoEmpty":    "Pozycji 0 z 0 dostępnych",
      "sInfoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
      "sInfoPostFix":  "",
      "sSearch":       "<span class='szukaj'></span>",
      "sUrl":          "",
      "oPaginate": {
          "sFirst":    "Pierwsza",
          "sPrevious": "Poprzednia",
          "sNext":     "Następna",
          "sLast":     "Ostatnia"
      },
      "sEmptyTable":     "Brak płatności",
      "sLoadingRecords": "Wczytywanie...",
      "oAria": {
          "sSortAscending":  ": aktywuj by posortować kolumnę rosnąco",
          "sSortDescending": ": aktywuj by posortować kolumnę malejąco"
      }
    }
  });
});