(function($) {
    $.extend($.expr[':'], {
        'containsi': function(elem, i, match, array) {
            return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });

    $.fn.multiSelectToCheckboxes = function(options) {
        options = $.extend({
            filterLabel : 'Szukaj:',
            filterPlaceholder : 'Szukaj',
            initPlaceholder : 'Wybierz',
            minFilterTextLength : 0,
            selectAllText : 'Zaznacz widoczne',
            deselectAllText : 'Odznacz widoczne',
            maxListToCount : 4, //po przekroczeniu tej wartosci pokaze ponizszy komunikat
            infoText : 'Wybrano {0} z {1}',
            showOnlyCountText : true, //czy pokazywac tylko powyzszy tekst w polu
            closeText : 'Zamknij'
        }, options);

        function fillUl($select, $ul) {
            var baseId = "_" + $select.attr("id");
            $select.find("option, optgroup").each(function(index) {
                var $t = $(this);
                if ($t.is("option")) {
                    var id = baseId + index;
                    var $li = $("<li></li>").appendTo($ul);
                    var $checkbox = $("<input type='checkbox' id='" + id + "'/>")
                        .appendTo($li)
                        .on('change', function() {
                            var $chk = $(this)
                            if ($chk.is(":checked")) {
                                $chk.parent().addClass('select');
                                $t.attr("selected", "selected");
                            } else {
                                $chk.parent().removeClass('select');
                                $t.removeAttr("selected");
                            }
                            updateFakeInput($select);
                        });
                    if ($t.is(":selected")) {
                        $checkbox.attr("checked", "checked");
                        $checkbox.parent().addClass('select');
                    }
                    $checkbox.after("<label for='" + id + "'>" + $t.text() + "</label>");
                } else {
                    var $li = $('<li class="group-name"><strong>' + $t.attr('label') + '</strong></li>').appendTo($ul);
                }
            });
            return $ul;
        };

        function updateList($select, $ul) {
            $ul.empty();
            fillUl($select, $ul);
        };

        function createMultiselectControl($select, $ul) {
            var id = $select.attr('id');
            var $headerContainer = $('<div class="multiselect-controls"></div>');

            //text filter container
            var $filterDiv = $('<div class="filter-cnt"></div>');
            var $filterLabel = $('<label for="'+id+'_filter"">'+options.filterLabel+'</label>')
            var $filterInput = $('<input type="text" id="'+id+'_filter" placeholder="'+options.filterPlaceholder+'" />');
            $filterInput.on('keyup', function() {
                var $li = $ul.children('li');
                var val = $(this).val();
                $ul.find('.empty-list').remove();

                if (val.length >= options.minFilterTextLength) {
                    $ul.children('li').hide();
                    $ul.children('li').filter(':containsi("'+val+'")').show();
                }

                if ($li.length) {
                    if (!$li.filter(':visible').length) {
                        if (!$ul.parent().find('.empty-list').length) {
                            $ul.parent().append('<strong class="empty-list">Brak wyników</strong>');
                        }
                    }
                }
            });
            $filterDiv.append($filterLabel).append($filterInput);

            //select, unselect buttons
            var $buttonsDiv = $('<div class="buttons-cnt"></div>');
            var $buttonSelectAll = $('<a href="#" class="select-all">'+options.selectAllText+'</a>');
            var $buttonDeselectAll = $('<a href="#" class="deselect-all">'+options.deselectAllText+'</a>');
            $buttonSelectAll.on('click', function(e) {
                e.preventDefault();
                $ul.find('li:visible :checkbox').prop('checked', 1);
                $ul.find(':checkbox').trigger('change');
            })
            $buttonDeselectAll.on('click', function(e) {
                e.preventDefault();
                $ul.find('li:visible :checkbox').prop('checked', 0);
                $ul.find(':checkbox').trigger('change');
            })
            $buttonsDiv.append($buttonSelectAll).append($buttonDeselectAll);

            $headerContainer.append($filterDiv).append($buttonsDiv);

            //close button
            var $closeButton = $('<a href="#" class="close" title="Zamknij">'+options.closeText+'</a>');
            $closeButton.on('click', function(e) {
                e.preventDefault();
                $select.parent().toggleClass('active');
            })
            $headerContainer.append($closeButton);

            return $headerContainer;
        }

        function updateFakeInput($select) {
            var $input = $select.parent().find('.fake-multiselect');
            var $chk = $select.parent().find(':checkbox');
            var $chkSelected = $chk.filter(':checked');

            if ($chkSelected.length==0) {
                $input.val(options.initPlaceholder);
            } else {
                if (options.showOnlyCountText || ($chkSelected.length > options.maxListToCount)) {

                    var tempArray = [$chkSelected.length, $chk.length];
                    var text = options.infoText.replace(/{(\d+)}/g, function(match, number) {
                        return typeof tempArray[number] != 'undefined'? tempArray[number]: match
                    });
                    $input.val(text);

                } else {
                    var labelsText = [];
                    $chkSelected.each(function() {
                        labelsText.push($(this).next('label').text());
                    })
                    $input.val(labelsText.join(','));

                }
            }
        }

        function createFakeInput($select, $ul) {
            var $input = $('<input type="text" class="fake-multiselect" value="'+options.initPlaceholder+'" />');
            $input.prop('readonly', 1);
            $input.on('click', function() {
                $select.parent().toggleClass('active');
            })
            return $input;
        }

        return this.each(function() {
            var $select = $(this);
            var $wrapper = $('<div class="multiselect"></div>');
            $select.wrap($wrapper);

            var $ulContainer = $('<div class="list-container"></div>');
            var $ul = $('<ul class="list"></ul>');
            $ulContainer.append($ul);

            var $controls = createMultiselectControl($select, $ul);
            var $container = $('<div class="multiselect-cnt"></div>');
            $container.append($controls).append($ulContainer);

            var $input = createFakeInput($select, $ul);
            $select.parent().append($container).prepend($input);

            fillUl($select, $ul);
            updateFakeInput($select);

            $select.hide();
            $select.on('update', function() {
                updateList($select, $ul)
            })
        })
    };
})(jQuery);