<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="UTF-8">
  <style>
    table {
      text-align: center;
      border-collapse: collapse;
    }

    th,
    td {
      padding: 5px;
      border: 1px solid #424242;
    }
  </style>
</head>
<body>

  <p>Drodzy Rodzice,</p>

  <p>Minął kolejny miesiąc treningów Waszego syna/Waszych synów w UKS Żagle. Mamy nadzieję, że jesteście usatysfakcjonowani ze współpracy z Klubem co najmniej równie mocno jak Wasz syn/Wasi synowie.</p>

  <p>Po zamknięciu księgowym poprzedniego miesiąca status Waszych rozliczeń jest: <strong><u>STATUS KONTA</u></strong></p>

  <p>Uprzejmie prosimy o zalogowanie się do portalu rodzica <strong>(jest to możliwe poprzez stronę internetową klubu <a href="http://ukszagle.pl">http://ukszagle.pl</a> zakładka, „LOGOWANIE DO PORTALU RODZICA”)</strong> i sprawdzenie czy tak jest w rzeczywistości.</p>

  <p><strong>W przypadku gdyby coś się nie zgadzało prosimy o mail na adres: płatności@ukszagle.pl</strong></p>

  <p><strong><u>Jeśli wszystko się zgadza prosimy nic nam nie wysyłać!</u></strong></p>

  <p>Ze sportowym pozdrowieniem,<br>
  Biuro UKS Żagle<br>
  biuro@ukszagle.pl</p>

</body>
</html>