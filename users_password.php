<?php

die;

require 'config.php';
require 'libs/hash.php';

try {
  $pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
  header('Content-Type: text/html; charset=utf-8');
  die('Nie można nawiązać połączenia z bazą danych.');
}

$sth = $pdo->prepare('SELECT * FROM opiekunowie where opiekun_active_link is null');
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_ASSOC);

/*
foreach ($result as $key => $value) {
  $password = HASH_OPIEKUN_PASSWORD_KEY.$value['opiekun_mail'];
  $password = Hash::create('sha512', $password, HASH_SITE_KEY);

  $sth2 = $pdo->prepare('UPDATE opiekunowie SET opiekun_password = "'.$password.'" WHERE opiekun_id = '.$value['opiekun_id'].' AND opiekun_mail != "brak"');
  $sth2->execute();
}
*/

foreach ($result as $key => $value) {
  $password = HASH_OPIEKUN_PASSWORD_KEY.$value['opiekun_mail'].'-'.$value['opiekun_imie'];
  $password = Hash::create('sha256', $password, HASH_ACTIVE_LINK_KEY);

  $sth2 = $pdo->prepare('UPDATE opiekunowie SET opiekun_active_link = "'.$password.'" WHERE opiekun_id = '.$value['opiekun_id'].' AND opiekun_mail != "brak"');

  $sth2->execute();
}