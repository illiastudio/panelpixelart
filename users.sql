-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 21 Lut 2020, 12:02
-- Wersja serwera: 10.3.16-MariaDB
-- Wersja PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `panel`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_login` char(50) COLLATE utf8_polish_ci NOT NULL,
  `user_name` varchar(500) COLLATE utf8_polish_ci NOT NULL,
  `user_level` enum('0','1','2') COLLATE utf8_polish_ci NOT NULL,
  `user_password` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `user_password_reset` char(50) COLLATE utf8_polish_ci DEFAULT NULL,
  `user_password_changed` enum('0','1') COLLATE utf8_polish_ci NOT NULL DEFAULT '0',
  `user_mail` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `user_token` char(128) COLLATE utf8_polish_ci DEFAULT NULL,
  `user_last_activity` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`user_id`, `user_login`, `user_name`, `user_level`, `user_password`, `user_password_reset`, `user_password_changed`, `user_mail`, `user_token`, `user_last_activity`) VALUES
(1, 'admin', 'admin', '2', '3fce96a9fe04d0857fd9cbdee23a92e52448350888db7049510ae1b3778c837fb67f4c360c7c5cfcb36682661c89b8e3135f2f6b274404bf7be224b738a00fc4', '', '1', 'uksza_panel@studiograficzne.com', 'ff20db1686ff7e638a58f4e2888a0a72d649261d149b368c5a5daccc163234ce3f3548d5aef30e582078371e33242e187b091a58e1ed766efaa3a2f22e527eb9', 1565010559),
(20, 'adminS', '', '2', '8df6ff29d5d3e8b375074bf5dcadc55465187a4b67cfbe96d8be624f43b56596a08ebcdbf730a17d50444f61e270cb18086a5d8198fa7b659d8319ce419d500c', NULL, '1', 'illia@studiograficzne.com', '68028fbadb964bd8322e298e89d93cdb59aefc2f2e660a2b7e638675e61bef69828f0b794d2608b1261f35edf7ff265d399977ffa5955c5fc2440f190a69ee35', 1582282941),
(22, 'userS', '', '1', '8df6ff29d5d3e8b375074bf5dcadc55465187a4b67cfbe96d8be624f43b56596a08ebcdbf730a17d50444f61e270cb18086a5d8198fa7b659d8319ce419d500c', NULL, '0', NULL, NULL, 0),
(23, 'studio', 'studio', '1', '8df6ff29d5d3e8b375074bf5dcadc55465187a4b67cfbe96d8be624f43b56596a08ebcdbf730a17d50444f61e270cb18086a5d8198fa7b659d8319ce419d500c', NULL, '1', 'studio@studiograficzne.com', NULL, 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
