<?php

require 'config.php';

try {
  $pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
  header('Content-Type: text/html; charset=utf-8');
  die('Nie można nawiązać połączenia z bazą danych.');
}

/*
$sth = $pdo->prepare('
  SELECT rodzina_id, rodzina_nazwisko, o.opiekun_imie AS ojciec_imie, o.opiekun_telefon AS ojeciec_telefon, o.opiekun_mail AS ojciec_mail, m.opiekun_imie AS matka_imie, m.opiekun_telefon AS matka_telefon, m.opiekun_mail AS matka_mail, SUM(platnosc_zaplacono) AS zaplacono, status_sort
  FROM platnosci
  INNER JOIN rodziny ON rodzina_id = platnosc_rodzina_id
  INNER JOIN opiekunowie o ON o.opiekun_rodzina_id = rodzina_id AND o.opiekun_typ = 1
  INNER JOIN opiekunowie m ON m.opiekun_rodzina_id = rodzina_id AND m.opiekun_typ = 2
  inner join (
  SELECT rodzina_id as rodzina, (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort FROM rodziny LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id LEFT JOIN (SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty FROM (SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty FROM platnosci_saldo GROUP BY ps_rodzina_id) ps LEFT JOIN (SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono FROM platnosci GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id GROUP BY rodzina_id
  ) tmp on rodzina_id = rodzina
  GROUP BY platnosc_rodzina_id'
);
*/

$sth = $pdo->prepare('
  SELECT rodzina_id, rodzina_nazwisko, o.opiekun_imie AS ojciec_imie, o.opiekun_telefon AS ojeciec_telefon, o.opiekun_mail AS ojciec_mail, m.opiekun_imie AS matka_imie, m.opiekun_telefon AS matka_telefon, m.opiekun_mail AS matka_mail, coalesce(SUM(platnosc_zaplacono), 0) AS zaplacono
  , status_sort
  FROM platnosci
  right JOIN rodziny ON rodzina_id = platnosc_rodzina_id
  INNER JOIN opiekunowie o ON o.opiekun_rodzina_id = rodzina_id AND o.opiekun_typ = 1
  INNER JOIN opiekunowie m ON m.opiekun_rodzina_id = rodzina_id AND m.opiekun_typ = 2

  inner join (
  SELECT rodzina_id as rodzina, (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort FROM rodziny LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id LEFT JOIN (SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty FROM (SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty FROM platnosci_saldo GROUP BY ps_rodzina_id) ps LEFT JOIN (SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono FROM platnosci GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id GROUP BY rodzina_id
  ) tmp on rodzina_id = rodzina

  GROUP BY rodzina_id
');

$sth->execute();

$arr = $sth->fetchAll(PDO::FETCH_OBJ);

$raport = "rodzina_id;nazwisko;ojciec_imie;ojeciec_telefon;ojciec_mail;matka_imie;matka_telefon;matka_mail;zaplacono;status \n";

foreach ($arr as $key => $val) {

  switch ($val->status_sort) {
    case 0:
      $status = 'NIEDOPŁATA';
      break;
    case 1:
      $status = 'NADPŁATA';
      break;
    case 2:
      $status = 'SALDO OK';
      break;
    case 3:
      $status = 'PROSIMY O KONTAKT Z BIUREM KLUBU';
      break;
  }


  $raport .= $val->rodzina_id.';'.$val->rodzina_nazwisko.';'.$val->ojciec_imie.';'.$val->ojeciec_telefon.';'.$val->ojciec_mail.';'.$val->matka_imie.';'.$val->matka_telefon.';'.$val->matka_mail.';'.$val->zaplacono.';'.$status."\n";
}

// disable caching
$now = gmdate("D, d M Y H:i:s");
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
header("Last-Modified: {$now} GMT");

// force download
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");

// disposition / encoding on response body
header("Content-Disposition: attachment;filename=raport-platnosci.txt");
header("Content-Transfer-Encoding: binary");

header('Content-Type: text/html; charset=utf-8');

echo $raport;
die();