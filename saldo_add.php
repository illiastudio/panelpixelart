<?php

die;

require 'config.php';

try {
  $pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
  header('Content-Type: text/html; charset=utf-8');
  die('Nie można nawiązać połączenia z bazą danych.');
}

$day = date('d', time());
$month = date('m-Y', time());

if ($day == 01) {
  $sth = $pdo->prepare('SELECT 1 FROM platnosci_saldo WHERE DATE_FORMAT(ps_okres, "%m-%Y") = :month LIMIT 1');
  $sth->execute(array(':month' => $month));

  $result = $sth->fetchAll(PDO::FETCH_ASSOC);

  if (empty($result)) {
    $sthInsert = $pdo->prepare('INSERT INTO platnosci_saldo (ps_rodzina_id, ps_do_zaplaty, ps_okres) SELECT pdz_rodzina_id, pdz_suma, DATE_FORMAT(NOW(), "%Y-%m-%d") AS DATA FROM v_platnosci_do_zaplaty');
    $sthInsert->execute();
  }
}