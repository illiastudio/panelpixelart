<?php

class Validation
{
  public function notEmpty($data, $name)
  {
    if ($data === "") {
      return 'Pole ' . ucfirst(strtolower($name)) . ' jest wymagane.';
    }
  }
  public function Empty($data, $name)
  {
    if ($data === "") {
      return false;
    }
  }

  public function digit($data, $name)
  {
    if (ctype_digit($data) == false) {
      return 'Pole ' . ucfirst(strtolower($name)) . ' musi być liczbą.';
    }
  }

  public function minlength($data, $arg, $name)
  {
    if (!empty($data) && strlen($data) < $arg) {
      return 'Pole ' . ucfirst(strtolower($name)) . ' musi zawierać minimum ' . $arg . ' znaków.';
    }
  }

  public function maxlength($data, $arg, $name)
  {
    if (!empty($data) && strlen($data) > $arg) {
      return 'Pole ' . ucfirst(strtolower($name)) . ' musi zawierać maksimum ' . $arg . ' znaków.';
    }
  }

  public function isMail($email, $name)
  {
    $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';

    if (!preg_match($regex, $email)) {
      return 'Pole ' . ucfirst(strtolower($name)) . ' zawiera nieprawidłowy adres e-mail.';
    }
  }

  public function equal($data, $fieldname, $msg)
  {
    if ($data != $_POST[$fieldname]) {
      return $msg;
    }
  }
}
