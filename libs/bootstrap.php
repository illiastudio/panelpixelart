<?php

class Bootstrap
{
  private $_controller = null;
  private $_url = null;

  public function init()
  {
    $this->_setUrl();
    $this->_runAdminOrPage();
  }

  private function _setUrl()
  {
    $this->_url = isset($_GET['url']) ? $_GET['url'] : null;
    $this->_url = str_replace('.html', '', $this->_url);
    $this->_url = explode('/', $this->_url);
  }

  private function _runAdminOrPage()
  {
    if ($this->_url[0] == 'admin') {
      $this->_runAdmin();
    } else {
      $this->_controller = new Page_Controller();
      $this->_controller->loadPage($this->_url);
    }
  }

  private function _runAdmin()
  {
    unset($this->_url[0]);
    if (isset($this->_url[1]) && !empty($this->_url[1])) {
      $modul = $this->_url[1];
      unset($this->_url[1]);

      if (isset($this->_url[2]) && !empty($this->_url[2])) {
        $action = $this->_url[2];
        unset($this->_url[2]);
      } else {
        switch ($modul) {
          case 'login':
            $action = 'panel';
            break;
          case 'user':
            $action = 'profil';
            break;
          case 'sezon':
            $action = 'index';
            break;
          case 'harmonogram':
            $action = 'index';
            break;
          case 'import':
          case 'platnosci':
            $action = 'index';
            break;
          default:
            $action = 'lista';
            break;
        }
      }
      $this->_url = array_values($this->_url);

      $controllerName = $modul . '_Admin_Controller';

      if (class_exists($controllerName) && method_exists($controllerName, $action)) {
        $reflection = new ReflectionMethod($controllerName, $action);

        if (!$reflection->isPublic()) {
          header('Location: ' . URL . 'admin/');
          exit();
        }

        $controller = new $controllerName();
        $controller->{$action}($this->_url);
      } else {
        header('Location: ' . URL . 'admin/');
        exit();
      }
    } else {
      $this->_adminIndexPage();
    }
  }

  private function _adminIndexPage()
  {
    $controller = new Index_Admin_Controller();
    $controller->index();
    exit();
  }
}
