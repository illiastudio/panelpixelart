<?php

class Auth {
  private $_pdo;
  private $_autoLogoutSeconds = 1800;

  public function __construct() {
    try {
      $this->_pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
      $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $e) {
      header('Content-Type: text/html; charset=utf-8');
      die('Nie można nawiązać połączenia z bazą danych.');
    }
  }

  public function createUser($login, $password) {
    $password = HASH_PASSWORD_KEY.$password;
    $password = Hash::create('sha512', $password, HASH_SITE_KEY);

    $sth = $this->_pdo->prepare("INSERT INTO users(user_login, user_password) VALUES (:login, :password);");
    $sth->execute(array(':login' => $login, ':password' => $password));

    if($sth->rowCount() == 1) {
      return true;
    }

    return false;
  }

  public function login($login, $password) {
    $password = HASH_PASSWORD_KEY.$password;
    $password = Hash::create('sha512', $password, HASH_SITE_KEY);

    $sth = $this->_pdo->prepare("SELECT user_id, user_mail, user_level FROM users WHERE user_login = :login AND user_password = :password LIMIT 1");
    $sth->execute(array(':login' => $login, ':password' => $password));
    $result = $sth->fetch(PDO::FETCH_ASSOC);

    if(!empty($result)) {
      $random = $this->_randomString();

      $token = $_SERVER['REMOTE_ADDR'] . $random;
      $token = Hash::create('sha512', $token, HASH_SITE_KEY);

      $_SESSION['token'] = $token;
      $_SESSION['user_id'] = $result['user_id'];
      $_SESSION['user_login'] = $login;
      $_SESSION['user_mail'] = $result['user_mail'];
      $_SESSION['time'] = time();
      $_SESSION['lang'] = 'pl';

      if ($result['user_level'] == 1) {
        $role = 'trener';
      } elseif ($result['user_level'] == 2) {
        $role = 'admin';
      } else {
        $role = 'none';
      }

      $_SESSION['role'] = $role;

      $sth = $this->_pdo->prepare("UPDATE users SET user_token = :token, user_last_activity = :time WHERE user_id = :id");
      $sth->execute(array(':token' => $token, ':time' => time(), ':id' => $_SESSION['user_id']));

      return true;
    }

    return false;
  }

  public function loginOpiekun($login, $password) {
    $password = HASH_OPIEKUN_PASSWORD_KEY.$password;
    $password = Hash::create('sha512', $password, HASH_SITE_KEY);

    $sth = $this->_pdo->prepare("SELECT opiekun_id, opiekun_rodzina_id, opiekun_imie, rodzina_nazwisko FROM opiekunowie INNER JOIN rodziny ON opiekun_rodzina_id = rodzina_id WHERE opiekun_mail = :login AND opiekun_password = :password LIMIT 1");
    $sth->execute(array(':login' => $login, ':password' => $password));
    $result = $sth->fetch(PDO::FETCH_ASSOC);

    if(!empty($result)) {
      $random = $this->_randomString();

      $token = $_SERVER['REMOTE_ADDR'] . $random;
      $token = Hash::create('sha512', $token, HASH_SITE_KEY);

      $_SESSION['opiekun_token'] = $token;
      $_SESSION['opiekun_id'] = $result['opiekun_id'];
      $_SESSION['opiekun_imie'] = $result['opiekun_imie'];
      $_SESSION['opiekun_rodzina_id'] = $result['opiekun_rodzina_id'];
      $_SESSION['opiekun_login'] = $login;
      $_SESSION['opiekun_rodzina_nazwisko'] = $result['rodzina_nazwisko'];
      $_SESSION['page_time'] = time();
      $_SESSION['page_lang'] = 'pl';

      $sth = $this->_pdo->prepare("UPDATE opiekunowie SET opiekun_token = :token, opiekun_last_activity = :time WHERE opiekun_id = :id");
      $sth->execute(array(':token' => $token, ':time' => time(), ':id' => $_SESSION['opiekun_id']));

      return true;
    }

    return false;
  }

  public function loginOpiekunByActiveLink($code) {
    $sth = $this->_pdo->prepare("SELECT opiekun_id, opiekun_rodzina_id, opiekun_imie, rodzina_nazwisko, opiekun_mail FROM opiekunowie INNER JOIN rodziny ON opiekun_rodzina_id = rodzina_id WHERE opiekun_active_link = :code AND opiekun_password_changed = '0' LIMIT 1");
    $sth->execute(array(':code' => $code));
    $result = $sth->fetch(PDO::FETCH_ASSOC);

    if(!empty($result)) {
      $random = $this->_randomString();

      $token = $_SERVER['REMOTE_ADDR'] . $random;
      $token = Hash::create('sha512', $token, HASH_SITE_KEY);

      $_SESSION['opiekun_token'] = $token;
      $_SESSION['opiekun_id'] = $result['opiekun_id'];
      $_SESSION['opiekun_imie'] = $result['opiekun_imie'];
      $_SESSION['opiekun_rodzina_id'] = $result['opiekun_rodzina_id'];
      $_SESSION['opiekun_login'] = $result['opiekun_mail'];
      $_SESSION['opiekun_rodzina_nazwisko'] = $result['rodzina_nazwisko'];
      $_SESSION['page_time'] = time();
      $_SESSION['page_lang'] = 'pl';

      $sth = $this->_pdo->prepare("UPDATE opiekunowie SET opiekun_token = :token, opiekun_last_activity = :time WHERE opiekun_id = :id");
      $sth->execute(array(':token' => $token, ':time' => time(), ':id' => $_SESSION['opiekun_id']));

      return true;
    }

    return false;
  }

  public function checkSession() {
    $sth = $this->_pdo->prepare("SELECT user_token, user_last_activity FROM users WHERE user_id = :id LIMIT 1");
    $sth->execute(array(':id' => $_SESSION['user_id']));
    $result = $sth->fetch(PDO::FETCH_ASSOC);

    if ($_SESSION['time'] < $result['user_last_activity']) {
      $this->_refreshSession();
      $this->checkSession();
    }

    if(!empty($result)) {
      $lastActivity = time() - $result['user_last_activity'];

      if($_SESSION['token'] == $result['user_token'] && $lastActivity < $this->_autoLogoutSeconds) {
        $this->_refreshSession();

        return true;
      }
    }

    return false;
  }

  public function checkSessionOpiekun() {
    $sth = $this->_pdo->prepare("SELECT opiekun_token, opiekun_last_activity FROM opiekunowie WHERE opiekun_id = :id LIMIT 1");
    $sth->execute(array(':id' => $_SESSION['opiekun_id']));
    $result = $sth->fetch(PDO::FETCH_ASSOC);

    if ($_SESSION['page_time'] < $result['opiekun_last_activity']) {
      $this->_refreshSessionOpiekun();
      $this->checkSessionOpiekun();
    }

    if(!empty($result)) {
      $lastActivity = time() - $result['opiekun_last_activity'];

      if($_SESSION['opiekun_token'] == $result['opiekun_token'] && $lastActivity < $this->_autoLogoutSeconds) {
        $this->_refreshSessionOpiekun();

        return true;
      }
    }

    return false;
  }

  private function _refreshSession() {
    $random = $this->_randomString();

    $token = $_SERVER['REMOTE_ADDR'] . $random;
    $token = Hash::create('sha512', $token, HASH_SITE_KEY);

    $time = time();
    $_SESSION['token'] = $token;
    $_SESSION['time'] = $time;

    $sth = $this->_pdo->prepare("UPDATE users SET user_token = :token, user_last_activity = :time WHERE user_id = :id");
    $sth->execute(array(':token' => $token, ':time' => $time, ':id' => $_SESSION['user_id']));

    return false;
  }

  private function _refreshSessionOpiekun() {
    $random = $this->_randomString();

    $token = $_SERVER['REMOTE_ADDR'] . $random;
    $token = Hash::create('sha512', $token, HASH_SITE_KEY);

    $time = time();
    $_SESSION['opiekun_token'] = $token;
    $_SESSION['page_time'] = $time;

    $sth = $this->_pdo->prepare("UPDATE opiekunowie SET opiekun_token = :token, opiekun_last_activity = :time WHERE opiekun_id = :id");
    $sth->execute(array(':token' => $token, ':time' => $time, ':id' => $_SESSION['opiekun_id']));

    return false;
  }

  public function logout() {
    session_destroy();
  }

  private function _randomString($length = 50) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $string = '';

    for ($p = 0; $p < $length; $p++) {
      $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
  }

}