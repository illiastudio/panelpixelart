<?php

class Messages {
  private $_typeArray = array('error', 'info', 'success');

  public function __construct() {
      if (!array_key_exists('flash_messages', $_SESSION)) {
        $_SESSION['flash_messages'] = array();
      }
  }

  public function add($type, $msg, $sticky = false, $redirect = null) {
    if (!in_array($type, $this->_typeArray)) {
      return false;
    }

    $params = array();
    $params['class'] = 'msg-'.$type;
    $params['msg'] = $msg;

    if ($sticky) {
      $params['sticky'] = true;
    }

    $_SESSION['flash_messages'][] = $params;

    if (!empty($redirect)) {
      header('Location:'.URL.$redirect);
      exit();
    }
  }

  public function render() {
    $view = new View();

    krsort($_SESSION['flash_messages']);

    foreach ($_SESSION['flash_messages'] as $key => $value) {
      $value = $view->usunBekarty($value['msg']);
    }

    $view->msgArray = $_SESSION['flash_messages'];
    unset($_SESSION['flash_messages']);

    $view->renderPage('admin/messages');
  }

}