<?php

use PHPImageWorkshop\ImageWorkshop;

class Image {
  private $_cache = 'cache';
  private $_allowedExtension = array('jpg', 'png');
  private $_update = false;
  private $_crop = false;
  private $_cropCords = array();
  private $_img;
  private $_path;
  private $_cachedName;
  private $_extension;
  private $_width;
  private $_height;


  public function __construct($imagePath) {
    require 'PHPImageWorkshop\ImageWorkshop.php';

    $this->_path = $imagePath;
  }

  public function setSize($width, $height) {
    $this->_width = $width;
    $this->_height = $height;
  }

  public function update() {
    $this->_update = true;
  }

  private function _createName() {
    $file = explode('/', $this->_path);
    $file = end($file);

    $fileArr = explode('.', $file);

    $fileName = $fileArr[0];
    $extension = $fileArr[1];

    $this->_extension = strtolower($extension);
    $this->_cachedName = md5($fileName).'.'.$this->_extension;
  }

  public function getImage($return = false) {
    if ($return) {
      return $this->_cache.'/'.$this->_cachedName;
    } else {
      echo $this->_cache.'/'.$this->_cachedName;
    }
  }

  public function crop($x1, $y1, $x2, $y2) {
    $this->_crop = true;
    $this->_cropCords = array(
      'x1' => $x1,
      'x2' => $x2,
      'y1' => $y1,
      'y2' => $y2
    );
  }

  private function _cropImage() {
    $width = $this->_cropCords['x2'] - $this->_cropCords['x1'];
    $height = $this->_cropCords['y2'] - $this->_cropCords['y1'];

    $this->_img->cropInPixel($width, $height, $this->_cropCords['x1'], $this->_cropCords['y1'], 'LT');
    $this->_img->resizeInPixel($this->_width, $this->_height);
  }

  private function _createThumbnail() {
    if ($this->_width > $this->_height) {
      $largestSide = $this->_width;
    } else {
      $largestSide = $this->_height;
    }

    $this->_img->cropMaximumInPixel(0, 0, "MM");
    $this->_img->resizeInPixel($largestSide, $largestSide);
    $this->_img->cropInPixel($this->_width, $this->_height, 0, 0, 'MM');
  }

  public function create() {
    $this->_createName();

    if (!file_exists($this->_cache.'/'.$this->_cachedName) || $this->_update) {
      $this->_img = ImageWorkshop::initFromPath($this->_path);

      if (!in_array($this->_extension, $this->_allowedExtension)) {
        return false;
      }

      if ($this->_crop) {
        $this->_cropImage();
      } else {
        $this->_createThumbnail();
      }

      $dirPath = $this->_cache;
      $filename = $this->_cachedName;
      $createFolders = true;
      $backgroundColor = null;
      $imageQuality = 95;

      $this->_img->save($dirPath, $filename, $createFolders, $backgroundColor, $imageQuality);
    }
  }

}
