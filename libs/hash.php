<?php

class Hash {
  public static function create($algo, $data, $salt) {
    return hash_hmac($algo, $data, $salt);
  }

}