<?php

class Form {
  private $_currentItem = null;
  private $_postData = array();
  private $_errors = array();

  public function __construct() {
    $this->_val = new Validation();
  }

  public function post($field, $acceptHtml = false) {
    if (!$acceptHtml) {
      $_POST[$field] = htmlspecialchars($_POST[$field]);
    }

    $this->_postData[$field] = trim($_POST[$field]);
    $this->_currentItem = $field;

    return $this;
  }

  public function fetch($fieldName = false) {
    if ($fieldName) {
      if (isset($this->_postData[$fieldName])) {
        return $this->_postData[$fieldName];
      } else {
        return false;
      }
    } else {
      return $this->_postData;
    }
  }

  public function val($type, $param1 = null, $param2 = null) {
    $error = $this->_val->{$type}($this->_postData[$this->_currentItem], $param1, $param2);

    if ($error) {
      $this->_errors[] = $error;
    }

    return $this;
  }

  public function errorCheck() {
    if (empty($this->_errors)) {
      return true;
    } else {
      return false;
    }
  }

  public function getErrors() {
    return $this->_errors;
  }

  public function showErrors() {
    $msg = new Messages();

    foreach ($this->_errors as $key => $value) {
      $msg->add('error', $value, false);
    }
  }

}