<?php


class Session {
  private $_pdo;

  public function __construct() {
    // session_set_save_handler(array($this, 'open'),
    //                          array($this, 'close'),
    //                          array($this, 'read'),
    //                          array($this, 'write'),
    //                          array($this, 'destroy'),
    //                          array($this, 'gc'));

   // register_shutdown_function('session_write_close');
  }

  public function startSession($sessionName = '_s', $secure = false) {
    $httponly = true;
    $sessionHash = 'sha512';

    if (in_array($sessionHash, hash_algos())) {
      ini_set('session.hash_function', $sessionHash);
    }

    ini_set('session.hash_bits_per_character', 5);
    ini_set('session.use_only_cookies', 1);
    ini_set('session.gc_divisor', 10);

    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams['lifetime'], $cookieParams['path'], $cookieParams['domain'], $secure, $httponly);

    if (!isset($_SESSION)) {
      session_name($sessionName);
      session_start();
    }

    if (isset($_SESSION['time']) && $_SESSION['time'] < time() - 300) {
      session_regenerate_id();
    }
  }

  public function open() {
    try {
      $this->_pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
      $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $e) {
      header('Content-Type: text/html; charset=utf-8');
      die('Nie można nawiązać połączenia z bazą danych.');
    }

     return true;
  }

  public function close() {
    $this->_pdo = null;

    return true;
  }

  public function read($id) {
    if (!isset($this->sthRead)) {
      $this->sthRead = $this->_pdo->prepare("SELECT session_data FROM sessions WHERE session_id = :id LIMIT 1");
    }

    $this->sthRead->execute(array(':id' => $id));
    $resultData = $this->sthRead->fetch(PDO::FETCH_ASSOC);

    $key = $this->_getKey($id);
    $data = $this->_decrypt($resultData['session_data'], $key);

    return $data;
  }

  public function write($id, $data) {
    $key = $this->_getKey($id);
    $data = $this->_encrypt($data, $key);
    $time = time();

    if (!isset($this->sthWrite)) {
      $this->sthWrite = $this->_pdo->prepare("REPLACE INTO sessions (session_id, session_set_time, session_data, session_key) VALUES (:id, :time, :data, :key)");
    }

    $params = array();
    $params[':id'] = $id;
    $params[':time'] = $time;
    $params[':data'] = $data;
    $params[':key'] = $key;

    $this->sthWrite->execute($params);

    return true;
  }

  public function destroy($id) {
    if (!isset($this->sthDelete)) {
      $this->sthDelete = $this->_pdo->prepare("DELETE FROM sessions WHERE session_id = :id");
    }

    $this->sthDelete->execute(array(':id' => $id));

    return true;
  }

  public function gc($max) {
     if (!isset($this->sthGarbage)) {
        $this->sthGarbage = $this->_pdo->prepare("DELETE FROM sessions WHERE session_set_time < :time");
     }

     $oldTime = time() - $max;
     $this->sthGarbage->execute(array(':time' => $oldTime));

     return true;
  }

  private function _getKey($id) {
    if(!isset($this->sthKey)) {
      $this->sthKey = $this->_pdo->prepare("SELECT session_key FROM sessions WHERE session_id = :id LIMIT 1");
    }

    $this->sthKey->execute(array(':id' => $id));
    $resultKey = $this->sthKey->fetch(PDO::FETCH_ASSOC);

    if (!empty($resultKey)) {

      return $resultKey['session_key'];
    } else {
      $randomKey = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));

      return $randomKey;
    }
  }

  private function _encrypt($data, $key) {
    $salt = HASH_SESSION_KEY;
    $key = substr(hash('sha256', $salt.$key.$salt), 0, 32);
    $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $data, MCRYPT_MODE_ECB, $iv));

    return $encrypted;
  }

  private function _decrypt($data, $key) {
     $salt = HASH_SESSION_KEY;
     $key = substr(hash('sha256', $salt.$key.$salt), 0, 32);
     $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
     $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
     $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode($data), MCRYPT_MODE_ECB, $iv);

     return $decrypted;
  }

}