<?php

abstract class Model
{
  private $_pdo;
  private $_fetchMode = PDO::FETCH_ASSOC;

  public function __construct()
  {
    try {
      $this->_pdo = new PDO('mysql:host=' . DB_HOST . ';dbport=' . DB_PORT . ';dbname=' . DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
      $this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $e) {
      header('Content-Type: text/html; charset=utf-8');
      die('Nie można nawiązać połączenia z bazą danych.');
    }
  }

  public function select($query, $bindsArray = array())
  {
    try {
      $sth = $this->_pdo->prepare($query);
    
      $sth->execute($bindsArray);
      return $sth->fetchAll($this->_fetchMode);
    } catch (PDOException $e) {
      // $this->_logErrors($e); //!disable
      return $sth->fetchAll($this->_fetchMode);
    }
  }

  public function insert($table, $dataArray)
  {
    try {
      ksort($dataArray);

      $fieldNames = implode('`, `', array_keys($dataArray));
      $fieldValues = ':' . implode(', :', array_keys($dataArray));

      $sth = $this->_pdo->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");
      return $sth->execute($dataArray);
    } catch (PDOException $e) {
      $this->_logErrors($e);
    }
  }

  public function update($table, $dataArray, $where = null, $bindsArray = array())
  {
    try {
      ksort($dataArray);

      $fieldDetails = NULL;
      $fieldBinds = array();

      foreach ($dataArray as $key => $value) {
        $fieldDetails .= "$key = :$key,";
        $fieldBinds[':' . $key . ''] = $value;
      }

      $bindsArray = array_merge($fieldBinds, $bindsArray);

      $fieldDetails = rtrim($fieldDetails, ',');

      if (empty($where)) {
        $whereString = '';
      } else {
        $whereString = 'WHERE ' . $where;
      }

      $sth = $this->_pdo->prepare("UPDATE $table SET $fieldDetails $whereString");

      return $sth->execute($bindsArray);
    } catch (PDOException $e) {
      $this->_logErrors($e);
    }
  }

  public function delete($table, $where, $bindsArray = array(), $limit = true)
  {
    try {
      if ($limit) {
        $limitString = 'LIMIT 1';
      } else {
        $limitString = '';
      }

      $sth = $this->_pdo->prepare("DELETE FROM $table WHERE $where $limitString");
      return $sth->execute($bindsArray);
    } catch (PDOException $e) {
      $this->_logErrors($e);
    }
  }

  public function getlastInsertId()
  {
    return $this->_pdo->lastInsertId();
  }

  private function _logErrors($e)
  {
    $time = date('Y-m-d H:m:s', time());
    $backtrace = debug_backtrace();

    $error = $e->errorInfo[0];

    if (isset($e->errorInfo[2])) {
      $error .= ' ' . $e->errorInfo[2];
    }

    $msg = $time . ' - (' . $backtrace[0]['file'] . ' - ' . $backtrace[0]['line'] . ') ' . $error . PHP_EOL;

    error_log($msg, 3, "./errors.log");
    echo $msg;
  }
}
