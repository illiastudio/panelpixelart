<?php

class View {
  public function renderPage($file, $echo = true) {
    if (!file_exists(TEMPLATES . $file .'.php')) {
      return '';
    }

    ob_start();

    require TEMPLATES . $file .'.php';

    $page = ob_get_contents();
    ob_end_clean();

    if ($echo) {
      echo $page;
    } else {
      return $page;
    }
  }

  public function usunBekarty($string){
    $returnString = preg_replace('/ ([a-zA-Z]{1}) /', " $1&nbsp;", $string);
    $returnString = stripslashes($returnString);

    return $returnString;
  }

}