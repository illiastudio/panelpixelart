<?php

die;

require 'config.php';
require 'libs/phpmailer.php';

try {
  $pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (Exception $e) {
  header('Content-Type: text/html; charset=utf-8');
  die('Nie można nawiązać połączenia z bazą danych.');
}

$months = array(
  1 => 'Styczeń',
  2 => 'Luty',
  3 => 'Marzec',
  4 => 'Kwiecień',
  5 => 'Maj',
  6 => 'Czerwiec',
  7 => 'Lipiec',
  8 => 'Sierpień',
  9 => 'Wrzesień',
  10 => 'Październik',
  11 => 'Listopad',
  12 => 'Grudzień'
);

$sth = $pdo->prepare('SELECT opiekun_rodzina_id, opiekun_typ, opiekun_imie, opiekun_mail, rodzina_nazwisko FROM opiekunowie INNER JOIN rodziny ON opiekun_rodzina_id = rodzina_id WHERE opiekun_rodzina_id IN (SELECT DISTINCT(ps_rodzina_id) AS rodzina_id FROM platnosci_saldo)');
$sth->execute();
$rodziny = $sth->fetchAll();

$rodzinyArr = array();

foreach ($rodziny as $key => $val) {
  $rodzinyArr[$val['opiekun_rodzina_id']][$val['opiekun_typ']]['imie_nazwisko'] = $val['opiekun_imie'].' '.$val['rodzina_nazwisko'];
  $rodzinyArr[$val['opiekun_rodzina_id']][$val['opiekun_typ']]['mail'] = $val['opiekun_mail'];
}

$saldoArr = array();

$statusSth = $pdo->prepare('SELECT rodzina_id, rodzina_nazwisko, COALESCE(NULL, do_zaplaty, 0) AS naleznosc,COALESCE(pb_bilans, 0) pb_bilans, (CASE WHEN COALESCE(pb_bilans, 0) < 0 THEN 0 WHEN COALESCE(pb_bilans, 0) > 0 THEN 1 WHEN (COALESCE(pb_bilans, 0) = 0 AND do_zaplaty IS NOT NULL) THEN 2 ELSE 3 END) AS status_sort FROM rodziny LEFT JOIN v_platnosci_do_zaplaty ON rodzina_id = pdz_rodzina_id LEFT JOIN platnosci_saldo ON rodzina_id = ps_rodzina_id LEFT JOIN (SELECT ps_rodzina_id AS pb_rodzina_id, (COALESCE(zaplacono, 0) - COALESCE(do_zaplaty, 0)) AS pb_bilans, do_zaplaty FROM (SELECT ps_rodzina_id, SUM(ps_do_zaplaty) AS do_zaplaty FROM platnosci_saldo GROUP BY ps_rodzina_id) ps LEFT JOIN (SELECT platnosc_rodzina_id, SUM(platnosc_zaplacono) AS zaplacono FROM platnosci GROUP BY platnosc_rodzina_id) p ON ps.ps_rodzina_id = p.platnosc_rodzina_id) tmp ON rodzina_id = pb_rodzina_id GROUP BY rodzina_id');
$statusSth->execute();
$statusResult = $statusSth->fetchAll();

$statusArr = array();

if (!empty($statusResult)) {
  foreach ($statusResult as $key => $val) {
    $statusArr[$val['rodzina_id']] = $val['status_sort'];
  }
}

if (!empty($rodzinyArr)) {
  $mail = new phpmailer();
  $mail->CharSet = "UTF-8";
  $mail->SetLanguage("pl", "libs/");
  $mail->IsHTML(true);

  // SMTP
  $mail->Mailer = 'smtp';
  $mail->Host = MAIL_HOST;
  $mail->Port = MAIL_PORT;
  $mail->SMTPKeepAlive = true;
  $mail->SMTPAuth = true;
  $mail->IsSMTP();
  $mail->Username = MAIL_USER;
  $mail->Password = MAIL_PASS;

  $mail->From = MAIL_FROM;
  $mail->FromName = MAIL_FROM_NAME;
  $mail->Subject = 'UKS Żagle – saldo';

  foreach ($rodzinyArr as $key => $val) {
    $rodzina = $saldoArr[$key];

    foreach ($val as $typ => $dane) {
      $msg = '';

      ob_start();
      require 'saldo_template.php';
      $msg = ob_get_contents();
      ob_end_clean();

      echo $msg;

      $mail->Body = $msg;
      $mail->AddAddress(MAIL_TEST);

      $mail->Send();
      $mail->ClearAddresses();
      $mail->ClearAttachments();
    }

    if ($key > 1) {
      die;
    }
  }

  $mail->SmtpClose();
}
