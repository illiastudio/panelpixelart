<?php
die;
require 'config.php';

try {
  $pdo = new PDO('mysql:host='.DB_HOST.';dbport='.DB_PORT.';dbname='.DB_NAME, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
  header('Content-Type: text/html; charset=utf-8');
  die('Nie można nawiązać połączenia z bazą danych.');
}

$sth = $pdo->prepare('
  SELECT dziecko_id, dziecko_klasa, klasa_kod_num, klasa_kod_str
  FROM dzieci
  INNER JOIN klasy ON dziecko_klasa = klasa_id');

$sth->execute();

$dzieci = $sth->fetchAll(PDO::FETCH_ASSOC);

foreach ($dzieci as $key => $val) {
  $sth = null;
  $sth = $pdo->prepare('SELECT klasa_id FROM klasy WHERE klasa_kod_num = :num AND klasa_kod_str = :str');

  if (
    $val['klasa_kod_num'] == 6 
    // || ($val['klasa_kod_num'] == 3 && substr($val['klasa_kod_str'], 0) == 'G') ||
    // ($val['klasa_kod_num'] == 3 && substr($val['klasa_kod_str'], 0) == 'L') ||
    // ($val['klasa_kod_num'] == 5 && substr($val['klasa_kod_str'], 0, 1) == 'P')
  ) {
  } else {
    $nextNum = $val['klasa_kod_num'] + 1;
    $nextStr = $val['klasa_kod_str'];
    // echo $val['klasa_kod_num'].' => '.$nextNum.' | '.$val['klasa_kod_str'].' => '.$nextStr.'<br><br>';

    $sth->execute(array(':num' => $nextNum, ':str' => $nextStr));

    $tmp = null;
    $tmp = $sth->fetchAll(PDO::FETCH_ASSOC);

    if (!empty($tmp)) {
      $s = $pdo->prepare("UPDATE dzieci SET dziecko_klasa = :klasa WHERE dziecko_id = :id");
      $s->execute(array(':klasa' => $tmp[0]['klasa_id'], ':id' => $val['dziecko_id']));
    } else {
      $s = $pdo->prepare("INSERT INTO klasy (klasa_nazwa, klasa_kod_num, klasa_kod_str) VALUES (:nazwa, :num, :str)");

      switch ($nextNum) {
        case 1:
          $n = 'I';
          break;
        case 2:
          $n = 'II';
          break;
        case 3:
          $n = 'III';
          break;
        case 4:
          $n = 'IV';
          break;
        case 5:
          $n = 'V';
          break;
        case 6:
          $n = 'VI';
          break;
      }

      $nazwa = $n.$nextStr;

      $s->execute(array(':nazwa' => $nazwa, ':num' => $nextNum, ':str' => $nextStr));
      $id = $pdo->lastInsertId();

      $s2 = $pdo->prepare("UPDATE dzieci SET dziecko_klasa = :klasa WHERE dziecko_id = :id");
      $s2->execute(array(':klasa' => $id, ':id' => $val['dziecko_id']));
    }

  }
}
echo 'KONIEC';